import UIKit
import Flutter
import GoogleMaps

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
      GeneratedPluginRegistrant.register(with: self)

//     GMSServices.provideAPIKey("AIzaSyA5SETWVxChJHPN_kAh2ROlfKkmlUo0PBk")
    GMSServices.provideAPIKey("AIzaSyASRP-taHE5xHZHfH6LDmJ8lJ59_Fs5TUs")
     // here
    if #available(iOS 10.0, *) {
      UNUserNotificationCenter.current().delegate = self as? UNUserNotificationCenterDelegate
    }

    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
}
