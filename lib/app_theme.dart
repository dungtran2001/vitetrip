import 'package:flutter/material.dart';

class AppTheme {
  AppTheme._();

  static const String fontMontserrat = "Montserrat";
  static const String fontSfPro = "sfPro";
  static const String fontCalibri = "Calibri";
  static const String fontDefault = "MyFlutterApp";
  static const String fontInter = "Inter";
  static const String fontEpilogue = "Epilogue";
  static const String fontPlayfair = "Playfair";

  static const Color notWhite = Color(0xFFEDF0F2);
  static const Color nearlyWhite = Color(0xFFFEFEFE);
  static const Color white = Color(0xFFFFFFFF);
  static const Color nearlyBlack = Color(0xFF323743);
  static const Color grey = Color(0xFFEDEDF2);
  static const Color orange = Color(0xFFED7D2D);
  static const Color red = Color(0xFFFF0000);
  static const Color yellow = Color(0xFFFFFF4B);
  static const Color dark_grey = Color(0xFF313A44);
  static const Color blue = Color(0xFF1091F4);
  static const Color nearlyBlue = Color(0xFF00B6F0);
  static const Color nearlyDarkBlue = Color(0xFF2633C5);
  static const Color darkText = Color(0xFF253840);
  static const Color darkerText = Color(0xFF17262A);
  static const Color lightText = Color(0xFF4A6572);
  static const Color deactivatedText = Color(0xFF767676);
  static const Color dismissibleBackground = Color(0xFF364A54);
  static const Color chipBackground = Color(0xFFEEF1F3);
  static const Color spacer = Color(0xFFF2F2F2);
  static const String fontName = fontInter;//'WorkSans';

  static Color mainColor =  Color(0xFF3c8dbc);// Color(0xFF0e4c8b);

  //dynamic color
  static Color mainColorLightmode = Colors.lightBlue.withOpacity(1);
  static Color mainColorNightmode = Colors.blue.withOpacity(.8);

  static Color subColorNightmode = AppTheme.backgroundMainNightmode.withOpacity(.6);
  static Color subColorLightmode = AppTheme.subColor2.withOpacity(.6);

  static Color iconColorLightmode =  Color(0xFF3c8dbc);// Color(0xFF0e4c8b);
  static Color iconColorNightmode = Colors.lightBlue.shade300;
  //end dynamic

  static const Color subColor = Color(0xFF42403a);

  static const Color mainColorLeft = Color(0xFF0061ca);
  static const Color mainColorRight = Color(0xFF013e98);

  static const Color subColor2 = Color(0xFF56daa4);
  static const Color highlight = Color(0xFFfc6d6e);

  //night or light mode
  //background color
  static const Color backgroundMain = Colors.white;
  static Color backgroundHighlight = Colors.lightBlue.shade300;
  static Color background = Color(0xFFF8F9FA);
  static const Color backgroundTile = Colors.white;
  static const Color backgroundButton = Colors.white;

  static Color backgroundMainNightmode = Colors.blueGrey.shade900;
  static Color backgroundHighlightNightmode = Colors.orange.shade200;
  static Color backgroundNightmode = Colors.blueGrey.shade500;
  static Color backgroundTileNightmode = Colors.blueGrey.shade700;
  static Color backgroundButtonNightmode = Colors.blueGrey.shade500;

  static const Color highlight1 = Color(0xFFfabbb6);
  static const Color highlight2 = Color(0xFFe8ffef);


  //hex color string
  static const String headerBackgroundColor = "#5db6fc";

  static const TextTheme textTheme = TextTheme(
    headline4: displayLightmode,
    headline5: headline,
    headline6: title,
    subtitle2: subtitle,
    bodyText2: body2,
    bodyText1: body1,
    caption: caption,
  );

  static const TextStyle titleAppBar = TextStyle(
    fontFamily: AppTheme.fontEpilogue,
    fontSize: 20,
    letterSpacing: -.4,
    fontWeight: FontWeight.bold,
    color: AppTheme.nearlyBlack
  );

  static const TextStyle displayLightmode = TextStyle( // h4 -> display1
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 36,
    letterSpacing: 0.4,
    height: 0.9,
    color: Colors.black54,
  );

   static const TextStyle display = TextStyle( // h4 -> display1
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 36,
    letterSpacing: 0.4,
    height: 0.9,
    color: darkerText,
  );


  static const TextStyle screenTitle = TextStyle( // h5 -> headline
    fontFamily: "sfPro",
    fontWeight: FontWeight.bold,
    fontSize: 17.5,
    letterSpacing: 0.5,
    color: Colors.black87,
  );

  static const TextStyle screenTitleNightmode = TextStyle( // h5 -> headline
    fontFamily: 'sfPro',
    fontWeight: FontWeight.bold,
    fontSize: 17.5,
    letterSpacing: 0.5,
    color: Colors.white70,
  );

  static const TextStyle buttonMenu = TextStyle( // h5 -> headline
    fontFamily: 'sfPro',
    fontWeight: FontWeight.bold,
    fontSize: 16,
    letterSpacing: 0.5,
    color: Colors.white,
  );


  //used
  static const TextStyle headlineNumber = TextStyle( // h5 -> headline
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 30,
    color: Colors.white,
  );

  //used
  static const TextStyle headline = TextStyle( // h5 -> headline
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 24,
    letterSpacing: 0.27,
    color: darkerText,
  );

  //used
  static const TextStyle headline2 = TextStyle( // h5 -> headline
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 20,
    letterSpacing: 0.27,
    color: Colors.black87,
  );
  //used
  static const TextStyle headline2Nightmode = TextStyle( // h5 -> headline
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 20,
    letterSpacing: 0.27,
    color: white,
  );
  //used
  static const TextStyle headline3 = TextStyle( // h5 -> headline
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 18,
    letterSpacing: 0.27,
    color: Colors.black54,
  );
  static const TextStyle headline3Nightmode = TextStyle( // h5 -> headline
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 18,
    letterSpacing: 0.27,
    color: Colors.white70,
  );

  static const TextStyle title = TextStyle( // h6 -> title
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 16,
    letterSpacing: 0.18,
    color: Colors.white,
  );

  static const TextStyle titleLightMode = TextStyle( // h6 -> title
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 16,
    letterSpacing: 0.18,
    color: darkerText,
  );

  static const TextStyle titleHighlight = TextStyle( // h6 -> title
    fontFamily: fontEpilogue,
    fontWeight: FontWeight.w400,
    fontSize: 15.5,
    // letterSpacing: 0.08,
    color: orange,
  );

  //used
  static const TextStyle buttonTitle = TextStyle( // subtitle2 -> subtitle
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 14,
    letterSpacing: -0.04,
    color: Colors.black54,
    shadows: [
      Shadow(
        blurRadius: 10.0,
        color: Colors.black26,
        offset: Offset(2.0, 2.0),
      ),
    ],
  );
    //used
  static const TextStyle buttonTitleNightmode = TextStyle( // subtitle2 -> subtitle
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 14,
    letterSpacing: -0.04,
    color: Colors.white70,
    shadows: [
      Shadow(
        blurRadius: 10.0,
        color: Colors.black26,
        offset: Offset(2.0, 2.0),
      ),
    ],
  );



  //used
  static const TextStyle subtitle = TextStyle( // subtitle2 -> subtitle
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 14,
    letterSpacing: -0.04,
    color: Colors.white70,
  );

  static const TextStyle subtitleLightMode = TextStyle( // subtitle2 -> subtitle
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 14,
    letterSpacing: -0.04,
    color: Colors.black54,
  );

  static const TextStyle subtitleProvinceLightMode = TextStyle( // subtitle2 -> subtitle
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 14,
    letterSpacing: -0.04,
    color: Colors.grey,
  );

  //used
  static const TextStyle subtitleBold = TextStyle( // body2 -> body1
    fontFamily: fontName,
    fontWeight: FontWeight.w500,
    fontSize: 15,
    letterSpacing: -0.05,
    color: Colors.white,
  );

  static const TextStyle subtitleBoldLightMode = TextStyle( // body2 -> body1
    fontFamily: fontInter,
    fontWeight: FontWeight.w600,
    fontSize: 16.5,
    letterSpacing: -0.05,
    color: Colors.black87,
  );

  static const TextStyle subtitleBoldLightModeSuccess = TextStyle( // body2 -> body1
    fontFamily: fontName,
    fontWeight: FontWeight.w600,
    fontSize: 15,
    letterSpacing: -0.05,
    color: Colors.green,
  );

  static const TextStyle subtitleBoldLightModeFail = TextStyle( // body2 -> body1
    fontFamily: fontName,
    fontWeight: FontWeight.w600,
    fontSize: 15,
    letterSpacing: -0.05,
    color: Colors.redAccent,
  );


  //used
  static const TextStyle subtitleBoldHighlight = TextStyle( // body2 -> body1
    fontFamily: fontName,
    fontWeight: FontWeight.w600,
    fontSize: 16.5,
    letterSpacing: -0.05,
    color: Colors.orange,
  );

  //used
  static const TextStyle subtitleVideo = TextStyle( // subtitle2 -> subtitle
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 12,
    letterSpacing: -0.04,
    color: Colors.white,
  );

  //used
  static const TextStyle subtitleMini = TextStyle( // subtitle2 -> subtitle
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 13,
    letterSpacing: -0.04,
    color: Colors.white70,
  );

  static const TextStyle subtitleMiniLightMode = TextStyle( // subtitle2 -> subtitle
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 13,
    letterSpacing: -0.04,
    color: Colors.black54,
  );

  static const TextStyle textStyleMini = TextStyle( // subtitle2 -> subtitle
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 11,
    letterSpacing: -0.04,
    color: Colors.white70,
  );

  static const TextStyle textStyleMiniLightMode = TextStyle( // subtitle2 -> subtitle
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 11,
    letterSpacing: -0.04,
    color: Colors.black87,
  );

  static const TextStyle bookingCompleted = TextStyle( // body2 -> body1
    fontFamily: fontName,
    fontSize: 14,
    letterSpacing: -0.05,
    color: Colors.green,
  );
  static const TextStyle bookingWaiting = TextStyle( // body2 -> body1
    fontFamily: fontName,
    fontSize: 14,
    letterSpacing: -0.05,
    color: Colors.blue,
  );
  static const TextStyle bookingBooked = TextStyle( // body2 -> body1
    fontFamily: fontName,
    fontSize: 14,
    letterSpacing: -0.05,
    color: Colors.deepOrange,
  );
  static const TextStyle bookingCancelled = TextStyle( // body2 -> body1
    fontFamily: fontName,
    fontSize: 14,
    letterSpacing: -0.05,
    color: Colors.black54,
  );
  static const TextStyle bookingError = TextStyle( // body2 -> body1
    fontFamily: fontName,
    fontSize: 14,
    letterSpacing: -0.05,
    color: Colors.red,
  );

  static const TextStyle bookingCompletedTitle = TextStyle( // body2 -> body1
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    letterSpacing: -0.05,
    color: Colors.green,
  );
  static const TextStyle bookingWaitingTitle = TextStyle( // body2 -> body1
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    letterSpacing: -0.05,
    color: Colors.blue,
  );
  static const TextStyle bookingBookedTitle = TextStyle( // body2 -> body1
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    letterSpacing: -0.05,
    color: Colors.deepOrange,
  );
  static const TextStyle bookingCancelledTitle = TextStyle( // body2 -> body1
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    letterSpacing: -0.05,
    color: Colors.black45,
  );
  static const TextStyle bookingErrorTitle = TextStyle( // body2 -> body1
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    letterSpacing: -0.05,
    color: Colors.red,
  );
  static const TextStyle bookingTypeText = TextStyle( // body2 -> body1
    fontFamily: fontName,
    fontSize: 14,
    letterSpacing: -0.05,
    color: Colors.black,
  );

  static const TextStyle noteAllPageText = TextStyle( //// body2 -> body1
    fontFamily: fontName,
    fontStyle: FontStyle.italic,
    fontSize: 14,
    letterSpacing: -0.05,
    color: Colors.grey,
  );

  //used
  static const TextStyle currency = TextStyle( // body2 -> body1
//    fontFamily: fontName,
    //fontWeight: FontWeight.bold,
    fontSize: 14,
//    letterSpacing: -0.05,
    color: Colors.grey,
  );

  //used
  static const TextStyle button = TextStyle( // body2 -> body1
//    fontFamily: fontName,
    //fontWeight: FontWeight.bold,
    fontSize: 16,
//    letterSpacing: -0.05,
    color: Colors.white,
  );

  static const TextStyle body2 = TextStyle( // body1 -> body2
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 14,
    letterSpacing: 0.2,
    color: darkText,
  );

  static const TextStyle body1 = TextStyle( // body2 -> body1
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 16,
    letterSpacing: -0.05,
    color: darkText,
  );

  static const TextStyle caption = TextStyle( // Caption -> caption
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 12,
    letterSpacing: 0.2,
    color: Colors.white70, // was lightText
  );

  static const TextStyle captionLightmode = TextStyle( // Caption -> caption
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 12,
    letterSpacing: 0.2,
    color: lightText, // was lightText
  );


  static TextTheme _buildTextTheme(TextTheme base) {
    const String fontName = 'WorkSans';
    return base.copyWith(
      headline1: base.headline1!.copyWith(fontFamily: fontName),
      headline2: base.headline2!.copyWith(fontFamily: fontName),
      headline3: base.headline3!.copyWith(fontFamily: fontName),
      headline4: base.headline4!.copyWith(fontFamily: fontName),
      headline5: base.headline5!.copyWith(fontFamily: fontName),
      headline6: base.headline6!.copyWith(fontFamily: fontName),
      button: base.button!.copyWith(fontFamily: fontName),
      caption: base.caption!.copyWith(fontFamily: fontName),
      bodyText1: base.bodyText1!.copyWith(fontFamily: fontName),
      bodyText2: base.bodyText2!.copyWith(fontFamily: fontName),
      subtitle1: base.subtitle1!.copyWith(fontFamily: fontName),
      subtitle2: base.subtitle2!.copyWith(fontFamily: fontName),
      overline: base.overline!.copyWith(fontFamily: fontName),
    );
  }
  static ThemeData buildLightTheme() {
    final Color primaryColor = HexColor('#54D3C2');
    final Color secondaryColor = HexColor('#54D3C2');
    final ColorScheme colorScheme = const ColorScheme.light().copyWith(
      primary: primaryColor,
      secondary: secondaryColor,
    );
    final ThemeData base = ThemeData.light();
    return base.copyWith(
      colorScheme: colorScheme,
      primaryColor: primaryColor,
      buttonColor: primaryColor,
      indicatorColor: Colors.white,
      splashColor: Colors.white24,
      splashFactory: InkRipple.splashFactory,
      accentColor: secondaryColor,
      canvasColor: Colors.white,
      backgroundColor: const Color(0xFFFFFFFF),
      scaffoldBackgroundColor: const Color(0xFFF6F6F6),
      errorColor: const Color(0xFFB00020),
      buttonTheme: ButtonThemeData(
        colorScheme: colorScheme,
        textTheme: ButtonTextTheme.primary,
      ),
      textTheme: _buildTextTheme(base.textTheme),
      primaryTextTheme: _buildTextTheme(base.primaryTextTheme),
      accentTextTheme: _buildTextTheme(base.accentTextTheme),
      platform: TargetPlatform.iOS,
    );
  }

  static Widget GradientLine( {double width=200, double height=2, Color color=mainColorLeft, double radius=50, Color boundColor = Colors.white70}) {
    return Container(
      height: height,
      width: width,
      //color:Colors.grey,
      decoration: BoxDecoration(
          gradient: RadialGradient(
            center: const Alignment(0, 0), // near the top right
            radius: radius,
            colors: [
              color, // yellow sun
              boundColor, // blue sky
            ],
            stops: [0.5, 1.2],
          )),
    );
  }
}


class HexColor extends Color {
  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));

  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll('#', '');
    if (hexColor.length == 6) {
      hexColor = 'FF' + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }
}
