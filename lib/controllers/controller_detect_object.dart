import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:mitta/commons/common.dart';

import '../model/archiverment/model_archiverment.dart';
import '../model/location/model_location.dart';
import '../model/object/model_object.dart';
import '../network/api/api.dart';
import '../network/response/response_archiverments.dart';
import '../network/response/response_objects.dart';
import '../services/service_storage.dart';
import '../services/services_locator.dart';
import '../views/detect/detect_object_result.dart';

class DetectObjectController extends GetxController {
  var isLoading = false.obs;

  var isShowAudioPlayer = true.obs;
  var isAlreadyAutoPlay = false.obs;
  var isShowObjectSheet = false.obs;

  Rx<ObjectInfo>? currentObjectSelected;

  final api = locator<Api>();
  final storage = locator<StorageLocal>();


  List<ObjectInfo> objectData = [];
  Future<void> getObjectData({locationId}) async {
    ObjectResponse _data = await api.client!.getObjectsData(locationId: locationId);
      objectData = _data.data;
      update();
  }

  List<ArchivermentInfo> archivermentData = [];
  Future<void> getArchivermentData({locationId}) async {
    if (storage.checkUserAlreadyLogin()){
      ArchivermentsResponse _data = await api.client!.getArchivermentsData(userId: storage.userData!.id.toString(), locationId: locationId, token: storage.userData!.token);
      archivermentData = _data.data;
      update();

    }
  }

  Future<bool> postRatingData({locationId, star}) async {
    if (storage.checkUserAlreadyLogin()){
      var params = {
        "location_id": locationId.id,
        "amount_rating": star,
        "user_id": storage.userData!.id.toString()
      };
      bool _data = await api.client!.postRatingLocation(params: params, token: storage.userData!.token);
      if (_data)
        ToastNotify.showSuccessToast(message: "Đánh giá thành công. Khám phá thêm nhiều địa điểm khác nhé");
      else
        ToastNotify.showInfoToast(message: "Gửi đánh giá gặp lỗi. Vui lòng thử lại sau nhé");

      return _data;
    }
    return false;
  }

  void navigateToScanResult(ObjectInfo obj, LocationInfo locationModel, ValueSetter<dynamic>? callback) {
    Get.to(()=> DetectObjectResult(model: obj, isFromScanView: false, locationModel: locationModel))?.then(callback ?? (_) {});
  }


}