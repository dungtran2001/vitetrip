import 'dart:async';

import 'package:geofence_service/geofence_service.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mitta/model/location/model_location.dart';
import 'package:mitta/services/service_notify_local.dart';

import '/configs/configs.dart';
import '/network/api/api.dart';
import '/services/service_storage.dart';
import '/services/services_locator.dart';
import '../commons/common.dart';
import '../model/category/model_object.dart';
import '../services/service_geolocation.dart';
import '../views/xplore/components/component_map_controller.dart';

class XploreController extends GetxController {
  //instance service
  final api = locator<Api>();
  final storage = locator<StorageLocal>();

  Rx<CategoryInfo>? currentTagSelect;

  var isAlreadyLogin = false.obs;

  ///MAP
  final MapComponentController mapController =
      Get.put(MapComponentController());

  List<LocationInfo> placesData = [];

  String tagId = '';

  ///update marker Parking with distance from current location pos
  Future<bool> showParkingMarkerWithDistance(LatLng centerPos) async {
    var _amountDistance = 100.0;

    var _placeToShow = findPlaceNearMe(placesData, centerPos, _amountDistance,
        isRemoveParkOutside: true);
    var data = _placeToShow
        .where((element) =>
    currentTagSelect == null ||
        currentTagSelect!.value.id == "0" ||
        element.categoryId == currentTagSelect!.value.id)
        .toList();
    if (data.length > 0) {
      for (var marker in data) {
        mapController.addMarker!(marker,
            type: MarkerType.normal, function: () => onMarkerTapped(marker));
      }
      // Auto select to nearest location

      Timer(const Duration(seconds: 1), () {
        if (currentLocationSelected == null ||
            currentLocationSelected!.value.id != data.last.id) {
          onMarkerTapped(data.last);
        }
      });

      return true;
    } else {
      return false;
    }
  }

  void showAllPlace() {
    var data = placesData
        .where((element) =>
            currentTagSelect == null ||
            element.categoryId == currentTagSelect!.value.id)
        .toList();
    for (var marker in data) {
      mapController.addMarker!(marker,
          type: MarkerType.normal, function: () => onMarkerTapped(marker));
    }

    //goto last location
    mapController.gotoPosition!(
        data.last.lat ?? 16.0608, data.last.long ?? 0, 108.2139);
  }

  var myCurrentLocation;

  Future<void> showMyLocation(myLocation) async {
    if (myLocation != null) {
      myCurrentLocation = myLocation;
      // print(myLocation);
      //ok
      mapController.gotoPosition!(
          myCurrentLocation.latitude, myCurrentLocation.longitude, 15.5);

      await mapController.setCustomMapPin!().then((_) async {
        //set user current position marker
        mapController.addMarker!(
            LocationInfo(
                id: "me", lat: myLocation.latitude, long: myLocation.longitude),
            type: MarkerType.mini);
        return;
      });
    }
  }

  final geolocation = locator<GeoLocationService>();

  void gotoMyLocation() {
    if (myCurrentLocation != null) {
      mapController.gotoPosition!(
          myCurrentLocation.latitude, myCurrentLocation.longitude, 15.5);
    } else {
      //request location
      geolocation.getCurrentLocation().then((value) {
        if (value != null) {
          // print(value);
          myCurrentLocation = value;
          mapController.gotoPosition!(
              myCurrentLocation.latitude, myCurrentLocation.longitude, 15.5);
        } else {
          //user ko dong y cap quyen //imp sau
        }
      });
    }
  }

  ///find parks near me
  List<LocationInfo> findPlaceNearMe(
      List<LocationInfo>? places, LatLng? currentPos, double? distance,
      {bool isRemoveParkOutside = false}) {
    List<LocationInfo> listPlaceToShow = [];

    //set value default
    if (places == null) places = placesData;
    if (currentPos == null) {
      //imp sau
      // currentPos = LatLng(locationData.latitude, locationData.longitude);
    }
    if (distance == null) distance = 100.0;

    if (places.length == 0) {
      print("List places null");
      return places;
    }

    var _nearest = double.infinity;
    LocationInfo _placeNearest;

    for (var place in places) {
      var _distance = Common.distanceBetween(
          lat1: place.lat,
          long1: place.long,
          lat2: currentPos!.latitude,
          long2: currentPos.longitude);
//      print("--distance to ${park.name}: $_distance");
      if (distance * 1000 >= _distance) {
        if (_nearest > _distance) {
          _placeNearest = place;
          _nearest = _distance;
        }
        //add to park near list
        listPlaceToShow.add(place);
      } else {
        if (isRemoveParkOutside) //remove old park to view
          mapController.removeMarker!(place);
      }
    }

    return listPlaceToShow;
  }

  Rx<LocationInfo>? currentLocationSelected;

  onMarkerTapped(LocationInfo place) {
    currentLocationSelected = place.obs;
    print("--Marker has trigger");
    mapController.createPolyline!(
        LatLng(myCurrentLocation.latitude, myCurrentLocation.longitude),
        LatLng(place.lat ?? 0, place.long ?? 0));

    update();
  }

  onClearPolyline() {
    mapController.clearPolyline!();
  }

  // Create a [GeofenceService] instance and set options.
  final geofenceService = GeofenceService.instance.setup(
      interval: 5000,
      accuracy: 100,
      loiteringDelayMs: 60000,
      statusChangeDelayMs: 10000,
      useActivityRecognition: true,
      allowMockLocations: false,
      printDevLog: false,
      geofenceRadiusSortType: GeofenceRadiusSortType.DESC);
  var geofenceList = <Geofence>[];

  void addLocationToObserverList() {
    geofenceService.clearGeofenceList();

    for (var location in placesData) {
      geofenceList.add(Geofence(
        id: location.id.toString(),
        latitude: location.lat ?? 0,
        longitude: location.long ?? 0,
        radius: [
          GeofenceRadius(id: 'radius_100m', length: 100),
          GeofenceRadius(id: 'radius_25m', length: 25),
          GeofenceRadius(id: 'radius_50m', length: 50),
          GeofenceRadius(id: 'radius_200m', length: 200),
        ],
      ));
    }
  }

  final _geofenceStreamController = StreamController<Geofence>();

  // This function is to be called when the geofence status is changed.
  Future<void> onGeofenceStatusChanged(
      Geofence geofence,
      GeofenceRadius geofenceRadius,
      GeofenceStatus geofenceStatus,
      Location location) async {
    // print('geofence: ${geofence.toJson()}');
    // print('geofenceRadius: ${geofenceRadius.toJson()}');
    // print('geofenceStatus: ${geofenceStatus.toString()}');

    if (geofenceStatus == GeofenceStatus.ENTER) {
      //find location info
      try {
        var location =
            placesData.firstWhere((element) => element.id == geofence.id);
        //send notification
        NotificationLocal.instance.createNotification(
            "AI Guidex", "Bạn đã tới điểm tham quan ${location.name}");
      } catch (e) {
        print('!! error: toi gan nhung khong thay thong tin location');
      }
    }

    _geofenceStreamController.sink.add(geofence);
  }

  // This function is used to handle errors that occur in the service.
  void onError(error) {
    final errorCode = getErrorCodesFromError(error);
    if (errorCode == null) {
      print('!! Undefined error: $error');
      return;
    }

    print('!! ErrorCode: $errorCode');
  }
}
