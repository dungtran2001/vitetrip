
import 'package:get/get.dart';

import '../configs/configs.dart';
import '../network/httpController.dart';
import '../services/service_storage.dart';
import '../services/services_locator.dart';

class SettingsController extends GetxController{

  //deactivate account
  Future<bool> deactivateAccount() async {

    final storage = locator<StorageLocal>();

    String url = Configs.hostName + Configs.apiURI['deactivate-account']! + storage.userData!.id.toString();
    var _response = await HTTPController.DeleteAuthorization(url, storage.userData!.token.toString());
    print(_response.toString());
    if (_response != null)
      return true;
    else
      return false;
  }

}