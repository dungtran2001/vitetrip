import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mitta/model/guide_author/guide_author.dart';
import 'package:mitta/model/guide_author/guide_content.dart';
import 'package:mitta/services/audio_player/text_to_speech_vm.dart';
import '../commons/common.dart';
import '../configs/configs.dart';
import '../model/archiverment/model_archiverment.dart';
import '../model/location/model_location.dart';
import '../model/object/model_object.dart';
import '../network/response/response_archiverments.dart';
import '../network/response/response_objects.dart';
import '../services/service_storage.dart';
import '../services/tflite/recognition.dart';
import '../services/tflite/stats.dart';
import '../views/detect/camera_view.dart';
import '../views/detect/components/detect_view_component_dialog.dart';
import '../views/detect/detect_object_result.dart';
import '/model/demo/model_demo.dart';
import '/network/api/api.dart';
import '/services/services_locator.dart';

class DetectViewController extends GetxController {
  var isLoading = false.obs;
  late BuildContext context;

  //instance api
  final api = locator<Api>();
  List<ObjectInfo> objectData = [];
  List<ObjectInfo> objectDetectedData = [];

  Future<void> getObjectData({locationId}) async {
    ObjectResponse _data =
        await api.client!.getObjectsData(locationId: locationId);
    objectData = _data.data;
  }

  ObjectInfo? selectedObject;

  //model data
  late LocationInfo locationModel;

  //detect
  /// Results to draw bounding boxes
  List<Recognition> results = [];

  /// Realtime stats
  Stats? stats;

  /// Callback to get inference results from [CameraView]
  void resultsCallback(List<Recognition> results) {
    objectDetectedData = [];

    this.results = results
        .where((element) => element.score >= Configs.thresholdScore)
        .toList();

    this.results.forEach((element) {
      print("DetectResult: ${element}");
      var item = null;
      try {
        item = objectData.firstWhere((e) => e.label == element.label);
      } catch (e) {
        print("--ERROR find elelement with label: $e");
      }
      if (item != null) objectDetectedData.add(item);
    });

    update();
  }

  /// Callback to get inference stats from [CameraView]
  void statsCallback(Stats stats) {
    this.stats = stats;
    update();
  }

  //control scan
  bool isCompleteScan = false;
  final cameraViewController = new CameraViewController();

  var scanTimer;
  var isScanning = false.obs;
  var scanningCountTime = 0.obs;

  void startScan() {
    //reset data
    objectDetectedData = [];

    //enable scan
    isScanning.value = true;
    update();
    cameraViewController.changeStateDetect!(true);
    //auto stop scan after time
    scanTimer = new Timer.periodic(Duration(milliseconds: 100), (Timer timer) {
      if (scanningCountTime.value >= Configs.timeScanning) {
        timer.cancel();
        cameraViewController.changeStateDetect!(false);
        isScanning.value = false;
        scanningCountTime.value = 0;
        update();

        //direct
        if (objectDetectedData.length > 1) {
          //show popup to choose object
          chooseObjectDialog(context, objectDetectedData, locationModel);
        } else if (objectDetectedData.length > 0) {
          //direct to screen result to play audio
          navigateToScanResult(objectDetectedData.first, (value) {
            objectDetectedData = [];
          });
          // Get.to(()=> DetectObjectResult(model:objectDetectedData.first , locationModel: locationModel,))?.then((value) {
          //   //remove old detected
          //   objectDetectedData = [];
          // });
        } else {
          //hien popup capture object image
          captureQuestionDialog(context, locationModel, cameraViewController);
        }
      } else {
        scanningCountTime.value = scanningCountTime.value + 1;
      }
    });
  }

  final storage = locator<StorageLocal>();

  List<ArchivermentInfo> archivermentData = [];

  Future<void> getArchivermentData({locationId}) async {
    ArchivermentsResponse _data = await api.client!.getArchivermentsData(
        userId: storage.userData!.id.toString(),
        locationId: locationId,
        token: storage.userData!.token);

    archivermentData = _data.data;
    update();
  }

  ///get app data by type select
  var isLoadingPostReport = false.obs;
  var imageFile;

  Future<bool> postReport(LocationInfo location) async {
    var _result = false;
    isLoadingPostReport.value = true;
    var map = {
      "location_id": location.id,
      "user_id": storage.userData!.id,
    };

    if (imageFile != null) {
      //check has image
      //call api
      await api.client!
          .postNewObjectReport(
              params: map,
              images: [imageFile],
              token: storage.userData!.token.toString())
          .then((data) {
        if (data) {
          ToastNotify.showSuccessToast(
              message: "Gửi phản hồi thành công, xin cảm ơn",
              isCenter: true,
              duration: 3);
          _result = true;
          //ok thi qua home
          // Get.off(()=> ReportSceneHomeView());

        } else {
          ToastNotify.showFailToast(
              message: "Đã xảy ra lỗi, vui lòng thử lại!");
          _result = false;
        }

        isLoadingPostReport.value = false;
        update();
        return _result;
      });
    }
    return _result;
  }

  var imagesSelectList = [].obs;
  var imageFileList = [];

  Future<File> imgFromCamera() async {
    final picker = ImagePicker();
    final _image = await picker.getImage(
        source: ImageSource.camera,
        maxWidth: 500,
        preferredCameraDevice: CameraDevice.front);

    if (_image != null) {
      imageFile = File(_image.path);
      imageFileList.add(imageFile); //set value to file
      //convert
      List<int> imageBytes = imageFile.readAsBytesSync();
      var base64AvatarImage = base64Encode(imageBytes);
      imagesSelectList.value.add(base64AvatarImage);
      update();
//
//      Get.back();
    } else {
      print('No image selected.');
    }
    return imageFile;
  }

  void selectObject(ObjectInfo obj) {
    this.selectedObject = obj;
    update();
  }

  void navigateToScanResult(
      ObjectInfo obj, ValueSetter<dynamic>? callback) {
    Get.to(() => DetectObjectResult(
        model: obj,
        isFromScanView: true,
        locationModel: locationModel))?.then(callback ?? (_) {});
  }
}
