
import 'dart:ffi';

import 'package:get/get.dart';
import 'package:mitta/model/guide_author/guide_author.dart';
import 'package:mitta/model/guide_author/guide_content.dart';
import 'package:mitta/model/location/model_country.dart';
import 'package:mitta/model/location/model_province.dart';
import '../model/location/model_location.dart';
import '/model/demo/model_demo.dart';
import '/network/api/api.dart';
import '/services/services_locator.dart';

class SelectProvinceController extends GetxController {
  bool isLoading = false;
  List<ProvinceInfo> listProvinceData = [];
  List<CountryInfo> listCountryData = [];
  List<LocationInfo> listLocationInfo = [];

  String searchWord = "";

  //instance api
  final api = locator<Api>();

  Future<void> fetchProvinceData({authorId = "", countryId = "", locationId = "", artifactId = ""}) async {
    isLoading = true;
    ///call api
   await api.client!.getProvincesData(search: searchWord, countryId: countryId).then((value) {
     this.listProvinceData = value.data;
     // print("lisst dataa: "+ value.data.toString());
   });

   update();
   isLoading = false;
  }

  Future<void> fetchCountryData() async {
    isLoading = true;
    ///call api
   await api.client!.getCountryData().then((value) {
     this.listCountryData = value.data;
   });

   update();
   isLoading = false;
  }

  Future<void> fetchNearbyLocationInfo(Float lat, Float lng) async {
    isLoading = true;
    await api.client!.getNearbyLocation(lat: lat, lng: lng).then((value) => {
      this.listLocationInfo = value
    });
    update();
    isLoading = false;
  }

}
