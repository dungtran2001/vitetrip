
import '/model/user/model_me.dart';
import '/network/api/api.dart';
import '/network/httpController.dart';
import '/services/service_storage.dart';
import '/services/services_locator.dart';
import '/views/home/home.dart';

import '/configs/configs.dart';
import 'package:get/get.dart';
import '/commons/common.dart';
import '/services/language/localization_service.dart';

import 'controller_main.dart';

class LoginController extends GetxController{
  final MainController mainController = Get.put(MainController());

  var isStartingRequest = false.obs;
  var message = "".obs;

  var isShowPassword = false.obs;
  var messagePhone = "".obs;
  var phoneError = false.obs;
  var messagePassword = "".obs;
  var passwordError = false.obs;



  //instance service
  final api = locator<Api>();
  final storage = locator<StorageLocal>();

  LoginController (){
//    mainController.fetchBackupServerConfigData().then((value) {
//      serverName.value = mainController.serverName;
//    });
//    mainController.fetchBackupBiometricData();
  }


  Future<bool> signIn(String username, String password, isGoRoot) async {
    //fetch local acc data
    mainController.fetchBackupAccData();

    Map<String, dynamic> map = {
      "username": username,
      "password": password,
    };
    String url = Configs.hostName + Configs.apiURI['login']! ;// + "?username=$username&password=$password";
    print(url +": "+ map.toString());

//    var _response = await HTTPController.PostHeader(url, map); //aluan
    var _response = await HTTPController.PostByJson(url, map);
    // print(_response.toString());


    if (_response != null) {
      try {
        var _status = _response["success"];
        if (_status) {
          //success
          //success
          //back up to keep login
          var _user = Me.fromJson(_response["data"]);


          await storage.backupUserDataToStorage(_user).then((value) {
            storage.fetchUserDataFromStorage().then((value) => Get.back());
          });

          if (isGoRoot){ // kiem tra co phải push ve home ko
            //ko phai //direct menu
            Get.offAll(()=> HomeView());
          }


          ToastNotify.showSuccessToast(message: 'Đăng nhập thành công', isCenter: true);
          isStartingRequest.value = false;

          return true;
        } else {
          //login fail
         // print("Login response: " + _response.toString());


         ToastNotify.showFailToast(message: message.value);
        }
      } catch (e) {
        ToastNotify.showFailToast(message: 'Đăng nhập không thành công');
        isStartingRequest.value = false;
        return false;
      }
    }else{
     ToastNotify.showFailToast(message: 'Đăng nhập không thành công, vui lòng kiểm tra lại!');

      isStartingRequest.value = false;
      return false;
    }

    return false; //default
  }

}