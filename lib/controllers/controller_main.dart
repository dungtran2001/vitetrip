
import 'dart:convert';

//import 'package:atoma_vms/language/localization_service.dart';
//import 'package:atoma_vms/response/response_firestore_server_config.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import '../model/user/model_user_account.dart';


class MainController extends GetxController{


  final storage = new FlutterSecureStorage();

  MainController(){
    initData();
  }

  initData()async {
//    await fetchBackupUserData();

    await fetchBackupAccData();
  }


  //USER

  String username = "";
  String? phone;
  String? email;
  String? userId;
  String token = "";

  bool checkUserAlreadyLogin(){
    if (token != null &&  token != "" && token.toLowerCase() != "null")
      return true;
    else
      return false;
  }


  void setUserData({String uName = "", String uPhone="", String uId="", String uEmail="", String uToken="" }) {

    phone = uPhone;
    username = uName;
    userId = uId;
    email = uEmail;
    token = uToken;

    //store
    backupUserData();
  }

  void backupUserData(){
    //prepare for list
    var temp = "";
//    roleList!.forEach((element) {
//      temp += "\"${element}\"";
//    });

    var map = '{"username":\"$username\", "phone":\"$phone\", "id":\"$userId\","email":\"$email\","token":\"$token\","roles":[$temp]}';
    storage.write(key: "user-data", value: map.toString());
  }

//  Future<bool> fetchBackupUserData() async {
//    await storage.read(key: "user-data").then((value) {
//      if (value != null){
//        var data = json.decode(value);
//        print(data.toString());
//        username = data["username"];
//        phone = data["phone"];
//        userId = data["id"];
//        email = data["email"];
//        token = data["token"];
//
////        return true;
//      }else{
////        return false;
//      }
//    });
//
//    return false;
//  }

  void clearUserData() {
    phone = "";
    username = "";
    userId = "";
    email = "";
    token = "";
    storage.write(key: "user-data", value: null);
  }





  //REGISTER ACCOUNT
  List<UserAccount> accountList = [];
  void backupAccountList(){
    var map = '{"listAccount": ${json.encode(accountList)}}';
    storage.write(key: "accounts", value: map.toString());
  }

  Future<bool> fetchBackupAccData() async {
    await storage.read(key: "accounts").then((value) {
      if (value != null){
        var data = json.decode(value);
//        print(data.toString());
        try {
          accountList = parseAccountList(data);
        }catch(e){

          print("get acc profile error");
        }

        return true;
      }else{
        return false;
      }
    });
    return false;
  }
  List<UserAccount> parseAccountList(map) {
    try{
      var list = map['listAccount'] as List;
      return list.map((p) => UserAccount.fromJson(p)).toList();
    }catch(e){
      print("ERROR parseAccountList $e");
      return [];
    }

  }

}
