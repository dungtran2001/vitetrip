
import 'package:get/get.dart';
import 'package:mitta/model/guide_author/guide_author.dart';
import '/model/demo/model_demo.dart';
import '/network/api/api.dart';
import '/services/services_locator.dart';

class GuideAuthorController extends GetxController {
  bool isLoading = false;
  List<GuideAuthorInfo> listAuthorData = [];


  //instance api
  final api = locator<Api>();

  fetchAuthorData({locationId = "", artifactId = ""}) async {
    isLoading = true;
    ///call api
   await api.client!.getGuideAuthorsData(locationId: locationId, artifactId: artifactId).then((value) {
     this.listAuthorData = value.data;
   });

   update();
   isLoading = false;
  }

}
