import 'dart:ffi';
import 'dart:math';

import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mitta/model/location/model_country.dart';
import 'package:mitta/model/location/model_location.dart';
import 'package:mitta/model/location/model_province.dart';
import 'package:diacritic/diacritic.dart';
import 'package:mitta/views/guide_content/components/component_content_guide_tile.dart';

import '../commons/common.dart';
import '../model/category/model_object.dart';
import '/model/user/model_me.dart';
import '/services/service_storage.dart';
import 'package:get/get.dart';
import '/network/api/api.dart';
import '/services/services_locator.dart';

import 'controller_select_province.dart';

class HomeController extends GetxController {
  //instance service
  final api = locator<Api>();
  final storage = locator<StorageLocal>();

  Rx<Me>? userData;

  Rx<LocationInfo>? currentLocationsData;

  Rx<ProvinceInfo>? currentProvinceSelect;

  Rx<CountryInfo>? currentCountrySelect;

  List<LocationInfo> locations = [];

  String tagId = '';

  late ProvinceInfo _nearestProvince;

  final SelectProvinceController locationController =
      Get.put(SelectProvinceController());

  void fetchBackupData(isAcceptLocation, lat, long) {
    storage.fetchLocationFromStorage().then((value) async {
      //check country select
      if (storage.provinceSelected != null) {
        currentCountrySelect = storage.countrySelected!.obs;
      } else {
        // will imp auto set location nearest
        await locationController.fetchCountryData().then((value) {
          if (isAcceptLocation)
            findNearestCountry(lat, long); // tim contry gan nhat
          else
            currentCountrySelect = locationController.listCountryData.first.obs;
        });
      }

      if (storage.provinceSelected != null) {
        currentProvinceSelect = storage.provinceSelected!.obs;
      } else {
        // Get.to(() => SelectProvinceView());
        await locationController.fetchProvinceData(
            countryId: currentCountrySelect!.value.id);
        var listProvinces = locationController.listProvinceData;
        listProvinces.sort((a, b) {
          return removeDiacritics(a.name!.toLowerCase())
              .compareTo(removeDiacritics(b.name!.toLowerCase()));
        });

        // if (isAcceptLocation) {
        //   findNearestProvince(lat, long); // will imp auto set location nearest
        // }
        // else if (listProvinces.length > 0)
        //   currentProvinceSelect = listProvinces.last.obs;
      }

      if (isAcceptLocation) {
        _findNearestLocation(lat, long);
      } else {
        _nearestProvince = locationController.listProvinceData.last;
        currentProvinceSelect = _nearestProvince.obs;
        currentLocationsData = _nearestProvince.locations!.last.obs;
      }

      // if (currentProvinceSelect != null &&
      //     currentProvinceSelect?.value != null) {
      //   if (isAcceptLocation)
      //     findNearestLocation(lat, long); // will imp auto set location nearest
      //   else
      //     currentLocationsData =
      //         currentProvinceSelect!.value.locations!.last.obs;
      // }
      update();
    });
  }

  var isAlreadyLogin = false.obs;

  Future<void> fetchUserDataFromStorage() async {
    await storage.fetchUserDataFromStorage().then((value) {
      if (storage.userData != null) {
        this.userData!.value = storage.userData!;
        //check already login
        if (this.userData!.value.token != "") isAlreadyLogin.value = true;
        update();
      }
    });
  }

  void fetchUserDataFromServer() async {
    //Refresh data
    var accessToken = storage.userData!.token;

    if (accessToken != null) {
      await api.client!
          .getMe(params: {'token': accessToken}).then((value) async {
        //success
        await storage.backupUserDataToStorage(value).then((value) {
          storage.fetchUserDataFromStorage();
        });
      });
    }
  }

  String filterNameToAvatar(String name) {
    if (name == "") return "";
    var temp = name[0];
    for (var i = 1; i < name.length; i++) {
      if (name[i - 1] == " " && name[i] != " ") temp += name[i];
    }
    return temp.length > 1
        ? temp.substring(temp.length - 2).toUpperCase()
        : temp.toUpperCase();
  }

  void changeProvince(ProvinceInfo province) {
    currentProvinceSelect = province.obs;
    //set location
    currentLocationsData = province.locations?.isEmpty == false
        ? province.locations?.first.obs
        : null;

    changeLocationsData(province.lat, province.long);

    update();
  }

  Future<void> changeCountry(CountryInfo countryInfo) async {
    currentCountrySelect = countryInfo.obs;
    //set provincei 1
    await api.client!.getProvincesData(countryId: countryInfo.id).then((value) {
      //set default first // will imp nearest
      changeProvince(value.data.first);
      // currentProvinceSelect = value.data.first.obs;
      // //set default location // will imp nearest
      // currentLocationsData = currentProvinceSelect!.value.locations!.first.obs;
    });
  }

  void findNearestCountry(lat, long) {
    var _nearestDistance = double.infinity;
    //find and compare
    for (var country in locationController.listCountryData) {
      var _distance = Common.distanceBetween(
          lat1: lat, long1: long, lat2: country.lat, long2: country.long);
      if (_nearestDistance > _distance) {
        currentCountrySelect = country.obs;
        _nearestDistance = _distance;
      }
    }
  }

  ProvinceInfo _findNearestProvince(lat, long) {
    var _nearestDistance = double.infinity;
    var _nearestProvince;
    for (var province in locationController.listProvinceData) {
      var _distance = Common.distanceBetween(
          lat1: lat, long1: long, lat2: province.lat, long2: province.long);
      if (_nearestDistance > _distance) {
        _nearestProvince = province;
        _nearestDistance = _distance;
      }
    }
    return _nearestProvince;
  }

  void findNearestProvince(lat, long) {
    //find and compare
    _nearestProvince = _findNearestProvince(lat, long);
    // currentProvinceSelect = _nearestProvince.obs;
  }

  void findNearestLocation(lat, long) {
    var _nearestDistance = double.infinity;

    //find and compare
    for (var location in currentProvinceSelect!.value.locations!) {
      var _distance = Common.distanceBetween(
          lat1: lat, long1: long, lat2: location.lat, long2: location.long);
      if (_nearestDistance > _distance) {
        currentLocationsData = location.obs;
        _nearestDistance = _distance;
      }
    }
  }

  void _findNearestLocation(lat, long) {
    var _nearestDistance = double.infinity;
    LocationInfo? _nearestLocation;

    for (var province in locationController.listProvinceData) {
      for (var location in province.locations!) {
        var _distance = Common.distanceBetween(
            lat1: lat, long1: long, lat2: location.lat, long2: location.long);
        if (_nearestDistance > _distance) {
          _nearestDistance = _distance;
          _nearestProvince = province;
          _nearestLocation = location;
        }
      }
    }
    currentProvinceSelect = _nearestProvince.obs;

    changeLocationsData(lat, long);

    currentLocationsData = _nearestLocation!.obs;
  }

  void changeLocationsData(lat, long) {
    locations.clear();
    api.client!.getNearbyLocation(lat: lat, lng: long).then((value) {
      // var _distanceFromDisneyland = Common.distanceBetween(
      //   lat1: lat, long1: long, lat2: 33.79338, long2: -117.79101);
      // if (_distanceFromDisneyland <= 30 * 1000) {
      var serverData = currentProvinceSelect!.value.locations ?? [];
      locations.addAll(serverData);
      if (serverData.length < 5) {
        locations.addAll(value.sublist(0, 8 - serverData.length));
      } else {
        locations.addAll(value.sublist(0, 3));
      }
      // locations.addAll(serverData);
      // locations.addAll(value);
      // } else {
      //   locations = value;
      // currentLocationsData = nearestLocation!.obs;
      // }
      update();
    });
  }

  int nearestLocationIndex(lat, long) {
    var _nearestDistance = double.infinity;
    var nearestIndex = 0;

    //find and compare
    var locations = currentProvinceSelect!.value.locations!;
    for (var index = 0; index < locations.length; index++) {
      var location = locations[index];
      var _distance = Common.distanceBetween(
          lat1: lat, long1: long, lat2: location.lat, long2: location.long);
      if (_nearestDistance > _distance) {
        _nearestDistance = _distance;
        nearestIndex = index;
      }
    }
    return nearestIndex >= 0 ? nearestIndex : 0;
  }

  List<CategoryInfo> tagList = [];
  Rx<CategoryInfo>? currentTagSelect;

  var isCategoryLoading = false.obs;

  Future<void> fetchCategoriesData() async {
    tagList = [];
    isCategoryLoading.value = true;

    ///call api
    await api.client!.getCategoriesData().then((value) {
      this.tagList = [...value.data];
    });

    tagList.insert(
        0, CategoryInfo(id: "0", name: "All", description: "default"));
    currentTagSelect = tagList[0].obs; // set default tag ALL

    update();
    isCategoryLoading.value = false;
  }

  void changeCategory(CategoryInfo tag) {
    currentTagSelect = tag.obs;
    tagId = tag.id ?? '0';
    update();
  }

  List<LocationInfo> searchLocationInfoList = [];

  void searchLocation(keyword) {
    locator<Api>().client!.getLocationsData(search: keyword).then((value) {
      searchLocationInfoList = value.data;
      update();
//      _setState((){});
    });
  }

  List<ProvinceInfo> getProvinces() {
    return locationController.listProvinceData;
  }

  List<LocationInfo> getNearestPlacesList() {
    return _nearestProvince.locations ?? List.empty();
  }

  List<LocationInfo> getProvincesLocation() {
    // if (_nearestProvince.id == currentProvinceSelect?.value.id) {
    //   return locations;
    // }
    // return currentProvinceSelect?.value.locations ?? [];
    return locations;
  }
}
