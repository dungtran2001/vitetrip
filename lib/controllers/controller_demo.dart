
import 'package:get/get.dart';
import '/model/demo/model_demo.dart';
import '/network/api/api.dart';
import '/services/services_locator.dart';

class DemoController extends GetxController {
  bool isLoading = false;
  bool isLoadingSlider = false;
  List<Demo> demoLists = [];
  int currentIndex = 0;

  //instance api
  final api = locator<Api>();

  fetchDemoDatas({idPagoda, idCategory, page = 1}) async {
    isLoadingSlider = true;
    isLoading = true;
    ///call api
//    await api.client!.getPagodas().then((value) => this.pagodas = value);
//    isLoadingSlider = false;
//    if (this.pagodas.length > 0) {
//      var pagodaId = this.pagodas[0].id;
//      await api.client!.getEvents({'pagoda_id': pagodaId, 'page': page}).then(
//              (value) => this.eventRes = value);
//      await api.client!.getFilterEvent().then((value) => this.filters = value);
//    } else {
//      eventRes.data = [];
//    }

    isLoading = false;
  }

}
