
import 'package:get/get.dart';
import 'package:mitta/model/guide_author/guide_author.dart';
import 'package:mitta/model/guide_author/guide_content.dart';
import 'package:mitta/services/audio_player/text_to_speech_vm.dart';
import '/model/demo/model_demo.dart';
import '/network/api/api.dart';
import '/services/services_locator.dart';

class GuideContentController extends GetxController {
  var isLoading = false.obs;
  List<GuideContentInfo> listContentData = [];

  GuideContentInfo? currentContentSelected;

  //instance api
  final api = locator<Api>();

  fetchContentData({authorId = "", locationId = "", artifactId = ""}) async {
    isLoading.value = true;
    ///call api
   await api.client!.getGuideContentsData(authorId: authorId, locationId: locationId, artifactId: artifactId).then((value) {
     this.listContentData = value.data;
   });

    isLoading.value = false;

    update();
  }

  final textToSpeech = TextToSpeech();
  void setPlayAudio(GuideContentInfo data){
    //play audio
    textToSpeech.setLoading(false);
    textToSpeech.setLocal(false);
    var url = data.audio;

    textToSpeech.setLoading(true);
    textToSpeech.setMp3(url);
    textToSpeech.init();
  }

  void playAudio(){
    textToSpeech.play();
  }

  void pauseOrResumeAudio(){
    textToSpeech.resumeOrPause();
  }

  void disposeAudio(){
    textToSpeech.dispose();
  }
}
