

//dialog country
import 'package:flutter/material.dart';
import 'package:mitta/commons/button/button_back.dart';
import 'package:mitta/commons/common.dart';
import 'package:mitta/model/location/model_country.dart';

import '../../../app_theme.dart';
import '../../../configs/configs.dart';

Future<CountryInfo?> countrySelectDialog(BuildContext context, List<CountryInfo> data, CountryInfo currentCountrySelected) async {

  var _selectCatalogue = currentCountrySelected;

  await showDialog(
    context: context,
    barrierColor: Colors.black.withOpacity(.7),
    builder: (context) => new AlertDialog(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      contentPadding: EdgeInsets.all(0),
      insetPadding:  EdgeInsets.all(20),
      content: StatefulBuilder(
        builder: (BuildContext context, StateSetter setState) {
          return Stack(
            children: [
              // Positioned(
              //     bottom: -20,
              //     right: 0,
              //     child: SizedBox(
              //         width: MediaQuery.of(context).size.width /4,
              //         child: Image.asset("assets/logo/app_logo.png", fit: BoxFit.contain,))),
              Container(
                decoration: BoxDecoration(
//                color: Colors.blue.withOpacity(.2),
                  borderRadius: BorderRadius.circular(5.0),
                ),
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        ///header
                        Center(
                          child: Row(
                            children: [
                              ButtonBack.backButtonSimple(backgroundColor: AppTheme.orange, iconColor: Colors.white),
                              Expanded(
                                child: Text(
                                  "Select country".toUpperCase(),
                                  style: Theme.of(context).textTheme.headline6!.copyWith(fontSize: 16, fontWeight: FontWeight.bold),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              ButtonBack.holderHeader()
                            ],
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          ),
                        ),


                        SizedBox(
                          height: 5,
                        ),

                        Divider(),

                        Container(
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.width,

                          child: GridView.builder(
                              gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                                  maxCrossAxisExtent: 200,
                                  childAspectRatio: 2 / 1,
                                  crossAxisSpacing: 20,
                                  mainAxisSpacing: 20),
                              itemCount: data.length,
                              itemBuilder: (BuildContext ctx, index) {
                                return GestureDetector(
                                  onTap: (){
                                    _selectCatalogue = data[index];
                                    // print(data[index].id);
                                    ToastNotify.showSuccessToast(message: "Change country successfully", isCenter: true);
                                    Navigator.of(context).pop(true);
                                  },
                                  child: Container(
                                    padding: EdgeInsets.all(3),
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                        color: _selectCatalogue.id.toString() == data[index].id.toString()
                                            ? AppTheme.white
                                            : Theme.of(context).backgroundColor,
                                        border: Border.all(color: _selectCatalogue.id.toString() == data[index].id.toString()
                                            ? AppTheme.orange
                                            : Theme.of(context).backgroundColor, width: 3),
                                        borderRadius: BorderRadius.circular(15),
                                        boxShadow: [
                                          BoxShadow(
                                              color: Color(0x575450).withOpacity(.052),
                                              offset: Offset(0, 3),
                                              blurRadius: 10,
                                              spreadRadius: 3)
                                        ],
                                      // image: new DecorationImage(
                                      //   image: new ExactAssetImage(
                                      //       data[index].images!.first.toString()
                                      //   ),
                                      //   fit: BoxFit.cover,
                                      // ),
                                    ),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        //image
                                        CircleAvatar(
                                          radius: 15.0,
                                          backgroundColor: Colors.white,
                                          child: Image.network(
                                            Configs.hostName +"/"+ data[index].images![0].toString(), //image,
                                            fit: BoxFit.cover,
                                            loadingBuilder: (BuildContext? context,
                                                Widget? child,
                                                ImageChunkEvent? loadingProgress) {
                                              if (loadingProgress == null) return child!;
                                              return Container(
                                                height: 35,
                                                width: 35,
                                                alignment: Alignment.center,
                                                child: CircularProgressIndicator(
                                                  color: Colors.orange,
                                                  value: loadingProgress.expectedTotalBytes !=
                                                      null
                                                      ? loadingProgress
                                                      .cumulativeBytesLoaded /
                                                      loadingProgress.expectedTotalBytes!
                                                          .toInt()
                                                      : null,
                                                ),
                                              );
                                            },
                                            errorBuilder: (BuildContext? context,
                                                Object? exception, StackTrace? stackTrace) {
                                              return Icon(Icons.language, size: 20,);
                                            },
                                          ),
                                        ),

                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: [
                                              Text(data[index].name.toString(),
                                                maxLines: 2, textAlign: TextAlign.center,overflow: TextOverflow.ellipsis,
                                                style: AppTheme.subtitleBoldLightMode,),
                                              if (data[index].name.toString() != 'null')
                                                Text(data[index].name.toString(),
                                                  style: Theme.of(context).textTheme.caption,
                                                  textAlign: TextAlign.center, maxLines: 2, overflow: TextOverflow.ellipsis,)
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              }),
                        ),

                      ],
                    ),
                  ),
                ),
              ),
            ],
          );
        },
      ),
    ),
  );
  return _selectCatalogue;
}
