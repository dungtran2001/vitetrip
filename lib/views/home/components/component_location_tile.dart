import 'dart:math';

import 'package:mitta/model/location/model_location.dart';


import '/app_theme.dart';
import '/commons/common.dart';
import '/configs/configs.dart';
import '/controllers/controller_home.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';


Widget locationInfoTile(LocationInfo app, context){
  return GestureDetector(
    onTap: (){

    },
    child: Container(
      child: new FittedBox(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Material(
              //color: Colors.amber[200],
              elevation: 1.0,
              borderRadius: BorderRadius.circular(15.0),
              //shadowColor: Color(0x802196F3),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      color: Theme.of(context).backgroundColor,
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(0,0,4,0),
                      child: Row(
                        children: [
                          //icon app
                          Container(
                            margin: EdgeInsets.only(right: 10),
                            width: 50,
                            height: 50,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                color: Colors.grey.shade300,
                                image: DecorationImage(
                                    image: AssetImage(app.images.toString()),
                                    fit: BoxFit.cover
                                )
                            ),
                          ),

                          //title
                          Expanded(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(app.name.toString(),
                                  style: TextStyle(
                                      fontSize: 18.0,
                                      fontFamily: AppTheme.fontSfPro,
                                      fontWeight: FontWeight.bold
                                  ), maxLines: 1,),
//            Divider(height: 2,),
                                Container(child: Text(app.description.toString(),
                                  style: Theme.of(context).textTheme.caption, maxLines: 2,)
                                ),
                              ],
                            ),
                          ),

                          //version
                          Container(child: Text(app.ranking.toString(),
                            style: Theme.of(context).textTheme.caption, maxLines: 2,)
                          ),
                        ],
                      ),
                    ),
                  ),

                ],)
          ),
        ),
      ),
    ),
  );
}


Widget locationInfoCard(LocationInfo app, context){
  return GestureDetector(
    onTap: (){

    },
    child: Material(
//        color: Colors.red[200],
//        elevation: 1.0,
        borderRadius: BorderRadius.circular(15.0),
        shadowColor: Color(0x802196F3),
        child: Container(
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            color: Theme.of(context).backgroundColor,
            borderRadius: BorderRadius.circular(15.0),
          ),
          child: Column(
            children: [
              //icon app
              Expanded(
                flex: 3,
                child: Container(
//                    margin: EdgeInsets.only(right: 10),
//                    width: 50,
//                    height: 50,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      color: Colors.grey.shade300,
                      image: DecorationImage(
                          image: AssetImage(app.images.toString()),
                          fit: BoxFit.cover
                      )
                  ),
                ),
              ),

              //title
              Expanded(
                flex: 1,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(app.name.toString(),
                      style: TextStyle(
//                            fontSize: 15.0,
                          fontFamily: AppTheme.fontSfPro,
                          fontWeight: FontWeight.bold
                      ), maxLines: 1,),
//            Divider(height: 2,),
                    Container(child: Text(app.description.toString(),
                      style: Theme.of(context).textTheme.caption, maxLines: 2,)
                    ),
                  ],
                ),
              ),

            ],
          ),
        )
    ),
  );
}


double roundDouble(double value, int places){
  double mod = pow(10.0, places).toDouble();
  return ((value * mod).round().toDouble() / mod);
}
