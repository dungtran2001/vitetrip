import 'dart:convert';

import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:qr_code_tools/qr_code_tools.dart';
import '/commons/common.dart';
import '/configs/configs.dart';
import 'package:vibration/vibration.dart';
import 'dart:io';

import '../../../app_theme.dart';


class QrView extends StatefulWidget{
  ValueChanged<String>? onchange;

  QrView({
    Key? key,
    this.onchange
  });

  @override
  State<QrView> createState() => _QRViewState();
}

class _QRViewState extends State<QrView> with TickerProviderStateMixin{
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  Barcode? result;
  late QRViewController controller;

  // In order to get hot reload to work we need to pause the camera if the platform
  // is android, or resume the camera if the platform is iOS.
  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      controller.pauseCamera();
    } else if (Platform.isIOS) {
      controller.resumeCamera();
    }
  }

  @override
  void initState(){

    super.initState();
  }

  //setting
  var isCamFlip = false;
  var isCamFlash = false;
  settingDialog(BuildContext context) {

    StateSetter _setState;
    showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        backgroundColor: Colors.white,

        contentPadding: EdgeInsets.all(5),
        actions: <Widget>[
          new MaterialButton(
            height:25,
//            color: Colors.grey.withOpacity(.2),
            onPressed: () {
              Navigator.of(context).pop(true);
            },
            child: new Text('XONG'),
          ),
        ],
//                                title: new Text('Cài đặt'),
        content: StatefulBuilder(
          builder: (BuildContext context, StateSetter setState){
            _setState = setState;
            return Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      'Tùy chọn',
                      style: AppTheme.subtitleBoldHighlight,
                      textAlign: TextAlign.start,
                    ),
                    SizedBox(height: 30,),

//                    Divider(),

                    GestureDetector(
                      onTap: (){
                        _getPhotoByGallery();

                      },
                      child: Row(
                        children: [
                          SizedBox(width: 32,),

                          Container(
                              padding: const EdgeInsets.all(3),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(30),
                                color: Colors.green.shade700,
                              ),
                              child: Icon(Icons.image, color: Colors.white, size: 18.5,)
                          ),
                          SizedBox(width: 13,),
                          Text(
                            'Chọn từ Ảnh',
                            textAlign: TextAlign.center,
                            style: AppTheme.subtitleLightMode,
                          ),
                        ],
                      ),
                    ),

                    Divider(),
                    GestureDetector(
                      onTap: (){
                        setState((){
                          controller.flipCamera();
                        });
                      },
                      child: Row(
                        children: [
                          SizedBox(width: 30,),

                          Icon(Icons.change_circle_rounded, color: Colors.green.shade700, size: 30,),
                          SizedBox(width: 10,),

                          Text(
                            'Đổi camera',
                            textAlign: TextAlign.center,
                            style: AppTheme.subtitleLightMode,
                          ),
                        ],
                      ),
                    ),

                    Divider(),
                    GestureDetector(
                      onTap: (){
                        setState((){
                          controller.toggleFlash();
                        });
                      },
                      child: Row(
                        children: [
                          SizedBox(width: 32,),

                          Container(
                              padding: const EdgeInsets.all(2),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(30),
                                color: Colors.green.shade700,
                              ),
                              child: Icon(Icons.light_mode_rounded, color: Colors.white, size: 22,)
                          ),
                          SizedBox(width: 13,),

                          Text(
                            'Bật đèn Flash',
                            textAlign: TextAlign.center,
                            style: AppTheme.subtitleLightMode,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  //pick qr image
  final picker = ImagePicker();
  String _qrcodeFile = '';
  String _data = '';
  void _getPhotoByGallery() async {
// ignore: deprecated_member_use
    var _image = await  picker.getImage(
        source: ImageSource.gallery, imageQuality: 30
    );

    if (_image != null){
//      setState(() {
      _qrcodeFile = _image.path; //backup this
//      });

      QrCodeToolsPlugin.decodeFrom(_image.path).then((value) {

        //get result ok
//        setState(() {
        _data = value;
//        });

        //process
        if (_data != null){
          var qrcodeData = _data;
          print("scan " + qrcodeData.toString());
          if (qrcodeData == "" || qrcodeData == null){
            //code khong hop le
            _error = "Mã QR code của bạn không đúng!";
            Common.showFlushBar(context, _error, Configs.error);
          }

        }else{
          //ma ko dung
          _error = "Mã QR code của bạn không đúng!";
        }

      }).onError((error, stackTrace) {
//        setState(() {
        _data = '';
//        });
        print('${error.toString()}');
        _error = "Mã QR code trong hình không đúng, vui lòng thử lại!";
        Common.showFlushBar(context, _error, Configs.error);

      });
    }
  }


  String _error = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: Column(
        children: <Widget>[
          Expanded(
            child: Stack(
              children: [
                QRView(
                  key: qrKey,
                  onQRViewCreated: _onQRViewCreated,
                  overlay: QrScannerOverlayShape(
                    borderColor: Colors.orange,
                    borderRadius: 10,
                    borderLength: 30,
                    borderWidth: 10,
                    cutOutSize: 200,
                  ),
                ),

                Positioned(
                    bottom: 10,
                    right: 0,
                    left: 0,
                    child:
                    Text("Đưa mã QR vào vùng quét",
                      textAlign: TextAlign.center,
                      style:TextStyle( // subtitle2 -> subtitle
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.italic,
                        fontSize: 14,
                        color: Colors.white,
                        shadows: [
                          Shadow(
                            blurRadius: 8.0,
                            color: Colors.grey.shade300,
                            offset: Offset(2.0, 2.0),
                          ),
                        ],
                      ),)
                ),

                //button add image from library
                Positioned(
                  top: 5,
                  right: 5,
                  child: GestureDetector(
                    onTap: (){
                      _getPhotoByGallery();
                    },
                    child: Container(
                      alignment: Alignment.center,
                      padding: const EdgeInsets.all(5),
//                    height: 80,
//                    width: 80,
                      decoration: BoxDecoration(
                        color: Colors.white.withOpacity(.2),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Wrap(
                        crossAxisAlignment: WrapCrossAlignment.center,
                        direction: Axis.horizontal,
                        children: [
                          Icon(Icons.photo_library_outlined, size: 25, color: Colors.white,),
                          SizedBox(width: 5,),
                          Text("Ảnh", style: AppTheme.subtitleMini,)
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  var oldResult = "";

  bool isHasVibrate = false;
  void _onQRViewCreated(QRViewController controller) {
    print("rung");
    this.controller = controller;
    this.controller.scannedDataStream.listen((scanData) async {
      result = scanData;

      if (!isHasVibrate && (await Vibration.hasVibrator() ?? false)) {
        //      Vibration.vibrate(duration: 1000, amplitude: 128);
        isHasVibrate = !isHasVibrate;

        if (await Vibration.hasCustomVibrationsSupport() ?? false) {
          Vibration.vibrate(duration: 500);
        } else {
          Vibration.vibrate();
          await Future.delayed(Duration(milliseconds: 500));
          Vibration.vibrate();
        }
      }

      //direct
      if (oldResult != result!.code){
        oldResult = result!.code!;
        if (result != null){
          var qrcodeData = result!.code;
          print("scan " + qrcodeData.toString());
          if (qrcodeData == "" || qrcodeData == null){
            //code khong hop le
            _error = "Mã QR code của bạn không đúng!";
            widget.onchange!(_error);

          }else{
            //get data
            try {
              widget.onchange!(qrcodeData);

              //clear old data
//              setState(() {
                result = null;
                isHasVibrate = false;
//              });

            } catch(e){

            }

          }

        }else{
          //ma ko dung
          _error = "Mã QR code của bạn không đúng!";
          widget.onchange!(_error);

        }

      }

    });
  }



  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}