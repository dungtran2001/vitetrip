

import '/commons/drawer_menu.dart';
import '/services/service_storage.dart';
import '/services/services_locator.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../app_theme.dart';

final storage = locator<StorageLocal>();

Widget inkwell(DrawerList listData, {GestureTapCallback? onTap}) {
  print("bb ${listData.labelName}  $onTap");

  return Material(
    color: Colors.transparent,
    child: InkWell(
//      splashColor: Colors.grey.withOpacity(0.5),
      highlightColor: Colors.transparent,
      onTap: onTap != null
          ? onTap
          : (){
            if (onTap!= null){
              onTap;

              print("aa $onTap");
            }
            else{
              print("aa2");
              navigationtoScreen(listData.index);

            }
      },
      child: Stack(
        children: <Widget>[
          Container(
            padding: const EdgeInsets.symmetric(vertical: 5),
            child: Row(
              children: <Widget>[
                Container(
                  width: 6.0,
                  height: 45.0,
                ),
                const Padding(
                  padding: EdgeInsets.all(4.0),
                ),
                listData.isAssetsImage
                    ? Container(
                  width: 30,
                  height: 30,
                  child: Image.asset(listData.imageName,
                      color: AppTheme.nearlyBlack
                  ),
                )
                    : Icon(listData.icon!.icon,
//                    color: AppTheme.nearlyBlack
                ),
                const Padding(
                  padding: EdgeInsets.all(4.0),
                ),
                Text(
                  listData.labelName,
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 16,
//                    color: AppTheme.nearlyBlack.withOpacity(.9),
                  ),
                  textAlign: TextAlign.left,
                ),
              ],
            ),
          ),

        ],
      ),
    ),
  );
}

void navigationtoScreen(index){
  switch (index){
    case DrawerIndex.Account:
      if (storage.userData == null) {
        showConfirmLogin();
      } else {
//        Navigator.push(
//            context,
//            new MaterialPageRoute(
//                builder: (context) => new MyProfile()));
      }
      break;
    case DrawerIndex.Wallet:
      if (storage.userData == null) {
        showConfirmLogin();
      } else {
//        Navigator.push(
//            context,
//            new MaterialPageRoute(
//                builder: (context) => new WalletMenu()));
      }
      break;
    case DrawerIndex.History:
      if (storage.userData == null) {
        showConfirmLogin();
      } else {
//        Navigator.push(
//            context,
//            new MaterialPageRoute(
//                builder: (context) => new HistoryMenu()));
      }
      break;
    case DrawerIndex.Notify:
      if (storage.userData == null) {
        showConfirmLogin();
      } else {
//        Navigator.push(
//            context,
//            new MaterialPageRoute(
//                builder: (context) => new NotifyScreen()));
      }
      break;
    case DrawerIndex.About:
//      _launchAbout();
      break;
    case DrawerIndex.Vote:
      print("danh gia");
//      triggerRateApp();
      break;
    case DrawerIndex.Help:
//      Navigator.push(
//          context,
//          new MaterialPageRoute(
//              builder: (context) => new SupportScreen()));
      break;
  }
}

void showConfirmLogin() {
  Get.defaultDialog(
    content: Container(
      height: 200,
      width: 300,
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Bạn chưa đăng nhập?",
              style: AppTheme.headline,
            ),
            SizedBox(
              height: 8,
            ),
            Text(
              "Hãy đăng nhập hoặc đăng ký để sử dụng dịch vụ",
              style: AppTheme.body1,
            ),
            SizedBox(
              width: 320.0,
              child: MaterialButton(
                onPressed: () {
                  Get.back();
//                        Navigator.push(
//                            context,
//                            new MaterialPageRoute(
//                                builder: (context) => new Login()))
//                            .then(onGoBack);
                },
                child: Text(
                  "Đăng nhập",
                  style: AppTheme.button,
                ),
                color: Colors.green,
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Center(
              child: GestureDetector(
                  onTap: () {
                    Get.back();
//                    Navigator.of(context, rootNavigator: true).pop('dialog');
//                          Navigator.push(
//                              context,
//                              new MaterialPageRoute(
//                                  builder: (context) => new Register()))
//                              .then(onGoBack);
                  },
                  child: Text(
                    "Đăng ký",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.blue),
                  )),
            ),
          ],
        ),
      ),
    )
  );
}

