import 'package:flutter/material.dart';

class BorderPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {

    final pointA = Offset(0, 0); //Offset(0, 0);
    final pointB = Offset(size.width * 0.8, 0); //Offset(240, 0);
    final pointC = Offset(size.width, size.height/5); //Offset(300, 30);
    final pointD = Offset(size.width, size.height); //Offset(300, 150);
    final pointE = Offset(0, size.height); //Offset(0, 150);

    final Path path = Path()
      ..moveTo(pointA.dx, pointA.dy) //
      ..lineTo(pointB.dx, pointB.dy) //
      ..lineTo(pointC.dx, pointC.dy) //
      ..lineTo(pointD.dx, pointD.dy) //
      ..lineTo(pointE.dx, pointE.dy) //
      ..close();

    final Paint redPaint = Paint() // tạo paint màu đỏ để vẽ cờ đỏ
      ..color = Colors.cyan
      ..strokeWidth = 2
      ..style = PaintingStyle.stroke; // tô màu hết cả shape

    canvas.drawPath(path, redPaint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}