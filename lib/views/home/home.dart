import 'dart:io';
import 'dart:ui';

import 'package:anim_search_bar/anim_search_bar.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:lottie/lottie.dart';
import 'package:mitta/controllers/controller_select_province.dart';
import 'package:mitta/model/location/model_location.dart';
import 'package:mitta/services/service_storage.dart';
import 'package:mitta/services/services_locator.dart';
import 'package:mitta/views/detect/detect_object.dart';
import 'package:system_settings/system_settings.dart';
import 'package:tiengviet/tiengviet.dart';

import '../../commons/common.dart';
import '../../network/httpController.dart';
import '../../services/service_geolocation.dart';
import '../../services/service_notify_local.dart';
import '../setting/settings_screen.dart';
import '../xplore/xplore_home.dart';
import '/views/login/login_screen.dart';

import '/app_theme.dart';
import '/configs/configs.dart';
import '/controllers/controller_home.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'components/components_dialog.dart';

class HomeView extends StatefulWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> with WidgetsBindingObserver {
  //get instance controller
  final HomeController controller = Get.put(HomeController());
  final SelectProvinceController locationController =
      Get.put(SelectProvinceController());
  final storage = locator<StorageLocal>();
  final geolocation = locator<GeoLocationService>();
  CarouselController sliderController = CarouselController();
  var currentSliderIndex = 0;

  var subscription;
  var networkConnected = false;

  var myLocation;

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    initialize();
    super.initState();
  }

  void initialize() async {
    await initConnectivity();
    // WidgetsBinding.instance
    //     .addPostFrameCallback((_) async {
    if (!networkConnected) return;
    // var firestore = new FireStoreData();
    // firestore.initializeFlutterFireAndGetConfig().then((value) {
    checkForMaintain();
    NotificationLocal.instance.init();
    if (!storage.isSkipPermission) {
      geolocation.getCurrentLocation().then((value) {
        if (value != null) {
          // print(value);
          myLocation = value;

          //tim place gan nhat neu nguoi dung chua chọn
          controller.fetchBackupData(
              true, myLocation.latitude, myLocation.longitude);
        } else {
          controller.fetchBackupData(false, 0, 0);
        }
      });
    } else {
      controller.fetchBackupData(false, 0, 0);
    }

    //fetch tags
    controller.fetchCategoriesData();
    // });

    // });
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // TODO: implement didChangeAppLifecycleState
    super.didChangeAppLifecycleState(state);
    switch (state) {
      case AppLifecycleState.resumed:
        print("Widget resumed");
        if (!networkConnected) initialize();
        break;
      case AppLifecycleState.inactive:
        print("Widget inactive");
        break;
      case AppLifecycleState.paused:
        print("Widget paused");
        break;
      case AppLifecycleState.detached:
        print("Widget detached");
        break;
    }
  }

  @override
  void didChangeDependencies() {
    print("--change");
    controller.update();

    super.didChangeDependencies();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
    subscription.cancel();
  }

  //CHECK NETWORK CONNECTIVITY
  Future<void> initConnectivity() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      print("--4g connect");
      networkConnected = true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      print("--wifi connect");
      networkConnected = true;
    } else if (connectivityResult == ConnectivityResult.none) {
      print("--no connect");
      networkConnected = false;

      noConnectNetworkDialog();
    }
  }

  noConnectNetworkDialog() {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            //backgroundColor: AppColors.black.withOpacity(0.89),
            content: Builder(builder: (context) {
              return Container(
                color: Colors.white,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 18,
                    ),
                    Icon(
                      Icons.info,
                      color: Colors.grey.shade300,
                      size: 40,
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Text(
                      "connect_network_fail".tr,
                      style: TextStyle(color: Colors.black54),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 4,
                    ),
                    Text(
                      "connect_network_fail_des".tr,
                      style: AppTheme.noteAllPageText,
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ElevatedButton(
                          child: Text('open_network_setting'.tr),
                          onPressed: () {
                            SystemSettings.wifi();
                            Navigator.pop(context);
                          },
                        ),
                        TextButton(
                            onPressed: () {
                              exit(0);
                            },
                            child: Text("exit_network_fail".tr)),
                      ],
                    )
                  ],
                ),
              );
            }),
          );
        });
  }

  Future<void> checkForMaintain() async {
    //get respone from serverjj
    await HTTPController.Ping(Configs.hostName).then((value) async {
      // print("ping server ${Configs.hostName}: $value");
      if (value != 200) {
        var g = await HTTPController.Ping("https://www.google.com/")
            .catchError((e) {
          print(e);
        });
        print("ping google: ${g.toString()}");
        if (g == 200) {
          await Future.delayed(const Duration(milliseconds: 3000), () {
            showMaintainDialog();
          });
        } else {
          //mat ket noi mang
          await Future.delayed(const Duration(milliseconds: 3000), () {
//            triggerNoInternet();
          });
        }
      }
    });
  }

  //Maintain server
  showMaintainDialog() {
    showDialog(
        barrierDismissible: true,
        context: context,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            //backgroundColor: AppColors.black.withOpacity(0.89),
            content: Builder(builder: (context) {
              return Container(
                color: Colors.white,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.warning,
                          color: Colors.redAccent,
                        ),
                        Text(
                          " " + "system_maintenance".tr,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 18),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "we_will_be_back".tr,
                      style: TextStyle(
                          fontWeight: FontWeight.normal, fontSize: 15),
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              );
            }),
          );
        });
  }

  audioCommingSoonDialog() {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            //backgroundColor: AppColors.black.withOpacity(0.89),
            content: Builder(builder: (context) {
              return Container(
                color: Colors.white,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 18,
                    ),
                    Icon(
                      Icons.info,
                      color: Colors.grey.shade300,
                      size: 40,
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Text(
                      "audio_coming_soon".tr,
                      style: TextStyle(color: Colors.black54),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 4,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ElevatedButton(
                          child: Text('OK'),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                      ],
                    ),
                  ],
                ),
              );
            }),
          );
        });
  }

  TextEditingController searchController = TextEditingController();
  String historySearch = "";

  @override
  Widget build(BuildContext context) {
    var _screenSize = MediaQuery.of(context).size;
    var _screenSizeRatio = _screenSize.width / _screenSize.height;
    var _sliderItemHeight = _screenSize.height - 318.5;
    var _sliderItemFraction = _screenSizeRatio < 0.5 ? 0.75 : 0.65;

    var listLocationFilter = [];
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
          backgroundColor: AppTheme.background,
          resizeToAvoidBottomInset: false,
          body: GetBuilder<HomeController>(builder: (controller) {
            if (controller.currentProvinceSelect != null) {
              if (historySearch != "") {
                if (controller.searchLocationInfoList.length > 0)
                  listLocationFilter = controller.searchLocationInfoList;
                else
                  listLocationFilter = [];
              } else {
                if (locationController != null &&
                    locationController.listProvinceData != null &&
                    controller.currentTagSelect != null &&
                    locationController.listProvinceData.length > 0) {
                  listLocationFilter = controller
                      .getProvincesLocation()
                      .where((i) =>
                          i.categoryId ==
                              controller.currentTagSelect!.value.id ||
                          controller.currentTagSelect!.value.id == "0")
                      .toList();
                }
              }
              return SizedBox(
                width: _screenSize.width,
                height: _screenSize.height,
                child: Column(
                  children: [
                    ///header
                    Stack(
                      children: [
                        Container(
                          width: _screenSize.width,
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(20, 50, 25, 0),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(right: 5, bottom: 10),
                                  // padding: EdgeInsets.fromLTRB(10, 3, 0, 3),
                                  alignment: Alignment.centerLeft,
                                  // width: _screenSize.width /4,
                                  height: 48,
                                  // color: AppTheme.grey.withOpacity(.7),
                                  child: SizedBox(
                                    height: 48,
                                    child:
                                        Image.asset("assets/logo/app_logo.png"),
                                  ),
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    GestureDetector(
                                      onTap: () {
                                        countrySelectDialog(
                                                context,
                                                locationController
                                                    .listCountryData,
                                                controller.currentCountrySelect!
                                                    .value)
                                            .then((value) {
                                          if (value != null) {
                                            // controller.changeCountry(value);
                                            // controller.update();
                                            controller.currentCountrySelect!
                                                .value = value;
                                            locationController
                                                .fetchProvinceData(
                                                    countryId: value.id)
                                                .then((value) {
                                                  controller.changeProvince(locationController
                                                      .listProvinceData.first);
                                            });
                                            // controller.update();
                                          }
                                        });
                                      },
                                      child: Wrap(
                                        crossAxisAlignment:
                                            WrapCrossAlignment.center,
                                        children: [
                                          Text(
                                            TiengViet.parse(controller
                                                .currentCountrySelect!
                                                .value
                                                .name
                                                .toString()),
                                            style: AppTheme
                                                .subtitleBoldLightMode
                                                .copyWith(
                                                    fontSize: 14.5,
                                                    fontWeight:
                                                        FontWeight.w500),
                                          ),
                                          Icon(Icons.keyboard_arrow_down,
                                              color: AppTheme.orange, size: 20),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),

                                // Spacer(),
                                //
                                // Container(
                                //   margin: EdgeInsets.only(right: 0),
                                //   decoration: BoxDecoration(
                                //       borderRadius: BorderRadius.circular(10),
                                //       // color: Colors.black.withOpacity(.01)
                                //   ),
                                //   child: IconButton(
                                //       icon: Icon(Icons.search, size: 30, color: Colors.black54,),
                                //       onPressed: () {
                                //         // Get.to(()=> YourGuideView(model: controller.currentLocationsData!.value));
                                //       }
                                //   ),
                                // ),
                              ],
                            ),
                          ),
                        ),
                        Positioned(
                          top: 50,
                          right: 15,
                          child: Container(
                            height: 48,
                            // color: Colors.red,
                            // child: AnimSearchBar(
                            //   width: _screenSize.width - 85,
                            //   textController: searchController,
                            //   onSuffixTap: () {
                            //     setState(() {
                            //       searchController.clear();
                            //       historySearch = "";
                            //     });
                            //   },
                            //   onSubmitted: (value) {
                            //     // print(value);
                            //     controller.searchLocation(value);
                            //     historySearch = value;
                            //   },
                            //   rtl: true,
                            //   closeSearchOnSuffixTap: true,
                            //   boxShadow: false,
                            //   helpText: historySearch,
                            //   // prefixIcon: Icon(Icons.search),
                            //   // suffixIcon: Icon(Icons.clear),
                            // ),
                          ),
                        )
                      ],
                    ),

                    ///province list
                    if (historySearch == "")
                      Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 10.0, vertical: 15),
                          child: locationController != null &&
                                  locationController.listProvinceData != null &&
                                  locationController.listProvinceData.length > 0
                              ? LayoutBuilder(builder: (context, constrains) {
                                  // var totalProvinces = locationController.listProvinceData.length;
                                  return SingleChildScrollView(
                                      scrollDirection: Axis.horizontal,
                                      child: ConstrainedBox(
                                          constraints: BoxConstraints(
                                              minWidth: constrains.maxWidth),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: [
                                              for (var item in controller
                                                  .getProvinces()) //.where((element) => element.countryId == controller.currentCountrySelect!.value.id).toList()
                                                Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 10.0),
                                                  child: GestureDetector(
                                                      onTap: () {
                                                        controller
                                                            .changeProvince(
                                                                item);
                                                        currentSliderIndex = controller
                                                            .nearestLocationIndex(
                                                                myLocation
                                                                    .latitude,
                                                                myLocation
                                                                    .longitude);
                                                        sliderController
                                                            .animateToPage(
                                                                currentSliderIndex);
                                                        controller.update();
                                                      },
                                                      child: Column(
                                                        children: [
                                                          Text(
                                                            item.name
                                                                .toString(),
                                                            style: controller
                                                                        .currentProvinceSelect!
                                                                        .value
                                                                        .id
                                                                        .toString() ==
                                                                    item.id
                                                                        .toString()
                                                                ? AppTheme
                                                                    .subtitleBoldHighlight
                                                                    .copyWith(
                                                                        fontFamily: AppTheme
                                                                            .fontEpilogue,
                                                                        fontSize:
                                                                            16)
                                                                : AppTheme.subtitleBoldLightMode.copyWith(
                                                                    color: Colors
                                                                        .grey,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w500,
                                                                    fontFamily:
                                                                        AppTheme
                                                                            .fontInter,
                                                                    fontSize:
                                                                        16),
                                                          ),
                                                          Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    top: 10),
                                                            width: item.name
                                                                    .toString()
                                                                    .length *
                                                                10,
                                                            height: 3.5,
                                                            color: controller
                                                                        .currentProvinceSelect!
                                                                        .value
                                                                        .id
                                                                        .toString() ==
                                                                    item.id
                                                                        .toString()
                                                                ? AppTheme
                                                                    .orange
                                                                : Colors
                                                                    .transparent,
                                                          )
                                                        ],
                                                      )),
                                                )
                                            ],
                                          )));
                                })
                              : SizedBox(
                                  height: _screenSize.height * 0.56,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      SizedBox(
                                        child: Lottie.asset(
                                            'assets/anim/anim_marker.json'),
                                        width: _screenSize.width / 2,
                                        height: _screenSize.width / 2,
                                      ),
                                      Text(
                                        "no_data_for_country".tr,
                                        style: AppTheme.noteAllPageText,
                                      ),
                                    ],
                                  ),
                                ))
                    else
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 18.0, vertical: 20),
                        child: listLocationFilter.length > 0 &&
                                listLocationFilter[currentSliderIndex] !=
                                    null &&
                                listLocationFilter[currentSliderIndex]
                                        .province !=
                                    null
                            ? Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Column(
                                    children: [
                                      Text(
                                          listLocationFilter[currentSliderIndex]
                                              .province
                                              .name
                                              .toString(),
                                          style: AppTheme.subtitleBoldHighlight
                                              .copyWith(
                                                  fontFamily:
                                                      AppTheme.fontEpilogue)),
                                      Container(
                                        margin: EdgeInsets.only(top: 10),
                                        width: listLocationFilter[
                                                    currentSliderIndex]
                                                .province
                                                .name
                                                .toString()
                                                .length *
                                            1,
                                        height: 4.5,
                                        color: AppTheme.orange,
                                      )
                                    ],
                                  ),
                                ],
                              )
                            : SizedBox(
                                // child: Text("Không tìm thấy kết quả phù hợp"),
                                ),
                      ),

                    ///contents
                    if (locationController != null &&
                        locationController.listProvinceData != null &&
                        locationController.listProvinceData.length > 0)
                      CarouselSlider(
                        carouselController: sliderController,
                        options: CarouselOptions(
                            // aspectRatio: _screenSizeRatio,
                            height: _sliderItemHeight,
                            viewportFraction: _sliderItemFraction,
                            initialPage: 0,
                            enableInfiniteScroll: true,
                            reverse: false,
                            autoPlay: false,
                            autoPlayInterval: Duration(seconds: 5),
                            autoPlayAnimationDuration:
                                Duration(milliseconds: 1000),
                            autoPlayCurve: Curves.fastOutSlowIn,
                            enlargeCenterPage: true,
                            scrollDirection: Axis.horizontal,
                            onPageChanged: (value, _) {
                              currentSliderIndex = value;
                              if (controller.currentLocationsData != null &&
                                  controller
                                      .getProvincesLocation()
                                      .isNotEmpty) {
                                controller.currentLocationsData?.value =
                                    controller
                                        .getProvincesLocation()
                                        .elementAt(value);
                              }
                              controller.update();
                            }),
                        items: listLocationFilter.length > 0
                            ? listLocationFilter.map((item) {
                                return Builder(
                                  builder: (BuildContext context) {
                                    return mainContent(item, _screenSize);
                                  },
                                );
                              }).toList()
                            : [
                                Column(
                                  children: [
                                    SizedBox(
                                      child: Lottie.asset(
                                          'assets/anim/injection.json'),
                                      width: _screenSize.width / 2,
                                      height: _screenSize.width / 2,
                                    ),
                                    Text(
                                      "text_loading".tr,
                                      style: AppTheme.subtitleLightMode,
                                    ),
                                  ],
                                )
                              ],
                      )
                    else
                      SizedBox(
                          // height: _screenSize.height * 0.56,
                          // child: Center(child: CircularProgressIndicator()),
                          ),

                    ///tags
                    if (!controller.isCategoryLoading.value)
                      Expanded(
                        child: SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 12),
                              child: Row(
                                children: [
                                  for (var tag in controller.tagList)
                                    GestureDetector(
                                      onTap: () {
                                        controller.changeCategory(tag);
                                        controller.update();
                                      },
                                      child: Container(
                                        padding: EdgeInsets.all(6),
                                        margin:
                                            EdgeInsets.symmetric(horizontal: 8),
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(7),
                                            color: controller.currentTagSelect!
                                                        .value ==
                                                    tag
                                                ? AppTheme.blue
                                                : AppTheme.grey
                                                    .withOpacity(.7)),
                                        child: Text(
                                          tag.name.toString(),
                                          style: AppTheme.subtitleLightMode
                                              .copyWith(
                                                  fontSize: 13,
                                                  color: controller
                                                              .currentTagSelect!
                                                              .value ==
                                                          tag
                                                      ? Colors.white
                                                      : Colors.black54),
                                        ),
                                      ),
                                    )
                                ],
                              ),
                            )),
                      ),
                    // Spacer(),
                    ///bottom bar
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      height: 70,

                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          //avatar
                          Container(
                            alignment: Alignment.centerLeft,
                            width: _screenSize.width / 6,
                            child: GestureDetector(
                              onTap: () {
                                //check already login
                                if (storage.checkUserAlreadyLogin()) {
                                  //already login
                                  Get.to(() => SettingsScreen());
                                } else {
                                  //has not yet login
                                  Get.to(
                                      () => LoginScreen(
                                            isBackable: true,
                                          ),
                                      transition: Transition.fadeIn,
                                      duration:
                                          Duration(milliseconds: 500)); //test
                                }
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  shape: BoxShape.circle,
                                  boxShadow: [BoxShadow(color: Colors.grey.withOpacity(0.5),
                                    spreadRadius: 2,
                                    blurRadius: 7,
                                    offset: Offset(0, 3), // changes position of shadow
                                  )],
                                ),
                                child: CircleAvatar(
                                  radius: 20.0,
                                  backgroundColor: Colors.white,
                                  child: storage.checkUserAlreadyLogin()
                                      ? Image.network(
                                    Configs.hostName + "/svg/user.png",
                                    //image,
                                    fit: BoxFit.fitWidth,
                                    loadingBuilder: (BuildContext? context,
                                        Widget? child,
                                        ImageChunkEvent? loadingProgress) {
                                      if (loadingProgress == null)
                                        return child!;
                                      return Container(
                                        height: 35,
                                        width: 35,
                                        alignment: Alignment.center,
                                        child: CircularProgressIndicator(
                                          color: Colors.orange,
                                          value: loadingProgress
                                              .expectedTotalBytes !=
                                              null
                                              ? loadingProgress
                                              .cumulativeBytesLoaded /
                                              loadingProgress
                                                  .expectedTotalBytes!
                                                  .toInt()
                                              : null,
                                        ),
                                      );
                                    },
                                    errorBuilder: (BuildContext? context,
                                        Object? exception,
                                        StackTrace? stackTrace) {
                                      return CircleAvatar(
                                        radius: 20.0,
                                        backgroundColor: Colors.white,
                                        backgroundImage: AssetImage(
                                          'assets/images/user.png',
                                        ),
                                      );
                                    },
                                  )
                                      : Image.asset("assets/images/user.png"),
                                ),
                              )
                            ),
                          ),

                          GestureDetector(
                            onTap: () {
                              Get.to(XploreViewInfo(
                                  tagList: controller.tagList,
                                  tagChoosed:
                                      controller.currentTagSelect!.value,
                                  placeList:
                                      controller.getNearestPlacesList()));
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                shape: BoxShape.circle,
                                boxShadow: [BoxShadow(color: Colors.grey.withOpacity(0.5),
                                  spreadRadius: 2,
                                  blurRadius: 7,
                                  offset: Offset(0, 3), // changes position of shadow
                                )],
                              ),
                              child: CircleAvatar(
                                radius: 20,
                                backgroundColor: Colors.white,
                                child: Padding(
                                  padding: EdgeInsets.symmetric(
                                    vertical: 5.0,
                                    horizontal: 5.0
                                  ),
                                  child: Image.asset("assets/images/park_marker_near.png"),
                                ),
                              ),
                            ),
                          )

                          // icon
                        ],
                      ),
                    )

                    ///bottom info
                  ],
                ),
              );
            } else
              return Center(
                  child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: _screenSize.width / 4,
                    vertical: _screenSize.height / 5),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    SizedBox(
                      child: Lottie.asset('assets/anim/injection.json'),
                      width: _screenSize.width / 2,
                      height: _screenSize.width / 2,
                    ),
                    Text(
                      " __ L O A D I N G __ ",
                      style: AppTheme.subtitleLightMode,
                    )
                    // ClipRRect(
                    //   borderRadius: BorderRadius.circular(2),
                    //   child: LinearProgressIndicator(
                    //     minHeight: 6.5,
                    //     valueColor: AlwaysStoppedAnimation<Color>(Color(0xFF00BDD6)),
                    //     backgroundColor: Color(0xFF00BDD6).withOpacity(.4),
                    //   ),
                    // ),
                  ],
                ),
              ));
          })
          //mainContent(_screenSize)
          ),
    );
  }

  mainContent(LocationInfo location, _screenSize) {
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          if (location.type == "server") {
            Get.to(() => DetectViewInfo(model: location));
          } else {
            audioCommingSoonDialog();
          }
        },
        child: Container(
          decoration: BoxDecoration(
            color: Colors.grey.shade400,
            borderRadius: BorderRadius.circular(25),
            image: (location != null &&
                    location.images != null &&
                    location.images!.length > 0)
                ? new DecorationImage(
                    image: NetworkImage(
                        location.images!.first.toString().contains("http")
                            ? location.images!.first +
                                "&maxWidthPx=${_screenSize.width.toInt()}"
                            : Configs.hostName + "/" + location.images!.first),
                    fit: BoxFit.cover,
                  )
                : new DecorationImage(
                    image: AssetImage('assets/images/default_img.png'),
                    fit: BoxFit.cover),
          ),
          child: Container(
            decoration: BoxDecoration(),
            child: (location != null)
                ? Stack(
                    children: [
                      if (location.type == "google")
                        Align(
                          alignment: Alignment(1.0, -0.8),
                          child: Container(
                            color: Colors.black.withOpacity(.5),
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: 8, horizontal: 12),
                              child: Text("audio_coming_soon".tr,
                                  style: TextStyle(color: Colors.white),
                                  textAlign: TextAlign.end),
                            ),
                          ),
                        ),

                      ///title and distance
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: ClipRRect(
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(25),
                              bottomRight: Radius.circular(25)),
                          child: Container(
                            color: Colors.black.withOpacity(.4),
                            child: BackdropFilter(
                              filter: ImageFilter.blur(
                                sigmaX: 0.0,
                                sigmaY: 0.50,
                              ),
                              child: Container(
                                width: _screenSize.width,
                                height: _screenSize.height * 0.55 / 6,
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 18.0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Padding(
                                          padding: const EdgeInsets.only(
                                              left: 5.0,
                                              right: 5.0,
                                              bottom: 5.0),
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Flexible(
                                                child: Text(
                                                  location.name.toString(),
                                                  maxLines: 2,
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontFamily:
                                                          AppTheme.fontInter),
                                                ),
                                              ),
                                              SizedBox(
                                                  height: 30,
                                                  width: 30,
                                                  child: Image.asset(
                                                      "assets/images/info.png"))
                                            ],
                                          )),

                                      //check  show distance
                                      if (location.lat != null &&
                                          location.long != null &&
                                          myLocation != null)
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(left: 5.0),
                                          child: Text(
                                            (Common.distanceBetween(
                                                            lat1: location.lat,
                                                            long1:
                                                                location.long,
                                                            lat2: myLocation
                                                                .latitude,
                                                            long2: myLocation
                                                                .longitude) /
                                                        1000)
                                                    .toStringAsFixed(1)
                                                    .toString() +
                                                " km" +
                                                "from_you".tr,
                                            style: TextStyle(
                                                color: AppTheme.blue,
                                                fontSize: 11,
                                                fontStyle: FontStyle.italic,
                                                fontWeight: FontWeight.normal,
                                                fontFamily: AppTheme.fontInter),
                                            maxLines: 2,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        )
                                      else
                                        GestureDetector(
                                          onTap: () {
                                            // request enable location
                                            geolocation
                                                .getCurrentLocation()
                                                .then((value) {
                                              if (value != null) {
                                                // print(value);
                                                myLocation = value;

                                                //tim place gan nhat neu nguoi dung chua chọn
                                                controller.fetchBackupData(
                                                    true,
                                                    myLocation.latitude,
                                                    myLocation.longitude);
                                              } else {
                                                controller.fetchBackupData(
                                                    false, 0, 0);
                                              }
                                            });
                                          },
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                                left: 5.0),
                                            child: Text(
                                              "___ km" + "from_you".tr,
                                              style: TextStyle(
                                                  color: AppTheme.blue,
                                                  fontSize: 12.5,
                                                  fontStyle: FontStyle.italic,
                                                  fontWeight: FontWeight.bold,
                                                  fontFamily:
                                                      AppTheme.fontInter),
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                          ),
                                        ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),

                      ///star rate
                      Positioned(
                        bottom: _screenSize.height * 0.55 / 6 - 10,
                        child: Padding(
                            padding: const EdgeInsets.only(left: 18.0),
                            child: Common.displayStarRating(
                                location.ranking ?? 5.0,
                                iconSize: 20.0)),
                      ),
                    ],
                  )
                : Center(child: CircularProgressIndicator()),
          ),
        ),
      ),
    );
  }
}
