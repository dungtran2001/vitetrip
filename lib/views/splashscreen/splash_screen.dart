

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mitta/views/language/choose_languge_screen.dart';
import '../../services/language/localization_service.dart';
import '/services/service_storage.dart';
import '/services/services_locator.dart';
import '/commons/animation_screen.dart';
import '/views/home/home.dart';
import '../../app_theme.dart';


class SplashScreen extends StatefulWidget {

//  final mainController = Get.put(MainController());
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  bool firstTimeChangeLang = true;

  final storage = locator<StorageLocal>();

  @override
  void initState(){

    init();

    super.initState();
  }



  void init(){


    //fetch data local
    storage.fetchSettingDataFromStorage().then((value) {
      if (storage.language == null || storage.language == ""){
        //already login
        Future.delayed(const Duration(milliseconds: 1000), () {
          Get.off(()=>ChooseLanguageView(), transition: Transition.fadeIn, duration: Duration(milliseconds: 1500)); //test
        });
      }else{
        LocalizationService.changeLocale(storage.language);

        storage.fetchUserDataFromStorage().then((value) {
          {
            //already login
            Future.delayed(const Duration(milliseconds: 1000), () {
              // Get.off(()=>OnBoardingView(), transition: Transition.fadeIn, duration: Duration(milliseconds: 1500)); //test
              Get.off(()=>HomeView(), transition: Transition.fadeIn, duration: Duration(milliseconds: 1500)); //test
            });
          }
        });
      }

    });



  }



  @override
  Widget build(BuildContext context) {

   return Scaffold(
     body: AnimationScreen(color: AppTheme.backgroundMainNightmode),
   );
  }
}
