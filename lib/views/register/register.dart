
import 'dart:math';

import 'package:get/get.dart';
import '../../commons/common.dart';
import '/services/service_storage.dart';
import '/services/services_locator.dart';
import '/services/service_notify_local.dart';
import '/commons/input_textField.dart';
import '../../model/user/model_user_account.dart';
import '/network/httpController.dart';
import '/views/login/login_screen.dart';


import '/../configs/configs.dart';
import 'package:flutter/material.dart';
import '/../app_theme.dart';
import 'components/input_field.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> with TickerProviderStateMixin {

//  final MainController mainController = Get.put(MainController());
  final storage = locator<StorageLocal>();

  Animation<double>? topBarAnimation;
  AnimationController? animationController;
  List<Widget> listViews = <Widget>[];

  final ScrollController scrollController = ScrollController();
  double topBarOpacity = 0.0;
  var _txtFullNameInput = TextEditingController();
  var _txtEmailInput = TextEditingController();
  var _txtRequestDescriptionInput = TextEditingController();
  var _txtPhoneInput = TextEditingController();
  var _txtPasswordInput = TextEditingController();
  var _txtConfirmPasswordInput = TextEditingController();
  var step = 1;
  var fullNameError = false;
  var emailError = false;
  var requestDescriptionError = false;
  var passwordError = false;
  var passwordConfirmError = false;
  var phoneError = false;
  var messageFullName = "";
  var messageEmail = "";
  var messageRequestDescription = "";
  var messagePhone = "";
  var messagePassword = "";
  var messageConfirmPassword = "";
  var isExistPhone = false;
  @override
  void dispose() {
    // Clean up the controller when the widget is removed from the
    // widget tree.
    _txtFullNameInput.dispose();
    _txtPhoneInput.dispose();
    _txtPasswordInput.dispose();
    _txtConfirmPasswordInput.dispose();
    super.dispose();
  }

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 600), vsync: this);
    topBarAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(
        CurvedAnimation(
            parent: animationController!,
            curve: Interval(0, 0.5, curve: Curves.fastOutSlowIn)));

    scrollController.addListener(() {
      if (scrollController.offset >= 24) {
        if (topBarOpacity != 1.0) {
          setState(() {
            topBarOpacity = 1.0;
          });
        }
      } else if (scrollController.offset <= 24 &&
          scrollController.offset >= 0) {
        if (topBarOpacity != scrollController.offset / 24) {
          setState(() {
            topBarOpacity = scrollController.offset / 24;
          });
        }
      } else if (scrollController.offset <= 0) {
        if (topBarOpacity != 0.0) {
          setState(() {
            topBarOpacity = 0.0;
          });
        }
      }
    });

    // get token to notify
//    NotificationLocal.instance.init();

    //fetch acc data in store
    storage.fetchUserDataFromStorage();

    super.initState();
  }

  @override
  void didChangeDependencies() async {
    if (step == 1) //not rebuild in step 2
      addAllListData();

    super.didChangeDependencies();
  }

  void addAllListData() {
    listViews = <Widget>[];
    const int count = 4;
    listViews.add(
      InputTextField(
        text: 'register_scr_input_user'.tr,
        inputType: TextInputType.text,
        isObscureText: false,
        inputController: _txtFullNameInput,
        onchange: (_) => validationData(),
        isError: fullNameError,
        message: messageFullName,
        style: const TextStyle(fontSize: 18,),
        maxLength: 30,
        animation: Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
            parent: animationController!,
            curve:
                Interval((1 / count) * 0, 1.0, curve: Curves.fastOutSlowIn))),
        animationController: animationController,
      ),
    );

    listViews.add(
      InputTextField(
        text: 'Email',
        inputType: TextInputType.text,
        isObscureText: false,
        inputController: _txtEmailInput,
        onchange: (_) => validationData(),
        isError: emailError,
        message: messageEmail,
        style: const TextStyle(fontSize: 18,),
        maxLength: 30,
        animation: Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
            parent: animationController!,
            curve:
                Interval((1 / count) * 0, 1.0, curve: Curves.fastOutSlowIn))),
        animationController: animationController,
      ),
    );

//    listViews.add(
//      InputTextField(
//        text: 'Số điện thoại',
//        inputType: TextInputType.number,
//        isObscureText: false,
//        isError: phoneError,
//        maxLength: 11,
//        message: messagePhone,
//        inputController: _txtPhoneInput,
//        animation: Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
//            parent: animationController!,
//            curve:
//                Interval((1 / count) * 1, 1.0, curve: Curves.fastOutSlowIn))),
//        animationController: animationController,
//      ),
//    );
    listViews.add(
      InputTextField(
        text: 'register_scr_input_pass'.tr,
        maxLength: 30,
        inputType: TextInputType.text,
        inputController: _txtPasswordInput,
        onchange: (_) => validationData(),
        isObscureText: true,
        isError: passwordError,
        message: messagePassword,
        style: const TextStyle(fontSize: 18,),
        animation: Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
            parent: animationController!,
            curve:
                Interval((1 / count) * 2, 1.0, curve: Curves.fastOutSlowIn))),
        animationController: animationController,
      ),
    );
    listViews.add(
      InputTextField(
        text: 'register_scr_input_cpass'.tr,
        maxLength: 30,
        inputType: TextInputType.text,
        isObscureText: true,
        isError: passwordConfirmError,
        message: messageConfirmPassword,
        inputController: _txtConfirmPasswordInput,
        onchange: (_) => validationData(),

        style: const TextStyle(fontSize: 18,),
        animation: Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
            parent: animationController!,
            curve:
                Interval((1 / count) * 3, 1.0, curve: Curves.fastOutSlowIn))),
        animationController: animationController,
      ),
    );

    listViews.add(buttonRegister());
    listViews.add(linkLogin());
  }

  Widget linkLogin() {
    return Padding(
        padding: const EdgeInsets.fromLTRB(10, 0, 0, 0.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              'register_scr_question'.tr,
              style: AppTheme.noteAllPageText,
            ),
            SizedBox(
              width: 10,
              height: 0,
            ),
            GestureDetector(
              onTap: () {
                // Get.to(()=>LoginScreen());
                Get.back();
              },
              child: Text(
                'register_scr_btn_login'.tr,
                style: TextStyle( decoration: TextDecoration.underline,
                    fontSize: 14, fontWeight: FontWeight.bold, color: Colors.blue),
              ),
            ),
          ],
        ));
  }
  void _checkPhone(String phone) async {
    String url = Configs.hostName + Configs.apiURI['check-phone']!;
    Map map = {
      "phone": phone
    };
    print(map);
    var _response = await HTTPController.PostByJson(url, map);
    var _status = _response["error"];
    setState(() {
      if (_status == false) {
        isExistPhone = false;
      } else {
        isExistPhone = true;
      }
    });
  }
  void _register(String fullName, String phone, String password, String confirmPassword) async {
    String url = Configs.hostName + Configs.apiURI['register']!;

    Map map = {
      "id": "",
      "name": fullName,
      "email": phone,
      "username": phone,
      "password": password,
      "repeatPassword": confirmPassword,
      "role": "user",
      "roles": ["user"],
      "deviceToken": Configs.deviceToken != null ? Configs.deviceToken : "",
    };

    print(map);

    var _response = await HTTPController.PostByJson(url, map);
    print(_response.toString());

    var _status;
    try{
      _status = _response["'success"] ?? true;
    }catch(e){

    }
    print(_status.toString());

//    await Future<dynamic>.delayed(const Duration(milliseconds: 2000)); //test

    //close popup
    Navigator.of(context).pop(true);

    if (_status == true) {
      //request success
      showDialog(
        context: context,
        builder: (context) => new AlertDialog(
          title: Row(
            children: [
              Icon(Icons.done, color: Colors.green,),
              new Text('register_scr_msg_requestok'.tr),
            ],
          ),
          content: new Text('register_scr_msg_requesttext'.tr),
          actions: <Widget>[
            new MaterialButton(
              onPressed: () {
                // Navigator.popUntil(context, ModalRoute.withName('/'));
                // Get.offAll(()=> LoginScreen(isBackable: true, isGoRoot: true,));
                Get.back();
                Get.back();
              },
              child: new Text('register_scr_btn_done'.tr),
            ),
          ],
        ),
      );
    } else {
      showDialog(
        context: context,
        builder: (context) => new AlertDialog(
          title: Row(
            children: [
              Icon(Icons.error, color: Colors.redAccent,),
              SizedBox(width: 10,),
              new Text(_response["error"] ?? 'text_error'.tr),
            ],
          ),
          content: new Text('register_scr_msg_note'.tr),
          actions: <Widget>[
            new MaterialButton(
              onPressed: () {
                Navigator.of(context).pop(true);
              },
              child: new Text('OK'),
            ),
          ],
        ),
      );
    }
  }

  bool validationData() {
    var isError = false;
    fullNameError = false;
    emailError = false;
    passwordError = false;
    passwordConfirmError = false;
    phoneError = false;
    messageFullName = "";
    messageEmail = "";
    messageRequestDescription = "";
    messagePhone = "";
    messagePassword = "";
    messageConfirmPassword = "";
    if(_txtFullNameInput.text == '') {
      fullNameError = true;
      messageFullName = 'register_scr_msg_user'.tr; //Common.getMessage(Configs.requiredTextField, 'vi', ['tên đăng nhập']);
      isError = true;
    }

   if(_txtEmailInput.text == '') {
     emailError = true;
     messageEmail = 'please_enter_email'.tr;
     isError = true;
   }else if (!Common.isEmailValid(_txtEmailInput.text)){
     emailError = true;
     messageEmail = 'invalid_email_address'.tr;
     isError = true;
   }

    if(_txtPasswordInput.text == '') {
      passwordError = true;
      messagePassword = 'register_scr_msg_pass'.tr; //Common.getMessage(Configs.requiredTextField, 'vi', ['mật khẩu']);
      isError = true;
    }
    if(_txtPasswordInput.text != _txtConfirmPasswordInput.text) {
      passwordConfirmError = true;
      messageConfirmPassword = 'register_scr_msg_cpass'.tr;//Common.getMessage(Configs.matchTextField, 'vi', []);
      isError = true;
    }

    addAllListData();
    if(isError) {
      return false;
    }else {
      return true;
    }
  }
  Widget buttonNext() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(40, 30, 40, 30.0),
      child: SizedBox(
        width: double.infinity,
        height: 40,
        child: Container(
          height: 40.0,
          child: GestureDetector(
            onTap: () {
              setState(() {
                validationData();
              });
            },
            child: Material(
              borderRadius: BorderRadius.circular(10.0),
              shadowColor: Colors.grey,
              color: Colors.blue,
              elevation: 3.0,
              child: Center(
                child: Text(
                  'register_scr_btn_continue'.tr,
                  style: TextStyle(
                      fontSize: 15,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Montserrat'),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget buttonRegister() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(40, 20, 40, 20.0),
      child: SizedBox(
        width: double.infinity,
        height: 40,
        child: Container(
          height: 40.0,
          child: GestureDetector(
            onTap: () {
              if (validationData()){
                //show popup loading
                //request success
                showDialog(
                  barrierDismissible: false,
                  context: context,
                  builder: (context) => new AlertDialog(
                    title: Container(
//                      height: 200,
//                      width: 200,
                      padding: EdgeInsets.all(20),
                        child:
                        Center(
                          child: Column(
                            children: [
                              SizedBox(
                                width: 40,
                                height: 40,
                                  child: new CircularProgressIndicator(backgroundColor: Colors.deepOrange.withOpacity(.8),)),
                              SizedBox(height: 10,),
                              Text('text_processing'.tr, style: AppTheme.subtitleLightMode,),
                            ],
                          ),
                        ),
                    ),
                  ),
                );
                //go register
               _register(
                   _txtFullNameInput.text,
                   _txtEmailInput.text,
                   _txtPasswordInput.text,
                   _txtConfirmPasswordInput.text,
               );
              }
            },
            child: Material(
              borderRadius: BorderRadius.circular(10.0),
              shadowColor: Colors.grey,
              color: Colors.blue,
              elevation: 5.0,
              child: Center(
                child: Text(
                  'register_scr_btn_register'.tr,
                  style: TextStyle(
                    fontSize: 15,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Montserrat'),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<bool> getData() async {
    await Future<dynamic>.delayed(const Duration(milliseconds: 50));
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Scaffold(
//          backgroundColor: AppTheme.backgroundMainNightmode,
          body: Stack(
            children: <Widget>[
              getMainListViewUI(),
              getHeaderUI(),
              SizedBox(
                height: MediaQuery.of(context).padding.bottom,
              )
            ],
          ),
        ));
  }

  Widget getMainListViewUI() {
    return FutureBuilder<bool>(
      future: getData(),
      builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
        if (!snapshot.hasData) {
          return const SizedBox();
        } else {
          return Padding(
            padding: const EdgeInsets.fromLTRB(20, 0,20, 0),
            child: ListView.builder(
              controller: scrollController,
              padding: EdgeInsets.only(
                top: AppBar().preferredSize.height +
                    MediaQuery.of(context).padding.top +
                    80,
                bottom: 62 + MediaQuery.of(context).padding.bottom,
              ),
              itemCount: listViews.length,
              scrollDirection: Axis.vertical,
              itemBuilder: (BuildContext context, int index) {
                animationController!.forward();
                return listViews[index];
              },
            ),
          );
        }
      },
    );
  }

  Widget getHeaderUI() {
    return Column(
      children: <Widget>[
        AnimatedBuilder(
          animation: topBarAnimation!,
          builder: (BuildContext context, Widget? child) {
            return FadeTransition(
              opacity: topBarAnimation!,
              child: Transform(
                transform: Matrix4.translationValues(
                    0.0, 30 * (1.0 - topBarAnimation!.value), 0.0),
                child: Container(
                  decoration: BoxDecoration(
                    color: AppTheme.white.withOpacity(topBarOpacity),
                    borderRadius: const BorderRadius.only(
                      bottomLeft: Radius.circular(32.0),
                    ),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          color: AppTheme.grey.withOpacity(0.4 * topBarOpacity),
                          offset: const Offset(1.1, 1.1),
                          blurRadius: 10.0),
                    ],
                  ),
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        height: MediaQuery.of(context).padding.top,
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            left: 16,
                            right: 16,
                            top: 16 - 8.0 * topBarOpacity,
                            bottom: 12 - 8.0 * topBarOpacity),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            step == 2
                                ? SizedBox(
                                    height: 38,
                                    width: 38,
                                    child: InkWell(
                                      highlightColor: Colors.transparent,
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(10.0)),
                                      onTap: () {
                                        setState(() {
                                          step = 1;
                                          addAllListData();
                                        });
                                      },
                                      child: Center(
                                        child: Icon(
                                          Icons.arrow_back,
                                          color: Colors.white70,
                                        ),
                                      ),
                                    ),
                                  )
                                : SizedBox(),
                            Expanded(
                              child: Padding(
                                padding: step == 2
                                    ? const EdgeInsets.fromLTRB(0, 0, 38, 00)
                                    : const EdgeInsets.fromLTRB(0, 0, 0, 00),
                                child: Center(
                                  child: Container(
                                    margin: EdgeInsets.only(top: 25),
                                    padding: EdgeInsets.all(5),
                                    width: 200,
                                    height: 50,
                                    child: Container(
                                      width: MediaQuery.of(context).size.width,
                                      height:
                                          MediaQuery.of(context).size.height,
                                     child: RichText(
                                         textAlign: TextAlign.center,
                                         text: TextSpan(
                                             text: "${Configs.appNameFirst} ",
                                             style: Theme.of(context).textTheme.headline5!
                                                 .copyWith(fontWeight: FontWeight.w500, fontSize: 20),
                                             children: [
                                               TextSpan(
                                                   text: "${Configs.appNameLast}",
                                                   style: TextStyle(
                                                     fontWeight: FontWeight.bold,
                                                     fontSize: 20,
                                                     letterSpacing: 0.27,
                                                     color: Colors.orange,
                                                   )),
                                             ])),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          },
        )
      ],
    );
  }

  Widget noteDescription(){
    return Padding(
      padding: const EdgeInsets.fromLTRB(20,0,15,0),
      child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          text: "${'register_scr_warning_tier'.tr}: ",
          style: TextStyle(fontSize: 14, color: Colors.white.withOpacity(.6), fontWeight: FontWeight.bold, fontStyle: FontStyle.italic),
          children: [
            TextSpan(text: "register_scr_warning_text".tr, style: TextStyle(fontSize: 14, fontWeight: FontWeight.normal, color: Colors.white.withOpacity(.6))),
//            TextSpan(text: "để có thể đăng nhập vào tài khoản.", style: TextStyle(color: Colors.white.withOpacity(.6), fontSize: 14, fontWeight: FontWeight.normal))
          ],
        ),
      ),
    );
  }
}
