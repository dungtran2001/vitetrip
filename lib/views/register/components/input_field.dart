import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../app_theme.dart';

class InputField extends StatefulWidget {
  final String hint;
  final Function(String value) onChange;
  final String? Function(String? value) validator;
  final TextEditingController controller;
  final TextInputType type;
  final bool obscureText;
  final Widget suffixIcon;
  InputField(
      {required this.hint,
      required this.controller,
      required this.onChange,
      required this.validator,
      this.type = TextInputType.text,
      this.suffixIcon = const Text(''),
      this.obscureText = false});

  @override
  State<StatefulWidget> createState() {
    return _InputField();
  }
}

class _InputField extends State<InputField> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 14),
      child: TextFormField(
        controller: widget.controller,
        onChanged: widget.onChange,
        validator: widget.validator,
        keyboardType: widget.type,
        decoration: InputDecoration(
          suffixIcon: widget.suffixIcon,
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(width: 1),
              borderRadius: BorderRadius.circular(10)),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: AppTheme.orange, width: 1),
              borderRadius: BorderRadius.circular(10)),
          errorBorder: OutlineInputBorder(
              borderSide: BorderSide(color: AppTheme.red, width: 1),
              borderRadius: BorderRadius.circular(10)),
          focusedErrorBorder: OutlineInputBorder(
              borderSide: BorderSide(color: AppTheme.red, width: 1),
              borderRadius: BorderRadius.circular(10)),
          labelText: widget.hint,
        ),
        obscureText: widget.obscureText,
      ),
    );
  }
}
