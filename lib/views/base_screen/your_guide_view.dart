
import 'dart:ui';

import 'package:audio_video_progress_bar/audio_video_progress_bar.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mitta/commons/button/button_back.dart';
import 'package:mitta/commons/components/audio/progress_notifier.dart';
import 'package:mitta/configs/configs.dart';
import 'package:mitta/model/location/model_location.dart';
import 'package:mitta/views/detect/painter/border_paint.dart';
import 'package:mitta/views/detect/painter/branch_paint.dart';
import '../../commons/components/audio/play_button_notifier.dart';
import 'package:mitta/model/object/model_object.dart';
import 'package:mitta/network/api/api.dart';
import 'package:mitta/network/response/response_objects.dart';
import 'package:mitta/services/services_locator.dart';
import 'package:mitta/services/tflite/recognition.dart';
import 'package:mitta/services/tflite/stats.dart';

import '../../app_theme.dart';



class YourGuideView extends StatefulWidget {
  final LocationInfo model;
  YourGuideView({required this.model});

  @override
  _YourGuideViewState createState() => _YourGuideViewState();
}

class _YourGuideViewState extends State<YourGuideView> {

  @override
  void initState(){
    super.initState();
  }



  @override
  Widget build(BuildContext context) {
    var _screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.black,
      body: Container(
        height: _screenSize.height,
        width: _screenSize.width,
        decoration: BoxDecoration(
          image: new DecorationImage(
            image: new NetworkImage(
                widget.model.images!.first ?? 'assets/images/locations/halong1.jpg'
            ),
            fit: BoxFit.cover,
            colorFilter: new ColorFilter.mode(Colors.black.withOpacity(0.4), BlendMode.overlay),
          ),
        ),
        child: Column(
          children: <Widget>[


            Expanded(
                child: Container(
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                        colors: [
                          Colors.black.withOpacity(.7),
                          Colors.black.withOpacity(.6),
                        ],
                        begin: Alignment.topCenter,
                        end: Alignment.center),
                  ),
                  child: Container(
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                          colors: [
                            Colors.cyanAccent.withOpacity(.2),
                            Colors.transparent,
                          ],
                          begin: Alignment.topCenter,
                          end: Alignment.center),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
              children: [
                    ///header
                    Container(
                      width: MediaQuery.of(context).size.width,
                      padding: const EdgeInsets.only(top: 45.0, right: 30),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          IconButton(
                              icon: Icon(Icons.center_focus_weak_rounded, color: Colors.yellow,size: 20),


                              onPressed: () { print("Pressed"); }
                          ),

                          Text("Hoi An".toString(),
                            style: TextStyle(
                                fontSize: 22,
                                fontFamily: AppTheme.fontMontserrat,
                                letterSpacing: -1, color: Colors.white,
                                shadows: [
                                  Shadow(
                                    blurRadius: 8.0,
                                    color: Colors.black38,
                                    offset: Offset(2.0, 2.0),
                                  ),
                                ],
                                fontWeight: FontWeight.w500),),


                          Spacer(),

                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
//                                color: Colors.black.withOpacity(.041)
                            ),
                            child: IconButton(
                                icon: Icon(Icons.grid_view_rounded, size: 30, color: Colors.white,),
                                onPressed: () { print("Pressed"); }
                            ),
                          ),
                        ],
                      ),
                    ),

              ],
            ),
                  ),
                )),

            ///bottom
            ClipRect(
              child: BackdropFilter(
                filter: ImageFilter.blur(
                  sigmaX: 4.0,
                  sigmaY: 4.0,
                ),
                child: Container(
                  width: MediaQuery.of(context).size.width,
//                  height: 170,
//                   color: Colors.black54.withOpacity(.4),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      AppTheme.GradientLine(
                          width: MediaQuery.of(context).size.width,
                        boundColor: Colors.cyanAccent.withOpacity(.6),
                        color: Colors.cyan,
                        height: 1.2
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 22.0, vertical: 20),
                        child: Text("Hoi An",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 25,
                            fontWeight: FontWeight.bold,
                            shadows: [
                              Shadow(
                                blurRadius: 8.0,
                                color: Colors.black38,
                                offset: Offset(2.0, 2.0),
                              ),
                            ],
                          ),),
                      ),
                    ],
                  ),
                ),
              ),
            ),

          ],
        ),
      ),
    );
  }

}
