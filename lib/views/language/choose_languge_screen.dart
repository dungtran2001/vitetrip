import 'package:flutter/material.dart';
import 'package:flutter_device_type/flutter_device_type.dart';
import 'package:get/get.dart';
import 'package:mitta/app_theme.dart';
import 'package:mitta/commons/common.dart';
import 'package:mitta/model/language/language.dart';
import 'package:mitta/views/home/components/component_inkwell.dart';
import 'package:mitta/views/home/home.dart';
import 'package:mitta/views/onboarding/onboarding_screen.dart';

import '../../services/language/localization_service.dart';

class ChooseLanguageView extends StatefulWidget {
  ChooseLanguageView({Key? key, this.isBackable = false}) : super(key: key);

  bool isBackable = false;

  @override
  State<ChooseLanguageView> createState() => _ChooseLanguageViewState();
}

class _ChooseLanguageViewState extends State<ChooseLanguageView> {
  List<Language> languageListData = <Language>[];
  var selectLanguage = Language(name: '', id: '', image: '');

  @override
  void initState() {
    languageListData = ListLanguages().languages;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      body: SizedBox(
        height: screenSize.height,
        width: screenSize.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.symmetric(vertical: 50),
              width: Device.width > 600
                  ? screenSize.width / 3
                  : screenSize.width / 2,
              height: Device.width > 600
                  ? screenSize.width / 3
                  : screenSize.width / 2,
              // color: AppTheme.grey,
              child: Image.asset("assets/logo/app_logo2.png"),
            ),

            RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                    text: "Choose your ",
                    style: TextStyle(
                      fontWeight: FontWeight.normal,
                      fontSize: 20,
                      letterSpacing: 0.27,
                      color: Colors.black54,
                    ),
                    children: [
                      TextSpan(
                          text: "language",
                          style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 25,
                            letterSpacing: 0.27,
                            color: Colors.black87.withOpacity(.7),
                          )),
                      TextSpan(
                          text: " app",
                          style: TextStyle(
                            fontWeight: FontWeight.normal,
                            fontSize: 20,
                            letterSpacing: 0.27,
                            color: Colors.black54,
                          )),
                    ])),

            //language list
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height / 3,
              padding: EdgeInsets.symmetric(horizontal: 30),
              child: GridView.builder(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3,
                      childAspectRatio: 1.9,
                      crossAxisSpacing: 18,
                      mainAxisSpacing: 40),
                  itemCount: languageListData.length,
                  itemBuilder: (BuildContext ctx, index) {
                    return GestureDetector(
                      onTap: () {
                        if (languageListData[index].isEnabled) {
                          LocalizationService.changeLocale(
                              languageListData[index].id);

                          setState(() {
                            selectLanguage = languageListData[index];
                          });
                        }
                      },
                      child: _buildImageContainer(languageListData[index])
                    );
                  }),
            ),

            Spacer(),
            Padding(
              padding: const EdgeInsets.all(30.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  if (widget.isBackable)
                    MaterialButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                      onPressed: () {
                        Get.back();
                      },
                      elevation: 0,
                      color: Colors.white,
                      child: Row(
                        children: [
                          Icon(
                            Icons.arrow_back_ios,
                            size: 17,
                          ),
                          Text(
                            "Back",
                            style: AppTheme.titleLightMode,
                          ),
                        ],
                      ),
                    )
                  else
                    MaterialButton(
                      onPressed: () {},
                      elevation: 0,
                      color: Colors.white,
                      child: Text(
                        "Next",
                        style: AppTheme.button,
                      ),
                    ),
                  if (selectLanguage.name.isNotEmpty)
                    Column(
                      children: [
                        Text(
                          "lang".tr,
                          style: TextStyle(
                              color: AppTheme.orange,
                              fontFamily: AppTheme.fontInter,
                              fontSize: 18),
                        ),
                        SizedBox(
                          height: 3,
                        ),
                        Container(
                          width: selectLanguage.name.toString().length * 10,
                          height: 2.5,
                          color: AppTheme.grey,
                        )
                      ],
                    )
                  else
                    SizedBox(),
                  MaterialButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                    onPressed: () {
                      if (selectLanguage.name.isNotEmpty &&
                          selectLanguage.id.isNotEmpty) {
                        storage.language = selectLanguage.id;
                        storage.backupSettingDataToStorage(
                            lang: selectLanguage.id);

                        if (widget.isBackable) {
                          // from setting screen
                          Get.back();
                          ToastNotify.showInfoToast(message: "success".tr);
                        } else {
                          //check da show onboarding chua
                          if (storage.onBoardingShowed) {
                            Future.delayed(const Duration(milliseconds: 1000),
                                () {
                              Get.off(() => HomeView(),
                                  transition: Transition.fadeIn,
                                  duration:
                                      Duration(milliseconds: 1500)); //test
                            });
                          } else {
                            Future.delayed(const Duration(milliseconds: 1000),
                                () {
                              Get.off(() => OnBoardingView(),
                                  transition: Transition.fadeIn,
                                  duration:
                                      Duration(milliseconds: 1500)); //test
                            });
                          }
                        }
                      } else {
                        ToastNotify.showInfoToast(
                            message:
                                "Please select the language to display in the app"); // Vui lòng chọn ngôn ngữ để hiển thị trong ứng dụng
                      }
                    },
                    color: AppTheme.orange,
                    child: Text(
                      "Next",
                      style: AppTheme.button,
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildImageContainer(Language language) {
    Widget languageImage = language.image.isNotEmpty
        ? Image.asset(
            language.image,
            fit: BoxFit.fitHeight,
          )
        : Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: AppTheme.grey,
            ),
          );
    Widget imageContainer = Container(
        margin: EdgeInsets.all(
          selectLanguage.id != language.id ? 3 : 0,
        ),
        alignment: Alignment.center,
        decoration: BoxDecoration(
            // color: AppTheme.grey,
            borderRadius: BorderRadius.circular(10),
            border: Border.all(
              color: selectLanguage.id == language.id
                  ? Color(0xFFED7D2D)
                  : AppTheme.grey,
              width: selectLanguage.id == language.id ? 3.2 : 0,
            )),
        child: languageImage);

    return language.isEnabled
        ? imageContainer
        : ColorFiltered(
            colorFilter: ColorFilter.mode(
              Colors.grey,
              BlendMode.saturation,
            ),
            child: languageImage,
          );
  }
}
