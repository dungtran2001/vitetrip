

import 'dart:io';
import 'dart:math';
import 'dart:ui';

import '../home/home.dart';
import '/configs/configs.dart';

import '/controllers/controller_login.dart';
import '/views/register/register.dart';

import '../../app_theme.dart';
import '/commons/authentication.dart';
import '/commons/common.dart';
import '/commons/input_textField.dart';
import 'package:flutter/material.dart';
import 'package:flutter_device_type/flutter_device_type.dart';
import 'package:get/get.dart';


class LoginScreen extends StatefulWidget {

  bool isBackable;
  bool isGoRoot;
  LoginScreen({this.isBackable = false, this.isGoRoot = false});
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> with TickerProviderStateMixin{

  final LoginController controller = Get.put(LoginController());

  Animation<double>? topBarAnimation;
  AnimationController? animationController;

  var _txtPhoneInput = TextEditingController();
  var _txtPasswordInput = TextEditingController();

  int count = 4;

  int _indexBackground = 1;

  @override
  void initState(){
    // random background
    Random random = new Random();
    _indexBackground = random.nextInt(4) + 1; // 1 -> 5

    animationController = AnimationController(
        duration: const Duration(milliseconds: 600), vsync: this);
    topBarAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(
        CurvedAnimation(
            parent: animationController!,
            curve: Interval(0, 0.5, curve: Curves.fastOutSlowIn)));

    //fetch acc data in store
//    HomeModel.instance.fetchBackupAccData();

    animationController!.forward();
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is removed from the
    // widget tree.
    animationController!.dispose();
    _txtPhoneInput.dispose();
    _txtPasswordInput.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
//        backgroundColor: AppTheme.backgroundMainNightmode,
        resizeToAvoidBottomInset: true,
        body: SingleChildScrollView(
          child: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [
                    Color(0xFF021A7A),
                    HexColor('#0098DA'),
                  ],
                  begin: Alignment.bottomLeft,
                  end: Alignment.topRight),
            ),
            padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            height: MediaQuery.of(context).size.height,
            child: Stack(
              children: [
                Positioned(
                  bottom: -10,
                  child: Container(
                    margin: EdgeInsets.only(bottom: 10),
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
//                    alignment: Alignment.center,
                    child: Image.asset("assets/images/locations/hoian2.jpg", fit: BoxFit.cover,)),),

                ClipRRect(child: BackdropFilter(
                  filter: ImageFilter.blur(sigmaX: 1.5, sigmaY: 0),
                  child: SizedBox(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: Column(
//                    mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [

                        //logo atoma
                        Center(
                          child: Container(
                            margin: EdgeInsets.only(bottom: 10, top: 50),
                              height: MediaQuery.of(context).size.width /4,
                              alignment: Alignment.center,
                              child: Image.asset("assets/images/logo_placehoder.png")),
                        ),

                        //app name
                        Center(
                          child: RichText(
                              textAlign: TextAlign.center,
                              text: TextSpan(
                                  text: "${Configs.appNameFirst} ",
                                  style: Theme.of(context).textTheme.headline5!
                                      .copyWith(fontWeight: FontWeight.w500, color: Colors.white, fontSize: 20, fontFamily: AppTheme.fontSfPro),
                                  children: [
                                    TextSpan(
                                        text: Configs.appNameLast.toUpperCase(),
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20,
                                          letterSpacing: 0.27,
                                          color: Colors.white,
                                        )),
                                  ])),
                        ),

                        SizedBox(height: 10,),


                        Container(
                          margin: EdgeInsets.all(20),
                          padding: EdgeInsets.all(20),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            color: Colors.white,
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                  color: Colors.black38.withOpacity(0.3),
                                  offset: const Offset(1.1, 1.5),
                                  blurRadius: 12.0),
                            ],
                          ),
                          child: Column(
                            children: [
                              //ten dang nhap
                              Obx(()=>
                                  InputTextField(
                                    text: 'login_scr_input_user'.tr,
                                    inputType: TextInputType.text,
                                    isObscureText: false,
                                    isError: controller.phoneError.value,
                                    maxLength: 30,
                                    message: controller.messagePhone.value,
                                    inputController: _txtPhoneInput,
                                    style: const TextStyle(fontSize: 18,),
                                    onchange: (_){
                                      //remove warning when start typing
                                      if (controller.phoneError.value == true)
                                        controller.phoneError.value = false;
                                    },
                                    animation: Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
                                        parent: animationController!,
                                        curve:
                                        Interval((1 / count) * 1, 1.0, curve: Curves.fastOutSlowIn))),
                                    animationController: animationController,
                                  ),
                              ),

                              //matkhau
                              Obx(()=>
                                  Stack(
                                    alignment: AlignmentDirectional.centerEnd,
                                    children: [
                                      InputTextField(
                                        text: 'login_scr_input_pass'.tr,
                                        maxLength: 30,
                                        onchange: (_){
                                          controller.passwordError.value = false;
                                        },
                                        inputType: TextInputType.text,
                                        inputController: _txtPasswordInput,
                                        isObscureText: !controller.isShowPassword.value,
                                        isError: controller.passwordError.value,
                                        message: controller.messagePassword.value,
                                        style: const TextStyle(fontSize: 18,),
                                        animation: Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
                                            parent: animationController!,
                                            curve:
                                            Interval((1 / count) * 2, 1.0, curve: Curves.fastOutSlowIn))),
                                        animationController: animationController,
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(right: 10.0),
                                        child: IconButton(
                                          onPressed: () {
                                            controller.isShowPassword.value = !controller.isShowPassword.value;
                                          },
                                          icon: Icon(Icons.remove_red_eye),
                                          color: Colors.grey.shade500,
                                        ),
                                      ),
                                    ],
                                  ),
                              ),

                              //button ok
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  //login with input
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(30, 20, 30, 20.0),
                                    child: SizedBox(
                                      width: MediaQuery.of(context).size.width - 200,
                                      height: 40,
                                      child: Container(
                                        height: 40.0,
                                        child: GestureDetector(
                                          onTap: () {
                                            var isError = false;
                                            if (_txtPhoneInput.text == ''){
                                              controller.phoneError.value = true;
                                              controller.messagePhone.value = 'login_scr_msg_user'.tr;//Common.getMessage(Configs.requiredTextField, 'vi', ['tên đăng nhập']);
                                              isError = true;
                                            }
                                            if (_txtPasswordInput.text == '') {
                                              controller.passwordError.value = true;
                                              controller.messagePassword.value = 'login_scr_msg_pass'.tr;//Common.getMessage(Configs.requiredTextField, 'vi', ['mật khẩu']);
                                              isError = true;
                                            }

                                            if (!isError) {
                                              controller.isStartingRequest.value = true;
                                              controller.signIn(_txtPhoneInput.text, _txtPasswordInput.text, widget.isGoRoot);
                                            }
                                          },
                                          child: Obx(()=> Material(
                                            borderRadius: BorderRadius.circular(15.0),
                                            shadowColor: Colors.grey,
                                            color: Color(0xFFED7D2D),
                                            elevation: 3.0,
                                            child: Center(
                                              child: controller.isStartingRequest.value
                                                  ? SizedBox(
                                                  height: 30,
                                                  width: 30,
                                                  child: CircularProgressIndicator(backgroundColor: Colors.white.withOpacity(.8)))
                                                  : Text(
                                                'login_scr_btn_login'.tr,
                                                style: TextStyle(
                                                    fontSize: 16.5,
                                                    color: Colors.white,
                                                    fontWeight: FontWeight.bold,
//                                                    fontFamily: 'Montserrat'
                                                ),
                                              ),
                                            ),
                                          )),
                                        ),
                                      ),
                                    ),
                                  ),

                                ],
                              ),

//                  if (Platform.isIOS)
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    'login_scr_question'.tr,
                                    style: Theme.of(context).textTheme.bodyText2!.copyWith(color: Theme.of(context).textTheme.bodyText2!.color!.withOpacity(.7)),
                                  ),
                                  SizedBox(
                                    width: 10,
                                    height: 0,
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      Get.to(()=>Register());
                                    },
                                    child: Text(
                                      'login_scr_btn_register'.tr,
                                      style: TextStyle( decoration: TextDecoration.underline,
                                          fontSize: 14, fontWeight: FontWeight.bold, color: Color(0xFFED7D2D),),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),

                        Spacer(),
                        if (widget.isBackable)
                          Center(
                            child: Padding(
                              padding: const EdgeInsets.only(top: 38.0, bottom: 20),
                              child: TextButton.icon(
                                  onPressed: (){
                                    Get.back();
                                  },
                                  icon: Icon(Icons.navigate_before, color: Colors.white,),
                                  label: Text("back".tr, style: TextStyle(color: Colors.white70),)),
                            ),
                          )

                      ],
                    ),
                  ),
                ),
                  ),
              ],
            ),
          ),
        ),
      )
    );
    throw UnimplementedError();
  }
}
