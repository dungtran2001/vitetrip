import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'dart:ui';

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/services.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter/material.dart';
import 'package:mitta/model/location/model_location.dart';

import '../../../app_theme.dart';
import '../../../commons/common.dart';
import '../../../configs/configs.dart';
import '../../../model/location/model_coordinate.dart';
import '../../../network/api/api.dart';
import '../../../services/services_locator.dart';
import 'component_map_controller.dart';


// mini map to apply detect license plate
class MapComponent extends StatefulWidget {
  String mapType;
  double height;
  bool? zoomGesturesEnabled;
  bool mapToolbarEnabled;
  Map<String, Marker>? markersList;
  bool myLocationButtonEnabled;
  bool zoomControlsEnabled;
  bool myLocationEnabled;
  LatLng? locationData;
  Set<Circle>? circles;
  Map<PolylineId, Polyline>? polylines;
  ValueChanged<String>? onchange;
  CameraPositionCallback? onCameraMove;
  VoidCallback? onCameraMoveDone;
  ArgumentCallback<LatLng>? onTap;
  List<dynamic>? listInfo;

   MapComponent(
      {Key? key,
        this.mapType = "MapType.normal",
        this.height = 250,
        this.zoomGesturesEnabled,
        this.mapToolbarEnabled = false,
        this.myLocationEnabled = true,
        this.myLocationButtonEnabled = false,
        this.zoomControlsEnabled = false,
        this.locationData,
        this.circles,
        this.polylines,
        this.markersList,
        this.onchange,
        this.onCameraMove,
        this.onCameraMoveDone,
        this.onTap,
        this.listInfo,
//        this.controller,
      })
      : super(key: key);

//  final MapComponentController? controller; //map controller
  //get instance controller

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return MapComponentState();
    throw UnimplementedError();
  }
}

class MapComponentState extends State<MapComponent> {

  final MapComponentController controller = Get.put(MapComponentController());

  //default
  CameraPosition _initialCamera = CameraPosition(
    target: LatLng(16.05431553577041, 108.21794986724854),
    zoom: 14.0000,
  );

  var _darkMapStyle, _normalMapStyle;

  @override
  void initState() {
//    widget.markersList = <String, Marker>{};

    if (widget.markersList != null)
      controller.markersList.value = widget.markersList!;

    if (widget.circles != null){
      controller.circles = RxSet();
      controller.circles!.value = widget.circles!;
    }
    else
      controller.circles = RxSet();

//    widget.polylines = <PolylineId, Polyline>{};

    MapComponentController _controller = controller;
    if (_controller != null) {
      _controller.gotoPosition = gotoLocation;
      _controller.addMarker = addMarker;
      _controller.createPolyline = createPolylines;
      _controller.clearPolyline = clearPolyline;
      _controller.addCurvedPolyline = addCurvedPolylines;
      _controller.changeMapTheme = changeMapTheme;
      _controller.setCustomMapPin = setCustomMapPin;
      _controller.setCircleBound = setCircleBound;
      _controller.removeMarker = removeMarker;
    }

    //ligh/night mode
    // rootBundle.loadString('assets/map_style/night_map_style.json').then((string) {
    //   _darkMapStyle = string;
    // });
    // rootBundle.loadString('assets/map_style/light_map_style.json').then((string) {
    //   _normalMapStyle = string;
    // });

    //set marker id default
//    if (widget.markersList!= null && widget.markersList!.entries.length > 0)
//      currentMarkerFocus = widget.markersList!.entries.first.value.markerId.value;

    setCustomMapPin();

    super.initState();
  }

  @override
  void dispose(){
    super.dispose();
  }


  void changeMapTheme(int theme) async {
    final GoogleMapController _controller = await _mapController.future;
    if (theme == 1)
      _controller.setMapStyle(_darkMapStyle);
    else
      _controller.setMapStyle(_normalMapStyle);

  }

  Completer<GoogleMapController> _mapController = Completer();


  ///goto position and animation camera
  Future<void> gotoLocation(double lat, double long, zoom) async {
    LatLng _target = lat != null && long != null
        ? LatLng(lat,long)
        : LatLng(widget.locationData!.latitude, widget.locationData!.longitude);

    final GoogleMapController controller = await _mapController.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
      target: _target,//LatLng(lat, long),
      zoom: zoom, ///Configs.zoomInAmount,
      tilt: zoom == Configs.zoomInAmountNormal? 0 : Configs.tiltFocusAmount,
      bearing: Configs.bearingFocusAmount,
    )));
  }

  late BitmapDescriptor pinLocationIcon; //marker icon
  late BitmapDescriptor pinLocationFocusIcon; // focus marker icon
  late BitmapDescriptor pinLocationCurrentIcon; // current position marker icon
  Future<void> setCustomMapPin() async {

    if (Platform.isIOS){
      final markerIcon = await Common.getBytesFromCanvas(0.9, 'assets/images/park_marker.png');
      pinLocationIcon = BitmapDescriptor.fromBytes(markerIcon);
    }else {
      pinLocationIcon = await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(devicePixelRatio: 2.5, size: Size(68, 68)),
          'assets/images/park_marker.png');
    }


    if (Platform.isIOS){
      final markerIcon = await Common.getBytesFromCanvas(0.9, 'assets/images/park_marker_big.png');
      pinLocationFocusIcon = BitmapDescriptor.fromBytes(markerIcon);
    }else {
      pinLocationFocusIcon = await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(devicePixelRatio: 2.5, size: Size(68, 68)),
          'assets/images/park_marker_big.png');
    }



    if (Platform.isIOS){
      var markerIcon = await Common.getBytesFromCanvas(0.9, 'assets/images/current_marker.png');
      pinLocationCurrentIcon = BitmapDescriptor.fromBytes(markerIcon);
    }else{
      pinLocationCurrentIcon = await BitmapDescriptor.fromAssetImage(
          ImageConfiguration(devicePixelRatio: 2.5, size: Size(68, 68)),
          'assets/images/current_marker.png');
    }

  }

  //this is the function to load custom map style json
  void changeMapMode(GoogleMapController mapController) {
    getJsonFile("assets/maps_config.json")
        .then((value) => setMapStyle(value, mapController));
  }

  //helper function
  void setMapStyle(String mapStyle, GoogleMapController mapController) {
    mapController.setMapStyle(mapStyle);
  }

  //helper function
  Future<String> getJsonFile(String path) async {
    ByteData byte = await rootBundle.load(path);
    var list = byte.buffer.asUint8List(byte.offsetInBytes,byte.lengthInBytes);
    return utf8.decode(list);
  }


  var currentMarkerFocus = "0"; // default focus to first marker in list

  final GGmapKey = new GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Container(
      child:
        GetBuilder<MapComponentController>(
          builder: (controller) =>
              SizedBox(
            height: widget.height,
            child: GoogleMap(
              key: GGmapKey,
              mapType: widget.mapType == "MapType.normal" ? MapType.normal : MapType.hybrid,
              zoomGesturesEnabled: widget.zoomGesturesEnabled ?? false,
              mapToolbarEnabled: widget.mapToolbarEnabled,
              myLocationEnabled: widget.myLocationEnabled,
              myLocationButtonEnabled: widget.myLocationButtonEnabled,//false,
              zoomControlsEnabled: widget.zoomControlsEnabled, //false,
              initialCameraPosition: CameraPosition(
                target: widget.locationData == null
                    ? _initialCamera.target
                    : LatLng(widget.locationData!.latitude, widget.locationData!.longitude),
                zoom: 14.4746,
              ),
              //_initialCamera,
//                      polylines: widget.polylines != null
//                          ? Set<Polyline>.of(widget.polylines!.values)
//                          : {},
              polylines: _polylines != null
                  ? _polylines
                  : {},
              onMapCreated: (GoogleMapController controller) {
                _mapController.complete(controller);
                if (_mapController != null) {
                  // if (Get.isDarkMode) {
                  //   controller.setMapStyle(_darkMapStyle);
                  // }
                  // else {
                  //   controller.setMapStyle(_normalMapStyle);
                  // }
                  changeMapMode(controller);
                }

              },
              onCameraIdle: widget.onCameraMoveDone,
              onTap: widget.onTap,
              onCameraMove: widget.onCameraMove,  //(object) {}
//                    markers: myModel.markersShowList != null ? Set<Marker>.of(myModel.markersShowList.values) : Set<Marker>.of({}),
              markers: controller.markersList.value != null
                  ? Set<Marker>.of(controller.markersList.value.values)
                  : Set<Marker>.of({}),

              circles: controller.circles!= null && controller.circles!.value != null ? controller.circles!.value : HashSet<Circle>(),
              gestureRecognizers: Set()..add(Factory<EagerGestureRecognizer>(() => EagerGestureRecognizer())),
            ),
          ),
        ),

    );
  }


  ///detect icon marker by type
  BitmapDescriptor iconMakerByType(MarkerType? type){
    if (type == null)
      return pinLocationIcon;
    else{
      switch (type){
        case MarkerType.big:
          return pinLocationFocusIcon;
        case MarkerType.normal:
          return pinLocationIcon;
        case MarkerType.mini:
          return pinLocationCurrentIcon;
        default:
          return pinLocationFocusIcon;
      }
    }
  }


  Future<void> addMarker(LocationInfo element, {MarkerType? type, Function? function}) async {


    final String markerIdVal = element.id.toString();
    final MarkerId markerId = MarkerId(markerIdVal);

    final Marker marker = Marker(
      markerId: markerId,
      position: LatLng(
          double.parse(element.lat.toString()) ?? 16.047079,
          double.parse(element.long.toString()) ?? 108.206230
      ),
      infoWindow: InfoWindow(title: element.name),// snippet: code),
      onTap: (){
        if (function != null)
          Future.delayed(const Duration(milliseconds: 200), () {
            function();
          });
      },
      icon: iconMakerByType(type)
    );

    if (controller.markersList.value != null){
      controller.markersList.value[markerIdVal] = marker;
      controller.update();
    }

  }


  Future<void> removeMarker(LocationInfo element) async {

    final String markerIdVal = element.id.toString();

    if (controller.markersList.value != null){
      controller.markersList.value.removeWhere((key, value) => key == markerIdVal);
      controller.update();

    }
  }


  Future<void> setCircleBound(LatLng point, double radius) async {
    //clear old circle
    controller.circles!.value.clear();

    var c = Circle(
        circleId: CircleId(DateTime.now().toString()),
        center: point,
        radius: radius * 1000, //km to met
        fillColor: Color(0xFF56daa4).withOpacity(.07),
        strokeWidth: 1,
        strokeColor: Color(0xFF00bbe3));

    controller.circles!.value.add(c);

    controller.update();
  }

  ///polyline

  // Object for PolylinePoints
  late PolylinePoints polylinePoints;

//  Map<PolylineId, Polyline> polylines = {};


// List of coordinates to join
  List<LatLng> polylineCoordinates = [];
  double totalDistance = 0.0;

// Map storing polylines created by connecting
// two points
  double _coordinateDistance(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
    return 12742 * asin(sqrt(a));
  }

  void calcTrafficDistance(){
    totalDistance = 0.0;

    for (int i = 0; i < polylineCoordinates.length - 1; i++) {
      totalDistance += _coordinateDistance(
        polylineCoordinates[i].latitude,
        polylineCoordinates[i].longitude,
        polylineCoordinates[i + 1].latitude,
        polylineCoordinates[i + 1].longitude,
      );
    }
  }
  // Create the polylines for showing the route between two places
//   Future<void> createPolylines(LatLng start, LatLng destination) async {
//
//     //check grant permission
// //    if (locationData == null){
// //      //show snack bar
// //      Common.showFlushBar(context, Configs.grantLocationPermissionMesage["vi"], Configs.error);
// //
// //      return;
// //    }
//
//     // Initializing PolylinePoints
//     polylineCoordinates = [];
//     var polylinePoints = PolylinePoints();
//
// //    if (start == null)
// //      start = LatLng(locationData.latitude, locationData.longitude);
//
//     // Generating the list of coordinates to be used for
//     // drawing the polylines
//     PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
//       Configs.mapApikey, // Google Maps API Key
//       PointLatLng(start.latitude, start.longitude),
//       PointLatLng(destination.latitude, destination.longitude),
//       travelMode: TravelMode.driving,
//     );
//
//     // Adding the coordinates to the list
//     if (result.points.isNotEmpty) {
//       result.points.forEach((PointLatLng point) {
//         polylineCoordinates.add(LatLng(point.latitude, point.longitude));
//       });
//
//       //calc to show
//       calcTrafficDistance();
//     }
//
//     // Defining an ID
//     PolylineId id = PolylineId('poly');
//
//     // Initializing Polyline
//     Polyline polyline = Polyline(
//       polylineId: id,
//       color: Colors.amber.shade800,
//       points: polylineCoordinates,
//       width: 5,
//     );
//
//     // Adding the polyline to the map
//     setState(() {
//       widget.polylines![id] = polyline;
//     });
//   }

  List<LatLng> latLong = [];
  Future<void> createPolylines(LatLng start, LatLng destination) async {

      _polylines.clear();
      latLong.clear();

      getPolyline(start.latitude, start.longitude, destination.latitude, destination.longitude);

      List<Coordinate> coordinates = await locator<Api>().client!
          .getPathCoordinates(
          source: LatLng(start.latitude, start.longitude),
          target: LatLng(destination.latitude, destination.longitude));
      coordinates.forEach((element) {
        LatLng data =
        LatLng(element.yCoordinate, element.xCoordinate);
        latLong.add(data);
      });
      setState(() {
        _polylines.add(Polyline(
          polylineId: PolylineId(start.toString()),
          visible: true,
          width: 3,
          //latlng is List<LatLng>
          points: latLong,
          color: AppTheme.orange,
        ));

        // isLoadingPolyline = false;

      });

  }

  getPolyline(startLatitude, startLongitude, destinationLatitude,
      destinationLongitude) async {
    double miny = (startLatitude <= destinationLatitude)
        ? startLatitude
        : destinationLatitude;
    double minx = (startLongitude <= destinationLongitude)
        ? startLongitude
        : destinationLongitude;
    double maxy = (startLatitude <= destinationLatitude)
        ? destinationLatitude
        : startLatitude;
    double maxx = (startLongitude <= destinationLongitude)
        ? destinationLongitude
        : startLongitude;

    double southWestLatitude = miny;
    double southWestLongitude = minx;

    double northEastLatitude = maxy;
    double northEastLongitude = maxx;

    // Accommodate the two locations within the
    // camera view of the map
    final GoogleMapController controller = await _mapController.future;
    controller.animateCamera(
      CameraUpdate.newLatLngBounds(
        LatLngBounds(
          northeast: LatLng(northEastLatitude, northEastLongitude),
          southwest: LatLng(southWestLatitude, southWestLongitude),
        ),
        10.0,
      ),
    );
  }

  void clearPolyline(){
    _polylines.clear();
    latLong.clear();
  }

  ///Curved line
  Set<Polyline> _polylines = Set(); //  final Set<Polyline> _polyline = {};
  void addCurvedPolylines(id, LatLng start, LatLng destination) {
    setState(() {
      _polylines.add(
          Polyline(
            polylineId: PolylineId(id),
            visible: true,
            width: 2,
            //latlng is List<LatLng>
            patterns: [PatternItem.dash(30), PatternItem.gap(10)],
            points: [start, destination],// MapsCurvedLines.getPointsOnCurve(start, destination), //line or curved
            color: Colors.red.shade800,
          )
      );
    });


  }
}
