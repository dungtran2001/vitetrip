

import 'package:mitta/model/location/model_location.dart';

import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../../configs/configs.dart';


typedef DefGotoLocation(double lat, double lng, double zoom);
typedef DefAddMarker(LocationInfo marker, {MarkerType? type, Function? function});
typedef DefRemoveMarker(LocationInfo marker);
typedef DefCreatPolyline(LatLng start, LatLng destination);
typedef DefClearPolyline();
typedef DefCreatCurvedPolyline(String id, LatLng start, LatLng destination);
typedef DefChangeMapTheme(int mode);
typedef DefSetCustomMapPin();
typedef DefSetCircleBound(LatLng latLng, double radius);

class MapComponentController extends GetxController {
  DefGotoLocation? gotoPosition;
  DefAddMarker? addMarker;
  DefRemoveMarker? removeMarker;
  DefCreatPolyline? createPolyline;
  DefClearPolyline? clearPolyline;
  DefCreatCurvedPolyline? addCurvedPolyline;
  DefChangeMapTheme? changeMapTheme;
  DefSetCustomMapPin? setCustomMapPin;
  DefSetCircleBound? setCircleBound;

  void dispose() {
    gotoPosition = null;
    addMarker = null;
    createPolyline = null;
    clearPolyline = null;
    addCurvedPolyline = null;
    changeMapTheme = null;
    setCustomMapPin = null;
    setCircleBound = null;
    removeMarker = null;
  }

//  Map<String, Marker>?
  var markersList = <String, Marker>{}.obs;
//  Set<Circle>?
  RxSet<Circle>? circles;
}