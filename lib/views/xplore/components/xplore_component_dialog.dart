

import 'dart:io';

import 'package:camera/camera.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mitta/model/location/model_location.dart';
import 'package:mitta/model/object/model_object.dart';

import '../../../app_theme.dart';
import '../../../configs/configs.dart';

//dialog thong bao chon diem tham quan
void pickGuideDialog(BuildContext context) async {

  await showDialog(
    context: context,
    barrierColor: Colors.black.withOpacity(.01),
    builder: (context) => new AlertDialog(
      backgroundColor: Colors.transparent,
      contentPadding: EdgeInsets.all(0),
      insetPadding:  EdgeInsets.all(0),
      content: StatefulBuilder(
        builder: (BuildContext context, StateSetter setState) {
          return GestureDetector(
            onTap: (){
              Navigator.of(context).pop(true);

              print("tap exit dialog");
            },
            child: Container(
              // height: MediaQuery.of(context).size.height /4,
              width: MediaQuery.of(context).size.width * .8,
              color: Colors.transparent,
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.black54,
                  borderRadius: BorderRadius.circular(18.0),
                  border: Border.all(color: AppTheme.orange)
                ),
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(
                                text: "xplore_choose_place".tr,
                                style: AppTheme.subtitle,
                                children: [
                                  TextSpan(
                                      text: "pick_guide".tr,
                                      style: AppTheme.subtitleBold),
                                ])),
                      ),
                      MaterialButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                        onPressed: (){
                          Get.back();
                        },
                        color: AppTheme.blue,
                        child: Text("OK", style: AppTheme.button,),

                      )
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      ),
    ),
  );
}


//dialog thong bao chon diem tham quan
void entranceTicketDialog(BuildContext context) async {

  await showDialog(
    context: context,
    barrierColor: Colors.black.withOpacity(.01),
    builder: (context) => new AlertDialog(
      backgroundColor: Colors.transparent,
      contentPadding: EdgeInsets.all(0),
      insetPadding:  EdgeInsets.all(0),
      content: StatefulBuilder(
        builder: (BuildContext context, StateSetter setState) {
          return GestureDetector(
            onTap: (){
              Navigator.of(context).pop(true);

              print("tap exit dialog");
            },
            child: Container(
              // height: MediaQuery.of(context).size.height /4,
              width: MediaQuery.of(context).size.width * .8,
              color: Colors.transparent,
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.black54,
                  borderRadius: BorderRadius.circular(18.0),
                  border: Border.all(color: AppTheme.orange)
                ),
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(
                                text: "xplore_choose_place".tr,
                                style: AppTheme.subtitle,
                                children: [
                                  TextSpan(
                                      text: "ticket".tr,
                                      style: AppTheme.subtitleBold),
                                ])),
                      ),
                      MaterialButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                        onPressed: (){
                          Get.back();
                        },
                        color: AppTheme.blue,
                        child: Text("OK", style: AppTheme.button,),

                      )
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      ),
    ),
  );
}


//dialog thong bao chon diem tham quan
void noLocationNearDialog(BuildContext context) async {

  await showDialog(
    context: context,
    barrierColor: Colors.black.withOpacity(.01),
    builder: (context) => new AlertDialog(
      backgroundColor: Colors.transparent,
      contentPadding: EdgeInsets.all(0),
      insetPadding:  EdgeInsets.all(0),
      content: StatefulBuilder(
        builder: (BuildContext context, StateSetter setState) {
          return GestureDetector(
            onTap: (){
              Navigator.of(context).pop(true);

              print("tap exit dialog");
            },
            child: Container(
              // height: MediaQuery.of(context).size.height /4,
              width: MediaQuery.of(context).size.width * .8,
              color: Colors.transparent,
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.black54,
                  borderRadius: BorderRadius.circular(18.0),
                  border: Border.all(color: AppTheme.orange)
                ),
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(
                                text: "xplore_no_place".tr,
                                style: AppTheme.subtitle,
                                children: [
                                  TextSpan(
                                      text: "xplore_radius".tr,
                                      style: AppTheme.subtitleBold),
                                ])),
                      ),
                      MaterialButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                        onPressed: (){
                          Get.back();
                        },
                        color: AppTheme.blue,
                        child: Text("OK", style: AppTheme.button,),

                      )
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      ),
    ),
  );
}
