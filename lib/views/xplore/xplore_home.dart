import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:geofence_service/geofence_service.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart' as geol;

import 'package:mitta/commons/button/button_back.dart';
import 'package:mitta/configs/configs.dart';
import 'package:mitta/model/location/model_location.dart';
import 'package:mitta/services/audio_player/audio_manager.dart';
import '../../commons/common.dart';
import 'package:mitta/services/services_locator.dart';

import '../../app_theme.dart';
import '../../controllers/controller_xplore.dart';
import '../../model/category/model_object.dart';
import '../../services/service_geolocation.dart';
import '../../services/service_storage.dart';
import '../detect/detect_object.dart';
import '../../model/location/model_province.dart';
import 'components/component_map.dart';
import 'components/xplore_component_dialog.dart';

/// hien thi cac object preview truoc khi scan
class XploreViewInfo extends StatefulWidget {
  List<CategoryInfo> tagList;
  CategoryInfo tagChoosed;
  List<LocationInfo> placeList;

  XploreViewInfo(
      {required this.tagList,
      required this.tagChoosed,
      required this.placeList});

  @override
  _XploreViewInfoState createState() => _XploreViewInfoState();
}

class _XploreViewInfoState extends State<XploreViewInfo> {
  bool isShowAudioPlayer = false;
  final mediaManager = locator<MediaManager>();

  final XploreController controller = Get.put(XploreController());
  final storage = locator<StorageLocal>();
  final geolocation = locator<GeoLocationService>();

  var myLocation;

  Timer? _timer;
  static const durationRefresh = const Duration(seconds: 5);

  StreamSubscription<ServiceStatus>? serviceStatusStream;

  bool isLocationEnable = false;

  @override
  void initState() {
    //set category
    controller.currentTagSelect = widget.tagChoosed.obs;
    controller.placesData = widget.placeList;
    controller.addLocationToObserverList();
    isLocationEnable = storage.isEnableXplore;

    print(isLocationEnable);
    Geolocator.isLocationServiceEnabled().then((value) {
      // isLocationEnable  = value;
      controller.update();
    });
    if (!storage.isSkipPermission) {
      print("isSkipPermission");
      serviceStatusStream =
          Geolocator.getServiceStatusStream().listen((ServiceStatus status) {
        print("((((((((((((((((((((( GPS ))))))))))))))))))))))))))");
        if (status == ServiceStatus.enabled) {
          // setState(() {
          //   isLocationEnable = true;
          // });
          geolocation.getCurrentLocation().then((value) async {
            if (value != null) {
              myLocation = value;
              //show my location
              await controller.showMyLocation(myLocation);
              //show marker and check
              controller
                  .showParkingMarkerWithDistance(
                      LatLng(myLocation.latitude, myLocation.longitude))
                  .then((isHasLocation) {
                if (!isHasLocation) {
                  //thong bao khong co location nao trong vong 100km
                  noLocationNearDialog(context);
                }
              });
            }
          });
          observerLocationNear();
        } else {
          // setState(() {
          //   isLocationEnable = false;
          // });
        }
        print(status);
      });

      // get first time to check 100km
      geolocation.getCurrentLocation().then((value) async {
        if (value != null) {
          // isLocationEnable = true;

          myLocation = value;
          //show my location
          await controller.showMyLocation(myLocation);
          //show marker and check
          controller
              .showParkingMarkerWithDistance(
                  LatLng(myLocation.latitude, myLocation.longitude))
              .then((isHasLocation) {
            if (!isHasLocation) {
              //thong bao khong co location nao trong vong 100km
              noLocationNearDialog(context);
            }
          });
          observerLocationNear();
        } else {
          // khong cho phep gps
          // hien diem tham quan moi nhat
          controller.showAllPlace();
        }
      });

      //schedule 5s refresh
      _timer = new Timer.periodic(
        durationRefresh,
        (Timer timer) {
          geolocation.getCurrentLocationWithoutPrompt().then((value) {
            if (value != null) {
              // print(value);
              myLocation = value;
              //show my location
              // controller.showMyLocation(myLocation);
              //show marker
              // controller.showParkingMarkerWithDistance(LatLng(myLocation.latitude, myLocation.longitude));
            } else {
              //user ko dong y cap quyen //imp sau
            }
          });
        },
      );
    } else {
      //imp sau
    }

    super.initState();
  }

  //start fence
  void observerLocationNear() {
    //observer location
    WidgetsBinding.instance.addPostFrameCallback((_) {
      controller.geofenceService
          .addGeofenceStatusChangeListener(controller.onGeofenceStatusChanged);
      // controller.geofenceService.addLocationChangeListener(_onLocationChanged);
      // controller.geofenceService.addLocationServicesStatusChangeListener(_onLocationServicesStatusChanged);
      // controller.geofenceService.addActivityChangeListener(_onActivityChanged);
      // controller.geofenceService.addStreamErrorListener(_onError);
      controller.geofenceService
          .start(controller.geofenceList)
          .catchError(controller.onError);
    });
  }

  void updateSoonDialog() {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            //backgroundColor: AppColors.black.withOpacity(0.89),
            content: Builder(builder: (context) {
              return Container(
                color: Colors.white,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 18,
                    ),
                    Icon(
                      Icons.info,
                      color: Colors.grey.shade300,
                      size: 40,
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Text(
                      "soon_update".tr,
                      style: TextStyle(color: Colors.black54),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 4,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ElevatedButton(
                          child: Text('OK'),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                      ],
                    ),
                  ],
                ),
              );
            }),
          );
        });
  }

  @override
  void didChangeDependencies() {
    if (!storage.isSkipPermission) {
      serviceStatusStream =
          Geolocator.getServiceStatusStream().listen((ServiceStatus status) {
        print("((((((((((((((((((((( GPS ))))))))))))))))))))))))))");
        if (status == ServiceStatus.enabled) {
          setState(() {
            isLocationEnable = true;
          });
          geolocation.getCurrentLocation().then((value) async {
            if (value != null) {
              myLocation = value;
              //show my location
              controller.showMyLocation(myLocation);
              //show marker and check
              controller
                  .showParkingMarkerWithDistance(
                      LatLng(myLocation.latitude, myLocation.longitude))
                  .then((isHasLocation) {
                if (!isHasLocation) {
                  //thong bao khong co location nao trong vong 100km
                  noLocationNearDialog(context);
                }
              });
            }
          });
        } else {
          setState(() {
            isLocationEnable = false;
          });
        }
        print(status);
      });
    }
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    if (_timer != null) _timer!.cancel();

    if (serviceStatusStream != null) serviceStatusStream!.cancel();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var _screenSize = MediaQuery.of(context).size;
    return WillStartForegroundTask(
      onWillStart: () async {
        // You can add a foreground task start condition.
        return controller.geofenceService.isRunningService;
      },
      androidNotificationOptions: AndroidNotificationOptions(
        channelId: 'geofence_service_notification_channel',
        channelName: 'AI GuideX Notification',
        channelDescription:
            'You just passed the attraction. Tap to return to the app',
        channelImportance: NotificationChannelImportance.LOW,
        priority: NotificationPriority.LOW,
        isSticky: false,
      ),
      iosNotificationOptions: const IOSNotificationOptions(),
      notificationTitle: 'notification_title'.tr,
      notificationText: 'notification_desc'.tr,
      foregroundTaskOptions: ForegroundTaskOptions(),
      child: Scaffold(
        body: GetBuilder<XploreController>(builder: (controller) {
          return SafeArea(
            child: Container(
                decoration: BoxDecoration(color: Colors.white),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    ///header
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      height: _screenSize.height / 8 - 10,
                      color: Colors.white,
                      alignment: Alignment.center,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          ButtonBack.backButtonSimple(onTap: () {
                            Get.back();
                          }),
                          //title
                          RichText(
                              textAlign: TextAlign.center,
                              text: TextSpan(
                                  text: "X",
                                  style: AppTheme.titleAppBar.copyWith(
                                      color: AppTheme.orange,
                                      fontFamily: AppTheme.fontPlayfair,
                                      fontSize: 24),
                                  children: [
                                    TextSpan(
                                        text: "plore",
                                        style: AppTheme.titleAppBar.copyWith(
                                            fontFamily: AppTheme.fontPlayfair,
                                            fontSize: 22)),
                                  ])),

                          SizedBox(
                            height: 30,
                            width: 30,
                            // child: Icon(
                            //   Icons.search,
                            //   color: Colors.black54,
                            //   size: 24,
                            // ),
                          )
                        ],
                      ),
                    ),

                    //category
                    SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row(
                        children: [
                          for (var tag in widget.tagList)
                            GestureDetector(
                              onTap: () {
                                controller.currentTagSelect!.value = tag;
                                controller.update();
                              },
                              child: Container(
                                padding: EdgeInsets.all(6),
                                margin: EdgeInsets.symmetric(horizontal: 12),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(7),
                                    color: controller.currentTagSelect!.value ==
                                            tag
                                        ? AppTheme.blue
                                        : AppTheme.grey.withOpacity(.5)),
                                child: Text(
                                  tag.name.toString(),
                                  style: AppTheme.subtitleLightMode.copyWith(
                                      fontSize: 13,
                                      color:
                                          controller.currentTagSelect!.value ==
                                                  tag
                                              ? Colors.white
                                              : Colors.black54),
                                ),
                              ),
                            )
                        ],
                      ),
                    ),

                    SizedBox(
                      height: 20,
                    ),
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          setState(() {
                            isShowAudioPlayer = !isShowAudioPlayer;
                          });
                        },
                        child: Stack(
                          children: [
                            // Container(
                            //   color: Colors.grey.shade100,
                            // ),
                            MapComponent(
                                height: MediaQuery.of(context).size.height,
//                controller: controller.mapController,
                                myLocationEnabled: false,
                                onCameraMove: (CameraPosition pos) {},
                                zoomGesturesEnabled: true,
                                onTap: (_) {
                                  //remove current marker selected
                                  controller.currentLocationSelected = null;

                                  //remove line
                                  controller.onClearPolyline();

                                  controller.update();
                                },
                                onCameraMoveDone: () {
                                  try {} catch (e) {}
                                }),

                            //notify and ticket
                            // Positioned(
                            //     bottom: 10,
                            //     right: 0,
                            //     left: 0,
                            //     child: Column(
                            //       children: [
                            //         //ticket
                            //         Row(
                            //           mainAxisAlignment:
                            //               MainAxisAlignment.center,
                            //           children: [
                            //             GestureDetector(
                            //               onTap: () {
                            //                 // if (controller
                            //                 //         .currentLocationSelected !=
                            //                 //     null) {
                            //                 //   // not implement yet
                            //                 // } else {
                            //                 //   //show popup info
                            //                 //   pickGuideDialog(context);
                            //                 // }
                            //                 updateSoonDialog();
                            //               },
                            //               child: Container(
                            //                 padding: EdgeInsets.all(10),
                            //                 height: 50,
                            //                 decoration: BoxDecoration(
                            //                     borderRadius:
                            //                         BorderRadius.circular(18),
                            //                     border: Border.all(
                            //                         width: 1.7,
                            //                         color: AppTheme.orange),
                            //                     color: controller
                            //                                 .currentLocationSelected !=
                            //                             null
                            //                         ? AppTheme.orange
                            //                         : Colors.white),
                            //                 child: Row(
                            //                   children: [
                            //                     Icon(
                            //                       Icons.wallet,
                            //                       color: controller
                            //                                   .currentLocationSelected !=
                            //                               null
                            //                           ? AppTheme.white
                            //                           : AppTheme.orange,
                            //                     ),
                            //                     SizedBox(
                            //                       width: 10,
                            //                     ),
                            //                     Text(
                            //                       "pick_guide".tr,
                            //                       style: AppTheme.subtitleBold
                            //                           .copyWith(
                            //                               color: controller
                            //                                           .currentLocationSelected !=
                            //                                       null
                            //                                   ? AppTheme.white
                            //                                   : AppTheme
                            //                                       .orange),
                            //                     )
                            //                   ],
                            //                 ),
                            //               ),
                            //             ),
                            //             SizedBox(
                            //               width: 10,
                            //             ),
                            //             GestureDetector(
                            //               onTap: () {
                            //                 // if (controller
                            //                 //         .currentLocationSelected !=
                            //                 //     null) {
                            //                 //   // not implement yet
                            //                 // } else {
                            //                 //   //show popup info
                            //                 //   entranceTicketDialog(context);
                            //                 // }
                            //                 updateSoonDialog();
                            //               },
                            //               child: Container(
                            //                 padding: EdgeInsets.all(10),
                            //                 height: 50,
                            //                 decoration: BoxDecoration(
                            //                     borderRadius:
                            //                         BorderRadius.circular(18),
                            //                     border: Border.all(
                            //                         width: 1.7,
                            //                         color: AppTheme.blue),
                            //                     color: controller
                            //                                 .currentLocationSelected !=
                            //                             null
                            //                         ? AppTheme.blue
                            //                         : Colors.white),
                            //                 child: Row(
                            //                   children: [
                            //                     Icon(
                            //                       Icons
                            //                           .airplane_ticket_outlined,
                            //                       color: controller
                            //                                   .currentLocationSelected !=
                            //                               null
                            //                           ? AppTheme.white
                            //                           : AppTheme.blue,
                            //                     ),
                            //                     SizedBox(
                            //                       width: 10,
                            //                     ),
                            //                     Text(
                            //                       "text_ticket".tr,
                            //                       style: AppTheme.subtitleBold
                            //                           .copyWith(
                            //                               color: controller
                            //                                           .currentLocationSelected !=
                            //                                       null
                            //                                   ? AppTheme.white
                            //                                   : AppTheme.blue),
                            //                     )
                            //                   ],
                            //                 ),
                            //               ),
                            //             )
                            //           ],
                            //         ),
                            //
                            //         SizedBox(
                            //           height: 20,
                            //         ),
                            //
                            //         //notìfy
                            //         Container(
                            //           width: _screenSize.width,
                            //           color: Colors.grey.shade300,
                            //           padding: EdgeInsets.symmetric(
                            //               horizontal: 8, vertical: 5),
                            //           child: Row(
                            //             mainAxisAlignment:
                            //                 MainAxisAlignment.spaceBetween,
                            //             children: [
                            //               Expanded(
                            //                 child: RichText(
                            //                     text: TextSpan(
                            //                         text: "enable_notification"
                            //                             .tr,
                            //                         style: AppTheme
                            //                             .subtitleBoldLightMode
                            //                             .copyWith(fontSize: 14),
                            //                         children: [
                            //                       TextSpan(
                            //                           text:
                            //                               "enable_notification2"
                            //                                   .tr,
                            //                           style: AppTheme
                            //                               .subtitleBoldLightMode
                            //                               .copyWith(
                            //                                   fontWeight:
                            //                                       FontWeight
                            //                                           .normal,
                            //                                   fontSize: 14))
                            //                     ])),
                            //               ),
                            //               Switch(
                            //                   value: isLocationEnable,
                            //                   activeColor: Colors.orange,
                            //                   onChanged: (value) async {
                            //                     if (value == true) {
                            //                       //kiem tra neu cha co permission
                            //                       var permission =
                            //                           await Geolocator
                            //                               .checkPermission();
                            //
                            //                       var position;
                            //                       if (permission ==
                            //                           geol.LocationPermission
                            //                               .denied) {
                            //                         // if (status) {
                            //                         await geolocation
                            //                             .showLocationPermissionDialog()
                            //                             .then((value) async {
                            //                           if (value != null &&
                            //                               value) {
                            //                             await geolocation
                            //                                 .requestLocationPermission();
                            //                             // LocationPermission permission = await Geolocator.requestPermission();
                            //                             position = await Geolocator
                            //                                 .getCurrentPosition(
                            //                                     desiredAccuracy:
                            //                                         geol.LocationAccuracy
                            //                                             .high);
                            //                           } else {
                            //                             print(
                            //                                 'Location permission NO granted');
                            //                           }
                            //                         });
                            //                         print(
                            //                             'Location permission new granted');
                            //                         return position;
                            //                       }
                            //                       // await Geolocator.openLocationSettings();
                            //                       try {
                            //                         controller.geofenceService
                            //                             .resume();
                            //                         ToastNotify.showInfoToast(
                            //                             message:
                            //                                 'allow_to_search'
                            //                                     .tr);
                            //                       } catch (e) {
                            //                         observerLocationNear();
                            //                       }
                            //                     } else {
                            //                       controller.geofenceService
                            //                           .pause();
                            //                       ToastNotify.showInfoToast(
                            //                           message:
                            //                               'stop_searching'.tr);
                            //                     }
                            //                     setState(() {
                            //                       isLocationEnable =
                            //                           !isLocationEnable;
                            //                     });
                            //
                            //                     storage.backupSettingDataToStorage(
                            //                         isenableXplore:
                            //                             isLocationEnable); // save local
                            //                   })
                            //             ],
                            //           ),
                            //         ),
                            //       ],
                            //     )),

                            //my position
                            Positioned(
                                right: 0,
                                top: 25,
                                child: GestureDetector(
                                  onTap: () {
                                    controller.gotoMyLocation();
                                  },
                                  child: Container(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 15, vertical: 5),
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(8),
                                            bottomLeft: Radius.circular(8),
                                          ),
                                          color: Colors.black45),
                                      child: SizedBox(
                                          height: 30,
                                          width: 30,
                                          child: Image.asset(
                                              "assets/images/icon_position.png"))
                                      //Icon(Icons.place_outlined, color: Colors.cyanAccent, size: 30,),
                                      ),
                                )),

                            //location selected
                            if (controller.currentLocationSelected != null)
                              Positioned(
                                  top: 5,
                                  right: 50,
                                  child: GestureDetector(
                                    onTap: () {
                                      Get.to(() => DetectViewInfo(
                                          model: controller
                                              .currentLocationSelected!.value));
                                    },
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(22),
                                      child: Stack(
                                        children: [
                                          Container(
                                            width: _screenSize.width / 3,
                                            height: _screenSize.width / 3 + 50,
                                            color: Colors.grey,
                                            child: Image.network(
                                              controller
                                                      .currentLocationSelected!
                                                      .value
                                                      .images!
                                                      .last
                                                      .toString()
                                                      .contains("http")
                                                  ? controller
                                                      .currentLocationSelected!
                                                      .value
                                                      .images!
                                                      .last
                                                      .toString()
                                                  : Configs.hostName +
                                                      "/" +
                                                      controller
                                                          .currentLocationSelected!
                                                          .value
                                                          .images!
                                                          .last
                                                          .toString(),
                                              fit: BoxFit.cover,
                                            ),
                                          ),
                                          Positioned(
                                              bottom: 0,
                                              child: Container(
                                                width: _screenSize.width / 3,
                                                height: 30,
                                                color: Colors.black54,
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: [
                                                    Common.displayStarRating(
                                                        controller
                                                                .currentLocationSelected!
                                                                .value
                                                                .ranking ??
                                                            5.0,
                                                        iconSize: 16.0),
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              left: 8.0),
                                                      child: Icon(
                                                        Icons.bookmark_outlined,
                                                        size: 15,
                                                        color: Colors.blue,
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ))
                                        ],
                                      ),
                                    ),
                                  ))
                          ],
                        ),
                      ),
                    ),
                  ],
                )),
          );
        }),
      ),
    );
  }
}
