

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mitta/app_theme.dart';
import 'package:mitta/services/service_storage.dart';
import 'package:mitta/services/services_locator.dart';
import 'package:mitta/views/detect/components/detect_view_component_tile.dart';
import 'package:mitta/views/home/home.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class OnBoardingView extends StatefulWidget {
  const OnBoardingView({Key? key}) : super(key: key);

  @override
  State<OnBoardingView> createState() => _OnBoardingViewState();
}

class _OnBoardingViewState extends State<OnBoardingView> {

  final controller = PageController(
      viewportFraction: 1,
      // keepPage: true,
      initialPage: 0);

  List<Widget> pages = [];

  int currentPage = 0;

  @override
  void initState(){
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    pages = [];
    pages.add(page1(screenSize));
    // pages.add(page2(screenSize));
    // pages.add(page3(screenSize));
    pages.add(page4(screenSize));

    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 20),
        height: screenSize.height,
        width: screenSize.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(height: 20),
            ///logo
            Container(
              margin: EdgeInsets.symmetric(vertical: 20),
              // width: screenSize.width /2,
              height: screenSize.width /5,
              // color: Colors.grey.shade200,
              child: Image.asset("assets/logo/app_logo_banner.png", fit: BoxFit.cover,),
            ),

            Expanded(
              child: PageView.builder(
                controller: controller,
                onPageChanged: (index){
                  setState(() {
                    currentPage = index;
                    print("mmmm "+ controller.page.toString() + " -- real $index");
                  });
                },
                itemCount: pages.length,
                itemBuilder: (_, index) {
                  return pages[index % pages.length];
                },
              ),
            ),

            ///next button
            FutureBuilder(
              future: Future.value(true),
              builder: (BuildContext context, AsyncSnapshot<void> snap) {

                //If we do not have data as we wait for the future to complete,
                //show any widget, eg. empty Container
                if (!snap.hasData) {
                  return Container();
                }

                //Otherwise the future completed, so we can now safely use the controller.page
                return Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      if (currentPage < pages.length -1 )
                        SizedBox()
                      else
                        MaterialButton(
                          onPressed: (){
                            //setting don't request permission
                            storage.isSkipPermission = true;
                            locator<StorageLocal>().backupSettingDataToStorage(onboardingShowed: true, isskipPermission: true);

                            Future.delayed(const Duration(milliseconds: 200), () {
                              Get.off(()=>HomeView(), transition: Transition.fadeIn, duration: Duration(milliseconds: 1500)); //test
                            });
                          },
                          child: Text('text_later'.tr, style: AppTheme.subtitleLightMode,),),

                      if (currentPage < pages.length -1 )
                        MaterialButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                          onPressed: (){
                            if (currentPage < pages.length -1 ){
                              controller.nextPage(duration: Duration(microseconds: 100), curve: Curves.easeIn);
                            }else {

                            }
                            // print(controller.page);

                          },
                          color: AppTheme.orange,
                          child: Text("text_next".tr, style: AppTheme.button,),

                        )
                      else
                        MaterialButton(
                          onPressed: (){
                            locator<StorageLocal>().backupSettingDataToStorage(onboardingShowed: true);

                            Future.delayed(const Duration(milliseconds: 200), () {
                              Get.off(()=>HomeView(), transition: Transition.fadeIn, duration: Duration(milliseconds: 1500)); //test
                            });
                          },
                          color: AppTheme.orange,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          child: Text("OK", style: AppTheme.button,),
                        )


                    ],
                  ),
                );
              },
            ),




          ///indicator
          Container(
            margin: EdgeInsets.only(bottom: 20),
              width: screenSize.width /4,
              alignment: Alignment.center,
              child: SmoothPageIndicator(
                  controller: controller,  // PageController
                  count: 2,
                  effect:  WormEffect(
                    dotHeight: 12,
                    dotWidth: 12,
                    type: WormType.normal,strokeWidth: 2,
                    dotColor: Colors.grey.shade400,
                    activeDotColor: Colors.orange,
                    paintStyle: PaintingStyle.stroke
                  ),  // your preferred effect
                  onDotClicked: (index){
                    setState(() {

                    });
                  },
              )
            )
          ],
        ),
      ),
    );
  }

  Widget page1(Size screenSize){
    return Column(
      children: [
        RichText(
            textAlign: TextAlign.left,
            text: TextSpan(
                text: "p1_title1a".tr,
                style: TextStyle(
                    fontWeight: FontWeight.normal,
                    fontSize: 16,
                    // letterSpacing: 0.27,
                    color: Colors.black87,
                    fontFamily: AppTheme.fontInter
                ),
                children: [
                  TextSpan(
                      text: "p1_title1b".tr,
                      style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 18,
                          // letterSpacing: 0.27,
                          color: Colors.black87.withOpacity(.7),
                          fontFamily: AppTheme.fontInter

                      )),
                  TextSpan(
                      text: "p1_title1c".tr,
                      style: TextStyle(
                          fontWeight: FontWeight.normal,
                          fontSize: 15.5,
                          letterSpacing: 0.27,
                          color: Colors.black54,
                          fontFamily: AppTheme.fontInter

                      )),
                ])),

        SizedBox(height: 20,),
        RichText(
            textAlign: TextAlign.left,
            text: TextSpan(
                text: "p1_title2a".tr,
                style: TextStyle(
                    fontWeight: FontWeight.normal,
                    fontSize: 15.5,
                    // letterSpacing: 0.27,
                    color: Colors.black54,
                    fontFamily: AppTheme.fontInter

                ),
                children: [
                  TextSpan(
                      text: "p1_title2b".tr,
                      style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 18,
                          // letterSpacing: 0.27,
                          color: Colors.black87.withOpacity(.7),
                          fontFamily: AppTheme.fontInter
                      )),
                  TextSpan(
                      text: "p1_title2c".tr,
                      style: TextStyle(
                          fontWeight: FontWeight.normal,
                          fontSize: 15.5,
                          letterSpacing: 0.27,
                          color: Colors.black54,
                          fontFamily: AppTheme.fontInter
                      )),
                  TextSpan(
                      text: "\n\n" + "p1_title3".tr,
                      style: TextStyle(
                          fontWeight: FontWeight.normal,
                          fontSize: 15.5,
                          // letterSpacing: 0.27,
                          color: Colors.black54,
                          fontFamily: AppTheme.fontInter
                      )),
                ])),


        Spacer(),
        ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: Container(
            // width: screenSize.width /3,
            height: screenSize.width /3 * 4/3,
            child: Image.asset("assets/images/onboarding_1.jpg", fit: BoxFit.cover,),
          ),
        ),
        Spacer(),
      ],
    );
  }

  Widget page2(Size screenSize){
    return Column(
      children: [

        SizedBox(height: 20,),
        RichText(
            textAlign: TextAlign.left,
            text: TextSpan(
                text: "",
                style: TextStyle(
                    fontWeight: FontWeight.normal,
                    fontSize: 17,
                    // letterSpacing: 0.27,
                    color: Colors.black54,
                    fontFamily: AppTheme.fontInter

                ),
                children: [
                  TextSpan(
                      text: "text_service".tr + " ",
                      style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 20,
                          // letterSpacing: 0.27,
                          color: Colors.black87.withOpacity(.7),
                          fontFamily: AppTheme.fontInter
                      )),
                  TextSpan(
                      text: " ${"text_service_l2".tr}",
                      style: TextStyle(
                          fontWeight: FontWeight.normal,
                          fontSize: 17,
                          // letterSpacing: 0.27,
                          color: Colors.black54,
                          fontFamily: AppTheme.fontInter
                      )),
                ])),


        Spacer(),
        ListTile(
          contentPadding: EdgeInsets.symmetric(horizontal: 40),
          horizontalTitleGap: 30,
          leading: ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: Container(
              width: 60,
              height: 52,
              child: Image.asset("assets/images/picture_holder.png", fit: BoxFit.cover,),
            ),
          ),
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("entrance_ticket".tr, style: AppTheme.subtitleBoldLightMode,),
              Text("with_ads".tr, style: AppTheme.subtitleMiniLightMode,),
            ],
          ),
        ),

        SizedBox(height: 20,),

        ListTile(
          contentPadding: EdgeInsets.symmetric(horizontal: 40),
          horizontalTitleGap: 30,
          leading: ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: Container(
              width: 60,
              height: 52,
              child: Image.asset("assets/images/picture_holder.png", fit: BoxFit.cover,),
            ),
          ),
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("text_transport".tr, style: AppTheme.subtitleBoldLightMode,),
              Text("point_by_point".tr, style: AppTheme.subtitleMiniLightMode,),
            ],
          ),
        ),
        SizedBox(height: 20,),

        ListTile(
          contentPadding: EdgeInsets.symmetric(horizontal: 40),
          horizontalTitleGap: 30,
          leading: ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: Container(
              width: 60,
              height: 52,
              child: Image.asset("assets/images/picture_holder.png", fit: BoxFit.cover,),
            ),
          ),
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("M Wallet", style: AppTheme.subtitleBoldLightMode,),
              Text("M wallet", style: AppTheme.subtitleMiniLightMode,),
            ],
          ),
        ),

        Spacer(),
      ],
    );
  }

  Widget page3(Size screenSize){
    return SizedBox();
  }

  Widget page4(Size screenSize){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [

        SizedBox(height: 20,),
        RichText(
            textAlign: TextAlign.left,
            text: TextSpan(
                text: "p2_title1a".tr,
                style: TextStyle(
                    fontWeight: FontWeight.normal,
                    fontSize: 17,
                    // letterSpacing: 0.27,
                    color: Colors.black54,
                    fontFamily: AppTheme.fontInter

                ),
                children: [
                  TextSpan(
                      text: "p2_title1b".tr,
                      style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 20,
                          // letterSpacing: 0.27,
                          color: Colors.black87.withOpacity(.7),
                          fontFamily: AppTheme.fontInter
                      )),
                  TextSpan(
                      text: "p2_title1c".tr,
                      style: TextStyle(
                          fontWeight: FontWeight.normal,
                          fontSize: 17,
                          // letterSpacing: 0.27,
                          color: Colors.black54,
                          fontFamily: AppTheme.fontInter
                      )),
                ])),

        SizedBox(height: 15,),

        RichText(
            textAlign: TextAlign.left,
            text: TextSpan(
                text: "p2_title2".tr,
                style: TextStyle(
                    fontWeight: FontWeight.normal,
                    fontSize: 17,
                    // letterSpacing: 0.27,
                    color: Colors.black54,
                    fontFamily: AppTheme.fontInter

                ),
                children: [
                ])),

        SizedBox(height: 15,),

        RichText(
            textAlign: TextAlign.left,
            text: TextSpan(
                text: "p2_title3".tr,
                style: TextStyle(
                    fontWeight: FontWeight.normal,
                    fontSize: 17,
                    // letterSpacing: 0.27,
                    color: Colors.black54,
                    fontFamily: AppTheme.fontInter

                ),
                children: [
                ])),


        Spacer(),
        ListTile(
          contentPadding: EdgeInsets.symmetric(horizontal: 40),
          horizontalTitleGap: 30,
          leading: CircleAvatar(
            radius: 20,
              backgroundColor: Colors.blue,
              child: Icon(Icons.location_on, color: Colors.white,)),
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("p2_content1".tr, style: AppTheme.subtitleBoldLightMode.copyWith(fontFamily: AppTheme.fontEpilogue, fontWeight: FontWeight.w600),),
            ],
          ),
        ),

        SizedBox(height: 20,),

        ListTile(
          contentPadding: EdgeInsets.symmetric(horizontal: 40),
          horizontalTitleGap: 30,
          leading: Icon(Icons.notifications_active_rounded, size: 40, color: Colors.blue,),
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("p2_content2".tr, style: AppTheme.subtitleBoldLightMode.copyWith(fontFamily: AppTheme.fontEpilogue, fontWeight: FontWeight.w600),),
            ],
          ),
        ),

        Spacer(),
      ],
    );
  }
}
