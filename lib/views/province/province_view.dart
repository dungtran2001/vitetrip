
import 'dart:ui';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:mitta/controllers/controller_select_province.dart';


import '../../app_theme.dart';
import 'components/component_province_tile.dart';



class SelectProvinceView extends StatefulWidget {
//  final LocationInfo model;
//  SelectProvinceView({required this.model});

  @override
  _SelectProvinceViewState createState() => _SelectProvinceViewState();
}

class _SelectProvinceViewState extends State<SelectProvinceView> {

  final controller = Get.put(SelectProvinceController());

  @override
  void initState(){
    controller.fetchProvinceData();
    super.initState();
  }



  @override
  Widget build(BuildContext context) {
    var _screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.black,
      body: GetBuilder<SelectProvinceController>(
        builder: (controller) {
          return Container(
            height: _screenSize.height,
            width: _screenSize.width,
            decoration: BoxDecoration(
              image: new DecorationImage(
                image: new ExactAssetImage(
                    'assets/images/locations/halong1.jpg'
                ),
                fit: BoxFit.cover,
                colorFilter: new ColorFilter.mode(Colors.black.withOpacity(0.4), BlendMode.overlay),
              ),
            ),
            child: Column(
              children: <Widget>[
                Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                            colors: [
                              Colors.black.withOpacity(.7),
                              Colors.black.withOpacity(.6),
                            ],
                            begin: Alignment.topCenter,
                            end: Alignment.center),
                      ),
                      child: Container(
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                              colors: [
                                Colors.cyanAccent.withOpacity(.2),
                                Colors.transparent,
                              ],
                              begin: Alignment.topCenter,
                              end: Alignment.center),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            ///header
                            Container(
                              width: MediaQuery.of(context).size.width,
                              padding: const EdgeInsets.only(top: 45.0, right: 30),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  IconButton(
                                      icon: Icon(Icons.arrow_back_ios_rounded, color: Colors.yellow,size: 20),
                                      onPressed: () { Get.back(); }
                                  ),

                                  Text("Select location",
                                    style: TextStyle(
                                        fontSize: 22,
                                        fontFamily: AppTheme.fontMontserrat,
                                        letterSpacing: -1, color: Colors.white,
                                        shadows: [
                                          Shadow(
                                            blurRadius: 8.0,
                                            color: Colors.black38,
                                            offset: Offset(2.0, 2.0),
                                          ),
                                        ],
                                        fontWeight: FontWeight.w500),),


                                  Spacer(),

                                  Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
//                                color: Colors.black.withOpacity(.041)
                                    ),
                                    child: IconButton(
                                        icon: Icon(Icons.info_outline, size: 22, color: Colors.white,),
                                        onPressed: () { print("Pressed"); }
                                    ),
                                  ),
                                ],
                              ),
                            ),


                            ///body
                            if (!controller.isLoading)
                            Expanded(
                              child: Container(
                                margin: EdgeInsets.symmetric(horizontal: 35, vertical: 50),
                                decoration: BoxDecoration(
                                  border: Border.all(color: Colors.cyanAccent),
                                ),
                                child: controller.listProvinceData != null && controller.listProvinceData.length > 0
                                ? ListView.builder(
                                  itemCount: controller.listProvinceData.length,
                                  scrollDirection: Axis.vertical,
                                  itemBuilder: (BuildContext context, int index) {
                                    return provinceTile(controller.listProvinceData[index], context);
                                  },
                                )
                                : Text("Chưa có tác giả nào phù hợp!\n Vui lòng quay lại sau"),
                              ),
                            )
                            else
                              Center(child: CircularProgressIndicator())
                          ],
                        ),
                      ),
                    )),

                ///bottom
                ClipRect(
                  child: BackdropFilter(
                    filter: ImageFilter.blur(
                      sigmaX: 4.0,
                      sigmaY: 4.0,
                    ),
                    child: Container(
                      width: MediaQuery.of(context).size.width,
//                  height: 170,
//                   color: Colors.black54.withOpacity(.4),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          AppTheme.GradientLine(
                              width: MediaQuery.of(context).size.width,
                            boundColor: Colors.cyanAccent.withOpacity(.6),
                            color: Colors.cyan,
                            height: 1.2
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 22.0, vertical: 20),
                            child: Text("AI guideX tour",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 22,
                                fontWeight: FontWeight.bold,
                                shadows: [
                                  Shadow(
                                    blurRadius: 8.0,
                                    color: Colors.black38,
                                    offset: Offset(2.0, 2.0),
                                  ),
                                ],
                              ),),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),

              ],
            ),
          );
        }
      ),
    );
  }

}
