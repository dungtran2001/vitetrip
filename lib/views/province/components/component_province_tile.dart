import 'dart:math';
import 'dart:ui';

import 'package:intl/intl.dart';
import 'package:mitta/model/guide_author/guide_author.dart';
import 'package:mitta/model/location/model_location.dart';
import 'package:mitta/model/location/model_province.dart';
import 'package:mitta/services/service_storage.dart';
import 'package:mitta/services/services_locator.dart';
import 'package:mitta/views/guide_content/guide_content_view.dart';
import 'package:provider/provider.dart';


import '/app_theme.dart';
import '/commons/common.dart';
import '/configs/configs.dart';
import '/controllers/controller_home.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

final homeController = Get.put(HomeController());
final storage = locator<StorageLocal>();

Widget provinceTile(ProvinceInfo data, context){
  return GestureDetector(
    onTap: (){
      homeController.changeProvince(data);
      homeController.update();
      storage.backupLocationDataToStorage(province: data);

      Get.back();
    },
    child: ClipRect(
      child: BackdropFilter(
        filter: ImageFilter.blur(
          sigmaX: 3.0,
          sigmaY: 2.0,
        ),
        child:  Container(
          margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 5),
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
          // width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            color: Colors.cyanAccent.withOpacity(.08),//Colors.transparent,
            borderRadius: BorderRadius.circular(10.0),
            border: Border.all(color: homeController.currentProvinceSelect!= null && homeController.currentProvinceSelect!.value.id == data.id
                ? Colors.cyanAccent
                : Colors.transparent)
          ),
          child: Row(
            children: [
              //icon app
              Container(
                margin: EdgeInsets.only(right: 10),
                width: 50,
                height: 50,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Image.network(
                    data.image!.first.toString(),
                    fit: BoxFit.cover,),
                ),
              ),

              //title
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(data.name.toString(),
                      style: TextStyle(
                          fontSize: 18.0,
                          fontFamily: AppTheme.fontSfPro,
                          fontWeight: FontWeight.bold,
                        color: Colors.white
                      ), maxLines: 1,),
//            Divider(height: 2,),
                    Container(child: Text(data.description.toString(),
                      style: TextStyle(color: Colors.white), maxLines: 2,)
                    ),

                    //ranking
                    Container(child: Text(data.ranking.toString() + " \u2605",
                        style: TextStyle(color: Colors.yellow))
                    ),
                  ],
                ),
              ),


              //price
                Text(data.locationIds!.length.toString() + " Location", style: TextStyle(
                  color: Colors.cyanAccent,
                  fontWeight: FontWeight.bold
                ),)
            ],
          ),
        ),
      ),
    ),
  );
}


double roundDouble(double value, int places){
  double mod = pow(10.0, places).toDouble();
  return ((value * mod).round().toDouble() / mod);
}
