import 'dart:math';

import 'package:mitta/model/guide_author/guide_author.dart';
import 'package:mitta/model/location/model_location.dart';
import 'package:mitta/views/guide_content/guide_content_view.dart';


import '/app_theme.dart';
import '/commons/common.dart';
import '/configs/configs.dart';
import '/controllers/controller_home.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';


Widget authorGuideTile(GuideAuthorInfo author, context){
  return GestureDetector(
    onTap: (){

      Get.to(()=> GuideContentView(author: author,));
    },
    child: Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      padding: const EdgeInsets.symmetric(horizontal: 15),
      // width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        color: Colors.transparent,
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Row(
        children: [
          //icon app
          Container(
            margin: EdgeInsets.only(right: 10),
            width: 50,
            height: 50,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Image.network(
                author.image.toString(),
                fit: BoxFit.cover,),
            ),
          ),

          //title
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(author.name.toString(),
                  style: TextStyle(
                      fontSize: 18.0,
                      fontFamily: AppTheme.fontSfPro,
                      fontWeight: FontWeight.bold,
                    color: Colors.white
                  ), maxLines: 1,),
//            Divider(height: 2,),
                Container(child: Text(author.description.toString(),
                  style: TextStyle(color: Colors.white), maxLines: 2,)
                ),

                //ranking
                Container(child: Text(author.ranking.toString(),
                    style: TextStyle(color: Colors.yellow))
                ),
              ],
            ),
          ),


          //price
            Text(author.price == 0.0 ? "FREE" : author.price.toString() + " Mitcoin", style: TextStyle(
              color: Colors.cyanAccent,
              fontWeight: FontWeight.bold
            ),)
        ],
      ),
    ),
  );
}


double roundDouble(double value, int places){
  double mod = pow(10.0, places).toDouble();
  return ((value * mod).round().toDouble() / mod);
}
