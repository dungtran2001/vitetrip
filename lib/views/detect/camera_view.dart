import 'dart:io';
import 'dart:isolate';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import '../../model/location/model_location.dart';
import '/services/tflite/classifier.dart';
import '/services/tflite/recognition.dart';
import '/services/tflite/stats.dart';
import '/utils/isolate_utils.dart';

import 'camera_view_singleton.dart';

typedef ChangeStateDetectDef(bool state);
typedef Future<XFile?> TakePhotoDef();

class CameraViewController {
  ChangeStateDetectDef? changeStateDetect;
  TakePhotoDef? takePhoto;
  void dispose() {
    changeStateDetect = null;
    takePhoto = null;
  }
}
/// [CameraView] sends each frame for inference
class CameraView extends StatefulWidget {
  /// Callback to pass results after inference to [HomeView]
  final Function(List<Recognition> recognitions) resultsCallback;

  /// Callback to inference stats to [HomeView]
  final Function(Stats stats) statsCallback;

  final CameraViewController? controller; //map controller

  final LocationInfo locationModel;
  /// Constructor
  const CameraView({required this.resultsCallback, required this.statsCallback, required this.locationModel, this.controller});
  @override
  _CameraViewState createState() => _CameraViewState();
}

class _CameraViewState extends State<CameraView> with WidgetsBindingObserver {
  /// List of available cameras
  late List<CameraDescription> cameras;

  /// Controller
  CameraController? cameraController;

  /// true when inference is ongoing
  late bool predicting;

  /// Instance of [Classifier]
  late Classifier classifier;

  /// Instance of [IsolateUtils]
  late IsolateUtils isolateUtils;

  @override
  void initState() {
    super.initState();
    CameraViewController _controller = widget.controller!;
    if (_controller != null) {
      _controller.changeStateDetect = changeStateDetectFunc;
      _controller.takePhoto = takePhoto;
    }
    initStateAsync();
  }

  void initStateAsync() async {
    WidgetsBinding.instance!.addObserver(this);

    // Spawn a new isolate
    isolateUtils = IsolateUtils();
    // isolateUtils.setFileName("tflite/dinhdoclap/model.tflite", "tflite/dinhdoclap/labels.txt");
    await isolateUtils.start();

    // Camera initialization
    initializeCamera();

    // Create an instance of classifier to load model and labels
    // classifier = Classifier(MODEL_FILE_NAME: widget.locationModel.modelName.toString(), LABEL_FILE_NAME: widget.locationModel.labelsName.toString());
    print("Classification: model ${widget.locationModel.modelName}, labels: ${widget.locationModel.labelsName}");
    classifier = Classifier(
        MODEL_FILE_NAME: widget.locationModel.modelName ?? "tflite/dinhdoclap/model.tflite",
        LABEL_FILE_NAME: widget.locationModel.labelsName ?? "tflite/dinhdoclap/labels.txt"
    );

    // Initially predicting = false
    predicting = false;
  }

  /// Initializes the camera by setting [cameraController]
  void initializeCamera() async {
    cameras = await availableCameras();

    // cameras[0] for rear-camera
    cameraController =
        CameraController(cameras[0], ResolutionPreset.high, enableAudio: false);

    cameraController!.initialize().then((_) async {
      try {
        await cameraController?.lockCaptureOrientation(); //fix bug crash when take photo to feedback
      }
      catch(e){}
      // Stream of image passed to [onLatestImageAvailable] callback
      await cameraController!.startImageStream(onLatestImageAvailable);
      /// previewSize is size of each image frame captured by controller
      ///
      /// 352x288 on iOS, 240p (320x240) on Android with ResolutionPreset.low
      Size previewSize = cameraController!.value.previewSize!;

      /// previewSize is size of raw input image to the model
      CameraViewSingleton.inputImageSize = previewSize;

      // the display width of image on screen is
      // same as screenWidth while maintaining the aspectRatio
      Size screenSize = MediaQuery.of(context).size;
      CameraViewSingleton.screenSize = screenSize;
      CameraViewSingleton.ratio = screenSize.width / previewSize.height;
      setState(() {

      });
    });

  }

  Future<XFile?> takePhoto() async {
    try {
      if(cameraController != null){
        if(cameraController!.value.isInitialized){
          await cameraController!.stopImageStream();
          await Future<dynamic>.delayed(const Duration(milliseconds: 100));

          var _image = await cameraController!.takePicture(); //capture image
          // await cameraController!.resumePreview();

          // await Future<dynamic>.delayed(const Duration(milliseconds: 3000));
          await cameraController!.startImageStream(onLatestImageAvailable);

          return _image;
        }
      }
      return null;
    } catch (e) {
      print("--ERROR take a photo: $e"); //show error
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    // Return empty container while the camera is not initialized
    if (cameraController == null || !cameraController!.value.isInitialized) {
      return Center(child: CircularProgressIndicator());
    }

    return CameraPreview(cameraController!);
  }

  bool isEnableDetecting = false;
  void changeStateDetectFunc(value){
    isEnableDetecting = value;
  }

  /// Callback to receive each frame [CameraImage] perform inference on it
  onLatestImageAvailable(CameraImage cameraImage) async {
    if (isEnableDetecting && classifier.interpreter != null && classifier.labels != null) {
      print("scanning...");
      // If previous inference has not completed then return
      if (predicting) {
        return;
      }

      // setState(() {
        predicting = true;
      // });

      // var uiThreadTimeStart = DateTime.now().millisecondsSinceEpoch; // rem to optimize

      // Data to be passed to inference isolate
      var isolateData = IsolateData(
          cameraImage, classifier.interpreter!.address, classifier.labels);

      // We could have simply used the compute method as well however
      // it would be as in-efficient as we need to continuously passing data
      // to another isolate.

      /// perform inference in separate isolate
      Map<String, dynamic> inferenceResults = await inference(isolateData);

      // var uiThreadInferenceElapsedTime =
      //     DateTime.now().millisecondsSinceEpoch - uiThreadTimeStart; // rem to optimize

      // pass results to HomeView
      widget.resultsCallback(inferenceResults["recognitions"]);

      // pass stats to HomeView
      // widget.statsCallback((inferenceResults["stats"] as Stats)
      //   ..totalElapsedTime = uiThreadInferenceElapsedTime); // rem to optimize

      // set predicting to false to allow new frames
      // setState(() {
        predicting = false;
      // });
    }
  }

  /// Runs inference in another isolate
  Future<Map<String, dynamic>> inference(IsolateData isolateData) async {
    ReceivePort responsePort = ReceivePort();
    isolateUtils.sendPort
        .send(isolateData..responsePort = responsePort.sendPort);
    var results = await responsePort.first;
    return results;
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    switch (state) {
      case AppLifecycleState.paused:
        cameraController!.stopImageStream();
        break;
      case AppLifecycleState.resumed:
        if (!cameraController!.value.isStreamingImages) {
          await cameraController!.startImageStream(onLatestImageAvailable);
        }
        break;
      default:
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance!.removeObserver(this);
    cameraController!.dispose();
    super.dispose();
  }
}
