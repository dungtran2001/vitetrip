import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mitta/commons/button/button_back.dart';
import 'package:mitta/commons/common.dart';
import 'package:mitta/commons/components/audio/audio_player_widget.dart';
import 'package:mitta/commons/components/audio/progress_notifier.dart';
import 'package:mitta/configs/configs.dart';
import 'package:mitta/model/audio/resource_model.dart';
import 'package:mitta/model/location/model_location.dart';
import 'package:mitta/services/audio_player/audio_manager.dart';
import 'package:mitta/utils/ads_utils.dart';
import 'package:mitta/views/detect/components/detect_view_component_tile.dart';
import '../../commons/components/audio/play_button_notifier.dart';
import 'package:mitta/model/object/model_object.dart';
import 'package:mitta/network/api/api.dart';
import 'package:mitta/services/services_locator.dart';

import '../../app_theme.dart';
import '../../controllers/controller_detect_object.dart';
import '../../commons/button/play_button.dart';
import 'components/detect_view_component_dialog.dart';
import 'detect_view.dart';

/// hien thi cac object preview truoc khi scan
class DetectViewInfo extends StatefulWidget {
  final LocationInfo model;

  DetectViewInfo({required this.model});

  @override
  _DetectViewInfoState createState() => _DetectViewInfoState();
}

class _DetectViewInfoState extends State<DetectViewInfo> {
  final DetectObjectController controller = Get.put(DetectObjectController());

  final mediaManager = locator<MediaManager>();
  final adsUtils = locator<AdsUtils>();

  @override
  void initState() {
    // Check and show Ads
    if (adsUtils.isValidToShowAd()) {
      adsUtils.showAds(null, null);
    } else {
      adsUtils.increaseCounter();
    }

    //precache image
    if (widget.model.slider != null && widget.model.slider!.length > 0) {
      for (var e in widget.model.slider!) {
        e.preImage = new Image.network(
          e.image.toString().contains("http")
              ? e.image.toString()
              : Configs.hostName + "/" + e.image.toString(),
          fit: BoxFit.contain,
        );
      }
    }
    controller.getObjectData(locationId: widget.model.id).then((data) {
      if (storage.checkUserAlreadyLogin()) {
        controller
            .getArchivermentData(locationId: widget.model.id)
            .then((value) {
          //sort // if object exist in archiverment list and object list

          checkArchiverment();
        });
      }
    });

    WidgetsBinding.instance.addPostFrameCallback((_) {
      // print("-- add resource" + widget.model.audio.toString());
      mediaManager.loadPlaylist([
        ResourceModel(
            id: widget.model.id.toString(),
            title: widget.model.name,
            url: widget.model.audio.toString().contains("http")
                ? widget.model.audio.toString()
                : Configs.hostName + "/" + widget.model.audio.toString(),
            //"https://samplelib.com/lib/preview/mp3/sample-15s.mp3",
            // url: "https://samplelib.com/lib/preview/mp3/sample-15s.mp3", //"https://file-examples.com/storage/fea8fc38fd63bc5c39cf20b/2017/11/file_example_MP3_5MG.mp3",
            time: widget.model.audioDuration ?? "00:00:00")
      ]);
    });

    //https://file-examples.com/storage/fea8fc38fd63bc5c39cf20b/2017/11/file_example_MP3_2MG.mp3
    super.initState();
  }

  void checkArchiverment() {
    if (controller.archivermentData != null &&
        controller.archivermentData.length > 0) {
      for (var ob in controller.objectData) {
        try {
          if (controller.archivermentData.first.objectCollectedIds != null &&
              controller.archivermentData.first.objectCollectedIds!.length >
                  0) {
            var _exist = controller.archivermentData.first.objectCollectedIds!
                .firstWhere((element) => element == ob.id);
            if (_exist != null) ob.isVisited = true;
          }
        } catch (e) {}
      }
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (widget.model.slider != null && widget.model.slider!.length > 0) {
      for (var e in widget.model.slider!) {
        //precache image next
        precacheImage(e.preImage.image, context);
      }
      if (widget.model.slider != null && widget.model.slider!.length > 1) {
        _currentImageShowDuration =
            Common.durationParse(widget.model.slider![1].at);
      }

      // mediaManager.play();

    }
    if (!controller.isAlreadyAutoPlay.value) {
      // only auto play once
      // Future.delayed(const Duration(milliseconds: 1000), () async {
      //   mediaManager.play();
      // });
      // controller.isAlreadyAutoPlay.value = true;
    }

    controller.getArchivermentData(locationId: widget.model.id).then((value) {
      //sort // if object exist in archiverment list and object list
      checkArchiverment();
    });
  }

  @override
  void dispose() {
    mediaManager.stop();
    mediaManager.removePlaylist();
    super.dispose();
  }

  final api = locator<Api>();

  List<ObjectInfo> objectDetected = [];
  var _currentImageShowIndex = 0;
  var _currentImageShowDuration =
      new Duration(hours: 0, minutes: 0, seconds: 0);

  void resetSlider() {
    _currentImageShowIndex = 0;
    if (widget.model.slider != null && widget.model.slider!.length > 1) {
      _currentImageShowDuration =
          Common.durationParse(widget.model.slider![1].at);
    }
  }

  @override
  Widget build(BuildContext context) {
    var _screenSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Colors.grey,
      body: GetBuilder<DetectObjectController>(builder: (controller) {
        return SafeArea(
          child: Container(
            decoration: BoxDecoration(
                // image: new DecorationImage(
                //   image: NetworkImage(
                //     widget.model.images!.first.toString().contains("http")
                //         ? widget.model.images!.first
                //         : Configs.hostName + "/" + widget.model.images!.first
                //         ?? "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQiWwAFRoutuyPbPp4FzSimgdw6lvQxMqqU9A&usqp=CAU",
                //   ),
                //   fit: BoxFit.cover,
                //   colorFilter: new ColorFilter.mode(Colors.orange.withOpacity(0.2), BlendMode.overlay),
                // ),
                ),
            child: (widget.model != null)
                ? Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      ///header
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        height: _screenSize.height / 8 - 10,
                        color: Colors.white,
                        alignment: Alignment.center,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            ButtonBack.backButtonSimple(onTap: () {
                              if (storage.checkUserAlreadyLogin() &&
                                  controller.archivermentData.length <
                                      controller.objectData.length) {
                                //check backable

                                var notScanNum = controller.objectData.length -
                                    controller.archivermentData.length;
                                confirmExitDialog(context, notScanNum)
                                    .then((value) {
                                  if (value)
                                    Get.back(); //Get.offAll(()=> HomeView());
                                });
                              } else {
                                //show dialog rating
                                confirmRatingDialog(context, widget.model)
                                    .then((value) {
                                  if (value == -1) {
                                    //not vote
                                    Get.back(); //Get.offAll(()=> HomeView());
                                  } else {
                                    // call api to set rating
                                    controller.postRatingData(
                                        locationId: widget.model, star: value);
                                    Get.back(); //Get.offAll(()=> HomeView());
                                  }
                                });
                              }
                            }),
                            //title
                            RichText(
                                textAlign: TextAlign.center,
                                text: TextSpan(
                                    text: widget.model.name.toString()[0],
                                    style: AppTheme.titleAppBar
                                        .copyWith(color: AppTheme.orange),
                                    children: [
                                      TextSpan(
                                          text: widget.model.name
                                              .toString()
                                              .substring(1),
                                          style: AppTheme.titleAppBar),
                                    ])),

                            // SizedBox(
                            //   height: 30,
                            //   width: 30,
                            //   child: Image.asset("assets/images/icon_diamon.png"),
                            // )
                          ],
                        ),
                      ),

                      ///listen to reset slider until finish
                      ValueListenableBuilder<ButtonState>(
                        valueListenable: mediaManager.playButtonNotifier,
                        builder: (_, value, __) {
                          switch (value) {
                            case ButtonState.loading:
                              break;
                            case ButtonState.paused:
                              if (_currentImageShowIndex ==
                                  widget.model.slider!.length - 1)
                                resetSlider();
                              // else if (_currentImageShowIndex == 0)
                              //   mediaManager.play();
                              break;
                            case ButtonState.playing:
                              break;
                          }
                          return SizedBox();
                        },
                      ),

                      ///main contents
                      Expanded(
                        child: ValueListenableBuilder<ProgressBarState>(
                          valueListenable: mediaManager.progressNotifier,
                          builder: (_, value, __) {
                            // var fastestMarathon = new Duration(hours: 0, minutes: 0, seconds: 2);
                            // print(value.current);

                            if (value != null && value.current != null) {
                              if (value.current.inSeconds ==
                                      _currentImageShowDuration.inSeconds &&
                                  value.current.inMinutes ==
                                      _currentImageShowDuration.inMinutes &&
                                  value.current.inHours ==
                                      _currentImageShowDuration.inHours) {
                                if (_currentImageShowIndex <
                                    widget.model.slider!.length - 1) {
                                  //safe
                                  _currentImageShowIndex =
                                      _currentImageShowIndex + 1;
                                  if (_currentImageShowIndex !=
                                      widget.model.slider!.length - 1) {
                                    _currentImageShowDuration =
                                        Common.durationParse(widget
                                            .model
                                            .slider![_currentImageShowIndex + 1]
                                            .at);
                                  }
                                  // print(_currentImageShowDuration.toString());
                                }
                              }
                            }
                            return Container(
                              child: GestureDetector(
                                onTap: () {
                                  controller.isShowAudioPlayer.value =
                                      !controller.isShowAudioPlayer.value;
                                  controller.update();
                                },
                                child: Stack(
                                  alignment: Alignment.center,
                                  children: [

                                    /// SLIDER IMAGE WITH AUDIO
                                    if (widget.model.slider != null &&
                                        widget.model.slider!.length > 0)
                                      SizedBox(
                                        height: _screenSize.height,
                                        width: _screenSize.width,
                                        child: widget
                                            .model
                                            .slider![_currentImageShowIndex]
                                            .preImage,
                                      )
                                    else
                                      SizedBox(
                                        height: _screenSize.height,
                                        width: _screenSize.width,
                                        child: Image.network(
                                            widget.model.images!.first
                                                    .toString()
                                                    .contains("http")
                                                ? widget.model.images!.first
                                                    .toString()
                                                : Configs.hostName +
                                                    "/" +
                                                    widget.model.images!.first
                                                        .toString(),
                                            fit: BoxFit.cover),
                                      ),

                                    ///bottom
                                    if (!controller.isShowObjectSheet.value)
                                      Positioned(
                                        bottom: 0,
                                        child: GestureDetector(
                                          // onTap: (){
                                          //   //show bottom sheet
                                          //   showObjectDetected(_screenSize);
                                          //
                                          //   //hide button
                                          //   setState(() {
                                          //     isShowObjectSheet = true;
                                          //   });
                                          // },
                                          onVerticalDragDown: (_) {
                                            //show bottom sheet
                                            showObjectDetected(_screenSize);

                                            //hide button
                                            controller.isShowObjectSheet.value =
                                                true;
                                          },
                                          child: Container(
                                            height: 60,
                                            width: _screenSize.width,
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.only(
                                                  topLeft: Radius.circular(25),
                                                  topRight: Radius.circular(25),
                                                ),
                                                color: Colors.black26),
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              children: [
                                                Container(
                                                  height: 3,
                                                  width: _screenSize.width / 5,
                                                  decoration: BoxDecoration(
                                                    color: Colors.grey.shade400,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            1),
                                                    boxShadow: <BoxShadow>[
                                                      BoxShadow(
                                                          color: Colors.grey,
                                                          offset: const Offset(
                                                              .5, 1.1),
                                                          blurRadius: 1.0),
                                                    ],
                                                  ),
                                                ),

                                                //check: if already login -> show object detected, else show all objects
                                                if (storage
                                                    .checkUserAlreadyLogin())
                                                  Text(
                                                    "${controller.archivermentData.length}/${controller.objectData.length} " +
                                                        "object_detected".tr,
                                                    style: AppTheme
                                                        .subtitleLightMode
                                                        .copyWith(
                                                            color:
                                                                Colors.white70,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontStyle: FontStyle
                                                                .italic),
                                                  )
                                                else
                                                  Text(
                                                    "${controller.objectData.length} objects to detect",
                                                    style: AppTheme
                                                        .subtitleLightMode
                                                        .copyWith(
                                                            color:
                                                                Colors.white70,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontStyle: FontStyle
                                                                .italic),
                                                  )
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),

                                    if (controller.isShowAudioPlayer.value)
                                      Positioned(
                                          bottom: 0,
                                          child: AudioPlayerWidget()),

                                    ///button scan
                                    Obx(() => !controller
                                            .isShowObjectSheet.value
                                        ? Positioned(
                                            bottom: 120,
                                            child: MaterialButton(
                                              shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(20.0),
                                              ),
                                              color: AppTheme.orange,
                                              elevation: 2,
                                              height: 55,
                                              onPressed: () {
                                                if (controller
                                                    .objectData.isEmpty) {
                                                  return showNoObjectDialog();
                                                }
                                                mediaManager.stop();
                                                Get.to(
                                                    () => DetectView(
                                                          model: widget.model,
                                                        ),
                                                    routeName: "DetectView");
                                              },
                                              child: Text(
                                                "scan_object".tr,
                                                style: AppTheme.titleAppBar
                                                    .copyWith(
                                                        color: Colors.white,
                                                        fontWeight:
                                                            FontWeight.w400),
                                              ),
                                            ),
                                          )
                                        : SizedBox()),
                                  ],
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  )
                : CircularProgressIndicator(),
          ),
        );
      }),
    );
  }

  void showObjectDetected(_screenSize) {
    Get.bottomSheet(
      GetBuilder<DetectObjectController>(builder: (controller) {
        return GestureDetector(
          onTap: () {
            // remove objected selected
            controller.currentObjectSelected = null;
            controller.update();
          },
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25),
                  topRight: Radius.circular(25),
                ),
                color: Colors.black54),
            padding: const EdgeInsets.symmetric(vertical: 18.0),
            margin: EdgeInsets.all(0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Container(
                    height: 3,
                    width: _screenSize.width / 5,
                    decoration: BoxDecoration(
                      color: Colors.grey.shade400,
                      borderRadius: BorderRadius.circular(1),
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                            color: Colors.grey,
                            offset: const Offset(.5, 1.1),
                            blurRadius: 1.0),
                      ],
                    ),
                  ),
                ),
                Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 15.0, vertical: 10),
                    child: Row(
                      children: [
                        if (controller.currentObjectSelected != null)
                          Padding(
                              padding: const EdgeInsets.fromLTRB(0, 0, 10.0, 0),
                              child: PlayButton(onPressed: () {
                                controller.navigateToScanResult(
                                    controller.currentObjectSelected!.value,
                                    widget.model,
                                    null);
                              })),
                        Expanded(
                            child: Wrap(
                          direction: Axis.vertical,
                          children: [
                            Text(
                              controller.currentObjectSelected?.value.name ??
                                  widget.model.name.toString(),
                              style: AppTheme.title,
                            ),
                            Text(
                              controller.currentObjectSelected?.value
                                      .description ??
                                  widget.model.description.toString(),
                              style: AppTheme.subtitle,
                            ),
                          ],
                        ))
                      ],
                    )),
                if (controller.objectData != null &&
                    controller.objectData.length > 0)
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: [
                        for (var object in controller.objectData)
                          Container(
                            margin: EdgeInsets.symmetric(
                                vertical: 12, horizontal: 5),
                            height: 80,
                            width: 80,
                            decoration: BoxDecoration(
                              color: Colors.grey,
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(
                                  color: controller.currentObjectSelected !=
                                              null &&
                                          controller.currentObjectSelected!
                                                  .value.id ==
                                              object.id
                                      ? AppTheme.orange
                                      : Colors.transparent,
                                  width: 2.5),

                              // gradient: LinearGradient(
                              //     colors: [
                              //       Color(0xFF02BCFF),
                              //       AppTheme.mainColorLeft, //HexColor('#4E61FE'),
                              //     ],
                              //     begin: Alignment.bottomLeft,
                              //     end: Alignment.topRight),
                              boxShadow: <BoxShadow>[
                                BoxShadow(
                                    color: Colors.black,
                                    offset: const Offset(1.1, 1.5),
                                    blurRadius: 10.0),
                              ],
                            ),
                            child: Stack(
                              children: [
                                GestureDetector(
                                  onTap: () {
                                    //show current object selected
                                    controller.currentObjectSelected =
                                        object.obs;
                                    controller.update();
                                  },
                                  child: SizedBox(
                                    height: 100,
                                    width: 100,
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(10),
                                      child: Image.network(
                                        object.image.toString().contains("http")
                                            ? object.image.toString()
                                            : Configs.hostName +
                                                "/" +
                                                object.image.toString(),
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                ),
                                if (object.isVisited)
                                  Positioned(
                                      bottom: 8,
                                      left: 8,
                                      child: Icon(
                                        Icons.check_circle,
                                        color: Colors.blue,
                                      ))
                              ],
                            ),
                          ),
                      ],
                    ),
                  ),
                SizedBox(
                  height: 8,
                ),
              ],
            ),
          ),
        );
      }),

      // backgroundColor: Colors.white,
      // shape: RoundedRectangleBorder(
      //     borderRadius: BorderRadius.circular(20),
      //
      // ),
      barrierColor: Colors.transparent,
    ).then((value) {
      controller.isShowObjectSheet.value = false;
    });
  }

  void showNoObjectDialog() {
    showDialog(
        barrierDismissible: true,
        context: context,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            //backgroundColor: AppColors.black.withOpacity(0.89),
            content: Builder(builder: (context) {
              return Container(
                color: Colors.white,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.warning,
                          color: Colors.redAccent,
                        ),
                        Text(
                          'artifact_not_found'.tr,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 18),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'places_no_artifact'.tr,
                      style: TextStyle(
                          fontWeight: FontWeight.normal, fontSize: 15),
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              );
            }),
          );
        });
  }
}
