

import 'dart:io';

import 'package:camera/camera.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mitta/model/location/model_location.dart';
import 'package:mitta/model/object/model_object.dart';

import '../../../app_theme.dart';
import '../../../configs/configs.dart';
import '../../login/login_screen.dart';
import '../camera_view.dart';
import 'detect_view_component_tile.dart';

//dialog chon object de nghe audio
Future<ObjectInfo?> chooseObjectDialog(BuildContext context, List<ObjectInfo> objectList, LocationInfo location) async {

  var _objectSelected = null;
  List<Widget> pages = [];
  for (var i=0; i < (objectList.length / 2).ceil(); i++){
    pages.add(
        Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            objectContentTile(context, objectList[i], location),
            objectContentTile(context, objectList[i + 1], location),
          ],
        )
    );
  }

  await showDialog(
    context: context,
    barrierColor: Colors.black.withOpacity(.01),
    builder: (context) => new AlertDialog(
      backgroundColor: Colors.transparent,
      contentPadding: EdgeInsets.all(0),
      insetPadding:  EdgeInsets.all(0),
      content: StatefulBuilder(
        builder: (BuildContext context, StateSetter setState) {
          return GestureDetector(
            onTap: (){
              Navigator.of(context).pop(true);

              print("tap exit dialog");
            },
            child: Container(
              height: MediaQuery.of(context).size.height * 5/6,
              width: MediaQuery.of(context).size.width,
              color: Colors.transparent,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.end,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Expanded(
                      flex: 3,
                      child: Column(
                        children: [
                          //holder
                          SizedBox(height: 50,),

                          //main content object
                          GestureDetector(
                            onTap: (){},

                            child: Container(
                              decoration: BoxDecoration(
                                color: Color(0xFF424856),
                                borderRadius: BorderRadius.circular(18.0),
                              ),
                              width: MediaQuery.of(context).size.width,
                              child: Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: SingleChildScrollView(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.symmetric(vertical: 25.0),
                                        child: Text('choose_object'.tr, style: AppTheme.subtitleBold,),
                                      ),

                                      CarouselSlider(
                                          options: CarouselOptions(
                                            height: MediaQuery.of(context).size.height /3,
                                            viewportFraction: 1,
                                            initialPage: 0,
                                            enableInfiniteScroll: true,
                                            reverse: false,
                                            autoPlay: false,
                                            autoPlayInterval: Duration(seconds: 10),
                                            autoPlayAnimationDuration: Duration(milliseconds: 1000),
                                            autoPlayCurve: Curves.fastOutSlowIn,
                                            enlargeCenterPage: true,
                                            scrollDirection: Axis.horizontal,
                                          ),
                                          items: pages
                                      ),

                                      //page control
                                      Padding(
                                        padding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 20),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            IconButton(
                                                onPressed: (){},
                                                icon: Icon(Icons.arrow_back, color: Colors.white,)),
                                            IconButton(
                                                onPressed: (){},
                                                icon: Icon(Icons.arrow_forward, color: Colors.white))
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                  )),

                  Expanded(
                    flex: 1,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20, right: 20, bottom: 10),
                      child: Center(child: GestureDetector(
                        onTap: (){},
                          child: feedbackTile(context, location))),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    ),
  );
  return _objectSelected;
}

//dialog xac nhan exit detect
Future<bool> confirmExitDialog(BuildContext context, int num) async {

  var _isConfirm = false;

  await showDialog(
    context: context,
    // barrierColor: Colors.black.withOpacity(.71),
    builder: (context) => new AlertDialog(
      backgroundColor: Colors.transparent,
      contentPadding: EdgeInsets.all(0),
      insetPadding:  EdgeInsets.all(20),
      content: StatefulBuilder(
        builder: (BuildContext context, StateSetter setState) {
          return Container(
            decoration: BoxDecoration(
               color: Colors.white,
              borderRadius: BorderRadius.circular(18.0),
            ),
            width: MediaQuery.of(context).size.width,
            child: Padding(
              padding: const EdgeInsets.all(25.0),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(
                              text: "question_back_scan1".tr,
                              style: AppTheme.subtitleBoldLightMode,
                              children: [
                                TextSpan(
                                    text: num.toString(),
                                    style: AppTheme.subtitleBoldLightMode.copyWith(color: AppTheme.orange)),
                                TextSpan(
                                    text: "question_back_scan2".tr,
                                    style: AppTheme.subtitleBoldLightMode.copyWith(color: AppTheme.orange)),
                              ])),
                    ),

                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10.0),
                      child: Text("question_back_suggestion".tr,
                        style: AppTheme.subtitleBoldLightMode,),
                    ),

                    //page control
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          MaterialButton(
                            padding: EdgeInsets.symmetric(horizontal: 10),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(25.0),
                            ),
                            color: Color(0xFFB5B8C0),
                            elevation: 2,
                            height: 50,
                            onPressed: (){
                              _isConfirm = true;
                              Navigator.of(context).pop(true);
                            },
                            child: Text(" "+ "no_thanks".tr +" ",
                              style: AppTheme.title.copyWith(color: Colors.white, fontWeight: FontWeight.w400),),),

                          MaterialButton(
                            padding: EdgeInsets.symmetric(horizontal: 12),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(25.0),
                            ),
                            color: AppTheme.orange,
                            elevation: 2,
                            height: 50,
                            onPressed: (){
                              _isConfirm = false;
                              Navigator.of(context).pop(true);
                            },
                            child: Text(" " + 'scan_next'.tr + " ",
                              style: AppTheme.title.copyWith(color: Colors.white, fontWeight: FontWeight.w400),),),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    ),
  );
  return _isConfirm;
}

//dialog yeu cau rating
Future<int> confirmRatingDialog(BuildContext context, LocationInfo location) async {

  var _amountStar = 5;
  var _isChooseStar = [true,true,true,true,true,];

  await showDialog(
    context: context,
    // barrierColor: Colors.black.withOpacity(.71),
    builder: (context) => new AlertDialog(
      backgroundColor: Colors.transparent,
      contentPadding: EdgeInsets.all(0),
      insetPadding:  EdgeInsets.all(20),
      content: StatefulBuilder(
        builder: (BuildContext context, StateSetter setState) {
          return Container(
            decoration: BoxDecoration(
               color: Colors.white,
              borderRadius: BorderRadius.circular(18.0),
            ),
            width: MediaQuery.of(context).size.width,
            child: Padding(
              padding: const EdgeInsets.all(25.0),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(
                              text: "question_rating1".tr,
                              style: AppTheme.subtitleBoldLightMode,
                              children: [
                                TextSpan(
                                    text: "question_rating2".tr,
                                    style: AppTheme.subtitleBoldLightMode.copyWith(color: AppTheme.orange)),
                                TextSpan(
                                    text: "question_rating3".tr,
                                    style: AppTheme.subtitleBoldLightMode),
                              ])),
                    ),

                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10.0),
                      child:
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children:[
                            GestureDetector(
                                child: Icon(Icons.star, size:40,
                                  color: _isChooseStar[0]
                                      ? AppTheme.orange
                                      : Colors.grey.shade300,),
                              onTap: (){
                                  //set rating 1 *
                                setState((){
                                  for (var i = 0; i < _isChooseStar.length; i++){
                                    if (i > 0){
                                      _isChooseStar[i] = false;
                                    }
                                  }
                                });
                                _amountStar = 1;
                              },
                            ),
                            GestureDetector(
                                child: Icon(Icons.star, size:40,
                                  color: _isChooseStar[1]
                                      ? AppTheme.orange
                                      : Colors.grey.shade300,),
                              onTap: (){
                                setState((){
                                  for (var i = 0; i < _isChooseStar.length; i++){
                                    if (i > 1){
                                      _isChooseStar[i] = false;
                                    } else {
                                      _isChooseStar[i] = true;
                                    }
                                  }
                                });
                                _amountStar = 2;
                              },
                            ),
                            GestureDetector(
                                child: Icon(Icons.star, size:40,
                                  color: _isChooseStar[2]
                                      ? AppTheme.orange
                                      : Colors.grey.shade300,),
                              onTap: (){
                                setState((){
                                  for (var i = 0; i < _isChooseStar.length; i++){
                                    if (i > 2){
                                      _isChooseStar[i] = false;
                                    } else {
                                      _isChooseStar[i] = true;
                                    }
                                  }
                                });
                                _amountStar = 3;
                              },
                            ),
                            GestureDetector(
                                child: Icon(Icons.star, size:40,
                                  color: _isChooseStar[3]
                                      ? AppTheme.orange
                                      : Colors.grey.shade300,),
                              onTap: (){
                                setState((){
                                  for (var i = 0; i < _isChooseStar.length; i++){
                                    if (i > 3){
                                      _isChooseStar[i] = false;
                                    }else {
                                      _isChooseStar[i] = true;
                                    }
                                  }
                                });
                                _amountStar = 4;
                              },
                            ),
                            GestureDetector(
                                child: Icon(Icons.star, size:40,
                                  color: _isChooseStar[4]
                                      ? AppTheme.orange
                                      : Colors.grey.shade300,),
                              onTap: (){
                                setState((){
                                  for (var i = 0; i < _isChooseStar.length; i++){
                                      _isChooseStar[i] = true;
                                  }
                                });
                                _amountStar = 5;
                              },
                            ),
                          ]
                        )
                    ),

                    //page control
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          MaterialButton(
                            padding: EdgeInsets.symmetric(horizontal: 10),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(25.0),
                            ),
                            color: Color(0xFFB5B8C0),
                            elevation: 2,
                            height: 50,
                            onPressed: (){
                              _amountStar = -1; // not vote
                              Navigator.of(context).pop(true);
                            },
                            child: Text(" "+ "no_thanks".tr +" ",
                              style: AppTheme.title.copyWith(color: Colors.white, fontWeight: FontWeight.w400),),),

                          MaterialButton(
                            padding: EdgeInsets.symmetric(horizontal: 12),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(25.0),
                            ),
                            color: AppTheme.orange,
                            elevation: 2,
                            height: 50,
                            onPressed: (){
                              Navigator.of(context).pop(true);
                            },
                            child: Text("send".tr,
                              style: AppTheme.title.copyWith(color: Colors.white, fontWeight: FontWeight.w400),),),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    ),
  );
  return _amountStar;
}

//dialog thanks feedback
Future<bool> thanksFeedbackDialog(BuildContext context) async {


  await showDialog(
    context: context,
    // barrierColor: Colors.black.withOpacity(.71),
    builder: (context) => new AlertDialog(
      backgroundColor: Colors.transparent,
      contentPadding: EdgeInsets.all(0),
      insetPadding:  EdgeInsets.all(20),
      content: StatefulBuilder(
        builder: (BuildContext context, StateSetter setState) {
          return Container(
            decoration: BoxDecoration(
               color: Colors.white,
              borderRadius: BorderRadius.circular(18.0),
            ),
            width: MediaQuery.of(context).size.width,
            child: Padding(
              padding: const EdgeInsets.all(25.0),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(
                              text: "report_thanks_title".tr,
                              style: AppTheme.titleLightMode,
                              children: [
                              ])),
                    ),

                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10.0),
                      child: Text('report_thanks_content'.tr,
                        style: AppTheme.subtitleLightMode,),
                    ),

                  ],
                ),
              ),
            ),
          );
        },
      ),
    ),
  );
  return true;
}

//dialog thanks report object
Future<bool> thanksReportObjectDialog(BuildContext context) async {


  await showDialog(
    context: context,
    // barrierColor: Colors.black.withOpacity(.71),
    builder: (context) => new AlertDialog(
      backgroundColor: Colors.transparent,
      contentPadding: EdgeInsets.all(0),
      insetPadding:  EdgeInsets.all(20),
      content: StatefulBuilder(
        builder: (BuildContext context, StateSetter setState) {
          return Container(
            decoration: BoxDecoration(
               color: Colors.white,
              borderRadius: BorderRadius.circular(18.0),
            ),
            width: MediaQuery.of(context).size.width,
            child: Padding(
              padding: const EdgeInsets.all(25.0),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(
                              text: "report_thanks_title".tr,
                              style: AppTheme.titleLightMode,
                              children: [
                              ])),
                    ),

                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10.0),
                      child: Text('request_thanks_content'.tr,
                        style: AppTheme.subtitleLightMode,),
                    ),

                  ],
                ),
              ),
            ),
          );
        },
      ),
    ),
  ).then((value) => Get.back());
  return true;
}

//dialog send image feedback
Future<bool?> captureQuestionDialog(BuildContext context, LocationInfo location, CameraViewController cameraViewController) async {

  bool _isConfirm = false;

  await showDialog(
    context: context,
    barrierColor: Colors.black.withOpacity(.01),
    builder: (context) => new AlertDialog(
      backgroundColor: Colors.transparent,
      contentPadding: EdgeInsets.all(0),
      insetPadding:  EdgeInsets.all(0),
      content: StatefulBuilder(
        builder: (BuildContext context, StateSetter setState) {
          return GestureDetector(
            onTap: (){
              Navigator.of(context).pop(true);

              print("tap exit dialog");
            },
            child: Container(
              height: MediaQuery.of(context).size.height * 5/6,
              width: MediaQuery.of(context).size.width,
              color: Colors.transparent,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.end,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Expanded(
                      flex: 3,
                      child: Column(
                        children: [
                        ],
                      )),


                  Expanded(
                    flex: 1,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20, right: 20),
                      child: Center(child: GestureDetector(
                          onTap: (){},
                          child: captureQuestionTile(context, location, cameraViewController))),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    ),
  );
  return _isConfirm;
}

//dialog xac nhan exit detect
Future<bool> imageCapturedDialog(BuildContext context,LocationInfo location, File image) async {

  var _isConfirm = false;

  await showDialog(
    context: context,
    // barrierColor: Colors.black.withOpacity(.71),
    builder: (context) => new AlertDialog(
      backgroundColor: Colors.transparent,
      contentPadding: EdgeInsets.all(0),
      insetPadding:  EdgeInsets.all(3),
      content: StatefulBuilder(
        builder: (BuildContext context, StateSetter setState) {
          return Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              border: Border.all(width: 2, color: AppTheme.orange)
            ),
            width: MediaQuery.of(context).size.width,
            child: Padding(
              padding: const EdgeInsets.all(25.0),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    if (image == null)
                      Text('no_image_captured'.tr)
                    else
                      Padding(
                        padding: const EdgeInsets.only(top: 18.0, left: 20, right: 20),
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(18.0),
                            child: Image.file(image,
                              height: MediaQuery.of(context).size.height/2,
                              width: MediaQuery.of(context).size.width,
                              fit: BoxFit.cover,
                            )),
                      ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10.0),
                      child: Text('request_guide_object'.tr,
                        textAlign: TextAlign.center,
                        style: AppTheme.titleLightMode.copyWith(fontWeight: FontWeight.normal),),
                    ),

                    //page control
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          MaterialButton(
                            padding: EdgeInsets.symmetric(horizontal: 10),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(25.0),
                            ),
                            color: Colors.white,
                            elevation: 2,
                            height: 48,
                            onPressed: (){
                              _isConfirm = false;
                              Navigator.of(context).pop(true);
                            },
                            child: Text(" Cancel",
                              style: AppTheme.title.copyWith(color: Colors.grey, fontWeight: FontWeight.w400),),),

                          MaterialButton(
                            padding: EdgeInsets.symmetric(horizontal: 25),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            color: Colors.blue,
                            elevation: 2,
                            height: 48,
                            onPressed: (){
                              _isConfirm = true;

                              //check user exist
                              if (storage.checkUserAlreadyLogin()){
                                //return
                                Navigator.of(context).pop(true);
                              } else {
                                // direct to login
                                Get.to(()=>LoginScreen(isBackable: true, isGoRoot: false), transition: Transition.fadeIn, duration: Duration(milliseconds: 500))
                                    ?.then((value) => Get.back()); //test
                              }
                            },
                            child: Text(" Send ",
                              style: AppTheme.title.copyWith(color: Colors.white, fontWeight: FontWeight.w400),),),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    ),
  );
  return _isConfirm;
}
