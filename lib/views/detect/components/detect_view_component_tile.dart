

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mitta/app_theme.dart';
import 'package:mitta/controllers/controller_detect_view.dart';
import 'package:mitta/model/location/model_location.dart';
import 'package:mitta/views/guide_content/components/component_content_guide_tile.dart';
import '../../../model/object/model_object.dart';
import '../camera_view.dart';
import '../detect_object_result.dart';
import '/commons/common.dart';
import '/configs/configs.dart';
import '/services/service_storage.dart';
import '/services/services_locator.dart';
import 'detect_view_component_dialog.dart';

final storage = locator<StorageLocal>();


Widget objectContentTile(context, ObjectInfo objectInfo, LocationInfo locationInfo){
  var _screenSize = MediaQuery.of(context).size;
  return Container(
    padding: EdgeInsets.all(8),
    margin: EdgeInsets.symmetric(horizontal: 20),
    decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(16)
    ),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        //image
        Padding(
          padding: const EdgeInsets.only(right: 10.0),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(17),
            child: SizedBox(
              height: 80,
              width: 80,
              child: Image.network(objectInfo.image != null
                  ? objectInfo.image.toString().contains("http")
                  ? objectInfo.image.toString()
                  : Configs.hostName + objectInfo.image.toString()
                  : "",
                fit: BoxFit.cover,
                loadingBuilder: (BuildContext? context,
                    Widget? child,
                    ImageChunkEvent? loadingProgress) {
                  if (loadingProgress == null) return child!;
                  return Container(
                    height: 30,
                    width: 30,
                    alignment: Alignment.center,
                    child: CircularProgressIndicator(
                      color: Colors.blue.shade400,
                      value: loadingProgress.expectedTotalBytes !=
                          null
                          ? loadingProgress
                          .cumulativeBytesLoaded /
                          loadingProgress.expectedTotalBytes!
                              .toInt()
                          : null,
                    ),
                  );
                },
                errorBuilder: (BuildContext? context,
                    Object? exception, StackTrace? stackTrace) {
                  return Container(
                      height: 30,
                      width: 30,
                      alignment: Alignment.center,
                      color: Colors.blueAccent.withOpacity(.4),
                      child: Icon(Icons.image_not_supported_outlined, size: 40,));
                },
              ),
            ),
          ),
        ),

        //content
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (objectInfo.location != null)
                Text(objectInfo.location!.name.toString(), style: TextStyle(color: Colors.grey),),
              SizedBox(height: 10,),
              Text(objectInfo.name.toString(),style: AppTheme.subtitleBoldLightMode,)
            ],
          ),
        ),

        Padding(
          padding: const EdgeInsets.only(right: 10.0),
          child: MaterialButton(
            padding: EdgeInsets.symmetric(horizontal: 0),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            ),
            color: AppTheme.blue,
            elevation: 2,
            height: 28,
            onPressed: (){
              Get.to(()=> DetectObjectResult(locationModel: locationInfo, isFromScanView: true, model: objectInfo,));
            },
            child: Text("Guide",
              style: AppTheme.subtitle.copyWith(color: Colors.white),),),
        ),
      ],
    ),
  );
}


Widget feedbackTile(context, LocationInfo locationInfo){
  var _screenSize = MediaQuery.of(context).size;
  return Container(
    padding: EdgeInsets.symmetric(horizontal: 8, vertical: 15),
    decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(16)
    ),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        //content
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text("not_found_object".tr, style: TextStyle(color: Colors.grey),),
              SizedBox(height: 10,),
              Text("question_not_found_object".tr, style: AppTheme.subtitleBoldLightMode,)
            ],
          ),
        ),

        Padding(
          padding: const EdgeInsets.only(right: 10.0),
          child: MaterialButton(
            padding: EdgeInsets.symmetric(horizontal: 6.0),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            ),
            color: AppTheme.orange,
            elevation: 2,
            height: 35,
            onPressed: (){
             //post to back end
              //back
              Get.back();
              thanksFeedbackDialog(context);
            },
            child: Text("OK",
              style: AppTheme.subtitle.copyWith(color: Colors.white),),),
        ),
      ],
    ),
  );
}


Widget captureQuestionTile(context, LocationInfo locationInfo, CameraViewController cameraViewController){
  var _screenSize = MediaQuery.of(context).size;
  return Container(
    padding: EdgeInsets.symmetric(horizontal: 8, vertical: 15),
    decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(16),
      boxShadow: <BoxShadow>[
        BoxShadow(
            color: Colors.black26,
            offset: const Offset(.5, 2.1),
            blurRadius: 10.0),
      ],
    ),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        GestureDetector(
          onTap: (){
            var controller = Get.put(DetectViewController());

            controller.imgFromCamera().then((value) async {
              if (value != null) {
                await imageCapturedDialog(context, locationInfo, value).then((rs) {
                  if (rs){
                    // controller.imageFile = File(value.path);
                    controller.postReport(locationInfo).then((value){
                      if (value){
                        //show popup thanks
                        thanksReportObjectDialog(context);
                      }
                    });
                  }
                });
              }
            });

            // cameraViewController.takePhoto!().then((value) async {
            //   if (value != null) {
            //     await imageCapturedDialog(context, locationInfo, value).then((rs) {
            //       if (rs){
            //         controller.imageFile = File(value.path);
            //         controller.postReport(locationInfo).then((value){
            //           if (value){
            //             //show popup thanks
            //             thanksReportObjectDialog(context);
            //           }
            //         });
            //       }
            //     });
            //   }
            // });
          },
          child: Padding(
            padding: const EdgeInsets.only(right: 10.0, left: 5),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                  height: 50,
                    width: 50,
                    child: Image.asset("assets/images/icon_capture.png")),

                Container(
                  width: 40,
                  height: 4,
                  color: Colors.grey,
                )
              ],
            ),
          ),
        ),

        //content
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text("no_object".tr, style: TextStyle(color: Colors.grey),),
              SizedBox(height: 10,),
              Text("please_capture".tr,style: AppTheme.subtitleBoldLightMode,)
            ],
          ),
        ),

      ],
    ),
  );
}