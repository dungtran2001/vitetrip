
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mitta/commons/button/button_back.dart';
import 'package:mitta/commons/common.dart';
import 'package:mitta/commons/components/audio/audio_player_widget.dart';
import 'package:mitta/commons/components/audio/progress_notifier.dart';
import 'package:mitta/configs/configs.dart';
import 'package:mitta/model/audio/resource_model.dart';
import 'package:mitta/model/location/model_location.dart';
import 'package:mitta/services/audio_player/audio_manager.dart';
import 'package:mitta/views/detect/detect_view.dart';
import '../../commons/components/audio/play_button_notifier.dart';
import 'package:mitta/model/object/model_object.dart';
import 'package:mitta/network/api/api.dart';
import 'package:mitta/network/response/response_objects.dart';
import 'package:mitta/services/services_locator.dart';

import '../../app_theme.dart';
import '../../services/service_storage.dart';

/// man hinh play audio cua obj sau khi scan
class DetectObjectResult extends StatefulWidget {
  final ObjectInfo model;
  final LocationInfo locationModel;
  final bool isFromScanView;

  DetectObjectResult({required this.model, required this.isFromScanView, required this.locationModel});

  @override
  _DetectObjectResultState createState() => _DetectObjectResultState();
}

class _DetectObjectResultState extends State<DetectObjectResult> {

  bool isShowAudioPlayer = true;
  final mediaManager = locator<MediaManager>();
  final storage = locator<StorageLocal>();

  @override
  void initState(){

    //precache image
    if (widget.model.slider != null && widget.model.slider!.length > 0){
      for (var e in widget.model.slider!){
        e.preImage = new Image.network(e.image.toString().contains("http")
            ? e.image.toString()

            : Configs.hostName + "/" + e.image.toString(), fit: BoxFit.contain,);


      }
    }
    WidgetsBinding.instance
        .addPostFrameCallback((_) {
      // mediaManager.loadPlaylist([
      //   ResourceModel(
      //     id: widget.model.id.toString(),
      //     title: widget.model.name,
      //       url: widget.model.audio.toString().contains("http")
      //           ? widget.model.audio.toString()
      //           : Configs.hostName + "/" + widget.model.audio.toString(),//"https://samplelib.com/lib/preview/mp3/sample-15s.mp3",
      //       // url: "https://samplelib.com/lib/preview/mp3/sample-15s.mp3", //"https://file-examples.com/storage/fea8fc38fd63bc5c39cf20b/2017/11/file_example_MP3_5MG.mp3",
      //       time: "00:00:19"
      //   )
      // ]);
      mediaManager.add(ResourceModel(
          id: widget.model.id.toString(),
          title: widget.model.name,
          url: widget.model.audio.toString().contains("http")
              ? widget.model.audio.toString()
              : Configs.hostName + "/" + widget.model.audio.toString(),//"https://samplelib.com/lib/preview/mp3/sample-15s.mp3",
          // url: "https://samplelib.com/lib/preview/mp3/sample-15s.mp3", //"https://file-examples.com/storage/fea8fc38fd63bc5c39cf20b/2017/11/file_example_MP3_5MG.mp3",
          time: widget.model.audioDuration ?? "00:00:00"
      ));
      Future.delayed(const Duration(milliseconds: 1000), () async {

        mediaManager.next();
        // mediaManager.stop(); //rem to auto play

      });



    });

    //post to archiverment
    if (!widget.model.isVisited && storage.checkUserAlreadyLogin()){
      locator<Api>().client!.postArchivermentData(
        userId: storage.userData!.id,
        provinceId: widget.locationModel.provinceId,
        locationId: widget.locationModel.id,
        objectId: widget.model.id,
        token: storage.userData!.token.toString(),
      );
    }


    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (widget.model.slider != null && widget.model.slider!.length > 0) {
      for (var e in widget.model.slider!) {
        //precache image next
        precacheImage(e.preImage.image, context);
      }
      _currentImageShowDuration = Common.durationParse(widget
          .model.slider![1].at);
      // mediaManager.play();

    }
  }

  @override
  void dispose(){
    // mediaManager.stop();
    // mediaManager.previous();
    disposeCurrentMedia();
    // mediaManager.remove();
    // mediaManager.removePlaylist();
    super.dispose();
  }

  void disposeCurrentMedia() {
    mediaManager.stop();
    mediaManager.previous();
    mediaManager.remove();
  }

  final api = locator<Api>();
  ObjectInfo? objectData;
  Future<void> getObjectData({id}) async {
    ObjectResponse _data = await api.client!.getObjectsData(objectId: id);
    setState(() {
      objectData = _data.data.first;
    });
  }

  var _currentImageShowIndex = 0;
  var _currentImageShowDuration = new Duration(hours: 0, minutes: 0, seconds: 0);

  void resetSlider(){
    _currentImageShowIndex = 0;
    _currentImageShowDuration = Common.durationParse(widget
        .model.slider![1].at);
  }

  @override
  Widget build(BuildContext context) {
    var _screenSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Colors.grey,
      body: SafeArea(
        child: Container(
          decoration: BoxDecoration(
            // image: new DecorationImage(
            //   image: NetworkImage(
            //     widget.model.images!.first.toString().contains("http")
            //         ? widget.model.images!.first
            //         : Configs.hostName + "/" + widget.model.images!.first
            //         ?? "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQiWwAFRoutuyPbPp4FzSimgdw6lvQxMqqU9A&usqp=CAU",
            //   ),
            //   fit: BoxFit.cover,
            //   colorFilter: new ColorFilter.mode(Colors.orange.withOpacity(0.2), BlendMode.overlay),
            // ),
          ),
          child: (widget.model != null)
              ? Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [

              ///header
              Container(
                padding: EdgeInsets.symmetric(horizontal: 20),
                height: _screenSize.height /8 - 10,
                color: Colors.white,
                alignment: Alignment.center,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    ButtonBack.backButtonSimple(
                      onTap: (){
                        // mediaManager.stop();
                        // mediaManager.previous();
                        Get.back();
                        //back to detect object screen // all object screen
                        // Get.off(()=> DetectViewInfo(model: widget.locationModel,));
                        // Get.offAll(()=> DetectViewInfo(model: widget.locationModel,));
                      }
                    ),
                    //title
                    RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                            text: widget.model.name.toString()[0],
                            style: AppTheme.titleAppBar
                                .copyWith(color: AppTheme.orange),
                            children: [
                              TextSpan(
                                  text: widget.model.name.toString().substring(1),
                                  style: AppTheme.titleAppBar),
                            ])),

                    // SizedBox(
                    //   height: 30,
                    //   width: 30,
                    //   child: Image.asset("assets/images/icon_diamon.png"),
                    // )

                  ],
                ),
              ),


              ///listen to reset slider until finish
              ValueListenableBuilder<ButtonState>(
                valueListenable: mediaManager.playButtonNotifier,
                builder: (_, value, __) {
                  switch (value) {
                    case ButtonState.loading:
                      break;
                    case ButtonState.paused:
                      if (_currentImageShowIndex == widget.model.slider!.length -1)
                        resetSlider();
                      // else if (_currentImageShowIndex == 0)
                      //   mediaManager.play();
                      break;
                    case ButtonState.playing:
                      break;

                  }
                  return SizedBox();
                },
              ),

              ///main contents
              Expanded(
                child: ValueListenableBuilder<ProgressBarState>(
                  valueListenable: mediaManager.progressNotifier,
                  builder: (_, value, __) {
                    // var fastestMarathon = new Duration(hours: 0, minutes: 0, seconds: 2);
                    // print(value.current);

                    if (value != null && value.current != null) {
                      if (value.current.inSeconds == _currentImageShowDuration.inSeconds
                          &&value.current.inMinutes == _currentImageShowDuration.inMinutes
                          &&value.current.inHours == _currentImageShowDuration.inHours) {
                        if (_currentImageShowIndex < widget.model.slider!.length -1){ //safe
                          _currentImageShowIndex = _currentImageShowIndex + 1;
                          if (_currentImageShowIndex != widget.model.slider!.length -1) {
                            _currentImageShowDuration = Common.durationParse(widget
                                .model.slider![_currentImageShowIndex + 1].at);
                          }
                          print(_currentImageShowDuration.toString());

                        }

                      }
                    }
                    return Container(
                      child: GestureDetector(
                        onTap: (){
                          setState(() {
                            isShowAudioPlayer = !isShowAudioPlayer;
                          });
                        },
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            /// SLIDER IMAGE WITH AUDIO
                            if (widget.model.slider!= null && widget.model.slider!.length > 0)
                              SizedBox(
                                height: _screenSize.height,
                                width: _screenSize.width,
                                child: widget.model.slider![_currentImageShowIndex].preImage,
                              )
                            else
                              SizedBox(
                                height: _screenSize.height,
                                width: _screenSize.width,
                                child: Image.network(widget.model.image.toString().contains("http")
                                  ? widget.model.image.toString()
                                  : Configs.hostName + "/" + widget.model.image.toString(), fit: BoxFit.contain),
                              ),
                            if (isShowAudioPlayer)
                              Positioned(
                                  bottom: 0,
                                  child: AudioPlayerWidget()),

                            ///button scan
                              Positioned(
                              bottom: 120,
                              child: MaterialButton(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20.0),
                                ),
                                color: AppTheme.orange,
                                elevation: 2,
                                height: 55,
                                onPressed: (){
                                  //back
                                  // print("GetPreviousRoute: ${Get.previousRoute}");
                                  Get.back();
                                  if (!widget.isFromScanView) {
                                    Get.to(DetectView(model: widget.locationModel), routeName: "DetectView");
                                  }
                                },
                                child: Text("scan_next".tr,
                                  style: AppTheme.titleAppBar.copyWith(color: Colors.white, fontWeight: FontWeight.w400),),),
                            ),

                          ],
                        ),
                      ),
                    );
                  }
                ),
              ),
            ],
          )
              : CircularProgressIndicator(),
        ),
      ),
    );
  }

}

