import 'dart:math';

import 'package:flutter/material.dart';

class BorderPainter extends CustomPainter {

  double? stroke;
  BorderPainter({this.stroke = 4});

  @override
  void paint(Canvas canvas, Size size) {

    final pointA = Offset(0, 0); //Offset(0, 0);
    final pointB = Offset(size.width * 0.8, 0); //Offset(240, 0);
    final pointC = Offset(size.width, size.height/5); //Offset(300, 30);
    final pointD = Offset(size.width, size.height); //Offset(300, 150);
    final pointE = Offset(0, size.height); //Offset(0, 150);

    final Path path = Path()
      ..moveTo(pointA.dx, pointA.dy) //
      ..lineTo(pointB.dx, pointB.dy) //
      ..lineTo(pointC.dx, pointC.dy) //
      ..lineTo(pointD.dx, pointD.dy) //
      ..lineTo(pointE.dx, pointE.dy) //
      ..close();

    final Paint redPaint = Paint() // tạo paint màu đỏ để vẽ cờ đỏ
      ..color = Colors.cyan
      ..strokeWidth = stroke!//4
      ..style = PaintingStyle.stroke; // tô màu hết cả shape

    canvas.drawPath(path, redPaint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}


class BorderPainterClip extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final canvasRect = Offset.zero & size;
    var rectWidth = size.width * 0.8;
    var rectHeight = size.height * 0.55;
    // final rect = Rect.fromCircle(
    //   center: canvasRect.center,
    //   radius: rectWidth / 2,
    // );
    const offset = 24.0;
    const radius = 26.0;
    const strokeWidth = 3.0;
    const extend = radius + 0;
    const arcSize = Size.square(radius * 2);

    // var rect = Rect.fromCenter(center: canvasRect.center, width: rectWidth, height: rectHeight);
    var rect = Rect.fromLTWH(size.width * 0.1, size.height / 5, rectWidth, rectHeight);

    canvas.drawPath(
      Path()
        ..fillType = PathFillType.evenOdd
        ..addRRect(
          RRect.fromRectAndRadius(
            rect,
            const Radius.circular(radius),
          ).deflate(strokeWidth / 2),
        )
        ..addRect(canvasRect),
      Paint()..color = Colors.black.withOpacity(.15),
    );

    canvas.save();
    canvas.translate(rect.left, rect.top);
    final path = Path();
    for (var i = 0; i < 4; i++) {
      final l = i & 1 == 0;
      final t = i & 2 == 0;
      path
        ..moveTo(l ? 0 : rectWidth, t ? extend : rectHeight - extend)
        ..arcTo(
            Offset(l ? 0 : rectWidth - arcSize.width,
                t ? 0 : rectHeight - arcSize.width) &
            arcSize,
            l ? pi : pi * 2,
            l == t ? pi / 2 : -pi / 2,
            false)
        ..lineTo(l ? extend : rectWidth - extend, t ? 0 : rectHeight);
    }
    canvas.drawPath(
      path,
      Paint()
        ..color = Colors.white
        ..strokeWidth = strokeWidth
        ..style = PaintingStyle.stroke,
    );
    canvas.restore();


    // canvas.saveLayer(Rect.largest, Paint());
    // canvas.drawRect(Rect.fromLTWH(0, 0, size.width, size.height), Paint()..color = Colors.black.withOpacity(.15));
    //
    // var actWidth = size.width * 0.8;
    // var actHeight = size.height * 0.55;
    // // final pointA = Offset(size.width * 0.1, size.height / 6); //Offset(0, 0);
    // // final pointB = Offset(actWidth * 0.8, size.height / 6); //Offset(240, 0);
    // // final pointC = Offset(actWidth, actHeight/5 + size.height / 6); //Offset(300, 30);
    // // final pointD = Offset(actWidth, actHeight); //Offset(300, 150);
    // // final pointE = Offset(size.width * 0.1, actHeight); //Offset(0, 150);
    //
    // final Path path = Path();
    //   // ..moveTo(pointA.dx, pointA.dy) //
    //   // ..lineTo(pointB.dx, pointB.dy) //
    //   // ..lineTo(pointC.dx, pointC.dy) //
    //   // ..lineTo(pointD.dx, pointD.dy) //
    //   // ..lineTo(pointE.dx, pointE.dy) //
    //   // ..close();
    //
    // path.addRRect(
    //     RRect.fromRectAndRadius(
    //         Rect.fromLTWH(
    //             size.width * 0.1, size.height / 5,
    //             actWidth, actHeight),
    //         Radius.circular(26))
    // );
    //
    // final Paint clearPaint = Paint()
    //   ..blendMode = BlendMode.clear;
    //
    // final Paint cyanPaint = Paint() // tạo paint màu đỏ để vẽ cờ đỏ
    //   ..color = Colors.white
    //   ..strokeWidth = 2
    //   ..style = PaintingStyle.stroke; // tô màu hết cả shape
    //
    // canvas.drawPath(path, clearPaint);
    // canvas.restore();
    // canvas.drawPath(path, cyanPaint);

  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}