

import 'package:flutter/material.dart';

class InvertedRect extends CustomClipper<Path> {
  @override
  getClip(Size size) {
    print(size);
    var actWidth = size.width * 0.8;
    var actHeight = size.height * 0.55;
    return Path()
      ..addRect(Rect.fromLTWH(0.0, 0.0, size.width, size.height))
      ..addRRect(
          RRect.fromRectAndRadius(
              Rect.fromLTWH(
                  size.width * 0.1, size.height / 5,
                  actWidth, actHeight),
              Radius.circular(26))
      )
      ..fillType = PathFillType.evenOdd;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => true;
}