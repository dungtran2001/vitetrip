import 'package:flutter/material.dart';

class BranchPainter extends CustomPainter {

  @override
  void paint(Canvas canvas, Size size) {
    ///ve duong bao diem giua
    final double banKinh = 30;
    final toaDoTamDuongTron = Offset(0, 0);

    final paint = Paint()
      ..color = Colors.white
      ..style = PaintingStyle.stroke
      ..strokeWidth = 2;
    canvas.drawCircle(toaDoTamDuongTron, banKinh, paint);

    final paintBlur = Paint()
      ..color = Colors.cyanAccent.withOpacity(.12)
      ..style = PaintingStyle.stroke
      ..strokeWidth = 12;
    canvas.drawCircle(toaDoTamDuongTron, banKinh-2, paintBlur);

    ///ve duong line
    final pointA = Offset(0, 0); //Offset(0, 0);
    final pointB = Offset(size.width /2, size.height * 2/3); //Offset(10, 10);
    final pointC = Offset(size.width, size.height * 2/3); //Offset(20, 10);

    final Path path = Path()
      ..moveTo(pointA.dx, pointA.dy)
      ..lineTo(pointB.dx, pointB.dy)
      ..lineTo(pointC.dx, pointC.dy);

    final Paint redPaint = Paint() //
      ..color = Colors.cyanAccent.withOpacity(.65)
      ..strokeWidth = 2
      ..style = PaintingStyle.stroke; //

    canvas.drawPath(path, redPaint);

    ///ve tam
    final paintFill = Paint()
      ..color = Colors.yellow
      ..style = PaintingStyle.fill
      ..strokeWidth = 2;
    canvas.drawCircle(toaDoTamDuongTron, banKinh * 3/4 - 2, paintFill);


  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}