import 'dart:ui';

import 'package:audio_video_progress_bar/audio_video_progress_bar.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:mitta/commons/button/button_back.dart';
import 'package:mitta/commons/components/audio/progress_notifier.dart';
import 'package:mitta/configs/configs.dart';
import 'package:mitta/model/location/model_location.dart';
import 'package:mitta/views/detect/painter/border_paint.dart';
import 'package:mitta/views/detect/painter/cutout_paint.dart';
import 'package:mitta/model/object/model_object.dart';
import 'package:mitta/network/api/api.dart';
import 'package:mitta/services/services_locator.dart';
import 'package:mitta/services/tflite/recognition.dart';

import '../../app_theme.dart';
import '../../controllers/controller_detect_view.dart';
import '../../services/service_storage.dart';
import '../../commons/button/play_button.dart';

import 'box_widget.dart';
import 'camera_view.dart';

/// [DetectView] stacks [CameraView] and [BoxWidget]s with bottom sheet for stats
class DetectView extends StatefulWidget {
  final LocationInfo model;

  DetectView({required this.model});

  @override
  _DetectViewState createState() => _DetectViewState();
}

class _DetectViewState extends State<DetectView> with TickerProviderStateMixin {
  /// Scaffold Key
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();

  final controller = Get.put(DetectViewController());

  late AnimationController _animationController;
  bool _animationStopped = false;

  @override
  void initState() {
    controller.locationModel = widget.model;

    WidgetsBinding.instance.addPostFrameCallback((_) {
      controller.context = context;
      controller.getObjectData(locationId: widget.model.id).then((value) {
        if (locator<StorageLocal>().checkUserAlreadyLogin()) {
          controller
              .getArchivermentData(locationId: widget.model.id)
              .then((value) {
            //sort // if object exist in archiverment list and object list

            if (controller.archivermentData != null &&
                controller.archivermentData.length > 0) {
              for (var ob in controller.objectData) {
                try {
                  if (controller.archivermentData.first.objectCollectedIds !=
                          null &&
                      controller.archivermentData.first.objectCollectedIds!
                              .length >
                          0) {
                    var _exist = controller
                        .archivermentData.first.objectCollectedIds!
                        .firstWhere((element) => element == ob.id);
                    if (_exist != null) ob.isVisited = true;
                  }
                } catch (e) {}
              }
              //show bottom sheet
              showObjectDetected();
            }
          });
        } else {
          //show bottom sheet
          showObjectDetected();
        }
      });
    });

    //animation scan
    _animationController = new AnimationController(
        duration: new Duration(milliseconds: 1500), vsync: this);

    _animationController.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        animateScanAnimation(true);
      } else if (status == AnimationStatus.dismissed) {
        animateScanAnimation(false);
      }
    });

    animateScanAnimation(false); // Starts the animation.

    //end animation scan

    super.initState();
  }

  @override
  void dispose() {
    if (controller.scanTimer != null) controller.scanTimer.cancel();
    _animationController.dispose();
    super.dispose();
  }

  bool scanning = false;

  void animateScanAnimation(bool reverse) {
    if (reverse) {
      _animationController.reverse(from: 1.5);
    } else {
      _animationController.forward(from: 0.0);
    }
  }

  @override
  Widget build(BuildContext context) {
    var _screenSize = MediaQuery.of(context).size;
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Colors.grey,
      body: GetBuilder<DetectViewController>(builder: (controller) {
        return Stack(
          alignment: Alignment.center,
          children: <Widget>[
            /// Camera View
            if (controller.isCompleteScan)
              Container(
                height: _screenSize.height,
                width: _screenSize.width,
                decoration: BoxDecoration(
                  image: new DecorationImage(
                    image:
                        new NetworkImage(widget.model.images!.first.toString()),
                    fit: BoxFit.cover,
                    colorFilter: new ColorFilter.mode(
                        Colors.black.withOpacity(0.2), BlendMode.overlay),
                  ),
                ),
              )
            else
              Container(
                  height: _screenSize.height,
                  width: _screenSize.width,
                  child: CameraView(
                    locationModel: widget.model,
                    resultsCallback: controller.resultsCallback,
                    statsCallback: controller.statsCallback,
                    controller: controller.cameraViewController,
                  )),

            /// Bounding boxes
            // if (controller.results.length > 0 && !controller.isCompleteScan)
            // boundingBoxes(controller.results),

            ///border blur
            ClipPath(
              clipper: InvertedRect(),
              child: BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
                child: Container(color: Colors.transparent),
              ),
            ),

            ///border color
            CustomPaint(
              painter: BorderPainterClip(),
              size: Size(_screenSize.width, _screenSize.height),
            ),

            ///animation scan
            // Positioned(
            //   left: _screenSize.width * 0.1,
            //   right: _screenSize.width * 0.1,
            //   top: _screenSize.height / 5,
            //   child: ScannerAnimation(
            //     _animationStopped,_screenSize.width * 0.8,
            //     animation: _animationController,),
            // ),

            if (!isShowObjectSheet && controller.isScanning.value)
              Positioned(
                left: _screenSize.width * 0.1,
                right: _screenSize.width * 0.1,
                // top: _screenSize.height * 2/ 5,
                child: SizedBox(
                  child: Lottie.asset('assets/anim/anim_scan5.json'),
                  width: _screenSize.width,
                  height: _screenSize.width,
                ),
              ),

            ///header
            Positioned(
              top: 0,
              right: 0,
              left: 15,
              child: Container(
                padding: EdgeInsets.fromLTRB(0, 40, 0, 20),
                width: MediaQuery.of(context).size.width,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    IconButton(
                        icon: Icon(Icons.arrow_back_ios_new,
                            color: Colors.white, size: 20),
                        onPressed: () {
                          Get.back();
                        }),

                    Spacer(),

                    // GestureDetector(
                    //   onTap: (){
                    //   },
                    //   child: Container(
                    //     padding: EdgeInsets.fromLTRB(20, 40, 20, 20),
                    //     decoration: BoxDecoration(
                    //         borderRadius: BorderRadius.only(bottomLeft: Radius.circular(20)),
                    //         color: Colors.white.withOpacity(.99),
                    //     ),
                    //     child: SizedBox(
                    //         height: 30,
                    //         width: 30,
                    //         child: Image.asset("assets/images/icon_diamon.png")
                    //     ),
                    //   ),
                    // ),
                  ],
                ),
              ),
            ),

            ///button start scan
            if (!isShowObjectSheet && !controller.isScanning.value)
              Positioned(
                bottom: 120,
                child: MaterialButton(
                  padding: EdgeInsets.symmetric(horizontal: 30),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  color: AppTheme.orange,
                  elevation: 2,
                  height: 55,
                  onPressed: () {
                    controller.startScan();
                  },
                  child: Text(
                    " " + 'text_scan'.tr + " ",
                    style: AppTheme.titleAppBar.copyWith(
                        color: Colors.white, fontWeight: FontWeight.w400),
                  ),
                ),
              )
            else if (!isShowObjectSheet && controller.isScanning.value)
              Obx(
                () => Positioned(
                  bottom: 120 + 27,
                  left: _screenSize.width / 3.5,
                  right: _screenSize.width / 3.5,
                  child: Column(
                    children: [
                      Text(
                        (((controller.scanningCountTime.value.toDouble() /
                                        Configs.timeScanning) *
                                    100)
                                .ceil()
                                .toString()) +
                            "%",
                        style: AppTheme.subtitle,
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      LinearProgressIndicator(
                        minHeight: 10.5,
                        valueColor:
                            AlwaysStoppedAnimation<Color>(AppTheme.orange),
                        backgroundColor: Colors.white,
                        value: controller.scanningCountTime.value.toDouble() /
                            Configs.timeScanning,
                      ),
                    ],
                  ),
                ),
              ),

            ///bottom
            if (!isShowObjectSheet)
              Positioned(
                bottom: 0,
                child: GestureDetector(
                  onVerticalDragDown: (_) {
                    //show bottom sheet
                    showObjectDetected();

                    //hide button
                    setState(() {
                      isShowObjectSheet = true;
                    });
                  },
                  child: Container(
                    height: 60,
                    width: _screenSize.width,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(25),
                          topRight: Radius.circular(25),
                        ),
                        color: Colors.black26),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          height: 3,
                          width: _screenSize.width / 5,
                          decoration: BoxDecoration(
                            color: Colors.grey.shade400,
                            borderRadius: BorderRadius.circular(1),
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                  color: Colors.grey,
                                  offset: const Offset(.5, 1.1),
                                  blurRadius: 1.0),
                            ],
                          ),
                        ),

                        //check: if already login -> show object detected, else show all objects
                        if (locator<StorageLocal>().checkUserAlreadyLogin())
                          Text(
                            "${controller.archivermentData.length}/${controller.objectData.length} " +
                                "object_detected".tr,
                            style: AppTheme.subtitleLightMode.copyWith(
                                color: Colors.white70,
                                fontWeight: FontWeight.bold,
                                fontStyle: FontStyle.italic),
                          )
                        else
                          Text(
                            "${controller.objectData.length} objects to detect",
                            style: AppTheme.subtitleLightMode.copyWith(
                                color: Colors.white70,
                                fontWeight: FontWeight.bold,
                                fontStyle: FontStyle.italic),
                          )
                      ],
                    ),
                  ),
                ),
              ),

            ///show list object if accuracy is not guaranteed
//               if (controller.objectDetectedData != null && controller.objectDetectedData.length > 0)
//                 Positioned(
//                   top:  _screenSize.height * 0.67 /3,
//                   left: 50, right: 10,
//                   child: Container(
// //                height: 100,
//                       width: _screenSize.width,
//                       child: Column(
//                         children: [
//                           Row(
//                             mainAxisAlignment: MainAxisAlignment.start,
//                             children: [
//                               Text("Vật thể phát hiện",
//                                 style: TextStyle(
//                                   color: Colors.white,
//                                   shadows: [
//                                     Shadow(
//                                       blurRadius: 8.0,
//                                       color: Colors.black54,
//                                       offset: Offset(2.0, 2.0),
//                                     ),
//                                   ],
//                                 ),),
//                               SizedBox(width: 5,),
//                               Container(
//                                 padding: const EdgeInsets.all(5),
//                                 decoration: BoxDecoration(
//                                   shape: BoxShape.circle,
//                                   color: Colors.cyanAccent,
//                                 ),
//                                 child: Text(controller.objectDetectedData.length.toString(), style: AppTheme.subtitleBold,),
//                               )
//                             ],
//                           ),
//                           SizedBox(height: 2,),
// //                      ListObjectSame(objectDetectedData),
//                           Row(
//                             children: [
//                               for (var item in controller.objectDetectedData)
//                                 Container(
//                                   height: controller.isCompleteScan ? _screenSize.height /4 : 40,
//                                   width: controller.isCompleteScan ? _screenSize.height /6  : 40,
//                                   margin: EdgeInsets.all(4),
//                                   decoration: BoxDecoration(
//                                     border: Border.all(color: Colors.yellow, width: 1.5),
//                                     image: new DecorationImage(
//                                       image: new NetworkImage(
//                                           item.image.toString().contains("http")
//                                               ? item.image.toString()
//                                               : Configs.hostName + "/" + item.image.toString()
//                                       ),
//                                       fit: BoxFit.cover,
//                                     ),
//                                   ),
//                                 )
//
//                             ],
//                           )
//                         ],
//                       )),
//                 ),
          ],
        );
      }),
    );
  }

  bool isShowObjectSheet = true;

  // BottomSheet Object detected
  void showObjectDetected() {
    Get.bottomSheet(
      GetBuilder<DetectViewController>(builder: (controller) {
        return Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25),
                topRight: Radius.circular(25),
              ),
              color: Colors.black54),
          padding: const EdgeInsets.only(bottom: 15.0, top: 10),
          margin: EdgeInsets.all(0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Center(
                child: Container(
                  height: 3,
                  width: 100,
                  decoration: BoxDecoration(
                    color: Colors.grey.shade400,
                    borderRadius: BorderRadius.circular(1),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          color: Colors.grey,
                          offset: const Offset(.5, 1.1),
                          blurRadius: 1.0),
                    ],
                  ),
                ),
              ),
              Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 15.0, vertical: 10),
                  child: Row(
                    children: [
                      if (controller.selectedObject != null)
                        Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 10.0, 0),
                            child: PlayButton(onPressed: () {
                              controller.navigateToScanResult(
                                  controller.selectedObject!, null);
                            })),
                      Expanded(
                          child: Wrap(
                        direction: Axis.vertical,
                        children: [
                          Text(
                            controller.selectedObject?.name ??
                                widget.model.name.toString(),
                            style: AppTheme.title,
                          ),
                          Text(
                            controller.selectedObject?.description ??
                                widget.model.description.toString(),
                            style: AppTheme.subtitle,
                          ),
                        ],
                      )),
                    ],
                  )),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    for (var object in controller.objectData)
                      GestureDetector(
                        onTap: () {
                          controller.selectObject(object);
                        },
                        child: Container(
                          margin:
                              EdgeInsets.symmetric(vertical: 8, horizontal: 5),
                          height: 80,
                          width: 80,
                          decoration: BoxDecoration(
                            color: Colors.grey,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                                color: controller.selectedObject != null &&
                                        controller.selectedObject!.id ==
                                            object.id
                                    ? AppTheme.orange
                                    : Colors.transparent,
                                width: 2.5),
                            // gradient: LinearGradient(
                            //     colors: [
                            //       Color(0xFF02BCFF),
                            //       AppTheme.mainColorLeft, //HexColor('#4E61FE'),
                            //     ],
                            //     begin: Alignment.bottomLeft,
                            //     end: Alignment.topRight),
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                  color: Colors.black54,
                                  offset: const Offset(1.1, 1.5),
                                  blurRadius: 10.0),
                            ],
                          ),
                          child: Stack(
                            children: [
                              SizedBox(
                                height: 80,
                                width: 80,
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(8),
                                  child: Image.network(
                                    object.image.toString().contains("http")
                                        ? object.image.toString()
                                        : Configs.hostName +
                                            "/" +
                                            object.image.toString(),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                              if (object.isVisited)
                                Positioned(
                                    bottom: 8,
                                    left: 8,
                                    child: Icon(
                                      Icons.check_circle,
                                      color: Colors.blue,
                                    ))
                            ],
                          ),
                        ),
                      )
                  ],
                ),
              ),
            ],
          ),
        );
      }),
      // backgroundColor: Colors.white,
      // shape: RoundedRectangleBorder(
      //     borderRadius: BorderRadius.circular(20),
      //
      // ),
      barrierColor: Colors.transparent,
    ).then((value) {
      setState(() {
        isShowObjectSheet = false;
      });
    });
  }

  /// Returns Stack of bounding boxes
  Widget boundingBoxes(List<Recognition> results) {
    if (results == null) {
      return Container();
    }
    return Stack(
      children: results
          .map((e) => BoxWidget(
                result: e,
              ))
          .toList(),
    );
  }

  static const BOTTOM_SHEET_RADIUS = Radius.circular(24.0);
  static const BORDER_RADIUS_BOTTOM_SHEET = BorderRadius.only(
      topLeft: BOTTOM_SHEET_RADIUS, topRight: BOTTOM_SHEET_RADIUS);
}

/// danh sach hien vat gan giong
class ListObjectSame extends StatelessWidget {
  final List<ObjectInfo> list;

  ListObjectSame(this.list);

  @override
  Widget build(BuildContext context) {
    var _screenSize = MediaQuery.of(context).size;
    return CarouselSlider(
      options: CarouselOptions(
        height: _screenSize.height / 8,
        viewportFraction: 0.8,
        initialPage: 0,
        enableInfiniteScroll: true,
        reverse: false,
        autoPlay: list.length > 1 ? true : false,
        autoPlayInterval: Duration(seconds: 5),
        autoPlayAnimationDuration: Duration(milliseconds: 1000),
        autoPlayCurve: Curves.fastOutSlowIn,
        enlargeCenterPage: true,
        scrollDirection: Axis.horizontal,
      ),
      items: list.map((item) {
        return Builder(
          builder: (BuildContext context) {
            return ObjectSameTile(context, item, _screenSize);
          },
        );
      }).toList(),
    );
  }

  Widget ObjectSameTile(context, ObjectInfo item, _screenSize) {
    return GestureDetector(
      onTap: () {
        //open detail popup
        infoDialog(context, item);
      },
      child: Container(
        width: _screenSize.width * 0.8,
        margin: EdgeInsets.only(bottom: 15.0),
        padding: EdgeInsets.symmetric(horizontal: 15.0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.black54,
                offset: const Offset(.5, 1.1),
                blurRadius: 10.0),
          ],
        ),
        child: Row(
          children: [
            //thumb
            Container(
              margin: EdgeInsets.only(right: 10),
              width: 50,
              height: 50,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Colors.grey.shade300,
                  image: DecorationImage(
                      image: NetworkImage(item.image.toString()),
                      fit: BoxFit.cover)),
            ),

            //title
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    item.name.toString(),
                    style: TextStyle(
                        fontSize: 18.0,
                        fontFamily: AppTheme.fontSfPro,
                        fontWeight: FontWeight.bold),
                    maxLines: 1,
                  ),
//            Divider(height: 2,),
                  Container(
                      child: Text(
                    item.location.toString(),
                    style: AppTheme.captionLightmode,
                    maxLines: 2,
                  )),
                ],
              ),
            ),

            //version
            Container(
                child: Text(
              item.id.toString(),
              style: AppTheme.subtitleLightMode,
              maxLines: 2,
            )),
          ],
        ),
      ),
    );
  }

  //popup

  var _setState;

  infoDialog(BuildContext context, ObjectInfo data) {
    var imageLocation = null;
    locator<Api>().client!.getLocationsData().then((value) {
      imageLocation = value.data
          .firstWhere((element) => element.id == data.versionId.toString())
          .images;

//      _setState((){});
    });

    var _screenSize = MediaQuery.of(context).size;

    showDialog(
      context: context,
      barrierColor: Colors.black.withOpacity(.7),
      builder: (context) => new AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        contentPadding: EdgeInsets.all(0),
        insetPadding: EdgeInsets.all(20),
        content: StatefulBuilder(
          builder: (BuildContext context, StateSetter setState) {
            _setState = setState;

            return Container(
              decoration: BoxDecoration(
//                color: Colors.blue.withOpacity(.2),
                borderRadius: BorderRadius.circular(5.0),
              ),
              width: MediaQuery.of(context).size.width,
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Container(
                      height: _screenSize.height * 0.4,
                      width: _screenSize.width,
                      child: Stack(
                        children: [
                          Container(
                            height: _screenSize.height * 0.4,
                            width: _screenSize.width,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: NetworkImage(imageLocation))),
                            child: BackdropFilter(
                              filter: ImageFilter.blur(sigmaX: 1, sigmaY: 1),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(20),
                                child: new Container(
                                  decoration: new BoxDecoration(
                                      color: Colors.black.withOpacity(0.01)),
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            top: 0,
                            left: 0,
                            right: 0,
                            child: Container(
                              height: _screenSize.height * 0.4,
                              width: _screenSize.width,
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
//                                  color: Colors.grey
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  SizedBox(
                                    height: 40,
                                  ),
                                  Expanded(
                                      child: Column(
                                    children: [
                                      Expanded(
                                        child: Container(
                                          margin:
                                              const EdgeInsets.only(bottom: 5),
                                          // height: height * 0.45 * 0.5,
                                          // width: width * 0.3,
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              boxShadow: [
                                                BoxShadow(
                                                  blurRadius: 3,
                                                  spreadRadius: 2,
                                                  color: Colors.grey
                                                      .withOpacity(.8),
                                                  offset: Offset(0, 2),
                                                )
                                              ]
                                              // image: DecorationImage(
                                              //     image: NetworkImage(
                                              //         widget.book
                                              //             .getImage()),
                                              // )
                                              ),
                                          child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            child: Image.network(
                                              //network(
                                              data.image ?? "", //imageface,
                                              fit: BoxFit.cover,
//                              loadingBuilder: (BuildContext? context,
//                                  Widget? child,
//                                  ImageChunkEvent? loadingProgress) {
//                                if (loadingProgress == null) return child!;
//                                return Center(
//                                  child: CircularProgressIndicator(
//                                    color: Colors.grey.shade400,
//                                    value: loadingProgress.expectedTotalBytes !=
//                                        null
//                                        ? loadingProgress
//                                        .cumulativeBytesLoaded /
//                                        loadingProgress.expectedTotalBytes!
//                                            .toInt()
//                                        : null,
//                                  ),
//                                );
//                              },
                                              errorBuilder:
                                                  (BuildContext? context,
                                                      Object? exception,
                                                      StackTrace? stackTrace) {
                                                return Container(
                                                    width:
                                                        MediaQuery.of(context!)
                                                                .size
                                                                .width /
                                                            2,
                                                    height:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width /
                                                            2,
                                                    alignment: Alignment.center,
                                                    color: AppTheme.grey,
                                                    child: Text("error".tr,
                                                        style: AppTheme
                                                            .subtitleMini));
                                              },
                                            ),
                                          ),
                                        ),
                                      ),
                                      Text(
                                        data.name.toString().toUpperCase(),
                                        style: TextStyle(
                                            fontSize: 22,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white),
                                      ),
                                      SizedBox(
                                        height: 3,
                                      ),
                                      Container(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 12, vertical: 3),
                                        decoration:
                                            BoxDecoration(color: Colors.white),
                                        child: Text(
                                          data.location.toString(),
                                          style: TextStyle(color: Colors.blue),
                                        ),
                                      )
                                    ],
                                  )),
                                  SizedBox(
                                    height: 10,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Positioned(
                            top: 40,
                            left: 20,
                            child: ButtonBack.backButtonSimple(
                                backgroundColor: Colors.lightBlueAccent,
                                iconColor: Colors.white),
                          ),
                          Positioned(
                              top: 40,
                              right: 20,
                              child: ButtonBack.holderHeader())
                        ],
                      ),
                    ),

                    SizedBox(
                      height: 5,
                    ),

                    Divider(),

                    //mota
                    Padding(
                      padding: const EdgeInsets.only(left: 12.0),
                      child: SelectableText.rich(
                        TextSpan(
                          text: 'text_description'.tr + ": ",
                          style: Theme.of(context).textTheme.button,
                          children: [
                            TextSpan(
                                text: data.description, //time,
                                style: Theme.of(context).textTheme.button),
                          ],
                        ),
                      ),
                    ),

                    Container(
                      height: 100,
                      width: _screenSize.width,
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                                padding: EdgeInsets.symmetric(horizontal: 20),
                                width: double.infinity,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    PlayButton(),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    Expanded(
                                      child:
                                          Container(child: AudioProgressBar()),
                                    )
                                  ],
                                )),
                          ],
                        ),
                      ),
                    ),

                    SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}

/// Row for one Stats field
class StatsRow extends StatelessWidget {
  final String left;
  final String right;

  StatsRow(this.left, this.right);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 2.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            flex: 2,
            child: Text(
              left,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(color: Colors.white60, fontSize: 10),
            ),
          ),
          Expanded(
            flex: 1,
            child: Text(right,
                style: TextStyle(color: Colors.white70, fontSize: 10)),
          )
        ],
      ),
    );
  }
}

class AudioProgressBar extends StatelessWidget {
  const AudioProgressBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<ProgressBarState>(
      valueListenable: ProgressNotifier(), //textToSpeech.progressNotifier,
      builder: (_, value, __) {
        return ProgressBar(
          progress: value.current,
          buffered: value.buffered,
          total: value.total,
          onSeek: (_) {},
          //textToSpeech.seek,
          progressBarColor: Colors.lightBlueAccent,
          thumbColor: Colors.lightBlue,
          baseBarColor: Colors.lightBlueAccent.withOpacity(.2),
          barHeight: 3,
          timeLabelType: TimeLabelType.totalTime,
          timeLabelLocation: TimeLabelLocation.none,
        );
      },
    );
  }
}
