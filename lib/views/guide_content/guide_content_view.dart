
import 'dart:ui';

import 'package:audio_video_progress_bar/audio_video_progress_bar.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mitta/commons/button/button_back.dart';
import 'package:mitta/commons/components/audio/progress_notifier.dart';
import 'package:mitta/configs/configs.dart';
import 'package:mitta/controllers/controller_guide_author.dart';
import 'package:mitta/controllers/controller_guide_content.dart';
import 'package:mitta/model/guide_author/guide_author.dart';
import 'package:mitta/model/location/model_location.dart';
import 'package:mitta/services/audio_player/display/audio_play_status.dart';
import 'package:mitta/views/detect/detect_object.dart';
import 'package:mitta/views/detect/painter/border_paint.dart';
import 'package:mitta/views/detect/painter/branch_paint.dart';
import '../../commons/components/audio/play_button_notifier.dart';
import 'package:mitta/model/object/model_object.dart';
import 'package:mitta/network/api/api.dart';
import 'package:mitta/network/response/response_objects.dart';
import 'package:mitta/services/services_locator.dart';
import 'package:mitta/services/tflite/recognition.dart';
import 'package:mitta/services/tflite/stats.dart';

import '../../app_theme.dart';
import 'components/component_content_guide_tile.dart';



class GuideContentView extends StatefulWidget {
  final GuideAuthorInfo? author;
  final LocationInfo? location;
  GuideContentView({this.author, this.location});

  @override
  _GuideContentViewState createState() => _GuideContentViewState();
}

class _GuideContentViewState extends State<GuideContentView> {

  final controller = Get.put(GuideContentController());

  var locationId = "";
  var authorId = "";
  @override
  void initState(){
    if (widget.location != null)
      locationId = widget.location!.id.toString();
    if (widget.author != null)
      locationId = widget.author!.id.toString();

    controller.fetchContentData(authorId:authorId, locationId: locationId);
    super.initState();
  }

  @override
  void dispose(){
    super.dispose();
    controller.disposeAudio();
  }

  PageController contentController = PageController();

  @override
  Widget build(BuildContext context) {
    var _screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.black,
      body: GetBuilder<GuideContentController>(
        builder: (controller) {
          return Container(
            height: _screenSize.height,
            width: _screenSize.width,
            decoration: BoxDecoration(
              image: new DecorationImage(
                image: new NetworkImage(
                    widget.location!.images!.first.toString()
                ),
                fit: BoxFit.cover,
                colorFilter: new ColorFilter.mode(Colors.black.withOpacity(0.4), BlendMode.overlay),
              ),
            ),
            child: controller.isLoading.value
                ? Center(child: CircularProgressIndicator())
                : Column(
              children: <Widget>[
                Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                            colors: [
                              Colors.black.withOpacity(.7),
                              Colors.black.withOpacity(.6),
                            ],
                            begin: Alignment.topCenter,
                            end: Alignment.center),
                      ),
                      child: Container(
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                              colors: [
                                Colors.cyanAccent.withOpacity(.2),
                                Colors.transparent,
                              ],
                              begin: Alignment.topCenter,
                              end: Alignment.center),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            ///header
                            Container(
                              width: MediaQuery.of(context).size.width,
                              padding: const EdgeInsets.only(top: 45.0, right: 30),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  IconButton(
                                      icon: Icon(Icons.center_focus_weak_rounded, color: Colors.yellow,size: 20),


                                      onPressed: () { print("Pressed"); }
                                  ),

                                  Text(widget.location!.name.toString(),
                                    style: TextStyle(
                                        fontSize: 22,
                                        fontFamily: AppTheme.fontMontserrat,
                                        letterSpacing: -1, color: Colors.white,
                                        shadows: [
                                          Shadow(
                                            blurRadius: 8.0,
                                            color: Colors.black38,
                                            offset: Offset(2.0, 2.0),
                                          ),
                                        ],
                                        fontWeight: FontWeight.w500),),


                                  Spacer(),

                                  Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
//                                color: Colors.black.withOpacity(.041)
                                    ),
                                    child: IconButton(
                                        icon: Icon(Icons.grid_view_rounded, size: 30, color: Colors.white,),
                                        onPressed: () { print("Pressed"); }
                                    ),
                                  ),
                                ],
                              ),
                            ),


                            ///body
                            if (!controller.isLoading.value)
                            Expanded(
                              child: Stack(
                                children: [
                                  Container(
                                    margin: EdgeInsets.symmetric(horizontal: 35, vertical: 50),
                                    decoration: BoxDecoration(
                                      border: Border.all(color: Colors.cyanAccent),
                                    ),
                                    child: PageView(
                                      scrollDirection: Axis.horizontal,

                                      reverse: false,
                                      physics: BouncingScrollPhysics(),
                                      controller: contentController,
                                      onPageChanged: (num){
                                        print(">> $num");
                                      },
                                      children: [
                                        contentWidget(_screenSize),
                                        feedbackWidget(_screenSize),
//                                        SizedBox()
                                      ],
                                    ),
                                  ),


                                  //close button
                                  Positioned(
                                    top: 38,
                                      right: 22,
                                      child: GestureDetector(
                                        onTap: (){
                                          Get.back();
                                        },
                                        child: Container(
                                          padding: EdgeInsets.all(5),
                                          decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: Colors.grey.shade900
                                          ),
                                            child: Icon(Icons.cancel_outlined, color: Colors.redAccent,)),
                                      ))
                                ],
                              ),
                            )
                            else
                              Center(child: CircularProgressIndicator())
                          ],
                        ),
                      ),
                    )),

                ///bottom
                ClipRect(
                  child: BackdropFilter(
                    filter: ImageFilter.blur(
                      sigmaX: 4.0,
                      sigmaY: 4.0,
                    ),
                    child: Container(
                      width: MediaQuery.of(context).size.width,
//                  height: 170,
//                   color: Colors.black54.withOpacity(.4),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          AppTheme.GradientLine(
                              width: MediaQuery.of(context).size.width,
                            boundColor: Colors.cyanAccent.withOpacity(.6),
                            color: Colors.cyan,
                            height: 1.2
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 22.0, vertical: 20),
                                child: Text("Walking Tour\n${widget.location!.name}",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: AppTheme.fontMontserrat,
                                    fontSize: 23,
                                    fontWeight: FontWeight.bold,
                                    shadows: [
                                      Shadow(
                                        blurRadius: 8.0,
                                        color: Colors.black38,
                                        offset: Offset(2.0, 2.0),
                                      ),
                                    ],
                                  ),),
                              ),

                              //button play
                              PlayButton(
                                textToSpeech: controller.textToSpeech,
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),

              ],
            ),
          );
        }
      ),
    );
  }

  //danh sach noi dung
  Widget contentWidget(_screenSize){
    return controller.listContentData != null && controller.listContentData.length > 0
        ? Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Expanded(
          child: ListView.builder(
            itemCount: controller.listContentData.length,
            scrollDirection: Axis.vertical,
            itemBuilder: (BuildContext context, int index) {
              return contentGuideTile(controller.listContentData[index], context);
            },
          ),
        ),

        //button
        Container(
          height: 40,
          width: _screenSize.width,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              GestureDetector(
                onTap: (){
                  contentController.animateToPage(
                      1,
                      duration: Duration(milliseconds: 200),
                      curve: Curves.easeIn);
                },
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 25, vertical: 3),
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.cyanAccent),
                  ),
                  child: Text("  Explore  ", style: TextStyle(color: Colors.yellow),),
                ),
              ),
              GestureDetector(
                onTap: (){
                  //play audio
                  controller.playAudio();
                },
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 25, vertical: 3),
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.cyanAccent),
                      color: Colors.cyanAccent.withOpacity(.2)
                  ),
                  child: Text("Play now", style: TextStyle(color: Colors.yellow),),
                ),
              )
            ],
          ),
        )
      ],
    )
        : Center(child: Text("Chưa có noi dung nao!\n Chung toi se cap nhat noi dung moi ngay", style: AppTheme.noteAllPageText, textAlign: TextAlign.center,));
  }

  //danh sach danh gia
  Widget feedbackWidget(_screenSize){
    return Stack(
      children: [
        Container(
          child: controller.currentContentSelected != null
              ? Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              contentGuideTile(controller.currentContentSelected!, context, enableBorder: false),
              //button
              Container(
                height: 40,
                width: _screenSize.width,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    GestureDetector(
                      onTap: (){
                        contentController.animateToPage(
                            0,
                            duration: Duration(milliseconds: 200),
                            curve: Curves.easeIn);
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 25, vertical: 3),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.cyanAccent),
                        ),
                        child: Text("< back  ", style: TextStyle(color: Colors.yellow),),
                      ),
                    ),
                    GestureDetector(
                      onTap: (){
                        //play audio
                        controller.playAudio();
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 25, vertical: 3),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.cyanAccent),
                          color: Colors.cyanAccent.withOpacity(.2)
                        ),
                        child: Text(" Rating ", style: TextStyle(color: Colors.yellow),),
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Divider(color: Colors.cyanAccent,),
              ),
              Expanded(
                child: ListView.builder(
                  itemCount: controller.currentContentSelected!.feedbacks!.length,
                  scrollDirection: Axis.vertical,
                  padding: EdgeInsets.zero,
                  itemBuilder: (BuildContext context, int index) {
                    return contentFeedbackTile(controller.currentContentSelected!.feedbacks![index], context);
                  },
                ),
              ),

            ],
          )
              : Center(child: Text("Chưa có danh gia noi dung nao!\n Hay bat dau kham pha ngay", style: AppTheme.noteAllPageText, textAlign: TextAlign.center,)),
        ),

        Positioned(
          right: 6,
          bottom: 8,
          child: GestureDetector(
            onTap: (){
              Get.to(()=> DetectViewInfo(model: widget.location!,));

            },
            child: Row(
              children: [
                CustomPaint(
                  painter: BranchPainter(),
                  size: Size(40, 50),
                ),
                Column(
                  children: [
                    SizedBox(height: 30,),
                    Container(
                      padding: EdgeInsets.only(top: 5, left: 5, right: 5),
                      decoration: BoxDecoration(
                          color: Colors.black87,
                          border: Border.all(color: Colors.cyanAccent.withOpacity(.8),
                              width: 2.5)
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          RichText(text: TextSpan(
                              text: "click to be",
                              style: Theme.of(context).textTheme.bodyText1!.copyWith(color: Colors.lightBlueAccent),
                              children: [
                                TextSpan(
                                  text: " GUIDED",
                                  style: Theme.of(context).textTheme.button!.copyWith(color: Colors.yellow),
                                )
                              ]
                          )),

                          RichText(text: TextSpan(
                              text: "--",
                              style: Theme.of(context).textTheme.button!.copyWith(color: Colors.white),
                              children: [

                              ]
                          )),

                        ],
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),),

      ],
    );
  }
}
