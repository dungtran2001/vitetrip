import 'dart:math';

import 'package:mitta/controllers/controller_guide_content.dart';
import 'package:mitta/model/guide_author/guide_author.dart';
import 'package:mitta/model/guide_author/guide_author_feedback.dart';
import 'package:mitta/model/guide_author/guide_content.dart';
import 'package:mitta/model/location/model_location.dart';
import 'package:mitta/services/audio_player/text_to_speech_vm.dart';


import '/app_theme.dart';
import '/commons/common.dart';
import '/configs/configs.dart';
import '/controllers/controller_home.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';


final controller = Get.put(GuideContentController());

Widget contentGuideTile(GuideContentInfo data, context, {enableBorder = true}){
  return GestureDetector(
    onTap: (){
      //init audio
      controller.setPlayAudio(data);

      controller.currentContentSelected = data;
      controller.update();
    },
    child: Container(
      margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      padding: const EdgeInsets.symmetric(horizontal: 8),
      decoration: BoxDecoration(
        color: Colors.transparent,
        border: Border.all(color: enableBorder && controller.currentContentSelected != null && controller.currentContentSelected!.id == data.id
            ? Colors.cyanAccent
            : Colors.transparent),
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Row(
        children: [
          //icon app
          Container(
            margin: EdgeInsets.only(right: 10),
            width: 50,
            height: 50,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Image.network(
                data.image.toString(),
                fit: BoxFit.cover,),
            ),
          ),

          //title
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(data.name.toString(),
                  style: TextStyle(
                      fontSize: 18.0,
                      fontFamily: AppTheme.fontSfPro,
                      fontWeight: FontWeight.bold,
                    color: Colors.white
                  ), maxLines: 1,),
//            Divider(height: 2,),
                Container(child: Text(data.description.toString(),
                  style: TextStyle(color: Colors.white), maxLines: 2,)
                ),

                //ranking
                Container(child: Text(data.ranking.toString() + " \u2605",
                    style: TextStyle(color: Colors.yellow))
                ),
              ],
            ),
          ),


          //price
            Padding(
              padding: const EdgeInsets.only(left: 3.0),
              child: Text(data.price == 0.0 ? "FREE" : data.price.toString() + " Mitcoin", style: TextStyle(
                color: Colors.cyanAccent,
                fontWeight: FontWeight.bold
              ),),
            )
        ],
      ),
    ),
  );
}


Widget contentFeedbackTile(GuideAuthorFeedback data, context){
  return GestureDetector(
    onTap: (){

    },
    child: Container(
      margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      padding: const EdgeInsets.symmetric(horizontal: 8),
      decoration: BoxDecoration(
        color: Colors.transparent,
        border: Border.all(color: controller.currentContentSelected != null && controller.currentContentSelected!.id == data.id
            ? Colors.cyanAccent
            : Colors.transparent),
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Row(
        children: [


          //title
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(data.name.toString(),
                  style: TextStyle(
                      fontSize: 18.0,
                      fontFamily: AppTheme.fontSfPro,
                      fontWeight: FontWeight.bold,
                    color: Colors.white
                  ), maxLines: 1,),
//            Divider(height: 2,),
                Container(child: Text(data.description.toString(),
                  style: TextStyle(color: Colors.white), maxLines: 2,)
                ),

                //ranking
                Container(child: Text(data.ranking.toString() + " \u2605",
                    style: TextStyle(color: Colors.yellow))
                ),
              ],
            ),
          ),


          //icon app
          Container(
//            margin: EdgeInsets.only(right: 10),
            width: MediaQuery.of(context).size.width/5,
            height: MediaQuery.of(context).size.width/5 * 9/16,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Image.network(
                data.image.toString(),
                fit: BoxFit.cover,),
            ),
          ),
        ],
      ),
    ),
  );
}


double roundDouble(double value, int places){
  double mod = pow(10.0, places).toDouble();
  return ((value * mod).round().toDouble() / mod);
}
