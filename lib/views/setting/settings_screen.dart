
import 'dart:convert';


import 'package:flutter/material.dart';
import 'package:flutter_device_type/flutter_device_type.dart';
import 'package:get/get.dart';
import '../home/home.dart';
import '../language/choose_languge_screen.dart';
import '../login/login_screen.dart';
import '/commons/button/button_back.dart';
import '/commons/common.dart';
import '/configs/configs.dart';
import '/controllers/controller_main.dart';
import '/controllers/controller_settings.dart';
import '/network/httpController.dart';
import '/services/language/localization_service.dart';
import '/services/service_storage.dart';
import '/services/services_locator.dart';

import '../../app_theme.dart';
import '../../theme_service.dart';


class SettingsScreen extends StatelessWidget {

  String _selectedLang = LocalizationService.locale.languageCode;

  final SettingsController controller = Get.put(SettingsController());
  final mainController = Get.put(MainController());

  final storage = locator<StorageLocal>();

  String filterNameToAvatar(String name){
    if (name == "" || name == null)
      return "";

    var temp = name[0];
    for (var i=1; i< name.length; i++){
      if (name[i-1] == " " && name[i] != " ")
        temp += name[i];
    }
    return temp.length > 1 ? temp.substring(temp.length-2).toUpperCase() : temp.toUpperCase();
  }

//  var firestore = new FireStoreData();

  @override
  Widget build(BuildContext context) {
//        FireStoreData();
//  firestore.initializeFlutterFireAndGetConfig();

  var userData = locator<StorageLocal>().userData;
    return Scaffold(
        resizeToAvoidBottomInset: false,
//        backgroundColor: AppTheme.backgroundMainNightmode,
        body:
        Container(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              //header portrait
              if (MediaQuery.of(context).orientation == Orientation.portrait)
                Padding(
                  padding: EdgeInsets.fromLTRB(
                      5.0,
                      Device.get().hasNotch ? 50 : 30,
                      8, 10),

                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      ButtonBack.backButtonSimple(backgroundColor: AppTheme.orange, iconColor: Colors.white),

                      //title
                      Text('setting_scr_title'.tr.toUpperCase(),
                          style: Theme.of(context).textTheme.headline6!.copyWith(fontFamily: 'Montserrat', fontSize: 16, fontWeight: FontWeight.bold)),

                      ButtonBack.holderHeader()
                    ],
                  ),
                ),

              //content
              //avatar
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    margin: const EdgeInsets.only(bottom: 10.0, top: 0, left: 30),
                    padding: EdgeInsets.all(3),
//                    height: MediaQuery.of(context).size.width / 3,
//                    width: MediaQuery.of(context).size.width / 3,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        //border: Border.all(color: AppTheme.mainColor.withOpacity(.7),width: 1),
                        color: Colors.grey.shade200,
                        boxShadow: [
                          BoxShadow(
                              color: Color(0x575450).withOpacity(.052),
                              offset: Offset(0, 3),
                              blurRadius: 10,
                              spreadRadius: 3)
                        ]),
                    child: CircleAvatar(
                      radius: 50.0,
                      child: Image.network(
                        Configs.hostName + userData!.avatar.toString(), //image,
                        fit: BoxFit.fitWidth,
                        loadingBuilder: (BuildContext? context,
                            Widget? child,
                            ImageChunkEvent? loadingProgress) {
                          if (loadingProgress == null) return child!;
                          return Container(
                            height: 35,
                            width: 35,
                            alignment: Alignment.center,
                            child: CircularProgressIndicator(
                              color: AppTheme.orange,
                              value: loadingProgress.expectedTotalBytes !=
                                  null
                                  ? loadingProgress
                                  .cumulativeBytesLoaded /
                                  loadingProgress.expectedTotalBytes!
                                      .toInt()
                                  : null,
                            ),
                          );
                        },
                        errorBuilder: (BuildContext? context,
                            Object? exception, StackTrace? stackTrace) {
                          return  CircleAvatar(
                            radius: 50.0,
                            backgroundImage: AssetImage(
                              'assets/images/default-user.jpg',
                            ),
                          );
                        },
                      ),
                    )

                  ),

                  if (userData!= null)
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        //user info
                        Padding(
                          padding: const EdgeInsets.only(right: 0.0),
                          child: Container(
                            // width: MediaQuery.of(context).size.width * 2 / 3 - 40,
                            child: Text(userData.fullname.toString().toUpperCase(),
                              style: AppTheme.subtitleBoldLightMode.copyWith(color: AppTheme.orange),),
                          ),
                        ),

                      ],
                    ),
                  ),

                  SizedBox(width: 20,)
                ],
              ),


              Divider(),

              Text("setting_scr_des_security".tr, style: AppTheme.noteAllPageText,),

//              biometricSettingTile(context),

              //doi mat khau
              Container(
                margin: EdgeInsets.fromLTRB(30, 5, 30, 0),
                decoration: BoxDecoration(
                  color: Theme.of(context).bottomAppBarColor,
                  borderRadius: BorderRadius.circular(15),
                ),
                child: ListTile(
                  onTap: (){
                    changePassword(context);
                  },
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.vpn_key_sharp, color: Colors.blue,),
                      SizedBox(width: 15,),
                      Text("setting_scr_changepass".tr,
                        style: Theme.of(context).textTheme.button,),
                    ],
                  ),

                ),
              ),


              // de-active active button
              Container(
                margin: EdgeInsets.fromLTRB(30, 5, 30, 0),
                decoration: BoxDecoration(
                  color: Theme.of(context).bottomAppBarColor,
                  borderRadius: BorderRadius.circular(15),
                ),
                child: ListTile(
                  onTap: (){
                    deactivateConfirmDialog(context);
                  },
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.person_remove_alt_1_rounded, color: Colors.red,),
                      SizedBox(width: 15,),
                      Text("home_deactivate".tr,
                        style: Theme.of(context).textTheme.button,),
                    ],
                  ),

                ),
              ),


              SizedBox(height: 15,),

              Text("setting_scr_des_customize".tr, style: AppTheme.noteAllPageText,),

              ///theme mode
              // Container(
              //   margin: EdgeInsets.fromLTRB(30, 5, 30, 0),
              //   decoration: BoxDecoration(
              //     color: Theme.of(context).bottomAppBarColor,
              //     borderRadius: BorderRadius.circular(15),
              //   ),
              //   child: ListTile(
              //     onTap: (){
              //       chooseThemeModeDialog(context);
              //     },
              //     title: Row(
              //       mainAxisAlignment: MainAxisAlignment.center,
              //       children: [
              //         Icon(Get.isDarkMode ? Icons.mode_night : Icons.light_mode,
              //           color: Colors.blue,),
              //         SizedBox(width: 15,),
              //         Text('setting_scr_thememode'.tr,
              //           style: Theme.of(context).textTheme.button,),
              //       ],
              //     ),
              //
              //   ),
              // ),

            ///change lang
              Container(
                margin: EdgeInsets.fromLTRB(30, 5, 30, 0),
                decoration: BoxDecoration(
                  color: Theme.of(context).bottomAppBarColor,
                  borderRadius: BorderRadius.circular(15),
                ),
                child: ListTile(
                  onTap: (){
                    // chooseLanguageDialog(context);
                    Get.to(()=>ChooseLanguageView(isBackable: true,), transition: Transition.fadeIn, duration: Duration(milliseconds: 500)); //test

                  },
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.language, color: Colors.blue,),
                      SizedBox(width: 15,),
                      Text('setting_scr_language'.tr,
                        style: Theme.of(context).textTheme.button,),
                    ],
                  ),

                ),
              ),

              //logout
              Container(
                margin: EdgeInsets.fromLTRB(30, 25, 30, 0),
                decoration: BoxDecoration(
                  color: Colors.red.withOpacity(.3),
                  borderRadius: BorderRadius.circular(15),
                ),
                child: ListTile(
                  onTap: (){
//                    mainController.clearUserData();
                    locator<StorageLocal>().clearUserDataToStorage();
//                    Get.offAllNamed("/login");

                    Get.offAll(()=> HomeView());
                    ToastNotify.showSuccessToast(message: "Đăng xuất thành công", isCenter: true);
                  },
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.logout, color: Colors.red,),
                      SizedBox(width: 15,),
                      Text('setting_scr_logout'.tr,
                        style: Theme.of(context).textTheme.button,),
                    ],
                  ),

                ),
              ),
              

              Spacer(),

              Padding(
                padding: const EdgeInsets.only(bottom: 10.0),
                child: Text(Configs.versionName, style: TextStyle(
                  fontStyle: FontStyle.italic,
                  fontSize: 12,
                  letterSpacing: -0.05,
                  color: Colors.grey,
                ),),
              )
            ],
          )
        ),

    );
  }


  //CHANGE PASSWORD
  void changePassword(context) {
    var _curentPassword ="";
    var _newPassword ="";
    var _newPasswordConfirm ="";
    var _validateMessage = 'setting_pop_empt_allpass'.tr;

    StateSetter _setState;
//    var passwordState = false;
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
                _setState = setState;
                return Stack(
                  clipBehavior: Clip.none, children: <Widget>[
                    Form(
                      //onChanged: () {Form.of(primaryFocus.context).save();},
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Center(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                "setting_scr_changepass".tr,
                                style: AppTheme.headline3,
                              ),
                            ),
                          ),
                          //Center(child: GradientLine(200, 2, AppTheme.mainColorLeft, 50)),
                          TextFormField(
                            style: TextStyle(color: Colors.black87, fontSize: 19),
                            textAlign: TextAlign.left,
                            obscureText: true,
                            decoration: InputDecoration(
                              hintText: "setting_pop_tier_currpass".tr,
                              hintStyle: TextStyle(
                                  fontSize: 15,
                                  //fontWeight: FontWeight.bold,
                                  color: AppTheme.grey),
                              enabledBorder: UnderlineInputBorder(
                                  borderSide:
                                  new BorderSide(color: Colors.grey)),
                            ),
                            onChanged: (String value) {
                              _curentPassword = value;
//                              print("_curentPassword: " + _curentPassword);
                              setState(() {
//                                passwordState = _curentPassword != "" && _newPassword != "" && _newPassword == _newPasswordConfirm;
                                _validateMessage = validateInputPassword(_curentPassword, _newPassword, _newPasswordConfirm);
                              });
                            },
                          ),
                          SizedBox(height: 5,),
                          TextFormField(
                            style: TextStyle(color: Colors.black87, fontSize: 19),
                            textAlign: TextAlign.left,
                            obscureText: true,
                            decoration: InputDecoration(
                              hintText: "setting_pop_tier_newpass".tr,
                              hintStyle: TextStyle(
                                  fontSize: 15,
                                  //fontWeight: FontWeight.bold,
                                  color: AppTheme.grey),
                              enabledBorder: UnderlineInputBorder(
                                  borderSide:
                                  new BorderSide(color: Colors.grey)),
                            ),
                            onChanged: (String value) {
                              _newPassword = value;
//                              print("Value for field saved as " + _newPassword);
                              setState(() {
//                                passwordState = _curentPassword != "" && _newPassword != "" && _newPassword == _newPasswordConfirm;
                                _validateMessage = validateInputPassword(_curentPassword, _newPassword, _newPasswordConfirm);
                              });
                            },
                          ),
                          SizedBox(height: 8,),
                          TextFormField(
                            style: TextStyle(color: Colors.black87, fontSize: 19),
                            textAlign: TextAlign.left,
                            obscureText: true,
                            decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                  borderSide:
                                  new BorderSide(color: Colors.grey)),
                              hintText: "setting_pop_tier_conf_newpass".tr,
                              hintStyle: TextStyle(
                                  fontSize: 15,
                                  //fontWeight: FontWeight.bold,
                                  color: AppTheme.grey),
                            ),
                            onChanged: (String value) {
                              _newPasswordConfirm = value;
//                              print("_newPasswordConfirm: " + _newPasswordConfirm);
                              setState(() {
//                                passwordState = _curentPassword != "" && _newPassword != "" && _newPassword == _newPasswordConfirm;
                                _validateMessage = validateInputPassword(_curentPassword, _newPassword, _newPasswordConfirm);
                              });
                            },
                          ),
                          SizedBox(height: 10,),
                          if (_validateMessage != "")
                            Container(
                              padding: const EdgeInsets.all(5),
                              width: MediaQuery.of(context).size.width,
                              color: Colors.grey.shade200,
                              child: Center(
                                child: Text(_validateMessage.toString(),
                                  style: TextStyle(fontSize: 13,
                                      color: Colors.redAccent,
                                      fontStyle: FontStyle.italic),
                                ),
                              ),
                            )
                          else
                            Container(
                              width: MediaQuery.of(context).size.width,
                            ),

                          SizedBox(height: 10,),

                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              ButtonTheme(
                                minWidth: 100,
                                child: MaterialButton(
                                  child: Text(
                                    " ${'text_update'.tr} ",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                  color: _validateMessage == ""
                                      ? AppTheme.mainColorLeft
                                      : Colors.grey,
                                  shape: StadiumBorder(),
                                  onPressed: () {
                                    if (_validateMessage != ""){

                                      //start send request
                                      _verifyAndChangePassUser(_curentPassword,_newPassword).then((value) {
                                        if (value){
//                                          Common.showFlushBar(context, "Thay đổi mật khẩu thành công", Configs.ok);
                                          ToastNotify.showSuccessToast(message: "setting_changepass_success".tr, isCenter: true);
                                          Navigator.of(context).pop();
                                        }
                                        else
                                          ToastNotify.showFailToast(message: "setting_changepass_fail".tr, isCenter: true);
//                                          Common.showFlushBar(context, "Thay đổi mật khẩu không thành công", Configs.error);
                                      });
                                      FocusScope.of(context).unfocus();
                                    }
                                  },
                                ),
                              ),
                              TextButton(
                                child: Text("cancel".tr),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                );
              },
            ),
          );
        });
  }

  String validateInputPassword(oldPass, newPass, confirmNewPass){
    if (oldPass == "")
      return "setting_pop_empt_currpass".tr;
    else if (newPass == "")
      return "setting_pop_empt_newpass".tr;
    else if (confirmNewPass == "")
      return "setting_pop_empt_conf_newpass".tr;
    else if (newPass != confirmNewPass)
      return "setting_pop_err_notmatch".tr;
    return "";
  }

  Future<bool> _verifyAndChangePassUser(String password, String newPass) async {
    Map map =
    {
      "access_token": storage.userData!.token.toString(),
      "new_password": newPass,
      "old_password": password,
    };



    print(map.toString());
//    print(mainController.token);

    String url = Configs.hostName + Configs.apiURI['change-password']!;
    var _response = await HTTPController.PostAuthorization(url, map, storage.userData!.token.toString());
    print(_response.toString());
    bool _status = json.decode(_response)["success"];
    print(json.decode(_response)["error"]);
    if (_status == true) {
      //verify pass
      return true;
    } else {
      //verify fail
      return false;
    }
  }


  //LANGGUAGE
  chooseLanguageDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        backgroundColor: Colors.white,

        contentPadding: EdgeInsets.all(5),
        content: StatefulBuilder(
          builder: (BuildContext context, StateSetter setState){
            return Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      'setting_scr_popup_language'.tr,
                      style: AppTheme.titleLightMode,
                      textAlign: TextAlign.center,
                    ),
                    Divider(),
                    Padding(
                      padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                      child: new MaterialButton(
                        splashColor: Colors.blue,
                        height: 45,
                        color: Colors.blueGrey.shade50,
                        child:
                        Container(
                          decoration: BoxDecoration(
                              border: Border.all(
//                                  color: mainController.language == 'en' ? Colors.blue : Colors.transparent
                              ),
                            borderRadius: BorderRadius.circular(5)
                          ),
                          width: double.infinity,
                          alignment: Alignment.center,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Icon(Icons.language_outlined, color: Colors.green, size: 28,),
                              SizedBox(width: 10,),
                              Text("English",style: AppTheme.subtitleBoldLightMode,
                              ),
                            ],
                          ),
                        ),
                        onPressed: () {
//                          if (mainController.language != 'en') {
//                            LocalizationService.changeLocale("en");
//
//                            mainController.language = 'en';
//                            mainController.backupSettingData();
//                            homeController.addAllFunction();
//                            ToastNotify.showSuccessToast(message: 'msg_change_lang'.tr, isBottom: true);
//
//                            Get.back();
//                          }else{
//                            ToastNotify.showInfoToast(message: 'msg_error_lang'.tr, isBottom: true);
//                          }

                        },
                      ),
                    ),

                    Padding(
                      padding: EdgeInsets.fromLTRB(20, 10, 20, 0),
                      child: new MaterialButton(
                        splashColor: Colors.redAccent,
                        height: 45,
                        color: Colors.blueGrey.shade50,
                        child:
                        Container(
                          decoration: BoxDecoration(
                              border: Border.all(
//                                  color: mainController.language == 'vi' ? Colors.blue : Colors.transparent
                              ),
                              borderRadius: BorderRadius.circular(5)
                          ),
                          width: double.infinity,
                          alignment: Alignment.center,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Container(
                                  child: Icon(Icons.star, color: Colors.yellow, size: 20,),
                                padding: const EdgeInsets.all(2),
                                margin: const EdgeInsets.only(left: 3),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  color: Colors.red
                                ),
                              ),
                              SizedBox(width: 10,),
                              Text("Tiếng Việt",style: AppTheme.subtitleBoldLightMode,
                              ),
                            ],
                          ),
                        ),
                        onPressed: () {
//                          if (mainController.language != 'vi'){
//                            LocalizationService.changeLocale("vi");
//
//                            mainController.language = 'vi';
//                            mainController.backupSettingData();
//                            homeController.addAllFunction();
//
//                            ToastNotify.showSuccessToast(message: 'msg_change_lang'.tr, isBottom: true );
//
//                            Get.back();
//                          }else{
//                            ToastNotify.showInfoToast(message: 'msg_error_lang'.tr, isBottom: true);
//                          }
                        },
                      ),
                    ),

                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  //THEME MODE
  chooseThemeModeDialog(BuildContext context) {
    var currentThemeMode = ThemeService.currentTheme;

    showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        backgroundColor: Colors.white,

        contentPadding: EdgeInsets.all(5),
        content: StatefulBuilder(
          builder: (BuildContext context, StateSetter setState){
            return Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      'setting_scr_popup_theme'.tr,
                      style: AppTheme.titleLightMode,
                      textAlign: TextAlign.center,
                    ),
                    Divider(),
                    Padding(
                      padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                      child: new MaterialButton(
                        splashColor: Colors.blue,
                        height: 45,
                        color: Colors.blueGrey.shade50,
                        child:
                        Container(
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: currentThemeMode == ThemeService.lightmode ? Colors.blue : Colors.transparent
                              ),
                              borderRadius: BorderRadius.circular(5)
                          ),
                          width: double.infinity,
                          alignment: Alignment.center,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Container(
                                child: Icon(Icons.light_mode, color: Colors.yellow, size: 20,),
                                padding: const EdgeInsets.all(2),
                                margin: const EdgeInsets.only(left: 3),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20),
                                    color: Colors.blue.shade300
                                ),
                              ),
                              SizedBox(width: 10,),
                              Text("setting_scr_popup_theme_light".tr, style: AppTheme.subtitleBoldLightMode,
                              ),
                            ],
                          ),
                        ),
                        onPressed: () {

                          ThemeService().changeThemeMode(ThemeService.lightmode);
                          Get.back();
                        },
                      ),
                    ),

                    Padding(
                      padding: EdgeInsets.fromLTRB(20, 10, 20, 0),
                      child: new MaterialButton(
                        splashColor: Colors.redAccent,
                        height: 45,
                        color: Colors.blueGrey.shade50,
                        child:
                        Container(
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: currentThemeMode == ThemeService.nightmode ? Colors.blue : Colors.transparent
                              ),
                              borderRadius: BorderRadius.circular(5)
                          ),
                          width: double.infinity,
                          alignment: Alignment.center,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Container(
                                child: Icon(Icons.mode_night, color: Colors.yellow, size: 20,),
                                padding: const EdgeInsets.all(2),
                                margin: const EdgeInsets.only(left: 3),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20),
                                    color: Colors.black
                                ),
                              ),
                              SizedBox(width: 10,),
                              Text("setting_scr_popup_theme_night".tr, style: AppTheme.subtitleBoldLightMode,
                              ),
                            ],
                          ),
                        ),
                        onPressed: () {
                          ThemeService().changeThemeMode(ThemeService.nightmode);
                          Get.back();

                        },
                      ),
                    ),

                    //system setting
                    Padding(
                      padding: EdgeInsets.fromLTRB(20, 10, 20, 0),
                      child: new MaterialButton(
                        splashColor: Colors.redAccent,
                        height: 45,
                        color: Colors.blueGrey.shade50,
                        child:
                        Container(
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: currentThemeMode == ThemeService.sysmode ? Colors.blue : Colors.transparent
                              ),
                              borderRadius: BorderRadius.circular(5)
                          ),
                          width: double.infinity,
                          alignment: Alignment.center,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Container(
                                child: Icon(Icons.perm_device_info, color: Colors.black54, size: 20,),
                                padding: const EdgeInsets.all(2),
                                margin: const EdgeInsets.only(left: 3),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20),
                                    color: Colors.blue.shade300
                                ),
                              ),
                              SizedBox(width: 10,),
                              Text("setting_scr_popup_theme_sys".tr, style: AppTheme.subtitleBoldLightMode,
                              ),
                            ],
                          ),
                        ),
                        onPressed: () {
                          ThemeService().changeThemeMode(ThemeService.sysmode);
                          Get.back();

                        },
                      ),
                    ),

                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  //DEACTIVATE ACCOUNT
  deactivateConfirmDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        backgroundColor: Colors.white,

        contentPadding: EdgeInsets.all(5),
        content: StatefulBuilder(
          builder: (BuildContext context, StateSetter setState){
            return Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      "home_deactivate".tr.toUpperCase(),
                      style: AppTheme.titleLightMode,
                      textAlign: TextAlign.center,
                    ),
                    Divider(),
                    Text("home_deactivate_confirm1".tr,
                      style: AppTheme.subtitleBoldHighlight,
                      textAlign: TextAlign.center,
                    ),
                    Text("home_deactivate_confirm2".tr,
                      style: AppTheme.noteAllPageText,
                      textAlign: TextAlign.center,
                    ),

                    Divider(),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        TextButton(
                            onPressed: (){
                              //message
                              ToastNotify.showFailToast(message: "hold_confirm".tr);
                            },
                            onLongPress: (){
                              //delete
                              controller.deactivateAccount().then((value) {
                                if (value){
                                  locator<StorageLocal>().clearUserDataToStorage();

                                  Get.offAll(()=> HomeView());

                                  ToastNotify.showSuccessToast(message: "home_deactivate_success".tr,);
                                } else {
                                  Navigator.pop(context);

                                  ToastNotify.showFailToast(message: "home_deactivate_fail".tr,);
                                  print("koko");
                                }
                              });
                            },
                            child: Text("delete".tr,)),
                        TextButton(onPressed: (){
                          Navigator.pop(context);
                        }, child: Text("back".tr,
                          style: TextStyle(color: Colors.grey),))
                      ],
                    )
                  ],
                ),
              ),
            );
          },
        ),
      ),
    ).then((value) {
      // save setting

    });
  }


}
