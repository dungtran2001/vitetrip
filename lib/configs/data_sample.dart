
class DataSample{
  static const country = {
    "success": true,
    "data": [
      {"id": "vn", "name": "Viet Nam", "ranking": 4.5, "lat": "1.22", "long": "12.2", "rank_amount": 55, "total_download": 76, "version_id": 8, "description": "description Việt Nam", "images": ["assets/images/locations/hoian2.jpg"]},
      {"id": "laos", "name": "Laos", "ranking": 4.2, "lat": "20.2", "long": "20.2", "rank_amount": 17, "total_download": 186, "version_id": 21, "description": "description Laos", "images": ["assets/images/locations/danang2.jpg"]},
      {"id": "sing", "name": "Singapore", "ranking": 4.3, "lat": "1.47", "long": "14.7", "rank_amount": 35, "total_download": 22, "version_id": 10, "description": "description Sing", "images": ["assets/images/locations/hanoi1.jpg"]},
    ]
  };

  static const locations = {
    "success": true,
    "data": [
      {"id": "1", "name": "Hội An", "location_id": "hoi_an", "ranking": 3.5, "lat": "1.22", "long": "12.2", "rank_amount": 55, "total_download": 76, "version_id": 8, "description": "Quảng Nam, Việt Nam", "image": "assets/images/locations/hoian2.jpg"},
      {"id": "2", "name": "Đà Nẵng", "location_id": "da_nang", "ranking": 4.5, "lat": "20.2", "long": "20.2", "rank_amount": 17, "total_download": 186, "version_id": 21, "description": "Việt Nam", "image": "assets/images/locations/danang2.jpg"},
      {"id": "3", "name": "Hà Nội","location_id": "ha_noi",  "ranking": 2.3, "lat": "1.47", "long": "14.7", "rank_amount": 35, "total_download": 22, "version_id": 10, "description": "Việt Nam", "image": "assets/images/locations/hanoi1.jpg"},
      {"id": "4", "name": "Ninh Bình","location_id": "ninh_binh",  "ranking": 4.8, "lat": "33.0", "long": "33.0", "rank_amount": 52, "total_download": 63, "version_id": 29, "description": "Việt Nam", "image": "assets/images/locations/baidinh1.jpg"},
      {"id": "5", "name": "Hạ Long","location_id": "ha_long",  "ranking": 5.0, "lat": "1.93", "long": "19.3", "rank_amount": 33, "total_download": 27, "version_id": 19, "description": "Việt Nam", "image": "assets/images/locations/halong1.jpg"},
      {"id": "6", "name": "TP Hồ Chí Minh","location_id": "tp_ho_chi_minh",  "ranking": 5.0, "lat": "19.3", "long": "19.3", "rank_amount": 33, "total_download": 27, "version_id": 19, "description": "Việt Nam", "image": "assets/images/locations/saigon1.jpg"},
      {"id": "7", "name": "Dinh Độc Lập","location_id": "dinh_doc_lap",  "ranking": 5.0, "lat": "19.3", "long": "19.3", "rank_amount": 33, "total_download": 27, "version_id": 19, "description": "TP Hồ Chí Minh, Việt Nam", "image": "assets/images/locations/dinhdoclap1.jpg"},
    ]
  };

  static const objectsHoiAn = {
    "success": true,
    "data": [
      {"id": "1", "label": "diepdongnguyen_bang_hieu", "name": "Bảng hiệu Diệp Đồng Nguyên", "ranking": 3.5, "lat": "1.22", "long": "12.2", "rank_amount": 55, "total_download": 76, "version_id": 1, "location": "Hội An, Việt Nam", "description": "Hiện vật tại Hội An, Việt Nam", "image": "assets/images/objects/hoian/diepdongnguyen_bang_hieu.jpg"},
      {"id": "2", "label": "diepdongnguyen_shell", "name": "Diệp Đồng Nguyên Shell", "ranking": 4.5, "lat": "20.2", "long": "20.2", "rank_amount": 17, "total_download": 186, "version_id": 1, "location": "Hội An, Việt Nam",  "description": "Hiện vật tại Hội An, Việt Nam", "image": "assets/images/objects/hoian/diepdongnguyen_shell.jpg"},
      {"id": "3", "label": "ditich_chua_ba_mu", "name": "Chùa ba Mụ", "ranking": 2.3, "lat": "1.47", "long": "14.7", "rank_amount": 35, "total_download": 22, "version_id": 1, "location": "Việt Nam", "description": "Hiện vật tại Việt Nam", "image": "assets/images/objects/hoian/ditich_chua_ba_mu.jpg"},
      {"id": "4", "label": "ditich_hoi_quan_trieu_chau", "name": "Chùa Cầu", "ranking": 4.8, "lat": "33.0", "long": "33.0", "rank_amount": 52, "total_download": 63, "version_id": 1, "location": "Việt Nam", "description": "Hiện vật tại Việt Nam", "image": "assets/images/objects/hoian/ditich_chua_cau.jpg"},
      {"id": "5", "label": "nhaco_19leloi", "name": "Nhà cổ - 19 Lê Lợi", "ranking": 5.0, "lat": "1.93", "long": "19.3", "rank_amount": 33, "total_download": 27, "version_id": 1, "location": "Việt Nam", "description": "Hiện vật tại Việt Nam", "image": "assets/images/objects/hoian/nhaco_19leloi.jpg"},
      {"id": "6", "label": "tuong_kazik", "name": "Tượng Kazik", "ranking": 5.0, "lat": "19.3", "long": "19.3", "rank_amount": 33, "total_download": 27, "version_id": 1, "location": "Việt Nam", "description": "Hiện vật tại Việt Nam", "image": "assets/images/objects/hoian/tuong_kazik.jpg"},
    ]
  };

  static const objectsDinhDocLap = {
    "success": true,
    "data": [
      {"id": "1", "label": "hvn_bang_su_kien_nem_bong", "name": "Bảng sự kiện ném bom", "ranking": 3.5, "lat": "1.22", "long": "12.2", "rank_amount": 55, "total_download": 76, "version_id": 7, "location": "Hội An, Việt Nam", "description": "Hiện vật tại Hội An, Việt Nam", "image": "assets/images/objects/dinhdoclap/hvn_bang_su_kien_nem_bom.jpg"},
      {"id": "2", "label": "hvn_bang_truc_thang_uh1", "name": "Bảng trực thăng UH1", "ranking": 4.5, "lat": "20.2", "long": "20.2", "rank_amount": 17, "total_download": 186, "version_id": 7, "location": "Hội An, Việt Nam", "description": "Hiện vật tại Hội An, Việt Nam", "image": "assets/images/objects/dinhdoclap/hvn_bang_truc_thang_uh1.jpg"},
      {"id": "3", "label": "hvn_binh_gom_xanh", "name": "Bình gốm xanh", "ranking": 2.3, "lat": "1.47", "long": "14.7", "rank_amount": 35, "total_download": 22, "version_id": 7, "location": "Việt Nam", "description": "Hiện vật tại Việt Nam", "image": "assets/images/objects/dinhdoclap/hvn_binh_gom_xanh.jpg"},
      {"id": "4", "label": "hvl_buc_binh_ngo", "name": "Bức Bình Ngô", "ranking": 4.8, "lat": "33.0", "long": "33.0", "rank_amount": 52, "total_download": 63, "version_id": 7, "location": "Việt Nam", "description": "Hiện vật tại Việt Nam", "image": "assets/images/objects/dinhdoclap/hvl_buc_binh_ngo.jpg"},
      {"id": "5", "label": "hvl_buc_hai_nang_kieu", "name": "Bức hai nàng Kiều", "ranking": 5.0, "lat": "1.93", "long": "19.3", "rank_amount": 33, "total_download": 27, "version_id": 7, "location": "Việt Nam", "description": "Hiện vật tại Việt Nam", "image": "assets/images/objects/dinhdoclap/hvl_buc_hai_nang_kieu.jpg"},
      {"id": "6", "label": "hvl_buc_quoc_to", "name": "Bức Quốc to", "ranking": 5.0, "lat": "19.3", "long": "19.3", "rank_amount": 33, "total_download": 27, "version_id": 7, "location": "Việt Nam", "description": "Hiện vật tại Việt Nam", "image": "assets/images/objects/dinhdoclap/hvl_buc_quoc_to.jpg"},
      {"id": "7", "label": "hvn_chan_voi", "name": "Chạn voi", "ranking": 5.0, "lat": "19.3", "long": "19.3", "rank_amount": 33, "total_download": 27, "version_id": 7, "location": "Việt Nam", "description": "Hiện vật tại Việt Nam", "image": "assets/images/objects/dinhdoclap/hvn_chan_voi.jpg"},
      {"id": "8", "label": "hvn_thuyen_go", "name": "Thuyền gỗ", "ranking": 5.0, "lat": "19.3", "long": "19.3", "rank_amount": 33, "total_download": 27, "version_id": 7, "location": "Việt Nam", "description": "Hiện vật tại Việt Nam", "image": "assets/images/objects/dinhdoclap/hvn_thuyen_go.jpg"},
      {"id": "9", "label": "hvn_thuyen_long_kinh", "name": "Thuyền lòng kính", "ranking": 5.0, "lat": "19.3", "long": "19.3", "rank_amount": 33, "total_download": 27, "version_id": 7, "location": "Việt Nam", "description": "Hiện vật tại Việt Nam", "image": "assets/images/objects/dinhdoclap/hvn_thuyen_long_kinh.jpg"},
      {"id": "10", "label": "hvl_xe_tang_843", "name": "Xe Tăng 843", "ranking": 5.0, "lat": "19.3", "long": "19.3", "rank_amount": 33, "total_download": 27, "version_id": 7, "location": "Việt Nam", "description": "Hiện vật tại Việt Nam", "image": "assets/images/objects/dinhdoclap/hvl_xe_tang_843.jpg"},
      {"id": "11", "label": "hvn_binh_gom_trang", "name": "Bình gốm trắng", "ranking": 5.0, "lat": "19.3", "long": "19.3", "rank_amount": 33, "total_download": 27, "version_id": 7, "location": "Việt Nam", "description": "Hiện vật tại Dinh Độc Lập Việt Nam", "image": "assets/images/objects/dinhdoclap/hvn_binh_gom_trang.jpg"},
      {"id": "12", "label": "hvn_binh_gom_trang_xanh", "name": "Bình gốm trắng xanh", "ranking": 5.0, "lat": "19.3", "long": "19.3", "rank_amount": 33, "total_download": 27, "version_id": 7, "location": "Việt Nam", "description": "Hiện vật tại Dinh ĐộC LậP Việt Nam", "image": "assets/images/objects/dinhdoclap/hvn_binh_gom_trang_xanh.jpg"},
    ]
  };


  static const authorGuide = {
    "success": true,
    "data": [
      {
        "id": "1",
        "name": "Basic content",
        "ranking": 4.5,
        "rank_amount": 55,
        "totalVote": 15,
        "total_download": 76,
        "price": 0.0,
        "description": "Nội dung đề xuẩt mặc định",
        "image": "https://vnn-imgs-f.vgcloud.vn/2020/05/21/23/du-lich.jpg",
        "audio": "",
        "location": "Hội An, Việt Nam",
        "feedbacks": [
          {
            "id": "0011",
            "name": "Người dùng 1",
            "ranking": 4.5,
            "like": 12,
            "video": "",
            "description": "Good content",
            "image": "https://baoquocte.vn/stores/news_dataimages/tranlieu/092021/24/10/1646_phu-quoc_optimized.jpg?rt=20210924101647",
          },
          {
            "id": "00121",
            "name": "Người dùng 2",
            "ranking": 4.0,
            "like": 2,
            "video": "",
            "description": "Rất hay và hấp dẫn",
            "image": "https://statics.vinpearl.com/diem-du-lich-05_1632671806.jpg",
          }
        ]
      },
      {
        "id": "22",
        "name": "Nhà Văn A",
        "ranking": 4.9,
        "rank_amount": 35,
        "totalVote": 24,
        "total_download": 35,
        "price": 6.0,
        "description": "Nội dung do tác giả biên soạn",
        "image": "https://dulichviet.com.vn/images/bandidau/images/NOI-DIA/NINH-BINH/du-lich-he-tour-ninh-binh-gia-tot-du-lich-viet.jpg",
        "audio": "",
        "location": "Hội An, Việt Nam",
        "feedbacks": [
          {
            "id": "0011",
            "name": "Người dùng 3",
            "ranking": 4.5,
            "like": 12,
            "video": "",
            "description": "Quá xuất sắc luôn",
            "image": "https://cdn.cet.edu.vn/wp-content/uploads/2018/03/du-lich-la-hoat-dong-pho-bien.jpg",
          },
        ]
      },

    ]
  };
}


