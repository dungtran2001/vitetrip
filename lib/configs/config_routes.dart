
import '../views/login/login_screen.dart';
import '/views/home/home.dart';
import '/views/splashscreen/splash_screen.dart';

import '../views/welcome/welcome_screen.dart';
import 'package:get/get.dart';

class AppRoutes {
  static final routes = [
    GetPage(
      name: '/splashscreen',
      page: () => SplashScreen(),
    ),
    GetPage(
      name: '/home',
      page: () => HomeView(),
    ),

    GetPage(
      name: '/welcome',
      page: () => WelcomeScreen(),
    ),
 GetPage(
      name: '/login',
      page: () => LoginScreen(isBackable: true,),
    ),

  ];
}
