class Configs {
  static String appNameFirst = "AIGuide";
  static String appNameLast = "X";

  static String versionName = "\u00A9AI GuideX - ver 1.1.0(k)";

  static int versionNum = 1; //currentversioncodereceivefromfirebasestore

  static double thresholdScore = 0.5;

  static const int elementInPage = 8;

  static const int timeScanning = 70; //100 //5s

  ///MAP
  static String mapApikey =
      'AIzaSyBI6SZCWn7o9DDZJWNDP-XNwZICrhRyC9s'; //'AIzaSyA5SETWVxChJHPN_kAh2ROlfKkmlUo0PBk';

  static double zoomInAmountNormal = 15;
  static double zoomInAmount3km = 13.5;

  static double zoomInAmount5km = 12.5;

  static double tiltFocusAmount = 30;
  static double bearingFocusAmount = 25;

  ///

  // static String hostName = 'http://45.32.117.237:1234'; //dev
  static String hostName = "http://45.32.117.237"; // production

  static String urlAbout = 'https://www.mitta.com.vn/';

  static Map<String, String> apiURI = {
    'login': '/api/auth/login',
    'register': '/api/user/register',

    'get-nations': '/api/nation',
    'get-location': '/api/location',
    'get-content': '/api/content',
    'get-artifact': '/api/artifact',
    'get-feedback': '/api/feedback',
    'get-author': '/api/author',
    'get-province': '/api/province',
    'get-archiverment': '/api/archiverment',
    'add-archiverment': '/api/archiverment/add',
    'get-category': '/api/catalogue',
    'add-object-report': '/api/add-object-report',
    'add-rating-location': '/api/location/rating',
    'deactivate-account': '/api/users/',

    //OTP
    'otp-send': '/api/otp/send-code',
    'otp-verify': 'api/otp/send-code',
  };

  //status

  static bool fail = false;
  static bool success = true;

  static int error = 0;
  static int ok = 1;
  static int warning = 2;

  static String deviceToken = "";

  //version code
  static int versionCodeIOs = 2;
  static int versionCodeApk = 2;
}

enum MarkerType { mini, normal, big }
