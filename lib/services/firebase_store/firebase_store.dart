
import 'dart:convert';
import 'dart:io';


import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart' as getx;

import '../../configs/configs.dart';


class FireStoreData{

  FireStoreData(){
//    initializeFlutterFireAndGetConfig();

  }


  //FIRE STORE
  // Define an async function to initialize FlutterFire
  Future<void> initializeFlutterFireAndGetConfig() async {
    try {
      // Wait for Firebase to initialize and set `_initialized` state to true
     await Firebase.initializeApp();

      firestore = FirebaseFirestore.instance;
      await getFireStoreData();//.then((_) => GetServerInfo());
      print("-- initializeFlutterFire: success");

    } catch(e) {
      print("-- ERROR initializeFlutterFire: $e");
    }
  }

  FirebaseFirestore? firestore;
  Future<void> getFireStoreData() async {
    //For Android and Apple platforms, offline persistence is enabled by default. To disable persistence, set the PersistenceEnabled option to false.
    FirebaseFirestore.instance.settings = const Settings(persistenceEnabled: false);

    await FirebaseFirestore.instance
        .collection('config')
        .doc('server')
        .get()
        .then((DocumentSnapshot documentSnapshot) {
      if (documentSnapshot.exists) {
        print('Document data: ${documentSnapshot.data()}');
        Map<String, dynamic> data = documentSnapshot.data() as Map<String, dynamic> ;
        _apiServer = data['public'] ?? "";
//        print("DocumentSnapshot ${data['public']}");

      } else {
        print('--ERROR Server ip document does not exist on the database');
      }
    });

    //getversion
    await FirebaseFirestore.instance
        .collection('config')
        .doc('version')
        .get()
        .then((DocumentSnapshot documentSnapshot){
      if (documentSnapshot.exists){
        print('Documentdata:${documentSnapshot.data()}');
        Map<String, dynamic> data = documentSnapshot.data() as Map<String, dynamic> ;
        _versionCode=Platform.isIOS
            ?data['ios']??0
            :data['apk']??0;
        print("DocumentSnapshot${_versionCode}");
      }else{
        print('--ERRORversioncodedocumentdoesnotexistonthedatabase');
      }
    });

    setServerInfo();
  }

  int? _versionCode;
  String? _apiServer;
  Future<void> setServerInfo() async {
    try{
      if (_apiServer != ""){
        print("version code: $_versionCode, api server: $_apiServer");

        // Configs.hostName = _apiServer ?? "";
        Configs.versionNum = _versionCode ?? 0;
        //check maintain server
//        await checkForMaintain();

      }else{
        //show popup maintain
//        triggerMaintainApp();
      }

    }
    catch(e){
      print("--ERROR- GetServerInfo: $e");
      //show popup maintain
    }
  }
//


}