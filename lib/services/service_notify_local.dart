
import 'dart:async';
import 'dart:convert';

//import 'package:firebase_messaging/firebase_messaging.dart';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart' as scd;
//import 'package:flutter/scheduler.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';

import '../../main.dart';
import 'package:timezone/data/latest.dart' as tz;
import 'package:timezone/timezone.dart' as tz;
import 'package:http/http.dart' as http;

import '../configs/configs.dart';


class NotificationLocal {

  static final NotificationLocal _singleton = NotificationLocal._internal();

  factory NotificationLocal() => _singleton;
  NotificationLocal._internal() {
    init();
    initFCM();
  }
  static NotificationLocal get instance => _singleton;

//  var context;

  init(){
    initLocalPush();
    print("-- init notification");
  }

  initLocalPush(){
    var initializationSettingsAndroid =
    AndroidInitializationSettings('app_icon');
    var initializationSettingsIOs = DarwinInitializationSettings();
    var initSetttings = InitializationSettings(
        android: initializationSettingsAndroid,
        iOS: initializationSettingsIOs);

    flutterLocalNotificationsPlugin.initialize(initSetttings);
  }

  ///Firebase
  initFCM() async {
    FirebaseMessaging firebaseMessaging = FirebaseMessaging.instance;
    NotificationSettings settings = await firebaseMessaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );

    // print('User granted permission: ${settings.authorizationStatus}');

//    firebaseMessaging.configure(
//      onMessage: (Map<String, dynamic> message) async {
//        print("onMessage: "+message.toString());
//        createNotification(message['notification']['title'], message['notification']['body']);
//
//      },
//      onResume: (Map<String, dynamic> message) async {
//        print('onResume called: $message');
//      },
//      onLaunch: (Map<String, dynamic> message) async {
//        print('onLaunch called: $message');
//      },
//    );
    firebaseMessaging.getToken().then((token){
      // print(token);
      Configs.deviceToken = token.toString();
    });

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      print('Got a message whilst in the foreground!');
      print('Message data: ${message.data}');

      if (message.notification != null) {
        print('Message also contained a notification: ${message.notification}');
        if (message.notification?.android?.imageUrl != null){
          showPictureNotification(
              title: message.notification!.title.toString(),
              message: message.notification!.body.toString(),
              image: message.notification!.android!.imageUrl.toString()
          );
        }else{
          createNotification(message.notification!.title.toString(), message.notification!.body.toString());
        }
        print("onMessage: "+message.toString());
      }


    });
  }


  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();


  void createNotification(String title, String body) async {
    await showNotification(title, body);
//    await _showBigPictureNotification(title, body);
  }


  Future? onSelectNotification(String notify) {
    print("onSelect : $notify");
    return null;
  }

  var _identity=0;
  showNotification(String title, String description) async {
    var android = AndroidNotificationDetails(
        'id',
        'channel ',
        priority: Priority.high, importance: Importance.max);
    var iOS = DarwinNotificationDetails();
    var platform = new NotificationDetails(android: android, iOS: iOS);
    await flutterLocalNotificationsPlugin.show(
        _identity, title, description, platform);
    _identity++;
    print(_identity);
  }

  Future<void> showPictureNotification({
    String title = "Thông báo sự kiện",
    String message = "Sự kiện đã tới, hãy tham gia ngay!",
    String image = ""}) async {

    final String largeIconPath = await _downloadAndSaveFile(
        '${Configs.hostName}/media/logo.png', 'largeIcon');

    final String bigPicturePath = await _downloadAndSaveFile(image, 'eventImage');

    final BigPictureStyleInformation bigPictureStyleInformation = BigPictureStyleInformation(
        FilePathAndroidBitmap(bigPicturePath),
//        largeIcon: DrawableResourceAndroidBitmap("app_icon"),
        contentTitle: '<b>'+ title +'<//b>',
        htmlFormatContentTitle: true,
        summaryText: message, //body != null ? '<i>'+ body +'<//i>': '<i>Cảnh báo quan trọng!</i>',
        htmlFormatSummaryText: true
    );

    final AndroidNotificationDetails androidPlatformChannelSpecifics = AndroidNotificationDetails(
        'big text channel id',
        'big text channel name',
        importance: Importance.max,
        styleInformation: bigPictureStyleInformation);

    var iOSPlatformChannelSpecifics = DarwinNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
        android: androidPlatformChannelSpecifics,
        iOS: iOSPlatformChannelSpecifics
    );

    await flutterLocalNotificationsPlugin.show(
        0, title, message, platformChannelSpecifics);

  }

  Future<void> scheduleNotification({
    String title = "Thông báo sự kiện",
    String message = "Sự kiện đã tới, hãy tham gia ngay!",
    String image = "",
    int scheduleDays = 0,
    int scheduleHours = 0,
    int scheduleMinutes = 0}) async {
    tz.initializeTimeZones();
    var location = tz.getLocation('Europe/Warsaw');
    tz.setLocalLocation(location);

    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
      'repeatDailyAtTime channel id',
      'repeatDailyAtTime channel name',
      importance: Importance.max,
      ledColor: Color(0xFF3EB16F),
      ledOffMs: 1000,
      ledOnMs: 1000,
      enableLights: true,
    );
    var iOSPlatformChannelSpecifics = DarwinNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
        android: androidPlatformChannelSpecifics,
        iOS: iOSPlatformChannelSpecifics
    );

    await flutterLocalNotificationsPlugin.zonedSchedule(
      0,
      title,
      message,
      tz.TZDateTime.now(tz.local).add(Duration(days: scheduleDays, hours: scheduleHours, minutes: scheduleMinutes)),//currentDateTime,
      platformChannelSpecifics,
      androidAllowWhileIdle: true,
      uiLocalNotificationDateInterpretation: UILocalNotificationDateInterpretation.absoluteTime,
    );
  }

  Future<String> _downloadAndSaveFile(String url, String fileName) async {
    final Directory directory = await getApplicationDocumentsDirectory();
    final String filePath = '${directory.path}/$fileName';
    final http.Response response = await http.get(Uri.parse(url));
    final File file = File(filePath);
    await file.writeAsBytes(response.bodyBytes);
    return filePath;
  }

  Future<void> scheduleBigPictureNotification({
    String title = "Thông báo sự kiện",
    String message = "Sự kiện đã tới, hãy tham gia ngay!",
    String image = "",
    int scheduleDays = 0,
    int scheduleHours = 0,
    int scheduleMinutes = 0}) async {

    tz.initializeTimeZones();
    var location = tz.getLocation('Europe/Warsaw');
    tz.setLocalLocation(location);

    final String largeIconPath = await _downloadAndSaveFile(
        '${Configs.hostName}/media/logo.png', 'largeIcon');

    final String bigPicturePath = await _downloadAndSaveFile(image, 'eventImage');

    final BigPictureStyleInformation bigPictureStyleInformation = BigPictureStyleInformation(
        FilePathAndroidBitmap(bigPicturePath),
//        largeIcon: DrawableResourceAndroidBitmap("app_icon"),
        contentTitle: '<b>'+ title +'<//b>',
        htmlFormatContentTitle: true,
        summaryText: message, //body != null ? '<i>'+ body +'<//i>': '<i>Cảnh báo quan trọng!</i>',
        htmlFormatSummaryText: true
    );

    final AndroidNotificationDetails androidPlatformChannelSpecifics = AndroidNotificationDetails(
        'big text channel id',
        'big text channel name',
        importance: Importance.max,
        styleInformation: bigPictureStyleInformation);

    var iOSPlatformChannelSpecifics = DarwinNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
        android: androidPlatformChannelSpecifics,
        iOS: iOSPlatformChannelSpecifics
    );

//    await flutterLocalNotificationsPlugin.show(
//        0, 'big text title', 'silent body', platformChannelSpecifics);

    await flutterLocalNotificationsPlugin.zonedSchedule(
      0,
      title,
      message,
      tz.TZDateTime.now(tz.local).add(Duration(days: scheduleDays, hours: scheduleHours, minutes: scheduleMinutes)),//currentDateTime,
      platformChannelSpecifics,
      androidAllowWhileIdle: true,
      uiLocalNotificationDateInterpretation: UILocalNotificationDateInterpretation.absoluteTime,
    );
  }
}
