
import 'package:audio_service/audio_service.dart';
import 'package:get_it/get_it.dart';
import 'package:mitta/services/service_geolocation.dart';
import 'package:mitta/utils/ads_utils.dart';
import '/network/api/api.dart';
import 'audio_player/audio_handler.dart';
import 'audio_player/audio_manager.dart';
import 'service_storage.dart';

GetIt locator = GetIt.instance;
setupLocator() async {
  locator.registerLazySingleton(() => Api());
  locator.registerLazySingleton(() => StorageLocal());
  locator.registerLazySingleton(() => GeoLocationService());
  locator.registerLazySingleton(() => MediaManager());
  locator.registerSingleton<AudioHandler>(await initAudioService());
  locator.registerLazySingleton(() => AdsUtils());
}
