import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:mitta/model/location/model_country.dart';
import 'package:mitta/model/location/model_province.dart';
import 'package:permission_handler/permission_handler.dart';

import '../app_theme.dart';
import '../commons/common.dart';
import '../model/user/model_user_account.dart';

import '/model/user/model_me.dart';
import '/model/user/model_vehicle.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class GeoLocationService {
  GeoLocationService() {}

  Future<Position?> getCurrentLocation() async {
    // final status = await Permission.location.isGranted;
    var permission = await Geolocator.checkPermission();

    var position;
    if (permission == LocationPermission.denied) {
      // if (status) {
      await showLocationPermissionDialog().then((value) async {
        if (value != null && value) {
          var status = await requestLocationPermission();
          print("PermissionStatus: $status");
          if (status != PermissionStatus.granted) {
            return;
          }
          // LocationPermission permission = await Geolocator.requestPermission();
          position = await Geolocator.getCurrentPosition(
              desiredAccuracy: LocationAccuracy.high);
        } else {
          print('Location permission NO granted');
        }
      });
      print('Location permission new granted');
      return position;
    } else if (permission == LocationPermission.deniedForever) {
      print("!! khong cho phep quyen location");
      ToastNotify.showInfoToast(
          message:
              "Cho phép quyền vị trí để có được trải nghiệm ứng dụng tốt hơn!");
    } else {
      try {
        position = await Geolocator.getCurrentPosition(
            desiredAccuracy: LocationAccuracy.high);
      } catch (e) {
        print("!! khong get duoc position: $e");
      }
      return position;
    }
  }

  Future<Position?> getCurrentLocationWithoutPrompt() async {
    final status = await Permission.location.status;
    bool isLocationServiceEnabled = await Geolocator.isLocationServiceEnabled();
    if (status.isGranted && isLocationServiceEnabled) {
      try {
        Position position = await Geolocator.getCurrentPosition(
            desiredAccuracy: LocationAccuracy.high);
        return position;
      } catch (e) {
        print("!! khong enable gps");
      }
    }
  }

  Future<PermissionStatus> requestLocationPermission() async {
    print(Permission.location.isGranted);
    final status = await Permission.location.request();
    if (status != PermissionStatus.granted) {
      LocationPermission permission = await Geolocator.requestPermission();
      print('Location permission not granted');
      if (permission == LocationPermission.always ||
          permission == LocationPermission.whileInUse) {
        return PermissionStatus.granted;
      }
    }
    return status;
  }

  Future<bool?> showLocationPermissionDialog() async {
    var result = false;
    await Get.defaultDialog(
      titlePadding: EdgeInsets.only(
        top: 20,
      ),
      contentPadding: EdgeInsets.symmetric(horizontal: 15, vertical: 0),
      barrierDismissible: false,
      title: 'GPS Access',
      backgroundColor: Colors.white,
      titleStyle: TextStyle(color: AppTheme.orange),
      content: Container(
//        width: _screenSize.width,
//        height: 120,
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: 8,
            ),
            Text(
              "dialog_location_note1".tr,
              style: AppTheme.subtitleLightMode.copyWith(color: Colors.grey),
              textAlign: TextAlign.justify,
            ),
            RichText(
                textAlign: TextAlign.justify,
                text: TextSpan(
                    text: 'function'.tr + " Guide X ",
                    //Fitness Funds collects location data to enable fitness tracking even when the app is closed or not in use
                    style: AppTheme.subtitleLightMode
                        .copyWith(fontWeight: FontWeight.bold),
                    children: [
                      TextSpan(
                          text: "dialog_location_note2".tr,
                          style: AppTheme.subtitleLightMode
                              .copyWith(color: Colors.grey)),
                    ])),
            RichText(
                textAlign: TextAlign.justify,
                text: TextSpan(
                    text: "\n" + "function".tr + " Xplore ",
                    style: AppTheme.subtitleLightMode
                        .copyWith(fontWeight: FontWeight.bold),
                    children: [
                      TextSpan(
                          text: "dialog_location_note3".tr,
                          style: AppTheme.subtitleLightMode
                              .copyWith(color: Colors.grey)),
                    ])),
            Padding(
                padding: EdgeInsets.symmetric(vertical: 12, horizontal: 0),
                child: Text(
                  "dialog_location_note4".tr,
                  textAlign: TextAlign.justify,
                  style: AppTheme.noteAllPageText,
                )),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  // MaterialButton(
                  //   onPressed: () {
                  //     result = false;
                  //     Get.back();
                  //   },
                  //   child: Text(
                  //     "Later",
                  //     style: AppTheme.subtitleLightMode,
                  //   ),
                  // ),
                  MaterialButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                    onPressed: () async {
                      result = true;
                      Get.back();
                    },
                    color: AppTheme.orange,
                    child: Text(
                      "Continue",
                      style: AppTheme.button,
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    ).then((value) {
      return result;
    });
    return result;
  }
}
