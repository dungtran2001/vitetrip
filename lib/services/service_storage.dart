
import 'dart:convert';

import 'package:mitta/model/location/model_country.dart';
import 'package:mitta/model/location/model_province.dart';

import '../model/user/model_user_account.dart';

import '/model/user/model_me.dart';
import '/model/user/model_vehicle.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class StorageLocal{

  Me? userData;

  ProvinceInfo? provinceSelected;
  CountryInfo? countrySelected;

  final storage = new FlutterSecureStorage();

  StorageLocal(){
    fetchUserDataFromStorage();
  }

  bool checkUserAlreadyLogin(){
    if (userData != null &&  userData!.fullname != null &&  userData!.fullname != "" && userData!.fullname!.toLowerCase() != "null")
      return true;
    else
      return false;
  }


  Future<void> fetchUserDataFromStorage() async {
    try{
      var jwt = await storage.read(key: "user");
      if (jwt == null)
        return;
      print("--start fetchDataFromStorage");
      var data = await json.decode(jwt);
      var user = data['user'];

      userData = Me.fromJson(data);
      return;
//      await storage.read(key: "Bought_Ticket_Id").then((value) => tickerBoughtId = value);
    }catch (e){
      print("--ERROR fetchUserDataFromStorage: " + e.toString());
    }
  }

  Future<void> backupUserDataToStorage(Me data) async{
    try{
      await storage
          .write(key: "user", value: json.encode(data.toJson()).toString());
    }catch(e){
      print("--ERROR backupUserDataToStorage: " + e.toString());
    }

  }

  String language = "";
  bool onBoardingShowed = false;
  bool isSkipPermission = false;
  bool isEnableXplore = true;  // cho phep thong bao trong chuc nang explore
  Future<void> fetchSettingDataFromStorage() async {
    try{
      var jwt = await storage.read(key: "setting");
      if (jwt == null)
        return;
      print("--start fetchSettingDataFromStorage");
      var data = await json.decode(jwt);
      var user = data['setting'];

      language = data['language'];
      onBoardingShowed = data['onBoardingShowed'] as bool;
      isSkipPermission = data['isSkipPermission'] as bool;
      isEnableXplore = data['isEnableXplore'] as bool;
      return;
    }catch (e){
      print("--ERROR fetchUserDataFromStorage: " + e.toString());
      onBoardingShowed = false;
      isSkipPermission = false;
      isEnableXplore = true;
    }
  }

  Future<void> backupSettingDataToStorage({String? lang, bool? onboardingShowed, bool? isskipPermission, bool? isenableXplore}) async{
    try{
      var map = {
        "language": lang ?? language,
        "onBoardingShowed": onboardingShowed ?? onBoardingShowed,
        "isSkipPermission": isskipPermission ?? isSkipPermission,
        "isEnableXplore": isenableXplore ?? isEnableXplore
      };
      await storage
          .write(key: "setting", value: json.encode(map));
    }catch(e){
      print("--ERROR backupUserDataToStorage: " + e.toString());
    }

  }

  void clearUserDataToStorage() {
   userData = null;

   storage.write(key: "user", value: null);
  }



  ///backup country - province selected
  Future<void> fetchLocationFromStorage() async {
    try{
      var jwt = await storage.read(key: "province");
      if (jwt == null)
        return;
      var data = await json.decode(jwt);
      provinceSelected = ProvinceInfo.fromJson(data['province']);
      countrySelected = CountryInfo.fromJson(data['country']);
      return;
    }catch (e){
      print("--ERROR fetchProvinceFromStorage: " + e.toString());
    }
  }

  Future<void> backupLocationDataToStorage({CountryInfo? country, ProvinceInfo? province}) async{
    try{
      var map = {
        "country": country != null ? json.encode(country.toJson()) : countrySelected,
        "province": province != null ? json.encode(province.toJson()) : provinceSelected
      };
      await storage
          .write(key: "province", value: json.encode(map));
    }catch(e){
      print("--ERROR backupProvinceDataToStorage: " + e.toString());
    }

  }

  void clearProvinceDataToStorage() {
   userData = null;

   storage.write(key: "province", value: null);
  }



  //REGISTER ACCOUNT
  List<UserAccount> accountList = [];
  void backupAccountList(){
    var map = '{"listAccount": ${json.encode(accountList)}}';
    storage.write(key: "accounts", value: map.toString());
  }

  Future<bool> fetchBackupAccData() async {
    await storage.read(key: "accounts").then((value) {
      if (value != null){
        var data = json.decode(value);
//        print(data.toString());
        try {
          accountList = parseAccountList(data);
        }catch(e){

          print("get acc profile error");
        }

        return true;
      }else{
        return false;
      }
    });
    return false;
  }
  List<UserAccount> parseAccountList(map) {
    try{
      var list = map['listAccount'] as List;
      return list.map((p) => UserAccount.fromJson(p)).toList();
    }catch(e){
      print("ERROR parseAccountList $e");
      return [];
    }

  }


}