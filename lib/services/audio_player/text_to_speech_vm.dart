
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
//import 'package:just_audio/just_audio.dart';
import 'package:mitta/commons/components/audio/play_button_notifier.dart';
import 'package:mitta/commons/components/audio/progress_notifier.dart';

class TextToSpeech {
  final currentNotifier = ValueNotifier<String>('');
  final progressNotifier = ProgressNotifier();
  final playButtonNotifier = PlayButtonNotifier();
  final isFirst = ValueNotifier<bool>(true);
  final isLocal = ValueNotifier<bool>(false);
  final audioPlayer = AudioPlayer();
  init() {
    _listenToCurrentPosition();
    _listenToTotalDuration();
  }

  setMp3(url) {
    currentNotifier.value = url;
  }

  setLocal(isLocal) {
    this.isLocal.value = isLocal;
  }

  setLoading(isLoadingSuccess) {
    playButtonNotifier.value =
        isLoadingSuccess ? ButtonState.paused : ButtonState.loading;
  }

  resumeOrPause() {
    if (playButtonNotifier.value == ButtonState.paused) {
      print("--resume player");
      audioPlayer.resume();
      playButtonNotifier.value = ButtonState.playing;
    } else {
      print("--pause player");
      audioPlayer.pause();
      playButtonNotifier.value = ButtonState.paused;
    }
  }

  dispose(){
    audioPlayer.dispose();
  }

  play() async {
    // await audioPlayer.play(currentNotifier.value, isLocal: isLocal.value);
    isFirst.value = false;
    playButtonNotifier.value = ButtonState.playing;
  }

  void seek(Duration position) => audioPlayer.seek(position);

  void _listenToCurrentPosition() {
    audioPlayer.onDurationChanged.listen((position) {
      final oldState = progressNotifier.value;
      progressNotifier.value = ProgressBarState(
        current: oldState.current,
        buffered: oldState.buffered,
        total: position,
      );
    });
  }

  void _listenToTotalDuration() {
    // audioPlayer.onAudioPositionChanged.listen((position) {
    //   final oldState = progressNotifier.value;
    //   progressNotifier.value = ProgressBarState(
    //     current: position,
    //     buffered: oldState.buffered,
    //     total: oldState.total,
    //   );
    //   if (position == oldState.current && position != Duration.zero) { //bug fixed: state button = pause, right after click play
    //     playButtonNotifier.value = ButtonState.paused;
    //   }
    // });
  }
}
