import 'package:audio_service/audio_service.dart';
import 'package:flutter/foundation.dart';
import 'package:mitta/commons/components/audio/audio_resource_notify.dart';
import 'package:mitta/commons/components/audio/play_button_notifier.dart';
import 'package:mitta/commons/components/audio/progress_notifier.dart';
import 'package:mitta/model/audio/resource_model.dart';

import '../services_locator.dart';

class MediaManager {
  // Listeners: Updates going to the UI
  final currentSongTitleNotifier = ValueNotifier<String>('');
  final resourceNotifier = ResourceNotifier();
  final playlistNotifier = ValueNotifier<List<String>>([]);
  final progressNotifier = ProgressNotifier();
  final isFirstSongNotifier = ValueNotifier<bool>(true);
  final playButtonNotifier = PlayButtonNotifier();
  final isLastSongNotifier = ValueNotifier<bool>(true);
  final isShuffleModeEnabledNotifier = ValueNotifier<bool>(false);
  final visibleNotifier = ValueNotifier<bool>(false);
  final _audioHandler = locator<AudioHandler>();
  var listPlay = [];
  var resources = [];
  // Events: Calls coming from the UI
  void init() async {
    //await _loadPlaylist();
    _listenToChangesInPlaylist();
    _listenToPlaybackState();
    _listenToCurrentPosition();
    _listenToBufferedPosition();
    _listenToTotalDuration();
//    _listenToChangesInSong();
  }

  void checkVisible() {}

  loadPlaylist(List<ResourceModel> playlist) {
    _audioHandler.stop(); //hoa.td added

    // final songRepository = getIt<PlaylistRepository>();
    // final playlist = await songRepository.fetchInitialPlaylist();
    final mediaItems = playlist
        .map((resource) => MediaItem(
              id: resource.url ?? '',
              album: resource.author ?? '',
              title: resource.title ?? '',
              duration: resource.parseDuration(),
              extras: {'url': resource.url},
            ))
        .toList();
    listPlay = mediaItems;
    resources = playlist;
    var _amount = _audioHandler.queue.value;
    if (_amount.length > 0){ //check empty
      //foreach and remove all before add new list media
      for (var item in _audioHandler.queue.value){
        _audioHandler.removeQueueItem(item);
      }
    } else{
      _audioHandler.addQueueItems(mediaItems);
    }
  }

  void _listenToChangesInPlaylist() {
    _audioHandler.queue.listen((playlist) {
      if (playlist.isEmpty) {
        playlistNotifier.value = [];
        currentSongTitleNotifier.value = '';
      } else {
        final newList = playlist.map((item) => item.title).toList();
        playlistNotifier.value = newList;
      }
      _updateSkipButtons();

      if (_audioHandler != null &&_audioHandler.mediaItem.value != null){
        var resource = null;
        try{
          resource = resources.firstWhere((element) =>
          element.url == _audioHandler.mediaItem.value!.id &&
              element.title == _audioHandler.mediaItem.value!.title);
        } catch(e){
          print("--ERROR _listenToChangesInPlaylist: $e");
        }
        if (resource != null)
          resourceNotifier.setResource(resource);
      }

    });
  }

  void _listenToPlaybackState() {
    _audioHandler.playbackState.listen((playbackState) {
      final isPlaying = playbackState.playing;
      final processingState = playbackState.processingState;
      if (processingState == AudioProcessingState.loading ||
          processingState == AudioProcessingState.buffering) {
        playButtonNotifier.value = ButtonState.loading;
      } else if (!isPlaying) {
        playButtonNotifier.value = ButtonState.paused;
      } else if (processingState != AudioProcessingState.completed) {
        playButtonNotifier.value = ButtonState.playing;
      } else {
        _audioHandler.seek(Duration.zero);
        _audioHandler.pause();
      }
    });
  }

  void _listenToCurrentPosition() {
    AudioService.position.listen((position) {
      final oldState = progressNotifier.value;
      progressNotifier.value = ProgressBarState(
        current: position,
        buffered: oldState.buffered,
        total: oldState.total,
      );
    });
  }

  void _listenToBufferedPosition() {
    _audioHandler.playbackState.listen((playbackState) {
      final oldState = progressNotifier.value;
      progressNotifier.value = ProgressBarState(
        current: oldState.current,
        buffered: playbackState.bufferedPosition,
        total: oldState.total,
      );
    });
  }

  void _listenToTotalDuration() {
    _audioHandler.mediaItem.listen((mediaItem) {
      final oldState = progressNotifier.value;
      progressNotifier.value = ProgressBarState(
        current: oldState.current,
        buffered: oldState.buffered,
        total: mediaItem?.duration ?? oldState.total,
      );
    });
  }

  void _listenToChangesInSong() {
    _audioHandler.mediaItem.listen((mediaItem) {
      currentSongTitleNotifier.value = mediaItem?.title ?? '';
      if (resources.isNotEmpty) {
        try{
          var resource = resources.firstWhere((element) =>
          element.url == mediaItem!.id && element.title == mediaItem.title);
          resourceNotifier.setResource(resource);
        }catch(e){
          print("--Error _listenToChangesInSong: $e");
        }

      }
      _updateSkipButtons();
    });
  }

  void _updateSkipButtons() {
    final mediaItem = _audioHandler.mediaItem.value;
    final playlist = _audioHandler.queue.value;
    if (playlist.length < 2 || mediaItem == null) {
      isFirstSongNotifier.value = true;
      isLastSongNotifier.value = true;
    } else {
      isFirstSongNotifier.value = playlist.first == mediaItem;
      isLastSongNotifier.value = playlist.last == mediaItem;
    }
  }

  void playWithResource(resource){

    final mediaItem = MediaItem(
      id: resource.url ?? '',
      album: resource.author ?? '',
      title: resource.title ?? '',
      extras: {'url': resource.url},
    );

    _audioHandler.playMediaItem(mediaItem);
  }

  void removePlaylist(){
    var _playlist = _audioHandler.queue.value;
    for (var item in _playlist)
      _audioHandler.removeQueueItem(item);
    if (!isQueueEmpty())
      _audioHandler.removeQueueItemAt(0);
    print("remove all playlist success");
  }

  bool isQueueEmpty(){
    var _playlist = _audioHandler.queue.value;
   if (_playlist.length > 0)
     return false;
   else
     return true;
  }

  void play() => _audioHandler.play();
  void pause() => _audioHandler.pause();

  void seek(Duration position) => _audioHandler.seek(position);

  void previous() {
    print("--previous current" +  _audioHandler.mediaItem.value!.id.toString());
    var resource = resources.firstWhere((element) =>
        element.url == _audioHandler.mediaItem.value!.id &&
        element.title == _audioHandler.mediaItem.value!.title);
    resourceNotifier.setResource(resource);
    _audioHandler.skipToPrevious();
  }

  void next() {
    try{
      var resource = resources.firstWhere((element) =>
      element.url == _audioHandler.mediaItem.value!.id &&
          element.title == _audioHandler.mediaItem.value!.title);
      resourceNotifier.setResource(resource);
      _audioHandler.skipToNext();
    }catch(e){
      print("--ERROR audio manager next: $e");
    }
  }

  void shuffle() {
    final enable = !isShuffleModeEnabledNotifier.value;
    isShuffleModeEnabledNotifier.value = enable;
    if (enable) {
      _audioHandler.setShuffleMode(AudioServiceShuffleMode.all);
    } else {
      _audioHandler.setShuffleMode(AudioServiceShuffleMode.none);
    }
  }

  Future<void> add(ResourceModel resource) async {
    _audioHandler.stop().then((value) async {
      // final songRepository = getIt<PlaylistRepository>();
      // final song = await songRepository.fetchAnotherSong();
      final mediaItem = MediaItem(
        id: resource.url ?? '',
        album: resource.author ?? '',
        title: resource.title ?? '',
        extras: {'url': resource.url},
      );
      resources.add(resource); // add to playlist
      //_audioHandler.playMediaItem(mediaItem);
      resourceNotifier.setResource(resource);
      visibleNotifier.value = true;
      var index = _audioHandler.queue.value.indexWhere((element) => element.id == mediaItem.id);
//    var index = listPlay.indexWhere((element) => element.id == mediaItem.id);
      //listPlay = listPlay.sublist(index);
      //var index = _audioHandler.
      //_audioHandler.updateQueue([mediaItem]);
      if (index == -1){
        _audioHandler.addQueueItems([mediaItem]); //them vao danh sach cho player
        _audioHandler.skipToQueueItem(listPlay.length);
        listPlay.add(mediaItem); //them vao list play managa

        await Future.delayed(const Duration(milliseconds: 500), () {
          _audioHandler.play();
        });
      }else{
        _audioHandler.skipToQueueItem(index);
        await Future.delayed(const Duration(milliseconds: 200), () {
          _audioHandler.play();
        });
      }

      //_audioHandler.play();
      //_audioHandler.playMediaItem(mediaItem);
    }); //hoa.td added


  }

  void remove() {
    final lastIndex = _audioHandler.queue.value.length - 1;
    if (lastIndex < 0) return;
    _audioHandler.removeQueueItemAt(lastIndex);
    //remove in resource
    resources.removeLast();
  }

  void dispose() {
    _audioHandler.customAction('dispose');
    _audioHandler.stop();

  }

  void stop() {
    _audioHandler.stop();
  }
}
