import 'package:audio_video_progress_bar/audio_video_progress_bar.dart';
import 'package:flutter/material.dart';
import 'package:mitta/commons/components/audio/play_button_notifier.dart';
import 'package:mitta/commons/components/audio/progress_notifier.dart';

class PlayButton extends StatelessWidget {
  final textToSpeech;
  const PlayButton({Key? key, required this.textToSpeech}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ClipOval(
        child: Container(
            height: 45,
            width: 45,
            color: Colors.black54,
            child: Center(
                child: ValueListenableBuilder<ButtonState>(
                  valueListenable: textToSpeech.playButtonNotifier,
                  builder: (_, value, __) {
                    switch (value) {
                      case ButtonState.loading:
                        return Container(
                          margin: EdgeInsets.all(8.0),
                          width: 25,
                          height: 25,
                          color: Colors.black54,
                          child: CircularProgressIndicator(
                            color: Colors.cyanAccent,
                          ),
                        );
                      case ButtonState.paused:
                        return IconButton(
                          icon: Icon(Icons.play_arrow, color: Colors.cyanAccent),
                          iconSize: 25,
                          onPressed: textToSpeech.isFirst.value == true
                              ? textToSpeech.play
                              : textToSpeech.resumeOrPause,
                        );
                      case ButtonState.playing:
                        return IconButton(
                          icon: Icon(Icons.pause, color: Colors.cyanAccent),
                          iconSize: 25,
                          onPressed: textToSpeech.resumeOrPause,
                        );
                    }
                  },
                ))));
  }
}

class AudioProgressBar extends StatelessWidget {
  final textToSpeech;
  const AudioProgressBar({Key? key, required this.textToSpeech})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<ProgressBarState>(
      valueListenable: textToSpeech.progressNotifier,
      builder: (_, value, __) {
        return ProgressBar(
          progress: value.current,
          buffered: value.buffered,
          total: value.total,
          onSeek: textToSpeech.seek,
          progressBarColor: Colors.orange,
          thumbColor: Colors.orange,
          timeLabelType: TimeLabelType.totalTime,
          timeLabelLocation: TimeLabelLocation.none,
        );
      },
    );
  }
}