const Map<String, String> ko = {
  'text_hello': '안녕',
  'multipleLanguage': '다중 언어',
  'change_lang' : 'Tiếng Việt',
  'lang' : '한국어',

  'text_later': "나중에",
  'text_loading': '로드 중...',
  'text_error': '오류: ',
  'error_tier': '오류',
  'try_again': '다시 시도',
  'load_more': '더 불러오기...',
  'text_success': '성공',
  'connect_network_fail': '인터넷에 연결되어 있지 않습니다...',
  'connect_network_fail_des': 'Wi-Fi 또는 모바일 네트워크에 연결되어 있는지 확인하고 다시 시도하세요!',
  'open_network_setting': '설정 열기',
  'exit_network_fail': '종료/확인',
  'text_save': '저장',
  'text_next': '다음',
  'text_back': '뒤로',
  'input_please': '정보를 입력해주세요!',
  'text_page': '페이지',
  'text_search': '검색',
  'text_update': '업데이트',
  'text_cancel': '취소',
  'text_delete': '삭제',
  'text_send': ' 보내기 ',
  'hold_confirm': '실행하려면 길게 누르세요',

  // _집_
  'audio_coming_soon': '오디오 콘텐츠가 곧 제공될 예정입니다',
  'system_maintenance': "시스템 점검 중",
  'we_will_be_back': "곧 돌아올게요. 불편을 드려 죄송합니다!",
  'no_data_for_country': "이 국가에 대한 데이터가 없습니다!",
  'no_matching_locations': "일치하는 위치 없음",

  // _등록하다_
  'please_enter_email': "이메일을 입력하세요(로그인에 사용)",
  'invalid_email_address': "잘못된 이메일 주소",
  'text_processing': "처리 중",

  // _XPLORE_
  'notification_title': "AI GuideX 알림 도우미",
  'notification_desc': "어트랙션에 가까워지면 앱에서 자동으로 알려줍니다.",
  'allow_to_search': "주변 명소 검색을 계속하도록 허용됨",
  'stop_searching': "주변 명소 검색 중지",

  // 디텍트뷰 다이얼로그
  'no_image_captured': "캡쳐된 이미지 없음",

  // 디텍트뷰
  'text_scan': '스캔',
  'text_description': "설명",

  // 개체 감지
  'artifacts_not_found': "유물을 찾을 수 없음",
  'place_no_artifact': "이 명소에는 유물이 없습니다. 최신 알림을 받으려면 가입하세요!",

  'text_previous': '이전',
  'text_next_page': '다음',

  'msg_change_lang': '언어 변경 성공',
  'msg_error_lang': '이 언어는 이미 선택되었습니다',


  // 탑승 중
  'p1_title1a': 'AIGuideX는 ',
  'p1_title1b': '최초의 자동 투어 가이드',
  'p1_title1c': 'AI 기술 사용',
  'p1_title2a': '스캔만 하면 됩니다',
  'p1_title2b': 'AIGuideX',
  'p1_title2c': '건물, 풍경, 사진 또는 전시된 유물 등\n개체에 대한\n즉시 지침을 들을 수 있습니다.',
  'p1_title3': '스캔하고 듣기만 하면 아주 쉽게',


  'p2_title1a': 'AIGuideX는 ',
  'p2_title1b': '자동으로 알림',
  'p2_title1c': '관광 명소를 놓치지 않도록',

  'p2_title2': '탐색으로 이동하여 언제든지 이 기능을 쉽게 끌 수 있습니다.',
  'p2_title3': '열고 재미있게 즐기세요!',
  'p2_content1': '명소 찾기',
  'p2_content2': '자동 알림',

  'text_service': "앱",
  'text_service_l2': "AI 기술을 사용한 최초의\n 자동 투어 가이드 앱",

  'entrance_ticket': "입장권",
  'with_ads': "(광고 포함)",
  'text_transport': "운송",
  'point_by_point': "점별로",

  'function': '특징',

  // 팝업 위치
  'dialog_location_note1': '앱에 위치 권한이 필요합니다:',
  'dialog_location_note2': '앱이 열릴 때 위치를 사용하여 주변 장소를 제안합니다.',
  'dialog_location_note3': '어트랙션에 가까워지면 백그라운드에서 작동하거나 앱이 닫힐 때 위치를 사용하여 알립니다.',
  'dialog_location_note4': '위치 데이터는 누구와도 공유 및 수집되지 않습니다.',

  //설정 화면
  'setting_scr_title': '설정', //티엣 랩
  'setting_scr_biometric_enable': '생체 인식으로 로그인 사용', //dang nhap bang sinh trac hoc
  'setting_scr_biometric_disable': '생체 인식으로 로그인 비활성화', //dang nhap bang sinh trac hoc
  'setting_scr_ads': '회사 정보 섹션 표시', //phat
  'setting_scr_language': '언어 변경', //lang
  'setting_scr_thememode': '테마 모드', //테마
  'setting_scr_popup_theme': '테마 모드 선택',
  'setting_scr_popup_theme_light': '조명 모드',
  'setting_scr_popup_theme_night': '야간 모드',
  'setting_scr_popup_theme_sys': '시스템 설정',
  'setting_scr_popup_language': '언어 선택', //lang
  'setting_scr_logout': '로그아웃', //xoa
  'setting_scr_popup_title': '계정 확인', //확인
  'setting_scr_popup_pass': '비밀번호',
  'setting_scr_popup_btn_next': '계속',
  'setting_scr_popup_btn_cancel': '취소',
  'setting_scr_popup_pass_empt': '비밀번호를 입력하세요',
  'setting_scr_popup_pass_wrong': '잘못된 비밀번호',
  'setting_scr_des_security': '보안',
  'setting_scr_des_customize': '사용자 정의',

  'setting_scr_changepass': '비밀번호 변경',
  'setting_pop_empt_allpass': '모든 필드를 입력하세요',
  'setting_pop_tier_currpass': '현재 비밀번호',
  'setting_pop_empt_currpass': '현재 비밀번호를 입력하세요',
  'setting_pop_tier_newpass': '새 비밀번호',
  'setting_pop_empt_newpass': '새 비밀번호를 입력하세요',
  'setting_pop_tier_conf_newpass': '새 비밀번호 확인',
  'setting_pop_empt_conf_newpass': '새 비밀번호 확인을 입력하세요',
  'setting_pop_err_notmatch': '비밀번호 불일치',
  'setting_changepass_success': '성공입니다. 비밀번호 변경됨',
  'setting_changepass_fail': '실패했습니다. 유효한 비밀번호를 입력하세요',
  'home_deactivate': '계정 삭제',
  'home_deactivate_confirm1': '이 계정을 시스템에서 삭제하시겠습니까?',
  'home_deactivate_confirm2': '이렇게 하면 계정 데이터가 완전히 지워지고 복구할 수 없습니다. 신중하게 고려하십시오!',
  'home_deactivate_success': '계정 삭제 성공',
  'home_deactivate_fail': '오류가 있는 계정을 삭제하고 나중에 다시 시도하세요!',

  //로그인
  'login_scr_input_user': '사용자 이름',
  'login_scr_input_pass': '비밀번호',
  'login_scr_btn_login': '로그인',
  'login_scr_btn_register': '가입하기',
  'login_scr_question': '계정이 없습니까?',
  'login_scr_msg_user': '사용자 이름을 입력하세요',
  'login_scr_msg_pass': '비밀번호를 입력하세요',
  'login_scr_msg_ok': '로그인 성공',
  'login_scr_msg_bio_ok': '생체 인식을 통한 로그인 성공',
  'login_scr_msg_fail': '사용자 이름 또는 암호가 올바르지 않습니다.',
  'login_scr_msg_bio_fail': '생체 인식을 통해 로그인할 수 없습니다. 설정을 검토하세요.',
  'login_scr_popup_title': '인증 사용',
  'login_scr_popup_s1': '먼저 계정으로 로그인하세요',
  'login_scr_popup_s2': '\'설정\'을 입력하고 \'생체 인식으로 로그인 사용\'을 선택',
  'login_scr_popup_btn': '확인',

  //등록하다
  'register_scr_input_user': '사용자 이름',
  'register_scr_input_pass': '비밀번호',
  'register_scr_input_cpass': '비밀번호 확인',
  'register_scr_input_des': '계정 생성 요구 사항 설명(단위 - 목적...)',
  'register_scr_btn_login': '로그인',
  'register_scr_btn_continue': '계속',
  'register_scr_btn_register': '가입하기',
  'register_scr_question': '이미 계정이 있습니까?',
  'register_scr_msg_user': '사용자 이름을 입력하세요',
  'register_scr_msg_user_dup': '이미 사용자 이름이 등록됨',
  'register_scr_msg_pass': '비밀번호를 입력하세요',
  'register_scr_msg_cpass': '암호가 확인 암호와 일치하지 않습니다.',
  'register_scr_msg_ok': '새 계정 등록 성공',
  'register_scr_msg_fail': '등록 실패',
  'register_scr_msg_note': '등록 정보를 다시 확인해주세요!',
  'register_scr_warning_tier': '주의',
  'register_scr_warning_text': '계정 생성 요청이 관리자에게 전송됩니다. 계정에 로그인하기 전에\n확인 프로세스가 완료될 때까지 기다리십시오.',
  'register_scr_msg_requestok': ' 요청 보내기 성공',
  'register_scr_msg_requesttext': '지금 검색하려면 로그인하세요!',
  'register_scr_btn_done': '완료',
  'register_scr_msg_notify': '계정 등록 성공, 지금 로그인하여 사용',


  //투기
  'from_you': '당신에게서',
  'scan_object': '개체 스캔',
  'object_detected': ' 감지된 개체',
  'question_back_scan1': '있다',
  'question_back_scan2': ' 개체를 탐색해야 합니다.',
  'question_back_suggestion': '계속하시겠습니까?',
  'no_thanks': '아니요, 감사합니다',
  'scan_next': '다음 스캔',

  'report_thanks_title': '감사합니다!',
  'report_thanks_content': '귀하의 피드백을 기반으로\n앱이 더 잘 감지합니다.',
  'request_thanks_content': '최대한 빨리 개체를\n업데이트하기 위해 최선을 다하겠습니다.',
  'no_object': '앱에서 개체를 찾을 수 없음',
  'please_capture': '사진을 찍어 주시겠어요?',
  'not_found_object': '물체를 찾을 수 없습니까?',
  'question_not_found_object': '귀하의 의견을 부탁드립니다!',
  'request_guide_object': '객체에 대한 안내 추가',
  'choose_object': '안내를 들을 개체를 선택하세요',
  'text_guide': '안내',
  'pick_guide': '가이드 선택',
  'text_ticket': '입장권',
  'soon_update': '이 기능을 곧 업데이트할 예정입니다!',
  'enable_notification': ' 알림 받기',
  'enable_notification2': '시스템이 흥미로운 명소를 찾을 때',
  'question_rating1': '잠시 시간을 내어 주시면 감사하겠습니다',
  'question_rating2': '공유 가능',
  'question_rating3': ' 사용 경험',
  'xplore_choose_place': '지도에서\n 볼 명소를 선택하세요',
  'xplore_no_place': '내\n가까운 명소가 없습니다',
  'xplore_radius': '100km',
};