const Map<String, String> zh = {
  'text_hello': '你好',
  'multipleLanguage': '多种语言',
  'change_lang': '越南语',
  'lang': '中文',

  'text_later': "稍后",
  'text_loading': '正在加载...',
  'text_error': '错误：',
  'error_tier': '错误',
  'try_again': '再试一次',
  'load_more': '加载更多...',
  'text_success': '成功',
  'connect_network_fail': '没有互联网连接...',
  'connect_network_fail_des': '确保您已连接到 Wi-Fi 或移动网络，然后重试！',
  'open_network_setting': '打开设置',
  'exit_network_fail': '退出/确定',
  'text_save': '保存',
  'text_next': '下一个',
  'text_back': '返回',
  'input_please': '请输入信息！',
  'text_page': '页面',
  'text_search': '搜索',
  'text_update': '更新',
  'text_cancel': '取消',
  'text_delete': '删除',
  'text_send': '发送',
  'hold_confirm': '长按执行',

  // _家_
  'audio_coming_soon': '音频内容即将推出',
  'system_maintenance': "系统正在维护中",
  'we_will_be_back': "我们很快就会回来，很抱歉给您带来不便！",
  'no_data_for_country': "没有该国家/地区的可用数据！",
  'no_matching_locations': "没有匹配的位置",

  // _登记_
  'please_enter_email': "请输入电子邮件（用于登录）",
  'invalid_email_address': "电子邮件地址无效",
  'text_processing': "处理中",

  // _XPLORE_
  'notification_title': "AI GuideX 通知助手",
  'notification_desc': "如果您接近景点，应用程序会自动通知您",
  'allow_to_search': "允许继续搜索附近景点",
  'stop_searching': "停止搜索附近景点",

  // 检测ViewDialog
  'no_image_captured': "未捕获图像",

  // 检测视图
  'text_scan': '扫描',
  'text_description': "描述",

  // 检测对象
  'artifacts_not_found': "未找到工件",
  'place_no_artifact': "该景点没有文物。注册即可接收最新通知！",

  'text_previous': '上一个',
  'text_next_page': '下一页',

  'msg_change_lang': '语言更改成功',
  'msg_error_lang': '该语言已选择',

  //登机
  'p1_title1a': 'AIGuideX 是 ',
  'p1_title1b': '第一个自动导游',
  'p1_title1c': '使用AI技术',
  'p1_title2a': '您只需扫描',
  'p1_title2b': 'AIGuideX ',
  'p1_title2c': '在某个物体上\n您可以立即听到有关该物体的说明\n，无论它是建筑物、风景、照片还是展出的文物 ',
  'p1_title3': '只需扫描和聆听，一切都非常简单',

  'p2_title1a': 'AIGuideX 将 ',
  'p2_title1b': '自动通知您 ',
  'p2_title1c': '这样您就不会错过景点',

  'p2_title2': '您可以随时前往探索轻松关闭此功能',
  'p2_title3': '打开它并享受乐趣！',
  'p2_content1': '查找景点',
  'p2_content2': '自动通知',

  'text_service': "应用程序",
  'text_service_l2': "是第一个使用人工智能技术的自动导游应用\n",

  'entrance_ticket': "门票",
  'with_ads': "(带广告)",
  'text_transport': "运输",
  'point_by_point': "逐点",

  'function': '功能',

  // 弹出位置
  'dialog_location_note1': '该应用需要位置权限：',
  'dialog_location_note2': '当应用程序打开时，将使用位置来建议您附近的地点',
  'dialog_location_note3': '当您接近景点时或应用程序关闭时，将使用位置来通知您',
  'dialog_location_note4': '您的位置数据不会被任何人共享和收集。',

  //设置屏幕
  'setting_scr_title': '设置',
  //thiet lap
  'setting_scr_biometric_enable': '启用生物识别登录',
  //dang nhap bang sinh trac hoc
  'setting_scr_biometric_disable': '禁用生物识别登录',
  //dang nhap bang sinh trac hoc
  'setting_scr_ads': '可见公司信息部分',
  //phat
  'setting_scr_language': '更改语言',
  //lang
  'setting_scr_thememode': '主题模式',
  //主题
  'setting_scr_popup_theme': '选择主题模式',
  'setting_scr_popup_theme_light': '灯光模式',
  'setting_scr_popup_theme_night': '夜间模式',
  'setting_scr_popup_theme_sys': '系统设置',
  'setting_scr_popup_language': '选择语言',
  //lang
  'setting_scr_logout': '注销',
  //xoa
  'setting_scr_popup_title': '验证帐户',
  //验证
  'setting_scr_popup_pass': '密码',
  'setting_scr_popup_btn_next': '继续',
  'setting_scr_popup_btn_cancel': '取消',
  'setting_scr_popup_pass_empt': '请输入密码',
  'setting_scr_popup_pass_wrong': '密码错误',
  'setting_scr_des_security': '安全',
  'setting_scr_des_customize': '自定义',

  'setting_scr_changepass': '更改密码',
  'setting_pop_empt_allpass': '请输入所有字段',
  'setting_pop_tier_currpass': '当前密码',
  'setting_pop_empt_currpass': '请输入当前密码',
  'setting_pop_tier_newpass': '新密码',
  'setting_pop_empt_newpass': '请输入新密码',
  'setting_pop_tier_conf_newpass': '确认新密码',
  'setting_pop_empt_conf_newpass': '请输入确认新密码',
  'setting_pop_err_notmatch': '密码不匹配',
  'setting_changepass_success': '成功。 密码已更改',
  'setting_changepass_fail': '失败。 请输入有效密码',
  'home_deactivate': '删除帐户',
  'home_deactivate_confirm1': '您确定要从系统中删除此帐户吗？',
  'home_deactivate_confirm2': '这将完全删除您的帐户数据并且无法恢复。 请您慎重考虑！',
  'home_deactivate_success': '账户删除成功',
  'home_deactivate_fail': '删除出错的账户，稍后重试！',

  //登录
  'login_scr_input_user': '用户名',
  'login_scr_input_pass': '密码',
  'login_scr_btn_login': '登录',
  'login_scr_btn_register': '注册',
  'login_scr_question': '没有帐户？',
  'login_scr_msg_user': '请输入用户名',
  'login_scr_msg_pass': '请输入密码',
  'login_scr_msg_ok': '登录成功',
  'login_scr_msg_bio_ok': '生物识别登录成功',
  'login_scr_msg_fail': '您的用户名或密码不正确。',
  'login_scr_msg_bio_fail': '无法通过生物识别登录，请检查设置',
  'login_scr_popup_title': '启用授权',
  'login_scr_popup_s1': '请先使用账号登录',
  'login_scr_popup_s2': '输入\'设置\'并选择\'启用生物识别登录\'',
  'login_scr_popup_btn': '明白了',

  //登记
  'register_scr_input_user': '用户名',
  'register_scr_input_pass': '密码',
  'register_scr_input_cpass': '密码确认',
  'register_scr_input_des': '账户创建要求描述（单位 - 目的...）',
  'register_scr_btn_login': '登录',
  'register_scr_btn_continue': '继续',
  'register_scr_btn_register': '注册',
  'register_scr_question': '已经有帐户？',
  'register_scr_msg_user': '请输入用户名',
  'register_scr_msg_user_dup': '用户名已注册',
  'register_scr_msg_pass': '请输入密码',
  'register_scr_msg_cpass': '密码与确认密码不匹配',
  'register_scr_msg_ok': '新账户注册成功',
  'register_scr_msg_fail': '注册失败',
  'register_scr_msg_note': '请再次检查注册信息！',
  'register_scr_warning_tier': '注意',
  'register_scr_warning_text': '创建帐户的请求将发送给管理员。 请等待验证过程\n完成，然后才能登录您的帐户。',
  'register_scr_msg_requestok': '发送请求成功',
  'register_scr_msg_requesttext': '请立即登录发现！',
  'register_scr_btn_done': '完成',
  'register_scr_msg_notify': '账号注册成功，立即登录使用',

  //规格
  'from_you': '来自你',
  'scan_object': '扫描对象',
  'object_detected': '检测到物体',
  'question_back_scan1': '有 ',
  'question_back_scan2': '需要探索对象',
  'question_back_suggestion': '你想继续吗？',
  'no_thanks': '不，谢谢',
  'scan_next': '扫描下一个',

  'report_thanks_title': '谢谢！',
  'report_thanks_content': '应用程序将根据您的反馈\n更好地检测',
  'request_thanks_content': '我们将尽力尽快更新\n该对象',
  'no_object': '无法在应用程序中找到该对象',
  'please_capture': '能给我们一张照片吗？',
  'not_found_object': '找不到对象？',
  'question_not_found_object': '我们需要您的反馈！',
  'request_guide_object': '为对象添加指导',
  'choose_object': '请选择一个对象来听指导',
  'text_guide': '指导',
  'pick_guide': '选择指南',
  'text_ticket': '门票',
  'soon_update': '我们很快就会更新此功能！',
  'enable_notification': '收到通知',
  'enable_notification2': '当系统发现感兴趣的景点时',
  'question_ rating1': '如果您有时间的话，我们将不胜感激',
  'question_ rating2': '你可以分享',
  'question_ rating3': '您对我们的体验',
  'xplore_choose_place': '请在地图上选择\n景点查看',
  'xplore_no_place': '您附近没有景点\n',
  'xplore_radius': '100 公里',
};
