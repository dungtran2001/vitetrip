const Map<String, String> vi = {
  'text_hello': 'Xin chào',
  'multipleLanguage' : 'Đa ngôn ngữ',
  'change_lang' : 'English',
  'lang' : 'Tiếng Việt',

  'text_later': "Để sau",
  'text_loading': 'Đang tải...',
  'text_error': 'Lỗi: ',
  'error_tier': 'Lỗi',
  'try_again': 'Thử lại',
  'load_more': 'Xem thêm...',
  'text_success': 'thành công',
  'connect_network_fail': 'Không tìm thấy kết nối mạng...',
  'connect_network_fail_des': 'Bạn vui lòng mở kết nối mạng và thử lại nhé!',
  'open_network_setting': 'Mở mạng',
  'exit_network_fail': 'Đồng ý thoát',
  'text_save': 'Lưu',
  'text_next': 'Tiếp tục',
  'text_back': 'Trở lại',
  'input_please': 'Vui lòng điền thông tin!',
  'text_page': 'Trang',
  'text_search': 'Tìm kiếm',
  'text_update': 'Cập nhật',
  'text_cancel': 'Bỏ qua',
  'text_delete': 'Xóa',
  'text_send': ' Gởi ',

  // _HOME_
  'audio_coming_soon': 'Nội dung âm thanh sẽ được cập nhật sớm nhất',
  'system_maintenance': "Hệ thống đang bảo trì",
  'we_will_be_back': "Chúng tôi sẽ quay lại ngay, xin lỗi vì sự bất tiện này!",
  'no_data_for_country': "Không có dữ liệu cho quốc gia này!",
  'no_matching_locations': "Không có vị trí phù hợp",

  // _REGISTER_
  ' please_enter_email': "Vui lòng nhập email (dùng để đăng nhập)",
  'invalid_email_address': "Địa chỉ email không hợp lệ",
  'text_processing': "Đang xử lý",

  // _XPLORE_
  'notification_title': "Trợ lý thông báo AI GuideX",
  'notification_desc': "Ứng dụng sẽ tự động thông báo cho bạn nếu bạn đến gần điểm du lịch",
  'allow_to_search': "Được phép tiếp tục tìm kiếm các địa điểm lân cận",
  'stop_searching': "Dừng tìm kiếm các địa điểm lân cận",

  // DetectViewDialog
  'no_image_captured': "Chưa chụp được ảnh",

  // DetectView
  'text_scan': 'Quét',
  'text_description': "Mô tả",

  // _DETECT_OBJECT
  'artifacts_not_found': "Không tìm thấy hiện vật",
  'place_no_artifact': "Địa điểm này chưa có hiện vật. Hãy đăng ký để nhận thông báo mới nhất!",

  'text_previous': 'Trước',
  'text_next_page': 'Tiếp',

  'msg_change_lang' : 'Chuyển đổi ngôn ngữ thành công',
  'msg_error_lang' : 'Ngôn ngữ này đã được chọn',
  'hold_confirm': 'Nhấn giữ để thực hiện',

  //ON BOARDING
  'p1_title1a': 'AIGuideX là ',
  'p1_title1b': 'Hướng dẫn tour tự động ',
  'p1_title1c': '\nđầu tiên sử dụng công nghệ AI ',
  'p1_title2a': 'Bạn chỉ cần quét ',
  'p1_title2b': 'AIGuideX ',
  'p1_title2c': 'lên hiện vật\nlà có thể nghe được ngay hướng dẫn về\nhiện vật đấy, dù là một tòa nhà, một cảnh quan, một bức ảnh hay một hiện vật trưng bày ',
  'p1_title3': 'Tất cả đều rất dễ dàng chỉ bằng cách quét và nghe',


  'p2_title1a': 'AIGuideX sẽ ',
  'p2_title1b': 'Tự động thông báo ',
  'p2_title1c': 'để giúp bạn không bỏ lỡ các điểm tham quan hấp dẫn',

  'p2_title2': 'Bạn có thể vào mục Xplore dể dễ dàng tắt chức năng này bất kỳ lúc nào tùy thích',
  'p2_title3': 'Hãy mở và tận hưởng sự thú vị này nhé!',
  'p2_content1': 'Tìm điểm tham quan',
  'p2_content2': 'Tự động thông báo',

  'function': 'Tính năng',
  // POPUP LOCATION
  'dialog_location_note1': 'Ứng dụng cần quyền truy cập vị trí:',
  'dialog_location_note2': 'sẽ sử dụng vị trí để đưa ra gợi ý về địa điểm gần bạn khi ứng dụng được mở',
  'dialog_location_note3': 'sẽ sử dụng vị trí để thông báo khi bạn đi tới gần các địa điểm tham quan chế độ hoạt động ngay ở chế độ nền hoặc khi ứng dụng đóng',
  'dialog_location_note4': 'Dữ liệu vị trí của bạn sẽ không chia sẻ và thu thập bởi bất kì ai.',


  //SETTING SCREEN
  'setting_scr_title': 'Thiết lập', //thiet lap
  'setting_scr_biometric_enable': 'Bật đăng nhập bằng sinh trắc', //dang nhap bang sinh trac hoc
  'setting_scr_biometric_disable': 'Tắt đăng nhập bằng sinh trắc', //dang nhap bang sinh trac hoc
  'setting_scr_ads': 'Hiện mục giới thiệu', //ads
  'setting_scr_language': 'Đổi ngôn ngữ', //lang
  'setting_scr_thememode': 'Giao diện', //theme
  'setting_scr_popup_theme': 'Lựa chọn giao diện',
  'setting_scr_popup_theme_light': 'Chế độ sáng',
  'setting_scr_popup_theme_night': 'Chế độ tối',
  'setting_scr_popup_theme_sys': 'Theo hệ thống',
  'setting_scr_popup_language': 'Lựa chọn ngôn ngữ', //lang
  'setting_scr_logout': 'Đăng xuất', //log out
  'setting_scr_popup_title': 'Xác thực tài khoản', //verify
  'setting_scr_popup_pass': 'Mật khẩu',
  'setting_scr_popup_btn_next': 'Tiếp tục',
  'setting_scr_popup_btn_cancel': 'Hủy',
  'setting_scr_popup_pass_empt': 'Vui lòng điền mật khẩu',
  'setting_scr_popup_pass_wrong': 'Mật khẩu không chính xác',
  'setting_scr_des_security': 'Bảo mật',
  'setting_scr_des_customize': 'Tùy biến',

  'setting_scr_changepass': 'Đổi mật khẩu',
  'setting_pop_empt_allpass': 'Vui lòng nhập đầy đủ các thông tin',
  'setting_pop_tier_currpass': 'Mật khẩu hiện tại',
  'setting_pop_empt_currpass': 'Vui lòng nhập mật khẩu hiện tại',
  'setting_pop_tier_newpass': 'Mật khẩu mới',
  'setting_pop_empt_newpass': 'Vui lòng nhập mật khẩu mới',
  'setting_pop_tier_conf_newpass': 'Nhập lại mật khẩu mới',
  'setting_pop_empt_conf_newpass': 'Vui lòng nhập lại mật khẩu mới',
  'setting_pop_err_notmatch': 'Mật khẩu mới không trùng khớp',
  'setting_changepass_success': 'Thay đổi mật khẩu thành công',
  'setting_changepass_fail': 'Thay đổi mật khẩu không thành công',
  'home_deactivate': 'Xóa tài khoản',
  'home_deactivate_confirm1': 'Bạn có chắc chắc muốn xóa tài khoản này khỏi hệ thống?',
  'home_deactivate_confirm2': 'Điều này sẽ xóa hoàn toàn dữ liệu tài khoản của bạn và không thể khôi phục. Vui lòng cân nhắc kỹ!',
  'home_deactivate_success': 'Xóa tài khoản thành công',
  'home_deactivate_fail': 'Xóa tài khoản gặp lỗi, thử lại sau!',

  //LOGIN
  'login_scr_input_user': 'Email',
  'login_scr_input_pass': 'Mật khẩu',
  'login_scr_btn_login': 'Đăng nhập',
  'login_scr_btn_register': 'Đăng ký',
  'login_scr_question': 'Chưa có tài khoản?',
  'login_scr_msg_user': 'Vui lòng nhập tên đăng nhập',
  'login_scr_msg_pass': 'Vui lòng nhập mật khẩu',
  'login_scr_msg_ok': 'Đăng nhập thành công',
  'login_scr_msg_bio_ok': 'Đăng nhập qua xác thực sinh trắc thành công',
  'login_scr_msg_fail': 'Đăng nhập không thành công',
  'login_scr_msg_bio_fail': 'Không thể đăng nhập qua xác thực sinh trắc',
  'login_scr_popup_title': 'Bật Tính năng',
  'login_scr_popup_s1': 'Vui lòng đăng nhập vào tài khoản',
  'login_scr_popup_s2': 'Vào mục \'Cài đặt\' và chọn \'Bật tính năng đăng nhập bằng sinh trắc học\'',
  'login_scr_popup_btn': 'Đã hiểu',


  //REGISTER
  'register_scr_input_user': 'Họ Tên',
  'register_scr_input_pass': 'Mật khẩu',
  'register_scr_input_cpass': 'Xác nhận mật khẩu',
  'register_scr_input_des': 'Mô tả yêu cầu tạo tài khoản (Đơn vị - mục đích ...)',
  'register_scr_btn_login': 'Đăng nhập',
  'register_scr_btn_continue': 'Tiếp tục',
  'register_scr_btn_register': 'Đăng ký',
  'register_scr_question': 'Đã có tài khoản?',
  'register_scr_msg_user': 'Vui lòng nhập tên đăng nhập',
  'register_scr_msg_user_dup': 'Tên đăng nhập đã tồn tại',
  'register_scr_msg_pass': 'Vui lòng nhập mật khẩu',
  'register_scr_msg_cpass': 'Mật khẩu không trùng khớp với mật khẩu xác nhận',
  'register_scr_msg_ok': 'Đăng ký thành công',
  'register_scr_msg_fail': 'Đăng ký không thành công',
  'register_scr_msg_note': 'Vui lòng kiểm tra lại thông tin đăng ký!',
  'register_scr_warning_tier': 'Chú ý',
  'register_scr_warning_text': 'Yêu cầu tạo tài khoản sẽ được gửi tới quản trị viên. Vui lòng đợi quá trình xác thực hoàn tất\nđể có thể đăng nhập vào tài khoản.',
  'register_scr_msg_requestok': ' Đăng ký thành công',
  'register_scr_msg_requesttext': 'Vui lòng đăng nhập để khám phá ngay!',
  'register_scr_btn_done': 'Xong',
  'register_scr_msg_notify': 'Đăng ký tài khoản thành công, đăng nhập ngay để sử dụng',

  
  //SPEC
  'from_you': ' từ nơi bạn đang đứng',
  'scan_object': 'Quét Hiện Vật',
  'object_detected': ' hiện vật đã được khám phá',
  'question_back_scan1': 'Còn ',
  'question_back_scan2': ' hiện vật cần bạn khám phá',
  'question_back_suggestion': 'Bạn có muốn khám phá tiếp không?',
  'no_thanks': 'Không, cảm ơn',
  'scan_next': 'Quét Tiếp',

  'report_thanks_title': 'Cảm Ơn Bạn!',
  'report_thanks_content': 'Sự phản hồi của Bạn sẽ giúp\nhệ thống nhận dạng được tốt hơn',
  'request_thanks_content': 'Chúng tôi sẽ cố gắng nhanh\ncập nhật hiện vật mà Bạn yêu cầu',
  'no_object': 'Hệ thống chưa có hiện vật này',
  'please_capture': 'Nhờ bạn chụp ảnh lại nhé?',
  'not_found_object': 'Không tìm thấy hiện vật trên?',
  'question_not_found_object': 'Nhờ bạn gởi phản hồi nhé!',
  'request_guide_object': 'Yêu cầu thêm hướng dẫn\ncho hiện vật này',
  'choose_object': 'Mời bạn chọn hiện vật để nghe hướng dẫn',
  'text_guide': 'Hướng dẫn',
  'pick_guide': 'Chọn Hướng Dẫn',
  'text_ticket': 'Vé vào cổng',
  'soon_update': 'Chúng tôi sẽ sớm cập nhật tính năng này',
  'enable_notification': '  Bật thông báo',
  'enable_notification2': ' khi qua điểm tham quan',
  'question_rating1': 'Cho chúng tôi biết ',
  'question_rating2': ' Đánh giá của bạn',
  'question_rating3': ' tại điểm tham quan này nhé',

  'xplore_choose_place': 'Mời bạn chọn điểm tham quan trên\nbản đồ để coi ',
  'xplore_no_place': 'Không có điểm tham quan nào gần bạn\ntrong phạm vi ',
  'xplore_radius': '100 km',
};