const Map<String, String> ja = {
  'text_hello': 'こんにちは',
  'multipleLanguage': '複数の言語',
  'change_lang': 'Tiếng Việt',
  'lang': '日本語',

  'text_later': "後で",
  'text_loading': '読み込み中...',
  'text_error': 'エラー: ',
  'error_tier': 'エラー',
  'try_again': '再試行',
  'load_more': 'さらにロード...',
  'text_success': '成功しました',
  'connect_network_fail': 'インターネット接続がありません...',
  'connect_network_fail_des': 'Wi-Fi またはモバイル ネットワークに接続していることを確認して、もう一度お試しください。',
  'open_network_setting': '設定を開く',
  'exit_network_fail': '終了 / OK',
  'text_save': '保存',
  'text_next': '次へ',
  'text_back': '戻る',
  'input_please': '情報を入力してください!',
  'text_page': 'ページ',
  'text_search': '検索',
  'text_update': '更新',
  'text_cancel': 'キャンセル',
  'text_delete': '削除',
  'text_send': ' 送信 ',
  'hold_confirm': '長押しして実行',

  // _家_
  'system_maintenance': "システムはメンテナンス中です",
  'audio_coming_soon': '音声コンテンツは近日公開予定です',
  'we_will_be_back': "すぐに戻ります。ご迷惑をおかけして申し訳ありません!",
  'no_data_for_country': "この国では利用可能なデータがありません!",
  'no_matching_locations': "一致する場所はありません",

  // _登録する_
  'please_enter_email': "電子メールを入力してください (ログインに使用します)",
  'invalid_email_address': "無効な電子メール アドレス",
  'text_processing': "処理中",

  // _探検_
  'notification_title': "AI GuideX 通知アシスタント",
  'notification_desc': "アトラクションに近づくと、アプリが自動的に通知します",
  'allow_to_search': "近くの観光スポットの検索を続行できます",
  'stop_searching': "近くの観光スポットの検索を停止します",

  // 検出ビューダイアログ
  'no_image_captured': "画像はキャプチャされませんでした",

  // 検出ビュー
  'text_scan': 'スキャン',
  'text_description': "説明",

  // オブジェクトの検出
  'artifacts_not_found': "アーティファクトが見つかりません",
  'place_no_artifact': "このアトラクションにはアーティファクトがありません。最新の通知を受け取るにはサインアップしてください!",

  'text_previous': '前',
  'text_next_page': '次へ',

  'msg_change_lang': '言語が正常に変更されました',
  'msg_error_lang': 'この言語はすでに選択されています',

  // 搭乗中
  'p1_title1a': 'AIGuideX は ',
  'p1_title1b': '初の自動ツアーガイド ',
  'p1_title1c': 'AI テクノロジーを使用 ',
  'p1_title2a': 'スキャンするだけです ',
  'p1_title2b': 'AIGuideX ',
  'p1_title2c': 'オブジェクト上では\n建物、風景、写真、展示品など、そのオブジェクトに関する指示を\nすぐに聞くことができます ',
  'p1_title3': 'スキャンして聞くだけでとても簡単です',

  'p2_title1a': 'AIGuideX は ',
  'p2_title1b': '自動的に通知します ',
  'p2_title1c': 'アトラクションを見逃さないように',

  'p2_title2': 'Explore に移動すると、いつでもこの機能を簡単にオフにすることができます',
  'p2_title3': '開いて楽しんでください!',
  'p2_content1': 'アトラクションを検索',
  'p2_content2': '自動通知',

  'text_service': "アプリ",
  'text_service_l2': "AI テクノロジーを使用した初の自動ツアー ガイド\n アプリです",

  'entrance_ticket': "入場チケット",
  'with_ads': "(広告あり)",
  'text_transport': "トランスポート",
  'point_by_point': "ポイントごと",

  'function': '機能',

  // ポップアップの場所
  'dialog_location_note1': 'アプリには位置情報の許可が必要です:',
  'dialog_location_note2': 'アプリを開いているときに、位置情報を使用して近くの場所を提案します',
  'dialog_location_note3':
      'アトラクションに近づくとバックグラウンドで動作するか、アプリが閉じられると位置情報を使用して通知します',
  'dialog_location_note4': 'あなたの位置情報データは誰にも共有および収集されません。',

  //設定画面
  'setting_scr_title': '設定',
  //ラップ
  'setting_scr_biometric_enable': '生体認証によるログインを有効にする',
  //ダン・ナップ・バン・シン・トラックホック
  'setting_scr_biometric_disable': '生体認証によるログインを無効にする',
  // 危険です
  'setting_scr_ads': '会社情報セクションを表示',
  //phat
  'setting_scr_lang': '言語を変更',
  //lang
  'setting_scr_thememode': 'テーマモード',
  //テーマ
  'setting_scr_popup_theme': 'テーマモードを選択',
  'setting_scr_popup_theme_light': 'ライトモード',
  'setting_scr_popup_theme_night': 'ナイトモード',
  'setting_scr_popup_theme_sys': 'システム設定',
  'setting_scr_popup_lang': '言語を選択',
  //lang
  'setting_scr_logout': 'ログアウト',
  //xoa
  'setting_scr_popup_title': 'アカウントを確認',
  //確認
  'setting_scr_popup_pass': 'パスワード',
  'setting_scr_popup_btn_next': '続行',
  'setting_scr_popup_btn_cancel': 'キャンセル',
  'setting_scr_popup_pass_empt': 'パスワードを入力してください',
  'setting_scr_popup_pass_wrong': 'パスワードが間違っています',
  'setting_scr_des_security': 'セキュリティ',
  'setting_scr_des_customize': 'カスタマイズ',

  'setting_scr_changepass': 'パスワードを変更',
  'setting_pop_empt_allpass': 'すべてのフィールドを入力してください',
  'setting_pop_tier_currpass': '現在のパスワード',
  'setting_pop_empt_currpass': '現在のパスワードを入力してください',
  'setting_pop_tier_newpass': '新しいパスワード',
  'setting_pop_empt_newpass': '新しいパスワードを入力してください',
  'setting_pop_tier_conf_newpass': '新しいパスワードを確認します',
  'setting_pop_empt_conf_newpass': '確認用の新しいパスワードを入力してください',
  'setting_pop_err_notmatch': 'パスワードが一致しません',
  'setting_changepass_success': '成功しました。 パスワード変更済み',
  'setting_changepass_fail': '失敗。 有効なパスワードを入力してください',
  'home_deactivate': 'アカウントを削除',
  'home_deactivate_confirm1': 'このアカウントをシステムから削除してもよろしいですか?',
  'home_deactivate_confirm2': 'これによりアカウント データが完全に消去され、復元できなくなります。 慎重にご検討ください。」',
  'home_deactivate_success': 'アカウントは正常に削除されました',
  'home_deactivate_fail': 'エラーが発生したアカウントを削除してください。後でもう一度お試しください!',

//ログイン
  'login_scr_input_user': 'ユーザー名',
  'login_scr_input_pass': 'パスワード',
  'login_scr_btn_login': 'ログイン',
  'login_scr_btn_register': 'サインアップ',
  'login_scr_question': 'アカウントをお持ちですか?',
  'login_scr_msg_user': 'ユーザー名を入力してください',
  'login_scr_msg_pass': 'パスワードを入力してください',
  'login_scr_msg_ok': 'ログインに成功しました',
  'login_scr_msg_bio_ok': '生体認証によるログインに成功しました',
  'login_scr_msg_fail': 'ユーザー名またはパスワードが間違っていました。',
  'login_scr_msg_bio_fail': '生体認証経由でログインできません。設定を確認してください',
  'login_scr_popup_title': '認証を有効にする',
  'login_scr_popup_s1': '最初にアカウントでログインしてください',
  'login_scr_popup_s2': '「設定」を入力し、「生体認証によるログインを有効にする」を選択します。',
  'login_scr_popup_btn': 'わかりました',

//登録する
  'register_scr_input_user': 'ユーザー名',
  'register_scr_input_pass': 'パスワード',
  'register_scr_input_cpass': 'パスワードの確認',
  'register_scr_input_des': 'アカウント作成要件の説明 (単位 - 目的...)',
  'register_scr_btn_login': 'サインイン',
  'register_scr_btn_ continue': '続行',
  'register_scr_btn_register': 'サインアップ',
  'register_scr_question': 'すでにアカウントをお持ちですか?',
  'register_scr_msg_user': 'ユーザー名を入力してください',
  'register_scr_msg_user_dup': 'ユーザー名はすでに登録されています',
  'register_scr_msg_pass': 'パスワードを入力してください',
  'register_scr_msg_cpass': 'パスワードが確認パスワードと一致しません',
  'register_scr_msg_ok': '新しいアカウントが正常に登録されました',
  'register_scr_msg_fail': '登録に失敗しました',
  'register_scr_msg_note': '登録情報をもう一度確認してください!',
  'register_scr_warning_tier': '注意',
  'register_scr_warning_text':
      'アカウント作成リクエストが管理者に送信されます。 アカウントにログインするには、\n検証プロセスが完了するまでお待ちください。',
  'register_scr_msg_requestok': ' リクエストを正常に送信しました',
  'register_scr_msg_requesttext': '今すぐディスカバリーにログインしてください!',
  'register_scr_btn_done': '完了',
  'register_scr_msg_notify': 'アカウント登録が成功しました。今すぐログインして使用してください',

//スペック
  'from_you': ' あなたから',
  'scan_object': 'スキャンオブジェクト',
  'object_detected': ' オブジェクトが検出されました',
  'question_back_scan1': 'そこにあります ',
  'question_back_scan2': ' オブジェクトを探索する必要があります',
  'question_back_suggestion': '続行しますか?',
  'no_ thanks': 'いいえ、ありがとう',
  'scan_next': '次のスキャン',

  'report_ thanks_title': 'ありがとう!',
  'report_ thanks_content': 'フィードバックに基づいて、\nアプリはより適切に検出します',
  'request_ thanks_content': 'できるだけ早くオブジェクトを更新できるよう\n最善を尽くします',
  'no_object': 'アプリ内でオブジェクトが見つかりません',
  'please_capture': '写真をいただけますか?',
  'not_found_object': 'オブジェクトが見つかりませんか?',
  'question_not_found_object': 'フィードバックをお待ちしています!',
  'request_guide_object': 'オブジェクトのガイダンスを追加',
  'choose_object': 'ガイダンスを聞くオブジェクトを選択してください',
  'text_guide': 'ガイダンス',
  'pick_guide': 'ガイドを選択',
  'text_ticket': '入場券',
  'soon_update': 'この機能は間もなく更新される予定です!',
  'enable_notification': ' 通知を受け取る',
  'enable_notification2': ' システムが興味深いアトラクションを見つけたとき',
  'question_rated1': 'お時間がございましたら、よろしくお願いいたします ',
  'question_rated2': ' 共有できます',
  'question_rated3': ' 私たちとのあなたの経験',
  'xplore_choose_place': '地図上で\n表示する観光スポットを選択してください ',
  'xplore_no_place': '近くに\n範囲内の観光スポットはありません ',
  'xplore_radius': '100 km',
};
