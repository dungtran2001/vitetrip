const Map<String, String> en = {
  'text_hello': 'Hello',
  'multipleLanguage' : 'Multiple language',
  'change_lang' : 'Tiếng Việt',
  'lang' : 'English',


  'text_later': "Later",
  'text_loading': 'Loading...',
  'text_error': 'Error: ',
  'error_tier': 'Error',
  'try_again': 'Try again',
  'load_more': 'Load more...',
  'text_success': 'successfully',
  'connect_network_fail': 'There’s no internet connection...',
  'connect_network_fail_des': 'Make sure you\'re connected to a Wi-Fi or mobile network and try again!',
  'open_network_setting': 'Open setting',
  'exit_network_fail': 'Exit / OK',
  'text_save': 'Save',
  'text_next': 'Next',
  'text_back': 'Back',
  'input_please': 'Input info please!',
  'text_page': 'Page',
  'text_search': 'Search',
  'text_update': 'Update',
  'text_cancel': 'Cancel',
  'text_delete': 'Delete',
  'text_send': ' Send ',
  'hold_confirm': 'Long press to execute',

  // _HOME_
  'system_maintenance': "System is under maintenance",
  'audio_coming_soon': "Audio content is coming soon",
  'we_will_be_back': "We will be back shortly, sorry for the inconvenience!",
  'no_data_for_country': "No data available for this country!",
  'no_matching_locations': "No matching locations",

  // _REGISTER_
  'please_enter_email': "Please enter email (use for login)",
  'invalid_email_address': "Invalid email address",
  'text_processing': "Processing",

  // _XPLORE_
  'notification_title': "AI GuideX Notification assistant",
  'notification_desc': "The app will automatically notify you if you get close to the attractions",
  'allow_to_search': "Allowed to continue searching for nearby attractions",
  'stop_searching': "Stop searching nearby attractions",

  // DetectViewDialog
  'no_image_captured': "No image captured",

  // DetectView
  'text_scan': 'Scan',
  'text_description': "Description",

  // DetectObject
  'artifacts_not_found': "Artifacts not found",
  'place_no_artifact': "This attraction has no artifacts. Sign up to receive the latest notifications!",

  'text_previous': 'Previous',
  'text_next_page': 'Next',

  'msg_change_lang' : 'Changed language successfully',
  'msg_error_lang' : 'This language already selected',


  //ON BOARDING
  'p1_title1a': 'AIGuideX is ',
  'p1_title1b': 'the first automated tour guide ',
  'p1_title1c': 'using AI technology ',
  'p1_title2a': 'You just need to scan ',
  'p1_title2b': 'AIGuideX ',
  'p1_title2c': 'on an object\nand you can immediately hear instructions\nabout the object, whether it\'s a building, a landscape, a photograph or an artifact on display ',
  'p1_title3': 'All very easy just by scanning and listening',


  'p2_title1a': 'AIGuideX will ',
  'p2_title1b': 'automatically notify you ',
  'p2_title1c': 'so you don\'t miss the attractions',

  'p2_title2': 'You can go to Explore to easily turn off this function at any time you like',
  'p2_title3': 'Open it and enjoy the fun!',
  'p2_content1': 'Find attractions',
  'p2_content2': 'Automatic notifications',

  'text_service': "App",
  'text_service_l2': "is the first automatic tour guide\n app using AI technology",

  'entrance_ticket': "Entrance ticket",
  'with_ads': "(With ads)",
  'text_transport': "Transport",
  'point_by_point': "point by point",

  'function': 'Feature',

  // POPUP LOCATION
  'dialog_location_note1': 'The app needs location permission:',
  'dialog_location_note2': 'will use location to suggest places near you when the app is open',
  'dialog_location_note3': 'will use location to notify when you get close to attractions works in the background or when the app is closed',
  'dialog_location_note4': 'Your location data will not be shared and collected by anyone.',

  //SETTING SCREEN
  'setting_scr_title': 'Setting', //thiet lap
  'setting_scr_biometric_enable': 'Enable login with biometric', //dang nhap bang sinh trac hoc
  'setting_scr_biometric_disable': 'Disable login with biometric', //dang nhap bang sinh trac hoc
  'setting_scr_ads': 'Visible company info section', //phat
  'setting_scr_language': 'Change language', //lang
  'setting_scr_thememode': 'Theme mode', //theme
  'setting_scr_popup_theme': 'Choose theme mode',
  'setting_scr_popup_theme_light': 'Light mode',
  'setting_scr_popup_theme_night': 'Night mode',
  'setting_scr_popup_theme_sys': 'System setting',
  'setting_scr_popup_language': 'Choose language', //lang
  'setting_scr_logout': 'Log out', //xoa
  'setting_scr_popup_title': 'Verify account', //verify
  'setting_scr_popup_pass': 'Password',
  'setting_scr_popup_btn_next': 'Continue',
  'setting_scr_popup_btn_cancel': 'Cancel',
  'setting_scr_popup_pass_empt': 'Please input password',
  'setting_scr_popup_pass_wrong': 'Wrong password',
  'setting_scr_des_security': 'Security',
  'setting_scr_des_customize': 'Customization',

  'setting_scr_changepass': 'Change Password',
  'setting_pop_empt_allpass': 'Please enter all field',
  'setting_pop_tier_currpass': 'Current password',
  'setting_pop_empt_currpass': 'Please enter current password',
  'setting_pop_tier_newpass': 'New password',
  'setting_pop_empt_newpass': 'Please enter new password',
  'setting_pop_tier_conf_newpass': 'Confirm new password',
  'setting_pop_empt_conf_newpass': 'Please enter confirm new password',
  'setting_pop_err_notmatch': 'Mismatched password',
  'setting_changepass_success': 'Success. Password changed',
  'setting_changepass_fail': 'Fail. Please enter valid password',
  'home_deactivate': 'Delete account',
  'home_deactivate_confirm1': 'Are you sure you want to delete this account from the system?',
  'home_deactivate_confirm2': 'This will completely erase your account data and cannot be recovered. Please consider carefully!',
  'home_deactivate_success': 'Account deleted successfully',
  'home_deactivate_fail': 'Delete the account with the error, try again later!',

  //LOGIN
  'login_scr_input_user': 'Username',
  'login_scr_input_pass': 'Password',
  'login_scr_btn_login': 'Log in',
  'login_scr_btn_register': 'Sign up',
  'login_scr_question': 'Don\'t have an account?',
  'login_scr_msg_user': 'Please input username',
  'login_scr_msg_pass': 'Please input password',
  'login_scr_msg_ok': 'Login successfully',
  'login_scr_msg_bio_ok': 'Login via biometric successfully',
  'login_scr_msg_fail': 'Your username or password was incorrect.',
  'login_scr_msg_bio_fail': 'Can\'t login via biometric, review setting please',
  'login_scr_popup_title': 'Enable authorization',
  'login_scr_popup_s1': 'Please login with account first',
  'login_scr_popup_s2': 'Enter \'Setting\' and select \'Enable login with biometric\'',
  'login_scr_popup_btn': 'Got it',

  //REGISTER
  'register_scr_input_user': 'Username',
  'register_scr_input_pass': 'Password',
  'register_scr_input_cpass': 'Password confirm',
  'register_scr_input_des': 'Description of account creation requirements (Unit - purpose...)',
  'register_scr_btn_login': 'Sign in',
  'register_scr_btn_continue': 'Continue',
  'register_scr_btn_register': 'Sign up',
  'register_scr_question': 'Already has account?',
  'register_scr_msg_user': 'Please input username',
  'register_scr_msg_user_dup': 'Already username registered',
  'register_scr_msg_pass': 'Please input password',
  'register_scr_msg_cpass': 'Password does not match the confirmation password',
  'register_scr_msg_ok': 'Register new account successfully',
  'register_scr_msg_fail': 'Registration failed',
  'register_scr_msg_note': 'Please check registration information again!',
  'register_scr_warning_tier': 'Attention',
  'register_scr_warning_text': 'The request to create an account will be sent to the administrator. Please wait for the verification process\nto complete before you can log in to your account.',
  'register_scr_msg_requestok': ' Send request successfully',
  'register_scr_msg_requesttext': 'Please login to discovery now!',
  'register_scr_btn_done': 'Done',
  'register_scr_msg_notify': 'Successful account registration, login now to use',

  
  //SPEC
  'from_you': ' from you',
  'scan_object': 'Scan Object',
  'object_detected': ' objects detected',
  'question_back_scan1': 'There\'s ',
  'question_back_scan2': ' objects need to be explored',
  'question_back_suggestion': 'Do you want to continue?',
  'no_thanks': 'No, thanks',
  'scan_next': 'Scan Next',

  'report_thanks_title': 'Thank You!',
  'report_thanks_content': 'The app will detect better\nbased on your feedback',
  'request_thanks_content': 'We will try our best to update\nthe object as soon as possible',
  'no_object': 'Unable to find the object in the app',
  'please_capture': 'Could you give us a picture please?',
  'not_found_object': 'Unable to find the object?',
  'question_not_found_object': 'We want your feedback please!',
  'request_guide_object': 'Add guidance for the object',
  'choose_object': 'Choose an object to listen the guidance please',
  'text_guide': 'Guidance',
  'pick_guide': 'Pick a Guide',
  'text_ticket': 'Entrance ticket',
  'soon_update': 'We are going to update this feature soon!',
  'enable_notification': '  Get notified',
  'enable_notification2': ' when the system find interesting attractions',
  'question_rating1': 'If you have a moment, we would appreciate if ',
  'question_rating2': ' you could share',
  'question_rating3': ' your experience with us',
  'xplore_choose_place': 'Please select attractions on\n the map to see ',
  'xplore_no_place': 'There are no attractions near you\nwithin ',
  'xplore_radius': '100 km',
};