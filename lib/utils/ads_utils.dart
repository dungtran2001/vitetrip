import 'dart:ui';

import 'package:ironsource_mediation/ironsource_mediation.dart';

class OnInitCompleted extends IronSourceInitializationListener {

  VoidCallback? onCompleted;

  OnInitCompleted({this.onCompleted});

  @override
  void onInitializationComplete() {
    onCompleted?.call();
  }
}

class InterstitialListener extends LevelPlayInterstitialListener {

  VoidCallback onClosed;
  VoidCallback onFailed;
  VoidCallback? onLoaded;

  InterstitialListener({required this.onClosed, required this.onFailed, this.onLoaded}): super();

  @override
  void onAdClicked(IronSourceAdInfo adInfo) {
    // TODO: implement onAdClicked
  }

  @override
  void onAdClosed(IronSourceAdInfo adInfo) {
    onClosed();
  }

  @override
  void onAdLoadFailed(IronSourceError error) {
    print(error);
    onFailed();
  }

  @override
  void onAdOpened(IronSourceAdInfo adInfo) {
    // TODO: implement onAdOpened
  }

  @override
  void onAdReady(IronSourceAdInfo adInfo) {
    this.onLoaded?.call();
  }

  @override
  void onAdShowFailed(IronSourceError error, IronSourceAdInfo adInfo) {
    // TODO: implement onAdShowFailed
  }

  @override
  void onAdShowSucceeded(IronSourceAdInfo adInfo) {
    // TODO: implement onAdShowSucceeded
  }
  
}

class AdsUtils {

  var counterToShowAd = 3;
  var adShowCounter = 0;

  bool isValidToShowAd() {
    return adShowCounter == counterToShowAd;
  }

  void increaseCounter() {
    adShowCounter++;
    print("AdShowCounter: $adShowCounter");
  }

  void loadAd() {
    IronSource.loadInterstitial();
  }

  void showAds(VoidCallback? onClosed, VoidCallback? onFailed) async {
    var isAdReady = await IronSource.isInterstitialReady();
    if (isAdReady) {
      adShowCounter = 0;
      IronSource.setLevelPlayInterstitialListener(InterstitialListener(onClosed: () => {
        onClosed?.call()
      }, onFailed: () => {
        onFailed?.call()
      }));
      IronSource.showInterstitial();
    } else {
      onClosed?.call();
    }
  }
}