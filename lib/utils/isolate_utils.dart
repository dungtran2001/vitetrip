import 'dart:io';
import 'dart:isolate';

import 'package:camera/camera.dart';
import 'package:image/image.dart' as imageLib;
import '/services/tflite/classifier.dart';
import '/utils/image_utils.dart';
import 'package:tflite_flutter/tflite_flutter.dart';

/// Manages separate Isolate instance for inference
class IsolateUtils {
  static const String DEBUG_NAME = "InferenceIsolate";
  late final String modelFileName;
  late final String labelsFileName;
  static void entryPoint(SendPort sendPort) async {
    final port = ReceivePort();
    sendPort.send(port.sendPort);

    await for (final IsolateData isolateData in port) {
      if (isolateData != null) {
        Classifier classifier = Classifier(
          interpreter:
          Interpreter.fromAddress(isolateData.interpreterAddress!),
          labels: isolateData.labels!,
          MODEL_FILE_NAME: "",
          LABEL_FILE_NAME: "",
        );
        imageLib.Image image =
        ImageUtils.convertCameraImage(isolateData.cameraImage!)!;
        if (Platform.isAndroid) {
          image = imageLib.copyRotate(image, 90);
        }
        Map<String, dynamic> results = classifier.predict(image)!;
        isolateData.responsePort!.send(results);
      }
    }
  }

  void setFileName(
    modelFileName,
    labelsFileName,
){
    modelFileName = modelFileName;
    labelsFileName = labelsFileName;
  }
  late Isolate _isolate;
  ReceivePort _receivePort = ReceivePort();
  late SendPort _sendPort;

  SendPort get sendPort => _sendPort;

  Future<void> start() async {
    _isolate = await Isolate.spawn<SendPort>(
      entryPoint,
      _receivePort.sendPort,
      debugName: DEBUG_NAME,
    );

    _sendPort = await _receivePort.first;
  }

}

/// Bundles data to pass between Isolate
class IsolateData {
  CameraImage? cameraImage;
  int? interpreterAddress;
  List<String>? labels;
  SendPort? responsePort;

  IsolateData(
    this.cameraImage,
    this.interpreterAddress,
    this.labels,
  );
}
