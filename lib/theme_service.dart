import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';

class ThemeService {
  static final  storage =  new FlutterSecureStorage();
  static final _key = 'themeMode';

  static const String nightmode = "night";
  static const String lightmode = "light";
  static const String sysmode = "system";

  /// Get isDarkMode info from local storage and return ThemeMode
  ThemeMode get theme => currentTheme ==  nightmode ? ThemeMode.dark : currentTheme == lightmode ? ThemeMode.light : ThemeMode.system;


  ThemeMode getTheme (_curr){
//    var _curr = _loadThemeFromBox();
    switch(_curr){
      case nightmode:
        return ThemeMode.dark;

        case lightmode:
        return ThemeMode.light;

      default:
        return ThemeMode.system;
    }
  }

  static String currentTheme = sysmode;
  /// Load from storage
  String loadThemeFromBox() {
       storage.read(key: _key).then((value) {
        try{
          if (value != null){
            currentTheme = value;
            changeThemeMode(currentTheme);
            return value;
          }else{
            return sysmode;
          }
        }catch(e){
          return sysmode;
        }

      });
      return sysmode;
  }

  /// Switch theme and save to local storage
  void changeThemeMode(mode) {
    currentTheme = mode;
    Get.changeThemeMode(getTheme(mode));
    storage.write(key: _key, value: mode);
  }

}