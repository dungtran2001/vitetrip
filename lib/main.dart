import 'dart:ui';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:ironsource_mediation/ironsource_mediation.dart';
import 'package:mitta/services/audio_player/audio_manager.dart';
import 'package:mitta/utils/ads_utils.dart';
import 'package:path_provider/path_provider.dart';

import '/services/services_locator.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'configs/config_routes.dart';
import 'services/language/localization_service.dart';
import 'theme.dart';
import 'views/welcome/welcome_screen.dart';

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  await Firebase.initializeApp();
  print('onLaunch called: $message');
  print("Handling a background message: ${message.messageId}");
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  // Setup flutter crashlytics
  await Firebase.initializeApp();
  FlutterError.onError = (errorDetails) {
    FirebaseCrashlytics.instance.recordFlutterFatalError(errorDetails);
  };
  // Pass all uncaught asynchronous errors that aren't handled by the Flutter framework to Crashlytics
  PlatformDispatcher.instance.onError = (error, stack) {
    FirebaseCrashlytics.instance.recordError(error, stack, fatal: true);
    return true;
  };

  // Setup firebase messaging
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  await setupLocator();
  locator<MediaManager>().init();

  IronSource.setFlutterVersion("3.3.0");
  IronSource.init(
      appKey: "1b6897e15",
      initListener:
          OnInitCompleted(onCompleted: () => {IronSource.loadInterstitial()}));

  var dir = await getApplicationDocumentsDirectory();
  print('Dir: ${dir.path}');

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    MediaQueryData windowData =
        MediaQueryData.fromWindow(WidgetsBinding.instance.window);
    // windowData = windowData.copyWith(
    //   textScaleFactor: windowData.textScaleFactor > 1.1 ? 1.1 : windowData.textScaleFactor,
    // );
    return GetMaterialApp(
      builder: (BuildContext context, Widget? child) {
        final MediaQueryData data = MediaQuery.of(context);
        return MediaQuery(
          data: windowData.copyWith(
            textScaleFactor: windowData.textScaleFactor > 1.0
                ? 1.0
                : windowData.textScaleFactor,
          ),
          child: child!,
        );
      },
      title: 'AIGuideX.vn',
      theme: Themes.light,
      darkTheme: Themes.dark,
      themeMode: ThemeMode.light,
      debugShowCheckedModeBanner: false,
//      home: WelcomeScreen(),
      locale: LocalizationService.locale,
      fallbackLocale: LocalizationService.fallbackLocale,
      translations: LocalizationService(),
//      home: MyHomePage(),
      initialRoute: "/splashscreen",
      //"/stream", //
      getPages: AppRoutes.routes,
    );
  }
}
