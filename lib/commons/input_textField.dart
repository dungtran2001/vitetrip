import 'package:flutter/services.dart';

import '../app_theme.dart';
import 'package:flutter/material.dart';

class InputTextField extends StatelessWidget {
  final String text;
  final AnimationController? animationController;
  final Animation<double>? animation;
  final TextEditingController? inputController;
  final TextInputType? inputType;
  final bool? isObscureText;
  final bool? isReadOnly;
  final bool? isError;
  final String? message;
  final int? maxLength;
  final ValueChanged<String>? onchange;
  final TextCapitalization? textCapitalization;
  final List<TextInputFormatter>? formatInput;
  final TextStyle? style;
  const InputTextField(
      {Key? key,
      this.text: "",
      this.inputType,
      this.isObscureText,
      this.isError,
      this.message,
      this.maxLength,
      this.animationController,
      this.inputController,
        this.isReadOnly = false,
        this.onchange,
        this.textCapitalization = TextCapitalization.none,
        this.formatInput,
        this.style = const TextStyle(fontSize: 18, color: Colors.black),
      this.animation})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: animationController!,
      builder: (BuildContext context, Widget? child) {
        return FadeTransition(
          opacity: animation!,
          child: new Transform(
            transform: new Matrix4.translationValues(
                0.0, 30 * (1.0 - animation!.value), 3.0),
            child: Container(
              child: Padding(
                padding: const EdgeInsets.only(left: 5, right: 5),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        // left, top, right, and bottom.
                        padding: const EdgeInsets.fromLTRB(0 , 0, 0, 0.0),
                        child: TextField(
                          readOnly: isReadOnly!,
                            inputFormatters: formatInput,//[FilteringTextInputFormatter.deny(RegExp(r"\s")),],
                            textCapitalization: textCapitalization!,
                            onChanged: onchange,
                            controller: inputController,
                            keyboardType: inputType,
                            obscureText: isObscureText ?? false,
                            maxLength: maxLength,
                            minLines: 1,
                            maxLines: isObscureText! ? 1 : 5,
                            style: style,
                            decoration: InputDecoration(
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.grey),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.blue),
                                ),

                                labelText: text,
                                errorText: isError! ? message : null ,
                                labelStyle: TextStyle( fontSize: 17))),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
