import 'package:flutter/material.dart';

class TabIconData {
  TabIconData({
    this.icon,
    this.name,
    this.index = 0,
    this.isSelected = false,
    this.animationController,
  });

  Icon? icon;
  bool isSelected;
  int index;
  String? name;
  AnimationController? animationController;
  static void resetSelect() {
    tabIconsList.forEach((TabIconData tab) {
      tab.isSelected = false;
    });
  }
  static List<TabIconData> tabIconsList = <TabIconData>[
    TabIconData(
      icon: Icon(Icons.menu),
      name: 'Menu',
      index: 0,
      isSelected: false,
      animationController: null,
    ),
    TabIconData(
      icon: Icon(Icons.account_circle),
      name: 'Tài khoản',
      index: 1,
      isSelected: false,
      animationController: null,
    ),
    TabIconData(
      icon: Icon(Icons.history),
      name: 'Lịch sử',
      index: 2,
      isSelected: false,
      animationController: null,
    ),
    TabIconData(
      icon: Icon(Icons.notifications),
      name: 'Thông báo',
      index: 3,
      isSelected: false,
      animationController: null,
    )
  ];
}
