import 'package:flutter/material.dart';
import 'package:mitta/configs/configs.dart';
import 'package:mitta/views/detect/painter/border_paint.dart';
import '../app_theme.dart';
import 'button/button_rotation.dart';
import 'hole_painter.dart';
import 'staggered_raindrop_animation.dart';

class AnimationScreen extends StatefulWidget {
  AnimationScreen({this.color});

  final Color? color;

  @override
  _AnimationScreenState createState() => _AnimationScreenState();
}

class _AnimationScreenState extends State<AnimationScreen>
    with SingleTickerProviderStateMixin {
  Size size = Size.zero;
  AnimationController? _controller;
  StaggeredRaindropAnimation? _animation;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(milliseconds: 5000),
      vsync: this,
    );
    _animation = StaggeredRaindropAnimation(_controller!);
    _controller?.forward();

    _controller?.addListener(() {
      setState(() {});
    });
  }

  @override
  void didChangeDependencies() {
    setState(() {
      size = MediaQuery.of(context).size;
    });
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Stack(
          alignment: Alignment.center,
          children: [
        // Container(
        //     width: size.width,
        //     height: size.height,
        //     decoration: BoxDecoration(
        //       image: new DecorationImage(
        //         image: new ExactAssetImage(
        //             "assets/images/locations/halong1.jpg"
        //         ),
        //         fit: BoxFit.cover,
        //       ),
        //     ),
        //     child: CustomPaint(
        //         painter: HolePainter(
        //             color: widget.color!.withOpacity(.4),
        //             holeSize: _animation?.holeSize.value ?? 0 * size.width))),

        Positioned(
            bottom: size.height/5,
            left: 0,
            right: 0,
            child: Opacity(
                opacity: _animation!.textOpacity.value,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: size.width/4),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(2),
                    child: LinearProgressIndicator(
                      minHeight: 6.5,
                      valueColor: AlwaysStoppedAnimation<Color>(Color(0xFF00BDD6)),
                      backgroundColor: Color(0xFF00BDD6).withOpacity(.4),
                    ),
                  ),
                )
            )),


        ///title app
        Positioned(
            top: size.height/5,
            child: Opacity(
                opacity: _animation!.textOpacity.value,
                child: Container(
                  width: size.width /2,
                  height: size.width /2,
                  // color: AppTheme.grey,
                  child: Image.asset("assets/logo/app_logo2.png"),
                )
            )),

      ]),
    );
  }

  @override
  void dispose() {
    _controller!.dispose();
    super.dispose();
  }
}

class DropPainter extends CustomPainter {
  DropPainter({this.visible = true});

  bool visible;

  @override
  void paint(Canvas canvas, Size size) {
    if (!visible) {
      return;
    }

    Paint paint = Paint();
    Path path;

    double w = size.width + 32;
    double h = size.height + 32;
    // Path number 1


    paint.color = Color(0xffa0cc00);
    path = Path();
    path.lineTo(w * 0.06, h * 1.2);
    path.cubicTo(w * 0.08, h * 1.23, w * 0.12, h * 1.23, w * 0.14, h * 1.19);
    path.cubicTo(w * 0.14, h * 1.19, w / 5, h * 1.04, w / 5, h * 1.04);
    path.cubicTo(w / 5, h * 1.04, w / 5, h * 1.04, w / 5, h * 1.04);
    path.cubicTo(w * 0.26, h, w / 3, h, w * 0.39, h * 1.02);
    path.cubicTo(w * 0.39, h * 1.02, w * 0.39, h * 1.02, w * 0.39, h * 1.02);
    path.cubicTo(w * 0.48, h * 1.06, w * 0.59, h * 1.06, w * 0.68, h * 1.02);
    path.cubicTo(w * 0.68, h * 1.02, w * 0.68, h * 1.02, w * 0.68, h * 1.02);
    path.cubicTo(w * 0.82, h * 0.97, w * 0.92, h * 0.83, w * 0.96, h * 0.65);
    path.cubicTo(w * 0.96, h * 0.65, w * 1.05, h * 0.22, w * 1.05, h * 0.22);
    path.cubicTo(w * 1.05, h * 0.22, w * 0.94, h * 0.3, w * 0.94, h * 0.3);
    path.cubicTo(w * 0.84, h * 0.38, w * 0.71, h * 0.39, w * 0.59, h * 0.35);
    path.cubicTo(w * 0.59, h * 0.35, w * 0.59, h * 0.35, w * 0.59, h * 0.35);
    path.cubicTo(w / 2, h * 0.31, w * 0.4, h / 3, w / 3, h * 0.41);
    path.cubicTo(w / 3, h * 0.41, w / 3, h * 0.41, w / 3, h * 0.41);
    path.cubicTo(w * 0.26, h * 0.47, w * 0.22, h * 0.57, w * 0.22, h * 0.68);
    path.cubicTo(w * 0.22, h * 0.68, w / 5, h * 0.85, w / 5, h * 0.85);
    path.cubicTo(w / 5, h * 0.89, w / 5, h * 0.93, w * 0.19, h * 0.96);
    path.cubicTo(w * 0.19, h * 0.96, w * 0.17, h, w * 0.17, h);
    path.cubicTo(w * 0.17, h, w * 0.07, h * 1.12, w * 0.07, h * 1.12);
    path.cubicTo(w * 0.05, h * 1.14, w * 0.05, h * 1.17, w * 0.06, h * 1.2);
    path.cubicTo(w * 0.06, h * 1.2, w * 0.06, h * 1.2, w * 0.06, h * 1.2);
    canvas.drawPath(path, paint);


    // Path number 2


    paint.color = Color(0xffffffff).withOpacity(0);
    path = Path();
    path.lineTo(w * 0.59, h * 0.89);
    path.cubicTo(w * 0.58, h * 0.9, w * 0.57, h * 0.9, w * 0.56, h * 0.89);
    path.cubicTo(w / 2, h * 0.84, w * 0.43, h * 0.82, w * 0.36, h * 0.84);
    path.cubicTo(w * 0.36, h * 0.84, w * 0.28, h * 0.87, w * 0.28, h * 0.87);
    path.cubicTo(w * 0.27, h * 0.87, w * 0.26, h * 0.87, w * 0.26, h * 0.85);
    path.cubicTo(w * 0.26, h * 0.85, w * 0.26, h * 0.85, w * 0.26, h * 0.85);
    path.cubicTo(w * 0.26, h * 0.84, w * 0.26, h * 0.83, w * 0.27, h * 0.83);
    path.cubicTo(w * 0.27, h * 0.83, w * 0.35, h * 0.8, w * 0.35, h * 0.8);
    path.cubicTo(w * 0.43, h * 0.77, w * 0.52, h * 0.79, w * 0.58, h * 0.86);
    path.cubicTo(w * 0.59, h * 0.86, w * 0.59, h * 0.88, w * 0.59, h * 0.89);
    path.cubicTo(w * 0.59, h * 0.89, w * 0.59, h * 0.89, w * 0.59, h * 0.89);
    canvas.drawPath(path, paint);


    // Path number 3


    paint.color = Color(0xffffffff).withOpacity(0);
    path = Path();
    path.lineTo(w * 0.78, h * 0.73);
    path.cubicTo(w * 0.77, h * 0.74, w * 0.76, h * 0.75, w * 0.75, h * 0.74);
    path.cubicTo(w * 0.67, h * 0.68, w * 0.57, h * 0.66, w * 0.48, h * 0.68);
    path.cubicTo(w * 0.48, h * 0.68, w * 0.43, h * 0.7, w * 0.43, h * 0.7);
    path.cubicTo(w * 0.43, h * 0.7, w * 0.43, h * 0.7, w * 0.43, h * 0.7);
    path.cubicTo(w * 0.43, h * 0.7, w * 0.38, h * 0.71, w * 0.38, h * 0.71);
    path.cubicTo(w * 0.37, h * 0.72, w * 0.36, h * 0.71, w * 0.36, h * 0.7);
    path.cubicTo(w * 0.36, h * 0.7, w * 0.36, h * 0.7, w * 0.36, h * 0.7);
    path.cubicTo(w * 0.36, h * 0.69, w * 0.36, h * 0.67, w * 0.37, h * 0.67);
    path.cubicTo(w * 0.37, h * 0.67, w * 0.42, h * 0.65, w * 0.42, h * 0.65);
    path.cubicTo(w * 0.42, h * 0.65, w * 0.42, h * 0.65, w * 0.42, h * 0.65);
    path.cubicTo(w * 0.42, h * 0.65, w * 0.48, h * 0.64, w * 0.48, h * 0.64);
    path.cubicTo(w * 0.58, h * 0.61, w * 0.68, h * 0.64, w * 0.77, h * 0.7);
    path.cubicTo(w * 0.78, h * 0.71, w * 0.78, h * 0.72, w * 0.78, h * 0.73);
    path.cubicTo(w * 0.78, h * 0.73, w * 0.78, h * 0.73, w * 0.78, h * 0.73);
    canvas.drawPath(path, paint);


    // Path number 4


    paint.color = Color(0xffffffff).withOpacity(0);
    path = Path();
    path.lineTo(w * 0.83, h * 0.63);
    path.cubicTo(w * 0.81, h * 0.63, w * 0.79, h * 0.63, w * 0.77, h * 0.63);
    path.cubicTo(w * 0.77, h * 0.63, w * 0.72, h * 0.62, w * 0.72, h * 0.62);
    path.cubicTo(w * 0.72, h * 0.62, w * 0.62, h * 0.6, w * 0.62, h * 0.6);
    path.cubicTo(w * 0.61, h * 0.6, w * 0.61, h * 0.59, w * 0.61, h * 0.58);
    path.cubicTo(w * 0.61, h * 0.58, w * 0.61, h * 0.58, w * 0.61, h * 0.58);
    path.cubicTo(w * 0.61, h * 0.56, w * 0.62, h * 0.56, w * 0.63, h * 0.56);
    path.cubicTo(w * 0.63, h * 0.56, w * 0.73, h * 0.58, w * 0.73, h * 0.58);
    path.cubicTo(w * 0.73, h * 0.58, w * 0.78, h * 0.59, w * 0.78, h * 0.59);
    path.cubicTo(w * 0.82, h * 0.59, w * 0.86, h * 0.59, w * 0.89, h * 0.58);
    path.cubicTo(w * 0.9, h * 0.58, w * 0.91, h * 0.59, w * 0.91, h * 0.6);
    path.cubicTo(w * 0.91, h * 0.6, w * 0.91, h * 0.6, w * 0.91, h * 0.6);
    path.cubicTo(w * 0.92, h * 0.61, w * 0.91, h * 0.62, w * 0.9, h * 0.63);
    path.cubicTo(w * 0.88, h * 0.63, w * 0.85, h * 0.63, w * 0.83, h * 0.63);
    path.cubicTo(w * 0.83, h * 0.63, w * 0.83, h * 0.63, w * 0.83, h * 0.63);
    canvas.drawPath(path, paint);


    // Path number 5


    paint.color = Color(0xffffffff).withOpacity(0);
    path = Path();
    path.lineTo(w * 0.32, h * 0.73);
    path.cubicTo(w * 0.31, h * 0.74, w * 0.3, h * 0.74, w * 0.3, h * 0.73);
    path.cubicTo(w * 0.3, h * 0.73, w * 0.29, h * 0.71, w * 0.29, h * 0.71);
    path.cubicTo(w * 0.27, h * 0.68, w * 0.27, h * 0.64, w * 0.27, h * 0.59);
    path.cubicTo(w * 0.27, h * 0.58, w * 0.28, h * 0.57, w * 0.29, h * 0.58);
    path.cubicTo(w * 0.29, h * 0.58, w * 0.3, h * 0.58, w * 0.3, h * 0.58);
    path.cubicTo(w * 0.3, h * 0.58, w * 0.31, h * 0.59, w * 0.31, h * 0.6);
    path.cubicTo(w * 0.3, h * 0.63, w * 0.31, h * 0.66, w * 0.32, h * 0.69);
    path.cubicTo(w * 0.32, h * 0.69, w * 0.32, h * 0.7, w * 0.32, h * 0.7);
    path.cubicTo(w * 0.32, h * 0.7, w / 3, h * 0.7, w / 3, h * 0.7);
    path.cubicTo(w / 3, h * 0.71, w / 3, h * 0.72, w * 0.32, h * 0.73);
    path.cubicTo(w * 0.32, h * 0.73, w * 0.32, h * 0.73, w * 0.32, h * 0.73);
    canvas.drawPath(path, paint);


    // Path number 6


    paint.color = Color(0xffffffff).withOpacity(0);
    path = Path();
    path.lineTo(w * 0.45, h * 0.65);
    path.cubicTo(w * 0.45, h * 0.65, w * 0.45, h * 0.65, w * 0.45, h * 0.65);
    path.cubicTo(w * 0.44, h * 0.65, w * 0.43, h * 0.64, w * 0.43, h * 0.63);
    path.cubicTo(w * 0.43, h * 0.63, w * 0.43, h * 0.55, w * 0.43, h * 0.55);
    path.cubicTo(w * 0.43, h / 2, w * 0.44, h * 0.45, w * 0.46, h * 0.41);
    path.cubicTo(w * 0.46, h * 0.4, w * 0.47, h * 0.39, w * 0.48, h * 0.4);
    path.cubicTo(w * 0.48, h * 0.4, w * 0.48, h * 0.4, w * 0.48, h * 0.4);
    path.cubicTo(w * 0.49, h * 0.41, w * 0.49, h * 0.42, w * 0.49, h * 0.43);
    path.cubicTo(w * 0.47, h * 0.46, w * 0.47, h / 2, w * 0.47, h * 0.55);
    path.cubicTo(w * 0.47, h * 0.55, w * 0.47, h * 0.63, w * 0.47, h * 0.63);
    path.cubicTo(w * 0.47, h * 0.64, w * 0.46, h * 0.65, w * 0.45, h * 0.65);
    path.cubicTo(w * 0.45, h * 0.65, w * 0.45, h * 0.65, w * 0.45, h * 0.65);
    canvas.drawPath(path, paint);


    // Path number 7


    paint.color = Color(0xffffffff).withOpacity(0);
    path = Path();
    path.lineTo(w * 0.58, h * 0.57);
    path.cubicTo(w * 0.58, h * 0.57, w * 0.58, h * 0.57, w * 0.58, h * 0.57);
    path.cubicTo(w * 0.57, h * 0.56, w * 0.56, h * 0.55, w * 0.57, h * 0.54);
    path.cubicTo(w * 0.57, h * 0.54, w * 0.57, h * 0.53, w * 0.57, h * 0.53);
    path.cubicTo(w * 0.58, h * 0.48, w * 0.6, h * 0.45, w * 0.63, h * 0.43);
    path.cubicTo(w * 0.64, h * 0.43, w * 0.65, h * 0.43, w * 0.66, h * 0.44);
    path.cubicTo(w * 0.66, h * 0.44, w * 0.66, h * 0.45, w * 0.66, h * 0.45);
    path.cubicTo(w * 0.66, h * 0.46, w * 0.65, h * 0.47, w * 0.65, h * 0.47);
    path.cubicTo(w * 0.63, h * 0.49, w * 0.61, h * 0.51, w * 0.6, h * 0.54);
    path.cubicTo(w * 0.6, h * 0.54, w * 0.6, h * 0.55, w * 0.6, h * 0.55);
    path.cubicTo(w * 0.6, h * 0.56, w * 0.59, h * 0.57, w * 0.58, h * 0.57);
    path.cubicTo(w * 0.58, h * 0.57, w * 0.58, h * 0.57, w * 0.58, h * 0.57);
    canvas.drawPath(path, paint);


    // Path number 8


    paint.color = Color(0xffffffff).withOpacity(0);
    path = Path();
    path.lineTo(w * 1.05, h * 0.22);
    path.cubicTo(w * 1.05, h * 0.22, w * 0.95, h * 0.41, w * 0.95, h * 0.41);
    path.cubicTo(w * 0.87, h * 0.54, w * 0.75, h * 0.62, w * 0.63, h * 0.62);
    path.cubicTo(w * 0.49, h * 0.62, w * 0.36, h * 0.71, w * 0.29, h * 0.87);
    path.cubicTo(w * 0.29, h * 0.87, w / 5, h * 1.04, w / 5, h * 1.04);
    path.cubicTo(w / 5, h * 1.04, w * 0.14, h * 1.19, w * 0.14, h * 1.19);
    path.cubicTo(w * 0.12, h * 1.23, w * 0.07, h * 1.23, w * 0.06, h * 1.19);
    path.cubicTo(w * 0.05, h * 1.16, w * 0.05, h * 1.13, w * 0.07, h * 1.12);
    path.cubicTo(w * 0.07, h * 1.12, w * 0.17, h, w * 0.17, h);
    path.cubicTo(w * 0.17, h, w * 0.24, h * 0.82, w * 0.24, h * 0.82);
    path.cubicTo(w * 0.32, h * 0.64, w * 0.46, h * 0.53, w * 0.62, h * 0.53);
    path.cubicTo(w * 0.72, h * 0.53, w * 0.82, h * 0.48, w * 0.9, h * 0.4);
    path.cubicTo(w * 0.9, h * 0.4, w * 1.05, h * 0.22, w * 1.05, h * 0.22);
    canvas.drawPath(path, paint);


    // Path number 9


    paint.color = Color(0xff719600);
    path = Path();
    path.lineTo(w / 5, h * 1.04);
    path.cubicTo(w / 5, h * 1.04, w * 0.14, h * 1.19, w * 0.14, h * 1.19);
    path.cubicTo(w * 0.12, h * 1.23, w * 0.07, h * 1.23, w * 0.06, h * 1.19);
    path.cubicTo(w * 0.05, h * 1.16, w * 0.05, h * 1.13, w * 0.07, h * 1.12);
    path.cubicTo(w * 0.07, h * 1.12, w * 0.17, h, w * 0.17, h);
    path.cubicTo(w * 0.17, h, w / 5, h * 1.04, w / 5, h * 1.04);
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
