
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ButtonBack{
  static Widget backButtonSimple(
      {
        Color backgroundColor = Colors.white60,
        Color iconColor = Colors.black54,
        GestureTapCallback? onTap
      }
      ){
    return GestureDetector(
      onTap: onTap == null ? ()=>  Get.back() : onTap,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: backgroundColor,
        ),
        alignment: Alignment.centerRight,
        width: 30,
        height: 30,
        child: Icon(Icons.arrow_back_ios, size: 22, color: iconColor,),
      ),
    );
  }

  static Widget holderHeader(){
    return GestureDetector(
      onTap: ()=> Get.back(),
      child: Container(
        width: 30,
        height: 30,
//        child: Icon(Icons.arrow_back_ios, size: 22, color: Colors.black54,),
      ),
    );
  }
}