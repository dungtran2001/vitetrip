import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../components/audio/play_button_notifier.dart';

class PlayButton extends StatelessWidget {
  final VoidCallback? onPressed;

  const PlayButton({Key? key, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipOval(
        child: Container(
            height: 40,
            width: 40,
            color: Colors.lightBlue.shade600,
            child: Center(
                child: ValueListenableBuilder<ButtonState>(
              valueListenable: PlayButtonNotifier(),
              //textToSpeech.playButtonNotifier,
              builder: (_, value, __) {
                switch (value) {
                  case ButtonState.loading:
                    return Container(
                      margin: EdgeInsets.all(8.0),
                      width: 25,
                      height: 25,
                      color: Colors.lightBlue,
                      child: CircularProgressIndicator(
                        color: Colors.white,
                      ),
                    );
                  case ButtonState.paused:
                    return IconButton(
                        icon: Icon(Icons.play_arrow, color: Colors.white),
                        iconSize: 25,
                        onPressed: onPressed ?? () {}
//                          textToSpeech.isFirst.value == true
//                              ? textToSpeech.play
//                              : textToSpeech.resumeOrPause,
                        );
                  case ButtonState.playing:
                    return IconButton(
                      icon: Icon(Icons.pause, color: Colors.white),
                      iconSize: 25,
                      onPressed:
                          onPressed ?? () {}, //textToSpeech.resumeOrPause,
                    );
                }
              },
            ))));
  }
}
