

import 'package:flutter/material.dart';
import 'dart:math' as math;


class SemiCircleWidget extends CustomPainter {
  SemiCircleWidget({
    this.fromAngle = 180,
    this.toAngle = 90,
    this.color = Colors.cyanAccent});
  final double? fromAngle;
  final double? toAngle;
  final Color? color;

  @override
  void paint(Canvas canvas, Size size) {
    final Paint paint = Paint()
      ..strokeWidth = 8.0   // 1.
      ..style = PaintingStyle.stroke   // 2.
      ..color = color!;   // 3.

    double degToRad(double deg) => deg * (math.pi / 180.0);

    final path = Path()
      ..arcTo(   // 4.
          Rect.fromCenter(
            center: Offset(size.height / 2, size.width / 2),
            height: size.height,
            width: size.width,
          ),   // 5.
          degToRad(fromAngle!),   // 6.
          degToRad(toAngle!), // 7.
          false);

    canvas.drawPath(path, paint);   // 8.
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}