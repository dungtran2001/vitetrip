import 'dart:math' as math;

import 'package:flutter/material.dart';

import 'painter/painter_circle.dart';

class ButtonRotation extends StatefulWidget {

  @override
  _ButtonRotationState createState() => _ButtonRotationState();
}

class _ButtonRotationState extends State<ButtonRotation> with SingleTickerProviderStateMixin{

  AnimationController? _controller;
  @override
  void initState(){
    _controller = AnimationController(vsync: this, duration: Duration(seconds: 2))..repeat();

    super.initState();
  }

  @override
  void dispose(){
    _controller!.stop();
    _controller!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return  Center(
        child:
        AnimatedBuilder(
          animation: _controller!,
          builder: (_, child) {
            return Transform.rotate(
              angle: _controller!.value * 2 * math.pi,
              child: child,
            );
          },
          child:
        Stack(
          alignment: Alignment.center,
            children: [
              Container(
                width: 50,
                  height: 50,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(25),
                  gradient: RadialGradient(

                    colors: [
                      Colors.lightBlueAccent, // yellow sun
                      Colors.lightBlueAccent.withOpacity(.2), // blue sky
                    ],
                    stops: [0.8, 2],
                  )
                ),
              ),

              CustomPaint(
                painter: SemiCircleWidget(
                  fromAngle: 180,
                  toAngle: 90,
                  color: Colors.yellowAccent
                ),
                size: Size(50, 50),

              ),
              CustomPaint(
                painter: SemiCircleWidget(
                  fromAngle: 0,
                  toAngle: 90,
                  color: Colors.yellowAccent
                ),
                size: Size(50, 50),

              ),

              Container(
                width: 36,
                height: 36,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(25),
                    color: Colors.blueGrey.shade800
                ),
              ),
            ],
          ),
        ),

    );
  }
}