
import 'package:audio_video_progress_bar/audio_video_progress_bar.dart';
import 'package:flutter/material.dart';
import 'package:mitta/app_theme.dart';
import 'package:mitta/commons/components/audio/play_button_notifier.dart';
import 'package:mitta/commons/components/audio/progress_notifier.dart';
import 'package:mitta/services/audio_player/audio_manager.dart';
import 'package:mitta/services/services_locator.dart';

import '../../common.dart';


class AudioPlayerWidget extends StatefulWidget {
  @override
  _AudioPlayerWidgetState createState() => _AudioPlayerWidgetState();
}

class _AudioPlayerWidgetState extends State<AudioPlayerWidget>
    with TickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final _screenSize = MediaQuery.of(context).size;
    return Container(
      width: _screenSize.width,
      height: _screenSize.height * 7/8 + 10,
      color: Colors.black54,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SizedBox(
            height: 70,
          ),

          //control play audio
          SizedBox(
            width: _screenSize.width * 2/3,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [

                SeekForward10s(),

                AudioPlayButton(),

                SeekNext10s(),
              ],
            ),
          ),


          SizedBox(
            height: 80,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: AudioProgressBar()
              // ProgressBar(
              //   progress: Duration(seconds: 71),
              //   total: Duration(seconds: 170),
              //   bufferedBarColor: Colors.cyan,
              //   progressBarColor: Colors.cyanAccent,
              //   barHeight: 4,
              //   thumbColor: Colors.white,
              //   thumbGlowColor: Colors.white54,
              //   baseBarColor: Colors.white70,
              //   thumbRadius: 7,
              //   timeLabelTextStyle: TextStyle(color: Colors.transparent),
              // ),
            ),
          ),
        ],
      ),
    );

      // Container(
      //   color: Colors.transparent,
      //   height: 140,
      //   child: Column(
      //     mainAxisAlignment: MainAxisAlignment.start,
      //     children: [
      //       Container(
      //         child: Stack(
      //           alignment: Alignment.center,
      //           children: [
      //             Container(
      //               height: 100,
      //               child: Column(
      //                 mainAxisAlignment: MainAxisAlignment.center,
      //                 children: [
      //                   SizedBox(height: 25,),
      //
      //                   Container(
      //                     color: Colors.white,
      //                     width: width,
      //                     child: Row(
      //                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
      //                       children: [
      //                         Container(
      //                           child: Row(
      //                             mainAxisAlignment:
      //                                 MainAxisAlignment.spaceAround,
      //                             children: [
      //                               PreviousAudioButton(),
      //                               PlayButton(),
      //                               NextAudioButton()
      //                             ],
      //                           ),
      //                         ),
      //                       ],
      //                     ),
      //                   ),
      //                 ],
      //               ),
      //             ),
      //             Positioned(
      //               top: 3,
      //               left: 0,
      //               child: SizedBox(
      //                 width: width,
      //                   child: AudioProgressBar()),
      //             ),
      //           ],
      //         ),
      //       ),
      //     ],
      //   ));
  }
}

class AudioProgressBar extends StatelessWidget {
  const AudioProgressBar({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final pageManager = locator<MediaManager>();
    return ValueListenableBuilder<ProgressBarState>(
      valueListenable: pageManager.progressNotifier,
      builder: (_, value, __) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 12.0),
              child: Text("${Common.formatDurationToString(value.current)} / ${Common.formatDurationToString(value.total)}",
                style: TextStyle(color: Colors.white, fontSize: 10),),
            ),
            ProgressBar(
              progress: value.current,
              buffered: value.buffered,
              total: value.total,
              onSeek: pageManager.seek,
              bufferedBarColor: Colors.white,
              progressBarColor: Colors.cyanAccent,
              barHeight: 4,
              thumbColor: Colors.white,
              thumbGlowColor: Colors.white54,
              baseBarColor: Colors.white70,
              thumbRadius: 7,
              timeLabelTextStyle: TextStyle(color: Colors.transparent),
            ),
          ],
        );
      },
    );
  }
}


class SeekForward10s extends StatelessWidget {
  const SeekForward10s({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final pageManager = locator<MediaManager>();
    return ValueListenableBuilder<ProgressBarState>(
      valueListenable: pageManager.progressNotifier,
      builder: (_, value, __) {
        //value.current
        return GestureDetector(
          onTap:(){
            pageManager.seek(value.current - Duration(seconds: 10));
          },
          child: SizedBox(
            height: 28,
            width: 28,
            child: Stack(
              alignment: Alignment.center,
              children: [
                Image.asset("assets/images/btn_replay.png"),
                Text("10", style: AppTheme.titleAppBar
                    .copyWith(fontSize: 10, color: Colors.white),)
              ],
            ),
          ),
        );
      },
    );
  }
}


class SeekNext10s extends StatelessWidget {
  const SeekNext10s({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final pageManager = locator<MediaManager>();
    return ValueListenableBuilder<ProgressBarState>(
      valueListenable: pageManager.progressNotifier,
      builder: (_, value, __) {
        //value.current
        return GestureDetector(
          onTap:(){
            pageManager.seek(value.current + Duration(seconds: 10));
          },
          child: SizedBox(
            height: 28,
            width: 28,
            child: Stack(
              alignment: Alignment.center,
              children: [
                Image.asset("assets/images/btn_seek.png"),
                Text("10", style: AppTheme.titleAppBar
                    .copyWith(fontSize: 10, color: Colors.white),)
              ],
            ),
          ),
        );
      },
    );
  }
}


class PreviousAudioButton extends StatelessWidget {
  const PreviousAudioButton({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final pageManager = locator<MediaManager>();
    return ValueListenableBuilder<bool>(
      valueListenable: pageManager.isFirstSongNotifier,
      builder: (_, isFirst, __) {
        return IconButton(
          icon: Icon(
            Icons.skip_previous,
            color: (isFirst) ? AppTheme.grey : AppTheme.orange,
          ),
          onPressed: (isFirst) ? null : pageManager.previous,
        );
      },
    );
  }
}

class AudioPlayButton extends StatelessWidget {
  const AudioPlayButton({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final pageManager = locator<MediaManager>();
    return ClipOval(
        child: Container(
            height: 50,
            width: 50,
            // color: AppTheme.orange,
            child: Center(
                child: ValueListenableBuilder<ButtonState>(
              valueListenable: pageManager.playButtonNotifier,
              builder: (_, value, __) {
                switch (value) {
                  case ButtonState.loading:
                    return Container(
                      margin: EdgeInsets.all(8.0),
                      width: 25,
                      height: 25,
                      // color: AppTheme.orange,
                      child: CircularProgressIndicator(
                        color: AppTheme.white,
                      ),
                    );
                  case ButtonState.paused:
                    return IconButton(
                      icon: Icon(Icons.play_arrow, color: AppTheme.white),
                      iconSize: 40,
                      onPressed: pageManager.play,
                    );
                  case ButtonState.playing:
                    return GestureDetector(
                      onTap: pageManager.pause,
                      child: SizedBox(
                        height: 35,
                        width: 35,
                        child: Image.asset("assets/images/btn_pause.png"),
                      ),
                    );
                }
              },
            ))));
  }
}

class NextAudioButton extends StatelessWidget {
  const NextAudioButton({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final pageManager = locator<MediaManager>();
    return ValueListenableBuilder<bool>(
      valueListenable: pageManager.isLastSongNotifier,
      builder: (_, isLast, __) {
        return IconButton(
          icon: Icon(Icons.skip_next, color: AppTheme.orange),
          onPressed: (isLast) ? null : pageManager.next,
        );
      },
    );
  }
}

class CustomTrackShape extends RoundedRectSliderTrackShape {
  final double trackWidth;
  CustomTrackShape({required this.trackWidth});
  Rect getPreferredRect({
    required RenderBox parentBox,
    Offset offset = Offset.zero,
    required SliderThemeData sliderTheme,
    bool isEnabled = false,
    bool isDiscrete = false,
  }) {
    final double trackHeight = 1;
    final double trackLeft = offset.dx;
    final double trackTop = 0;
    return Rect.fromLTWH(trackLeft, trackTop, trackWidth, trackHeight);
  }
}
