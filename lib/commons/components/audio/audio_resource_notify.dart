import 'package:flutter/foundation.dart';
import 'package:mitta/model/audio/resource_model.dart';

class ResourceNotifier extends ValueNotifier<ResourceModel> {
  ResourceNotifier() : super(_initialValue);
  static var _initialValue = ResourceModel();
  setResource(model) {
    value = model;
  }
}

class ResourceState {
  const ResourceState({
    required this.current,
  });
  final ResourceModel current;
}
