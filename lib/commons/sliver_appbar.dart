import 'package:flutter/material.dart';
import '/app_theme.dart';


class AppBarWidget extends StatelessWidget {
  // 1
  final Widget text;
  final String imagePath;
  final bool centerTitle;
  final Color color;
  final Widget? leadingButton;
  Widget? actionButton;

  AppBarWidget({
    Key? key,
    required this.text,
    required this.imagePath,
    this.centerTitle = false,
    this.color = Colors.white,
    this.leadingButton = null,
    this.actionButton = null,
  }) : assert(text != null),
        assert(imagePath != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      title: text,
      leading: leadingButton == null ? null : leadingButton, //hide if set button back is null
      actions: [
       actionButton != null ? Padding(
         padding: const EdgeInsets.only(right: 12.0),
         child: Center(child: actionButton!),
       ) : SizedBox()
      ],
      backgroundColor: color,
      centerTitle: centerTitle,
      automaticallyImplyLeading: leadingButton == null ? false : true,
      expandedHeight: MediaQuery.of(context).size.height / 5,
      pinned: true, //false thi theo scroll bi mat
      elevation: 0,
      flexibleSpace: FlexibleSpaceBar(
        background: imagePath!= null && imagePath != ""
            ?  Image.asset(imagePath, fit: BoxFit.fitWidth,)
            : Container(color: Theme.of(context).scaffoldBackgroundColor,),
      ),
    );
  }
}