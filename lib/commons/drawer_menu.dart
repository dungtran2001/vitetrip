import 'package:flutter/material.dart';

class DrawerList {
  DrawerList({
    this.isAssetsImage = false,
    this.labelName = '',
    this.icon,
    this.index,
    this.imageName = '',
  });

  String labelName;
  Icon? icon;
  bool isAssetsImage;
  String imageName;
  DrawerIndex? index;
}

enum DrawerIndex {
  Map,
  FeedBack,
  History,
  Wallet,
  About,
  Account,
  Notify,
  Vote,
  Help,
  Login,
  Logout
}