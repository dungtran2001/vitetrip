import 'dart:async';
import 'dart:math';
import 'dart:ui';

import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:vector_math/vector_math.dart' as vector;



import '/app_theme.dart';
import '/configs/configs.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'dart:ui' as ui;

class Common {
  static void showSnackBar(BuildContext context, String item, bool status) {
    var snackBar = SnackBar(
      duration: Duration(seconds: 2),
      content: Text(
        item,
        style: TextStyle(color: Colors.black87, fontStyle: FontStyle.italic),
      ),
      backgroundColor: status
          ? Colors.green.withOpacity(0.8)
          : AppTheme.subColor2.withOpacity(0.7),
      action: SnackBarAction(
          label: "",
          onPressed: () {
            debugPrint('displayed tooltip.');
          }),
    );

    // Scaffold.of(context).showSnackBar(snackBar);
  }

  /// Get message with parameter
  /// config: Map of message
  /// Key: language of message
  static String getMessage(
      Map<String, String> config, String key, List<String> arts) {
    String message;
    message = config[key].toString();
    if (message != null && message != '') {
      for (int i = 0; i < arts.length; i++) {
        message = message.replaceAll('{' + i.toString() + '}', arts[i]);
      }
    }
    return message;
  }

  static List<dynamic> parseList(json){
    var result = null;
    if (json != null){
      if (json is String){
        result = [json];

      }else{
        try{
          result = json as List<dynamic>;
        }catch(e){}
      }
    }

    return result;
  }

  /// Type: 0 is success, 1 is Error, other is warning
  static void showFlushBar(BuildContext context, String message, int type) {
    Flushbar(
      // title: "Hey Ninja",
      message: message,
      flushbarPosition: FlushbarPosition.TOP,
      flushbarStyle: FlushbarStyle.FLOATING,
      reverseAnimationCurve: Curves.decelerate,
      // forwardAnimationCurve: Curves.elasticOut,
      //backgroundColor: Colors.red,
//      boxShadows: [
//        BoxShadow(
//            color: Colors.blue[800], offset: Offset(0.0, 2.0), blurRadius: 3.0)
//      ],
      backgroundGradient: type == Configs.ok
          ? LinearGradient(colors: [AppTheme.grey.withOpacity(.5), Colors.green.withOpacity(.5)])
          : type == Configs.error
              ? LinearGradient(colors: [AppTheme.grey.withOpacity(.5), Colors.redAccent.withOpacity(.5)])
              : LinearGradient(colors: [AppTheme.grey.withOpacity(.5), Colors.amber.withOpacity(.5)]),
      isDismissible: false,
      duration: Duration(seconds: 3),
      icon: Icon(
        type == Configs.ok
            ? Icons.check
            : type == Configs.error ? Icons.error : Icons.warning,
        color: Colors.white,
      ),
      // mainButton: FlatButton(
      //   onPressed: () {},
      //   child: Text(
      //     "CLAP",
      //     style: TextStyle(color: Colors.amber),
      //   ),
      // ),
      showProgressIndicator: false,
      progressIndicatorBackgroundColor: Colors.blueGrey,
      // titleText: Text(
      //   "Hello Hero",
      //   style: TextStyle(
      //       fontWeight: FontWeight.bold, fontSize: 20.0, color: Colors.yellow[600], fontFamily: "ShadowsIntoLightTwo"),
      // ),
      messageText: Text(
        message,
        style: TextStyle(
            fontSize: 16.0,
            color: Colors.white,
            fontFamily: "ShadowsIntoLightTwo"),
      ),
    ).show(context);
  }

  /// Convert date time to local time
  static String convertUtcToLocalTime(String utc){
    try{
      var dateTime = DateFormat("yyyy-MM-ddTHH:mm:ssZ").parse(utc, true).toLocal();
      var date = DateFormat.yMd().format(dateTime);
      var time = DateFormat.Hm().format(dateTime);

      var t = date + " - " + time;
      return t;
    }catch(e){
      return "";
    }

    //return dateTime.toString();
  }
  static String convertUtcToHumanTime(String utc){
    DateTime time = DateTime.parse(utc);
    timeago.setLocaleMessages('vi_short', timeago.ViMessages());
    return timeago.format(time, locale: 'vi_short').replaceAll("khoảng ", "");
  }
  static String convertCurrencyDecimal(int price){
    final formatCurrency = new NumberFormat.decimalPattern();

    try{
      return formatCurrency.format(price);
    }
    on FormatException {
      // code for handling exception
      return price.toString();
    }
  }

  //bitmap resize
  static Future<Uint8List> getBytesFromCanvas(double escala, urlAsset) async {

    final PictureRecorder pictureRecorder = PictureRecorder();
    final Canvas canvas = Canvas(pictureRecorder);

    final ByteData datai = await rootBundle.load(urlAsset);
    var imaged = await loadImage(new Uint8List.view(datai.buffer));

    double width = ((imaged.width.toDouble() * escala).toInt()).toDouble();
    double height = ((imaged.height.toDouble() * escala).toInt()).toDouble();

    canvas.drawImageRect(imaged, Rect.fromLTRB(0.0, 0.0, imaged.width.toDouble(), imaged.height.toDouble()),
      Rect.fromLTRB(0.0, 0.0, width, height),
      new Paint(),
    );

    final img = await pictureRecorder.endRecording().toImage(width.toInt(), height.toInt());
    final data = await img.toByteData(format: ImageByteFormat.png);
    return data!.buffer.asUint8List();

  }

  static Future < ui.Image > loadImage(img) async {
    final Completer < ui.Image > completer = new Completer();
    ui.decodeImageFromList(img, (ui.Image img) {

      return completer.complete(img);
    });
    return completer.future;
  }

  static Duration durationParse(String time) { // 00:00:00
    final ts = DateFormat('y-MM-dd').format(DateTime.now());
    final dt = DateTime.parse('$ts $time');
    return Duration(hours: dt.hour, minutes: dt.minute, seconds: dt.second);
  }

  static formatDurationToString(Duration d) => d.toString().split('.').first.padLeft(5, "0"); // 8: 00:00:00 - 5: 00:00
  // static Duration durationParse(String time) {
  //   int hours = 0;
  //   int minutes = 0;
  //   int micros;
  //   List<String> parts = time.split(':');
  //   if (parts.length > 2) {
  //     hours = int.parse(parts[parts.length - 3]);
  //   }
  //   if (parts.length > 1) {
  //     minutes = int.parse(parts[parts.length - 2]);
  //   }
  //   micros = (double.parse(parts[parts.length - 1]) * 1000000).round();
  //   return Duration(hours: hours, minutes: minutes, microseconds: micros);
  // }
  static bool isEmailValid(String email) {
    String pattern =
        r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+";
    RegExp regex = new RegExp(pattern);
    return regex.hasMatch(email);
  }

  static double _distanceBetween({lat1, long1, lat2, long2}) {
    // double distanceInMeters = Geolocator.distanceBetween(lat1, long1, lat2, long2);
    // return distanceInMeters;
    var R = 6378137;
    var dLat = vector.radians(lat2 - lat1);
    var dLong = vector.radians(long2 - long1);

    var a = sin(dLat / 2) * sin(dLat / 2) +
        cos(vector.radians(lat1)) * cos(vector.radians(lat2)) *
            sin(dLong / 2) * sin(dLong / 2);
    var c = 2 * atan2(sqrt(a), sqrt(1 - a));
    var d = R * c;
    return d; // returns the distance in meter
  }

  static double distanceBetween({lat1, long1, lat2, long2}) {
    return Geolocator.distanceBetween(lat1, long1, lat2, long2);
  }

  static double parseDouble(json){
    var result = null;
    if (json != null){
      if (json is double){
        result = json;

      }else{
        try{
          result = json + 0.0;
        }catch(e){
          // json double is String
          result = double.parse(json) + 0.0;
        }
      }
    }

    return result;
  }

  static Widget displayStarRating(double ranking, {iconSize = 25.0}){
    return Row(
      children: [
        if (ranking > 1)
          Icon(Icons.star_rounded, color: AppTheme.yellow, size: iconSize,)
        else
          Icon(Icons.star_half_rounded, color: AppTheme.yellow, size: iconSize,),

        if (ranking > 1.5)
          Icon(Icons.star_rounded, color: AppTheme.yellow, size: iconSize,)
        else if (ranking < 1)
          Icon(Icons.star_border_rounded, color: AppTheme.yellow, size: iconSize,)
        else
          Icon(Icons.star_half_rounded, color: AppTheme.yellow, size: iconSize,),

        if (ranking > 2.5)
          Icon(Icons.star_rounded, color: AppTheme.yellow, size: iconSize,)
        else if (ranking < 2)
          Icon(Icons.star_border_rounded, color: AppTheme.yellow, size: iconSize,)
        else
          Icon(Icons.star_half_rounded, color: AppTheme.yellow, size: iconSize,),

        if (ranking > 3.5)
          Icon(Icons.star_rounded, color: AppTheme.yellow, size: iconSize,)
        else if (ranking < 3)
          Icon(Icons.star_border_rounded, color: AppTheme.yellow, size: iconSize,)
        else
          Icon(Icons.star_half_rounded, color: AppTheme.yellow, size: iconSize,),

        if (ranking > 4.5)
          Icon(Icons.star_rounded, color: AppTheme.yellow, size: iconSize,)
        else if (ranking < 4)
          Icon(Icons.star_border_rounded, color: AppTheme.yellow, size: iconSize,)
        else
          Icon(Icons.star_half_rounded, color: AppTheme.yellow, size: iconSize,),

      ],
    );
  }
}

class CurrencyInputFormatter extends TextInputFormatter {

  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    if(newValue.selection.baseOffset == 0){
      return newValue;
    }

    double value = double.parse(newValue.text);


//    print(value.round());
    String newText = Common.convertCurrencyDecimal(value.round());

    return newValue.copyWith(
        text: newText,
        selection: new TextSelection.collapsed(offset: newText.length));
  }
}


class ToastNotify {

  static var tContext;
  ///show info toast
  static void showInfoToast({
    String message = "",
    int duration = 2,
    bool isBottom = false,
    bool isTop = false,
    bool isCenter = false,
  }) {

    var position =  isTop
        ? ToastGravity.TOP
        : isBottom
        ? ToastGravity.BOTTOM
        : ToastGravity.CENTER; //default

    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: position, //ToastGravity.CENTER,
        timeInSecForIosWeb: duration,
        backgroundColor: Colors.blueGrey.withOpacity(.89),
        textColor: Colors.white,
        fontSize: 13.0
    );
  }

  ///show success toast
  static void showSuccessToast({
    String message = "",
    int duration = 2,
    bool isBottom = false,
    bool isTop = false,
    bool isCenter = false,
  }) {
    var position =  isCenter
        ? ToastGravity.CENTER
        : isBottom
        ? ToastGravity.BOTTOM
        : ToastGravity.TOP; //default

    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: position, //ToastGravity.TOP,
        timeInSecForIosWeb: duration,
        backgroundColor: Colors.green.withOpacity(.89),
        textColor: Colors.white,
        fontSize: 13.0
    );
  }

  ///show fail toast
  static void showFailToast({
    String message = "",
    int duration = 2,
    bool isBottom = false,
    bool isTop = false,
    bool isCenter = false,
  }) {
    var position =  isCenter
        ? ToastGravity.CENTER
        : isBottom
        ? ToastGravity.BOTTOM
        : ToastGravity.TOP; //default

    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: position, //ToastGravity.TOP,
        timeInSecForIosWeb: duration,
        backgroundColor: Colors.red.withOpacity(.89),
        textColor: Colors.white,
        fontSize: 13.0
    );
  }


}