
import '/configs/configs.dart';
import '/network/httpController.dart';

class Authentication {

  static Future<bool> verifyAccount(String username, String password, {localAccountList}) async {
    Map<String, dynamic> map = {
      "username": username,
      "password": password,
    };
    String url = Configs.hostName + Configs.apiURI['login']! ;// + "?username=$username&password=$password";
    // print(url +": "+ map.toString());

    var _response = await HTTPController.PostByJson(url, map);
    // print(_response.toString());


    if (_response != null) {
      try {
        var _status = _response["success"];
        if (_status) {

          return true;

        } else {
          //login fail
//          print("Login response: " + _response.toString());

          //tim trong account local
          try{
            var existAcc =  localAccountList.firstWhere((element) => element.username == username);
            if (localAccountList.length > 0 && existAcc != null){
              //co
              if (existAcc.password == password){
                //success
                return true;
              }else{
                //fail
                //ko tim thay
                return false;
              }
            }
          }catch(e){
            //ko tim thay
           return false;
          }

          //test
//          isStartingRequest.value = false;
//          message.value = _response["message"] ?? "fail";
//
//          ToastNotify.showFailToast(message: message.value);
        }
      } catch (e) {
        return false;
      }
    }else{
     return false;
    }

    //default
    return false;
  }


}
