
import '/app_theme.dart';
import 'package:flutter/material.dart';

final appBarTheme = AppBarTheme(centerTitle: false, elevation: 0);


class Themes {
  static final light = ThemeData.light().copyWith(
    primaryColor: AppTheme.nearlyBlue,
    backgroundColor: AppTheme.backgroundMain,
    buttonColor: Colors.blue.shade100,
    cardColor: Colors.grey.shade100,
    shadowColor: Colors.black38,
    bottomAppBarColor: Colors.white,
    scaffoldBackgroundColor: AppTheme.background,
    appBarTheme: appBarTheme.copyWith(color: AppTheme.backgroundMain),
    iconTheme: IconThemeData(color: Colors.blue.shade600),
    colorScheme: ColorScheme.light(
      primary: AppTheme.mainColorLightmode,
      secondary: AppTheme.subColor2,
      error: AppTheme.highlight,
    ),
    bottomNavigationBarTheme: BottomNavigationBarThemeData(
      backgroundColor: Colors.white,
      selectedItemColor: AppTheme.highlight,
      unselectedItemColor: AppTheme.grey.withOpacity(0.32),
      selectedIconTheme: IconThemeData(color: AppTheme.highlight1),
      showUnselectedLabels: true,
    ),
  );

  static final dark = ThemeData.dark().copyWith(
    primaryColor: AppTheme.nearlyBlue,
    shadowColor: Colors.black87,
    buttonColor: Colors.blueGrey.shade600,
    cardColor: AppTheme.backgroundTileNightmode,
    backgroundColor: AppTheme.backgroundTileNightmode,
    bottomAppBarColor: Colors.black,//AppTheme.nearlyBlack,
    scaffoldBackgroundColor: AppTheme.backgroundMainNightmode,
    appBarTheme: appBarTheme.copyWith(color: Colors.black),
    iconTheme: IconThemeData(
        color: Colors.white
    ),
    colorScheme: ColorScheme.dark().copyWith(
      primary: AppTheme.mainColorNightmode,
      secondary: AppTheme.iconColorNightmode,
      error: AppTheme.highlight,
    ),
    bottomNavigationBarTheme: BottomNavigationBarThemeData(
      backgroundColor: AppTheme.backgroundNightmode,
      selectedItemColor: AppTheme.highlight,
      unselectedItemColor: AppTheme.grey.withOpacity(0.32),
      selectedIconTheme: IconThemeData(color: AppTheme.highlight1),
      showUnselectedLabels: true,
    ),
  );
}