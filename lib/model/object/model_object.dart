
import 'dart:convert';

import '../location/model_location.dart';

class ObjectInfo {
  String? id;
  String? name;
  String? label;
  String? lat;
  String? long;

  double? ranking;
  int? totalVote;
  int? totalDownload;
  int? versionId;
  String? status;
  String? description;
  LocationInfo? location;
  String? image;
  String? audio;
  String? audioDuration;
  String? categoryId;
  List<dynamic>? slider;
  bool isVisited = false;

  ObjectInfo({
    this.id,
    this.name,
    this.label,
    this.lat,
    this.long,
    this.ranking,
    this.totalVote,
    this.totalDownload,
    this.versionId,
    this.status,
    this.description,
    this.location,
    this.image,
    this.audio,
    this.audioDuration,
    this.slider,
    this.categoryId,

  });

  factory ObjectInfo.fromJson(Map<String, dynamic> _json){
    var rank = 0.0;
    try{
      rank = _json['ranking'] as double;
    }catch(e){
      var num = _json['ranking'];
      rank = 0.0 + num;
    }
    var _slider = null;
    try{
      _slider = (json.decode(_json['slider']) as List<dynamic>);
    }catch(e){
      print("--ObjectInfo no slider");
    }

    return ObjectInfo(
      id: _json["id"],
      name: _json["name"],
      label: _json["label"],
      lat: _json["lat"].toString(),
      long: _json["long"].toString(),
      ranking: rank,
      totalVote: _json["rank_amount"],
      totalDownload: _json["total_download"],
      status: _json["status"],
      versionId: _json["version_id"],
      location: _json["location"] != null ? LocationInfo.fromJson(_json["location"]) : null,
      description: _json["description"].toString(),
      image: _json["image"].toString(),
      audio: _json["audio"].toString(),
      audioDuration: _json["audio_duration"].toString(),
        categoryId: _json["category_id"].toString(),
        slider: _slider != null
            ? _slider.map((e) => SliderImageInfo.fromJson(e as Map<String, dynamic>)).toList()
            : []
    );
  }

}
