import 'package:intl/intl.dart';
import 'package:mitta/configs/configs.dart';

class ResourceModel {
  String? id;
  String? title;
  String? description;
  String? url;
  String? image;
  String? youtubeIframe;
  String? place;
  bool? bookmark;
  int? character_id;
  int? category_id;
  String? author;
  int? currentTrackIndex;
  String? createdAt;
  String? time;
  String? type;
  ResourceModel({
    this.id,
    this.title,
    this.description,
    this.url,
    this.image,
    this.youtubeIframe,
    this.place,
    this.bookmark,
    this.character_id,
    this.category_id,
    this.author,
    this.currentTrackIndex,
    this.createdAt,
    this.time,
    this.type,
  });
  factory ResourceModel.fromJson(Map<String, dynamic> json) => ResourceModel()
    ..id = json['id']
    ..title = json['title'] as String
    ..url =
        (json['path_file_url'] == null) ? '' : json['path_file_url'] as String
    ..image = (json['image'] == null)
        ? 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQNK7-n-r_w_qCEIjsnu8VXMBamUkSmLUr9Eg&usqp=CAU'
        : json['image'] as String
    ..place = (json['place'] == null) ? '' : json['place'] as String
    ..youtubeIframe =
        (json['youtube_iframe'] == null) ? '' : json['youtube_iframe'] as String
    ..description =
        (json['description'] == null) ? '' : json['description'] as String
    ..bookmark = json['is_bookmark'] ?? false
    ..author = (json['author'] == null) ? '' : json['author'] as String
    ..character_id = json['character_id'] ?? 0 //as int
    ..category_id = json['category_id'] as int
    ..createdAt =
        (json['created_at'] == null) ? null : json['created_at'] as String
    ..time = json['time'] == null ? '00:00:00' : (json['time'] as String)
    ..type = json['type'] as String;
  getDate() {
    final dateTime = DateTime.parse(this.createdAt ?? '20112021');
    final format = DateFormat('dd-MM-yyyy');
    return format.format(dateTime);
  }

  getTime() {
    var list = this.time!.split(':');
    if (list[0] != "00")
      return '${list[0]}:${list[1]}:${list[2]}';
    else
      return '${list[1]}:${list[2]}';
  }

  Duration parseDuration() {
    int hours = 0;
    int minutes = 0;
    int micros;
    List<String> parts = this.time!.split(':');
    if (parts.length > 2) {
      hours = int.parse(parts[parts.length - 3]);
    }
    if (parts.length > 1) {
      minutes = int.parse(parts[parts.length - 2]);
    }
    micros = (double.parse(parts[parts.length - 1]) * 1000000).round();
    return Duration(hours: hours, minutes: minutes, microseconds: micros);
  }

  getImage() {
    if (this.image != null && !this.image!.contains('http')) {
      return '${Configs.hostName}${this.image}';
    }
    return this.image;
  }

  // getVideoYoutubeId(){
  //   String videoId;
  //   videoId = YoutubePlayerController.convertUrlToId(this.youtubeIframe.toString())!;
  //   return videoId;
  // } //hoa.td add to get id of video youtube
}
