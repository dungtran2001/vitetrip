
class Demo {
  late int id;
  late String title;
  late String date;
  late String content;
  late String image;
  late bool? isSetNotify;
  Demo();
  factory Demo.fromJson(Map<String, dynamic> json) => _fromJson(json);
  Map<String, dynamic> toJson() => _toJson(this);
}

Demo _fromJson(Map<String, dynamic> json) {
  return Demo()
    ..id = json['id'] as int
    ..title = json['title'] as String
    ..date = json['date'] as String
    ..content = json['content'] as String
    ..image = json['image'] as String

    ..isSetNotify = json['isSetNotify'] as bool?;
}


Map<String, dynamic> _toJson(Demo instance) => <String, dynamic>{
  'id': instance.id,
  'title': instance.title,
  'date': instance.date,
  'content': instance.content,
  'image': instance.image,
  'isSetNotify': instance.isSetNotify,
};
