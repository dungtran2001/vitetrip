class Coordinate {
  late double xCoordinate;
  late double yCoordinate;
  Coordinate();
  factory Coordinate.fromArray(List<dynamic> data) => Coordinate()
    ..xCoordinate = data[0]
    ..yCoordinate = data[1];
}
