
import 'dart:convert';

import 'package:mitta/commons/common.dart';
import 'package:mitta/model/location/model_location.dart';

class ProvinceInfo {
  String? id;
  String? countryId;
  String? name;
  double? lat;
  double? long;
  List<dynamic>? locationIds;

  double? ranking;
  int? totalVote;
  int? totalDownload;
  String? status;
  String? description;
  List<dynamic>? image;
  List<LocationInfo>? locations;

  ProvinceInfo({
    this.id,
    this.countryId,
    this.name,
    this.lat,
    this.long,
    this.ranking,
    this.totalVote,
    this.totalDownload,
    this.status,
    this.description,
    this.image,
    this.locationIds,
    this.locations,
  });

  factory ProvinceInfo.fromJson(Map<String, dynamic> json){
    var rank = 0.0;
    try{
      rank = json['ranking'] as double;
    }catch(e){
      var num = json['ranking'];
      rank = 0.0 + num;
    }

    return ProvinceInfo(
      id: json["id"],
        countryId: json["nation_id"],
      name: json["name"],
      lat: Common.parseDouble(json["lat"]),
      long: Common.parseDouble(json["long"]),
      ranking: rank,
      totalVote: json["voted_amount"],
      totalDownload: json["total_dowloaded"],
      status: json["status"],
      description: json["description"].toString(),
      image: Common.parseList(json["images"]),
      locationIds:  Common.parseList(json["location_ids"]),
      locations: json['locations'] != null ? (json['locations'] as List<dynamic>)
          .map((e) => LocationInfo.fromJson(e as Map<String, dynamic>))
          .toList()
          : []
    );
  }

  Map<String, dynamic> toJson() => _toJson(this);

}

Map<String, dynamic> _toJson(ProvinceInfo instance) =>
    <String, dynamic>{
      'id': instance.id,
      'nation_id': instance.countryId,
      'name': instance.name,
      'lat': instance.lat,
      'long': instance.long,
      'ranking': instance.ranking,
      'voted_amount': instance.totalVote,
      'total_dowloaded': instance.totalDownload,
      'status': instance.status,
      'description': instance.description,
      'images': (instance.image),
      'location_ids': (instance.locationIds),
      'locations': instance.locations!.map((e) => e.toJson()).toList(),
    };