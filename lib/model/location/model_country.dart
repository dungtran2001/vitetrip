
import 'dart:convert';

import 'package:mitta/commons/common.dart';

class CountryInfo {
  String? id;
  String? name;
  double? lat;
  double? long;
  String? locationId;

  double? ranking;
  int? totalVote;
  int? totalDownload;
  String? status;
  String? description;
  List<dynamic>? images;

  CountryInfo({
    this.id,
    this.name,
    this.lat,
    this.long,
    this.ranking,
    this.totalVote,
    this.totalDownload,
    this.status,
    this.description,
    this.images,
    this.locationId,
  });

  factory CountryInfo.fromJson(Map<String, dynamic> json){
    var rank = 0.0;
    try{
      rank = json['ranking'] as double;
    }catch(e){
      var num = json['ranking'];
      rank = 0.0 + num;
    }

    return CountryInfo(
      id: json["id"],
      name: json["name"],
      lat: Common.parseDouble(json["lat"]),
      long: Common.parseDouble(json["long"]),
      ranking: rank,
      totalVote: json["voted_amount"],
      totalDownload: json["total_dowloaded"],
      status: json["status"],
      description: json["description"].toString(),
      images: Common.parseList(json["images"]),
      locationId: json["location_id"].toString(),
    );
  }

  Map<String, dynamic> toJson() => _toJson(this);

}


Map<String, dynamic> _toJson(CountryInfo instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'lat': instance.lat,
      'long': instance.long,
      'ranking': instance.ranking,
      'voted_amount': instance.totalVote,
      'total_dowloaded': instance.totalDownload,
      'status': instance.status,
      'description': instance.description,
      'images': (instance.images),
      'location_id': instance.locationId,
    };
