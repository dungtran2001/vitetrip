import 'dart:convert';
import 'dart:ui';

import 'package:mitta/commons/common.dart';
import 'package:mitta/constants.dart';
import 'package:mitta/model/location/model_province.dart';

class LocationInfo {
  String? id;
  String? name;
  double? lat;
  double? long;
  String? provinceId;
  ProvinceInfo? province;
  String? categoryId;

  double? ranking;
  int? totalVote;
  int? totalDownload;
  String? status;
  String? description;
  String? audio;
  String? audioDuration;
  String? modelName;
  String? labelsName;
  List<dynamic>? images;
  List<dynamic>? slider;
  String? type;

  LocationInfo({
    this.id,
    this.name,
    this.lat,
    this.long,
    this.ranking,
    this.totalVote,
    this.totalDownload,
    this.status,
    this.description,
    this.images,
    this.audio,
    this.audioDuration,
    this.provinceId,
    this.province,
    this.slider,
    this.modelName,
    this.labelsName,
    this.categoryId,
    this.type,
  });

  factory LocationInfo.fromJson(Map<String, dynamic> _json,
      {String type = "server"}) {
    var rank = 0.0;
    try {
      rank = _json['ranking'] as double;
    } catch (e) {
      var num = _json['ranking'];
      rank = 0.0 + num;
    }
    var _slider = null;
    try {
      _slider = (json.decode(_json['slider']) as List<dynamic>);
    } catch (e) {
      print("--LocationInfo no slider");
    }

    return LocationInfo(
        id: _json["_id"],
        name: _json["name"],
        lat: Common.parseDouble(_json["lat"]),
        long: Common.parseDouble(_json["long"]),
        ranking: rank,
        totalVote: _json["voted_amount"],
        totalDownload: _json["total_dowloaded"],
        status: _json["status"],
        description: _json["description"].toString(),
        audio: _json["audio"].toString(),
        audioDuration: _json["audio_duration"].toString(),
        modelName: _json["model_file_name"].toString(),
        labelsName: _json["labels_file_name"].toString(),
        images: Common.parseList(_json["images"]),
        provinceId: _json["province_id"].toString(),
        province: _json["province"] != null
            ? ProvinceInfo.fromJson(_json["province"])
            : null,
        categoryId: _json["category_id"].toString(),
        slider: _slider != null
            ? _slider
                .map((e) => SliderImageInfo.fromJson(e as Map<String, dynamic>))
                .toList()
            : [],
        type: type);
  }

  factory LocationInfo.fromPlacesJson(Map<String, dynamic> _json) {
    var photos = (_json["photos"] as List<dynamic>).map((e) {
      return "https://places.googleapis.com/v1/${e["name"]}/media?key=$GOOGLE_MAP_API_KEY";
    }).toList();
    var categoryId = parseCategoryId(_json["primaryType"]);
    return LocationInfo(
        id: _json["id"],
        name: _json["displayName"]["text"],
        lat: Common.parseDouble(_json["location"]["latitude"]),
        long: Common.parseDouble(_json["location"]["longitude"]),
        ranking: Common.parseDouble(_json["rating"]),
        totalVote: 0,
        totalDownload: 0,
        status: "enable",
        description: _json["description"].toString(),
        audio: null,
        audioDuration: null,
        modelName: null,
        labelsName: null,
        images: photos,
        provinceId: _json["province_id"].toString(),
        province: _json["province"] != null
            ? ProvinceInfo.fromJson(_json["province"])
            : null,
        categoryId: categoryId,
        slider: [],
        type: "google");
  }

  Map<String, dynamic> toJson() => _toJson(this);
}

String parseCategoryId(primaryType) {
  switch (primaryType) {
    case "church":
    case "place_of_worship":
    case "art_gallery":
    case "cultural_center":
    case "performing_arts_theater":
      return "6473135ad263cda6030e40c5";
    case "historical_landmark":
    case "amusement_park":
    case "amusement_center":
    case "aquarium":
    case "tourist_attraction":
      return "647313617c87c8d30208d252";
    case "museum":
      return "6473136711b929b137012575";
    case "market":
    case "banquet_hall":
      return "6473136daf7bd82d840075e7";
    case "zoo":
      return "6473138b1841c921f10b63f3";
  }
  return "6473138b1841c921f10b63f3";
}

Map<String, dynamic> _toJson(LocationInfo instance) => <String, dynamic>{
      '_id': instance.id,
      'name': instance.name,
      'lat': instance.lat,
      'long': instance.long,
      'ranking': instance.ranking,
      'voted_amount': instance.totalVote,
      'total_dowloaded': instance.totalDownload,
      'status': instance.status,
      'description': instance.description,
      'images': (instance.images),
      'audio': (instance.audio),
      'province_id': instance.provinceId,
      'category_id': instance.categoryId,
      'modelName': instance.modelName,
      'labelsName': instance.labelsName,
      'slider': instance.slider?.map((e) => e.toJson()).toList(),
    };

class SliderImageInfo {
  String? title;
  String? image;
  String? at;
  dynamic preImage;

  SliderImageInfo({
    this.title,
    this.image,
    this.at,
  });

  factory SliderImageInfo.fromJson(Map<String, dynamic> json) {
    return SliderImageInfo(
      title: json["title"],
      image: json["image"],
      at: json["at"].toString(),
    );
  }

  Map<String, dynamic> toJson() => {
        'title': title,
        'image': image,
        'at': at,
      };
}
