
class Language {
  String id;
  String image;
  String name;
  bool isEnabled;

  Language({
    required this.name,
    required this.id,
    required this.image,
    this.isEnabled = true,
  });

}

class ListLanguages {
  final List<Language> languages = [
    Language(
        name: "Vietnamese",
        id: "vi",
        image: "assets/images/lang_imgs/1_vietnam.png"),
    Language(
        name: "English", id: "en", image: "assets/images/lang_imgs/2_us.png"),
    Language(
        name: "Korea", id: "ko", image: "assets/images/lang_imgs/3_korea.png"),
    Language(
        name: "Japanese",
        id: "ja",
        image: "assets/images/lang_imgs/4_japan.png",
        isEnabled: false),
    Language(
        name: "Chinese",
        id: "zh",
        image: "assets/images/lang_imgs/5_china.png",
        isEnabled: false),
  ];
}