
import 'guide_author_feedback.dart';

class GuideContentInfo {
  String? id;
  String? name;

  double? ranking;
  int? totalVote;
  double? price;
  String? status;
  String? authorId;
  String? description;
  String? locationId;
  String? image;
  String? audio;
  List<GuideAuthorFeedback>? feedbacks;

  GuideContentInfo({
    this.id,
    this.name,
    this.ranking,
    this.totalVote,
    this.authorId,
    this.price,
    this.status,
    this.description,
    this.locationId,
    this.image,
    this.audio,
    this.feedbacks,
  });

  factory GuideContentInfo.fromJson(Map<String, dynamic> json){
    var rank = 0.0;
    try{
      rank = json['ranking'] as double;
    }catch(e){
      var num = json['ranking'];
      rank = 0.0 + num;
    }

    return GuideContentInfo(
      id: json["id"],
      name: json["name"],
      price: json["price"] + 0.0,
      ranking: rank,
      totalVote: json["rank_amount"],
      authorId: json["author_id"],
      status: json["status"],
      feedbacks: (json["feedbacks"] as List<dynamic>)
          .map((e) => GuideAuthorFeedback.fromJson(e as Map<String, dynamic>))
          .toList(),
      locationId: json["location_id"].toString(),
      description: json["description"].toString(),
      image: json["image"].toString(),
      audio: json["audio"].toString(),
    );
  }

}
