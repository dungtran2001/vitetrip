
import 'guide_author_feedback.dart';

class GuideAuthorInfo {
  String? id;
  String? name;

  double? ranking;
  int? totalVote;
  int? totalDownload;
  double? price;
  String? status;
  String? description;
  String? location;
  String? image;
  String? audio;
  List<GuideAuthorFeedback>? feedbacks;

  GuideAuthorInfo({
    this.id,
    this.name,
    this.ranking,
    this.totalVote,
    this.totalDownload,
    this.price,
    this.status,
    this.description,
    this.location,
    this.image,
    this.audio,
    this.feedbacks,
  });

  factory GuideAuthorInfo.fromJson(Map<String, dynamic> json){
    var rank = 0.0;
    try{
      rank = json['ranking'] as double;
    }catch(e){
      var num = json['ranking'];
      rank = 0.0 + num;
    }

    return GuideAuthorInfo(
      id: json["id"],
      name: json["name"],
      price: json["price"],
      ranking: rank,
      totalVote: json["rank_amount"],
      totalDownload: json["total_download"],
      status: json["status"],
      feedbacks: (json["feedbacks"] as List<dynamic>)
          .map((e) => GuideAuthorFeedback.fromJson(e as Map<String, dynamic>))
          .toList(),
      location: json["location"].toString(),
      description: json["description"].toString(),
      image: json["image"].toString(),
      audio: json["audio"].toString(),
    );
  }

}
