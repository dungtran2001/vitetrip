
class GuideAuthorFeedback {
  String? id;
  String? name;

  double? ranking;
  int? totalVote;
  String? status;
  String? description;
  String? video;
  String? image;

  GuideAuthorFeedback({
    this.id,
    this.name,
    this.ranking,
    this.totalVote,
    this.status,
    this.description,
    this.video,
    this.image,
  });

  factory GuideAuthorFeedback.fromJson(Map<String, dynamic> json){
    var rank = 0.0;
    try{
      rank = json['ranking'] as double;
    }catch(e){
      var num = json['ranking'];
      rank = 0.0 + num;
    }

    return GuideAuthorFeedback(
      id: json["id"],
      name: json["name"],
      ranking: rank,
      totalVote: json["like"],
      video: json["video"].toString(),
      description: json["description"].toString(),
      image: json["image"].toString(),
    );
  }

}
