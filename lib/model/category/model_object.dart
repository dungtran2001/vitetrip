
import 'dart:convert';

import '../location/model_location.dart';

class CategoryInfo {
  String? id;
  String? name;
  String? description;

  CategoryInfo({
    this.id,
    this.name,
    this.description,
  });

  factory CategoryInfo.fromJson(Map<String, dynamic> _json){

    return CategoryInfo(
      id: _json["id"],
      name: _json["name"],
      description: _json["description"].toString(),
    );
  }

}
