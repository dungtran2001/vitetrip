
import 'dart:convert';

import 'package:mitta/commons/common.dart';
import 'package:mitta/model/location/model_location.dart';

import '../object/model_object.dart';

class ArchivermentInfo {
  String? id;
  String? userId;
  String? provinceId;
  String? locationId;
  List<dynamic>? objectCollectedIds;
  List<ObjectInfo>? objectCollected;

  ArchivermentInfo({
    this.id,
    this.userId,
    this.provinceId,
    this.locationId,
    this.objectCollectedIds,
    this.objectCollected,
  });

  factory ArchivermentInfo.fromJson(Map<String, dynamic> json){
    return ArchivermentInfo(
        id: json["id"],
        userId: json["user_id"],
        provinceId: json["province_id"],
        locationId: json["location_id"],
        objectCollectedIds:  Common.parseList(json["objects_collects_ids"]),
        objectCollected: (json['objects_collects'] as List<dynamic>)
          .map((e) => ObjectInfo.fromJson(e as Map<String, dynamic>))
          .toList()
    );
  }

  Map<String, dynamic> toJson() => _toJson(this);

}

Map<String, dynamic> _toJson(ArchivermentInfo instance) =>
    <String, dynamic>{
      'id': instance.id,
      'user_id': instance.userId,
      'province_id': instance.provinceId,
      'location_id': instance.locationId,
      'objects_collects_ids': (instance.objectCollectedIds),
      // 'objects_collects': instance.objectCollected!.map((e) => e.toJson()).toList(),
    };