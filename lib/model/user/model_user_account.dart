class UserAccount{
  String? username;
  String? password;
  String? description;

  UserAccount({
    this.username,
    this.password,
    this.description
  });

  factory UserAccount.fromJson(Map<String, dynamic> json) => UserAccount(
    username: json["username"],
    password: json["password"],
    description: json["description"],
  );

  Map<String, dynamic> toJson() => {
    'username': username,
    'password': password,
    'description': description,
  };
}