
class Vehicle {
  late int status;
  late String type;
  late String plateNumber;
  late String id;

  Vehicle();
  factory Vehicle.fromJson(Map<String, dynamic> json) => _fromJson(json);
  Map<String, dynamic> toJson() => _toJson(this);
}

Vehicle _fromJson(Map<String, dynamic> json) {
  return Vehicle()
    ..id = json['_id'] as String
    ..status = json['status'] as int
    ..type = json['type_car'] as String
    ..plateNumber = json['vehicle_number'] as String;

}


Map<String, dynamic> _toJson(Vehicle instance) => <String, dynamic>{
  '_id': instance.id,
  'status': instance.status,
  'type_car': instance.type,
  'vehicle_number': instance.plateNumber,
};
