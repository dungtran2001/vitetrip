

import 'dart:convert';

class Me {
 String? id;
 String? email;
 String? created;
 String? fullname;
 String? phone;
 String? updated;
 String? token;
 String? avatar;
 List<dynamic>? roles;
 List<dynamic>? permissions;

  Me({
    this.id,
    this.email,
    this.created,
    this.fullname,
    this.phone,
    this.token,
    this.updated,
    this.avatar,
    this.roles,
    this.permissions});

  factory Me.fromJson(Map<String, dynamic> json) => _fromJson(json);
  Map<String, dynamic> toJson() => _toJson(this);
}

Me _fromJson(Map<String, dynamic> json) {
  return Me(
      email : json['email'] as String,
      id : json['id'] as String,
      fullname : json['name'] as String,
      phone : json['phone'] as String,
      token : json['token'] as String,
      avatar : json['avatar'] as String,
      roles : json['roles'],
      permissions : json['permissions'],
//      updated : json['updated'] as String
  );
}


Map<String, dynamic> _toJson(Me instance) => {
  'email': instance.email,
  'id': instance.id,
  'name': instance.fullname,
  'phone': instance.phone,
//  'updated': instance.updated,
  'avatar': instance.avatar,
  'token': instance.token,
  'roles': instance.roles,
  'permissions': instance.permissions,
};
