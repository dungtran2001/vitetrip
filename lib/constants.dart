import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFF00BF6D);
const kSecondaryColor = Color(0xFFFE9901);
const kContentColorLightTheme = Color(0xFF1D1D3);
const kContentColorDarkTheme = Color(0xFFF5FCF9);
const kWarninngColor = Color(0xFFF3BB1C);
const kErrorColor = Color(0xFFF03738);

const kDefaultPadding = 20.0;

const GOOGLE_MAP_API_KEY = "AIzaSyASRP-taHE5xHZHfH6LDmJ8lJ59_Fs5TUs";

const NEARBY_PLACES_INCLUDED_TYPE = [
  "art_gallery","museum","performing_arts_theater", "amusement_center", "amusement_park",
  "aquarium","banquet_hall","cultural_center","historical_landmark","tourist_attraction","zoo"
];