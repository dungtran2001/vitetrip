


import 'package:mitta/model/location/model_location.dart';
import 'package:mitta/model/location/model_province.dart';

import '../../model/archiverment/model_archiverment.dart';


class ArchivermentsResponse {
  late bool success;
  late List<ArchivermentInfo> data;
  ArchivermentsResponse();
  factory ArchivermentsResponse.fromJson(Map<String, dynamic> json) =>
      _fromJson(json);
  Map<String, dynamic> toJson() => _toJson(this);
}


ArchivermentsResponse _fromJson(Map<String, dynamic> json) {
  return ArchivermentsResponse()
    ..success = json['success'] as bool
    ..data = (json['data'] as List<dynamic>)
        .map((e) => ArchivermentInfo.fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _toJson(ArchivermentsResponse instance) =>
    <String, dynamic>{
      'success': instance.success,
      'data': instance.data,
    };
