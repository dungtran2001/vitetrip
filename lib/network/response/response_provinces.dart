


import 'package:mitta/model/location/model_location.dart';
import 'package:mitta/model/location/model_province.dart';


class ProvinceResponse {
  late bool success;
  late List<ProvinceInfo> data;
  ProvinceResponse();
  factory ProvinceResponse.fromJson(Map<String, dynamic> json) =>
      _fromJson(json);
  Map<String, dynamic> toJson() => _toJson(this);
}


ProvinceResponse _fromJson(Map<String, dynamic> json) {
  return ProvinceResponse()
    ..success = json['success'] as bool
    ..data = (json['data'] as List<dynamic>)
        .map((e) => ProvinceInfo.fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _toJson(ProvinceResponse instance) =>
    <String, dynamic>{
      'success': instance.success,
      'data': instance.data,
    };
