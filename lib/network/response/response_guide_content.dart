


import 'package:mitta/model/guide_author/guide_author.dart';
import 'package:mitta/model/guide_author/guide_content.dart';
import 'package:mitta/model/object/model_object.dart';


class GuideContentResponse {
  late bool success;
  late List<GuideContentInfo> data;
  GuideContentResponse();
  factory GuideContentResponse.fromJson(Map<String, dynamic> json) =>
      _fromJson(json);
  Map<String, dynamic> toJson() => _toJson(this);
}


GuideContentResponse _fromJson(Map<String, dynamic> json) {
  return GuideContentResponse()
    ..success = json['success'] as bool
    ..data = (json['data'] as List<dynamic>)
        .map((e) => GuideContentInfo.fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _toJson(GuideContentResponse instance) =>
    <String, dynamic>{
      'success': instance.success,
      'data': instance.data,
    };
