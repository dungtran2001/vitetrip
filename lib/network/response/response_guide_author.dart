


import 'package:mitta/model/guide_author/guide_author.dart';
import 'package:mitta/model/object/model_object.dart';


class GuideAuthorResponse {
  late bool success;
  late List<GuideAuthorInfo> data;
  GuideAuthorResponse();
  factory GuideAuthorResponse.fromJson(Map<String, dynamic> json) =>
      _fromJson(json);
  Map<String, dynamic> toJson() => _toJson(this);
}


GuideAuthorResponse _fromJson(Map<String, dynamic> json) {
  return GuideAuthorResponse()
    ..success = json['success'] as bool
    ..data = (json['data'] as List<dynamic>)
        .map((e) => GuideAuthorInfo.fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _toJson(GuideAuthorResponse instance) =>
    <String, dynamic>{
      'success': instance.success,
      'data': instance.data,
    };
