


import 'package:mitta/model/object/model_object.dart';


class ObjectResponse {
  late bool success;
  late List<ObjectInfo> data;
  ObjectResponse();
  factory ObjectResponse.fromJson(Map<String, dynamic> json) =>
      _fromJson(json);
  Map<String, dynamic> toJson() => _toJson(this);
}


ObjectResponse _fromJson(Map<String, dynamic> json) {
  return ObjectResponse()
    ..success = json['success'] as bool
    ..data = (json['data'] as List<dynamic>)
        .map((e) => ObjectInfo.fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _toJson(ObjectResponse instance) =>
    <String, dynamic>{
      'success': instance.success,
      'data': instance.data,
    };
