
import '/model/demo/model_demo.dart';

class DemoResponse {
  late bool success;
  late List<Demo> data;
  DemoResponse();
  factory DemoResponse.fromJson(Map<String, dynamic> json) =>
      _fromJson(json);
  Map<String, dynamic> toJson() => _toJson(this);
}


DemoResponse _fromJson(Map<String, dynamic> json) {
  return DemoResponse()
    ..success = json['success'] as bool
    ..data = (json['data'] as List<dynamic>)
        .map((e) => Demo.fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _toJson(DemoResponse instance) =>
    <String, dynamic>{
      'success': instance.success,
      'data': instance.data,
    };
