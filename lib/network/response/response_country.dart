


import 'package:mitta/model/location/model_country.dart';


class CountryResponse {
  late bool success;
  late List<CountryInfo> data;
  CountryResponse();
  factory CountryResponse.fromJson(Map<String, dynamic> json) =>
      _fromJson(json);
  Map<String, dynamic> toJson() => _toJson(this);
}


CountryResponse _fromJson(Map<String, dynamic> json) {
  return CountryResponse()
    ..success = json['success'] as bool
    ..data = (json['data'] as List<dynamic>)
        .map((e) => CountryInfo.fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _toJson(CountryResponse instance) =>
    <String, dynamic>{
      'success': instance.success,
      'data': instance.data,
    };
