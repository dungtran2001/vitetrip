

import '../../model/category/model_object.dart';


class CategoryResponse {
  // late bool success;
  late List<CategoryInfo> data;
  CategoryResponse();
  factory CategoryResponse.fromJson(Map<String, dynamic> json) =>
      _fromJson(json);
  Map<String, dynamic> toJson() => _toJson(this);
}


CategoryResponse _fromJson(Map<String, dynamic> json) {
  return CategoryResponse()
    // ..success = json['success'] as bool
    ..data = (json['data'] as List<dynamic>)
        .map((e) => CategoryInfo.fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _toJson(CategoryResponse instance) =>
    <String, dynamic>{
      // 'success': instance.success,
      'data': instance.data,
    };
