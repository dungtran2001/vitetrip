


import 'package:mitta/model/location/model_location.dart';


class LocationResponse {
  late bool success;
  late List<LocationInfo> data;
  LocationResponse();
  factory LocationResponse.fromJson(Map<String, dynamic> json) =>
      _fromJson(json);
  Map<String, dynamic> toJson() => _toJson(this);
}


LocationResponse _fromJson(Map<String, dynamic> json) {
  return LocationResponse()
    ..success = json['success'] as bool
    ..data = (json['data'] as List<dynamic>)
        .map((e) => LocationInfo.fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _toJson(LocationResponse instance) =>
    <String, dynamic>{
      'success': instance.success,
      'data': instance.data,
    };
