
import 'dart:convert';
import 'dart:typed_data';

import 'package:mitta/constants.dart';
import 'package:mitta/model/location/model_location.dart';
import 'package:mitta/network/response/response_country.dart';
import 'package:mitta/network/response/response_guide_author.dart';
import 'package:mitta/network/response/response_guide_content.dart';
import 'package:mitta/network/response/response_objects.dart';
import 'package:mitta/network/response/response_provinces.dart';
import 'package:http/http.dart' as http;

import '../../model/location/model_coordinate.dart';
import '../response/response_archiverments.dart';
import '../response/response_category.dart';
import '/configs/data_sample.dart';
import '/network/response/response_locations.dart';

import '/configs/configs.dart';
import '/model/user/model_me.dart';
import 'package:dio/dio.dart';
import '../httpController.dart';
import '/model/demo/model_demo.dart';

class Api {
  ApiClient? client;
  Api() {
    final Dio dio = Dio();
    dio.options.connectTimeout = 30000;
    dio.options.receiveTimeout = 30000;
    dio.options.sendTimeout = 30000;
    dio.options.baseUrl = "env.BASE_API_URL";
    dio.options.headers['Content-Type'] = 'application/json';
    dio.options.headers['Accept'] = 'application/json';
    dio.interceptors.add(InterceptorsWrapper(
      onRequest: (options, _) {
        final token = "token_string";
        if (token != null) {
          options.headers['Authorization'] = 'Bearer $token';
        } else
          options.headers['Authorization'] = 'Bearer';
        //return options;
      },
    ));
    client = ApiClient();
  }
}

abstract class ApiClient {
  factory ApiClient() = _ApiClient;

  Future<Demo> getDemo({params});

  ///me
  Future<Me> getMe({params});

  ///country data
  Future<CountryResponse> getCountryData({search = ""});

  ///locations data
  Future<LocationResponse> getLocationsData({search, provinceId});

  ///object data
  Future<ObjectResponse> getObjectsData({locationId, objectId});

  ///archiverment data
  Future<ArchivermentsResponse> getArchivermentsData({userId, locationId, token});

  ///post archiverment data
  Future<bool> postArchivermentData({userId, provinceId, locationId, objectId, token});

  ///object data
  Future<GuideAuthorResponse> getGuideAuthorsData({locationId, artifactId});

  ///content data
  Future<GuideContentResponse> getGuideContentsData({authorId, locationId, artifactId});

  ///province data
  Future<ProvinceResponse> getProvincesData({search, countryId, provinceId});

  ///category data
  Future<CategoryResponse> getCategoriesData({search});


  /// polyline 2 places
  Future<List<Coordinate>> getPathCoordinates({source, target});

  ///push send report
  Future<bool> postNewObjectReport({params, images, token});

  ///push send rating location
  Future<bool> postRatingLocation({params, token});

  ///Get remote model
  Future<Uint8List> getDetectModelFile({modelFileName, token});

  Future<List<LocationInfo>> getNearbyLocation({lat, lng});
}



class _ApiClient implements ApiClient {
  _ApiClient() {
//    baseUrl ?? = 'http://...';
  }


  @override
  Future<Demo> getDemo({params}) async {
    var dio = Dio();
    dio.options.headers['Accept'] = 'application/json';
    var token = "tokenString";
    dio.options.headers['Authorization'] = token;
    final response = await dio.get('http://...',
        queryParameters: params);
    if (response.statusCode == 200) {
      // print(response.data['data']);
      return Demo.fromJson(response.data['data']);
    } else {
      throw Exception('');
    }
  }

  @override
  Future<Me> getMe({params}) async {
    var url = Configs.hostName + Configs.apiURI["me"]!;
    var response = await HTTPController.GetAuthorization(url, params.token);

    var _status = response["error"] as bool;
    if (_status == false) {
      // print(response.data['data']);

      //success
      if (response.data['data'] != null)
        return Me.fromJson(response.data['data']);

      return Me();
    } else {
      throw Exception('--not get ME');
    }
  }


  @override
  Future<CountryResponse> getCountryData({search = ""}) async {
    var url = Configs.hostName  + Configs.apiURI['get-nations']! + "?keyword="+ search;
    var response = await HTTPController.Get(url);
   // var response = DataSample.country;

    var _status = response != null;
    if (_status == true) {
      // print(response['data']);

      //success
      if (response['data'] != null)
        return CountryResponse.fromJson(response);

      return CountryResponse();
    } else {
      throw Exception('--not get all Parking data');
    }
  }


  @override
  Future<LocationResponse> getLocationsData({search = "", provinceId = ""}) async {
    var url = Configs.hostName  + Configs.apiURI['get-location']! + "?keyword="+ search;
    var response = await HTTPController.Get(url);
   // var response = DataSample.locations;

    var _status = response != null;
    if (_status == true) {
      // print(response['data']);

      //success
      if (response['data'] != null)
        return LocationResponse.fromJson(response);

      return LocationResponse();
    } else {
      throw Exception('--not get all Parking data');
    }
  }

  @override
  Future<ObjectResponse> getObjectsData({locationId = "", objectId = ""}) async {
    var url = Configs.hostName  + Configs.apiURI['get-artifact']! + "?location_id=$locationId&object_id=$objectId" ;
    var response = await HTTPController.Get(url);

   // var response;
   // if (locationId == "hoi_an")
   //   response = DataSample.objectsHoiAn;
   // else if (locationId == "dinh_doc_lap")
   //   response = DataSample.objectsDinhDocLap;
   //  print(response);

    var _status = response != null;
    if (_status == true) {
      print(response['data']);

      //success
      if (response['data'] != null)
        return ObjectResponse.fromJson(response);

      return ObjectResponse();
    } else {
      print('--ERROR not getObjectsData data of $locationId');
      return ObjectResponse();

    }
  }

  @override
  Future<ArchivermentsResponse> getArchivermentsData({userId = "", locationId, token}) async {
    var url = Configs.hostName  + Configs.apiURI['get-archiverment']! + "?user_id=$userId&location_id=$locationId" ;
    var response = await HTTPController.GetByJsonBearer(url, token);

    print(response);

    var _status = response != null;
    if (_status == true) {
      print(response['data']);

      //success
      if (response['data'] != null)
        return ArchivermentsResponse.fromJson(response);

      return ArchivermentsResponse();
    } else {
      print('--ERROR not getObjectsData data of $locationId');
      return ArchivermentsResponse();

    }
  }

  //post new archiverment
  @override
  Future<bool> postArchivermentData({userId, provinceId, locationId, objectId, token}) async {
    var map = {
      "user_id": userId,
      "province_id": provinceId,
      "location_id": locationId,
      "object_id": objectId,
    };
    var url = Configs.hostName  + Configs.apiURI['add-archiverment']!;
    var response = await HTTPController.PostByJsonBearer(url, map, token);

    var _status = response != null; //response["success"] as bool;
    if (_status == true) {
//      print(response['data']);

      //success
      return true;
    } else {
      return false;
      throw Exception('--not getChecklistData');
    }
  }

  @override
  Future<GuideAuthorResponse> getGuideAuthorsData({locationId = "", artifactId = "artifactId"}) async {
    var url = Configs.hostName  + Configs.apiURI['get-author']! + "?location_id=$locationId&artifact_id=$artifactId";
    var response = await HTTPController.Get(url);


   // var response = DataSample.authorGuide;

    var _status = response != null;
    if (_status == true) {
      print(response['data']);

      //success
      if (response['data'] != null)
        return GuideAuthorResponse.fromJson(response);

      return GuideAuthorResponse();
    } else {
      throw Exception('--not getGuideAuthorsData data of $locationId');
    }
  }

  @override
  Future<GuideContentResponse> getGuideContentsData({authorId, locationId, artifactId}) async {
    var url = Configs.hostName  + Configs.apiURI['get-content']! + "?author_id=$authorId&location_id=$locationId&artifact_id=$artifactId";
    var response = await HTTPController.Get(url);


   // var response = DataSample.authorGuide;

    var _status = response != null;
    if (_status == true) {
      print(response['data']);

      //success
      if (response['data'] != null)
        return GuideContentResponse.fromJson(response);

      return GuideContentResponse();
    } else {
      throw Exception('--not getGuideContentsData data of $locationId');
    }
  }

  @override
  Future<ProvinceResponse> getProvincesData({search, countryId, provinceId}) async {
    var url = Configs.hostName  + Configs.apiURI['get-province']! + "?province_id=$provinceId&nation_id=$countryId&keyword=$search";
    // print("Get provinces url $url}");
    var response = await HTTPController.Get(url);

    var _status = response != null;
    if (_status == true) {
      print(response['data']);

      //success
      if (response['data'] != null) {
        return ProvinceResponse.fromJson(response);
      }

      return ProvinceResponse();
    } else {
      throw Exception('--not getProvinceResponseData data ');
    }
  }


  @override
  Future<CategoryResponse> getCategoriesData({search}) async {
    var url = Configs.hostName  + Configs.apiURI['get-category']! + "?keyword=$search";
    var response = await HTTPController.Get(url);

    var _status = response != null;
    if (_status == true) {
      print(response['data']);

      //success
      if (response['data'] != null)
        return CategoryResponse.fromJson(response);

      return CategoryResponse();
    } else {
      throw Exception('--not getProvinceResponseData data ');
    }
  }

  @override
  Future<List<Coordinate>> getPathCoordinates({source, target}) async {
    var dio = Dio();
    dio.options.headers['Accept'] = 'application/json';
    dio.options.headers['Content-Type'] = 'application/json';
    // print(
    //     'https://api.openrouteservice.org/v2/directions/driving-car?api_key=5b3ce3597851110001cf624855a46bb6be524c2e961b453d21f8ad6e&start=${source.longitude},${source.latitude}&end=${target.longitude},${target.latitude}');
    // final response = await dio.get(
    //   'https://api.openrouteservice.org/v2/directions/driving-car?api_key=5b3ce3597851110001cf624855a46bb6be524c2e961b453d21f8ad6e&start=${source.longitude},${source.latitude}&end=${target.longitude},${target.latitude}',
    // );
    final res = await http.get(
        Uri.parse(
            'https://api.openrouteservice.org/v2/directions/driving-car?api_key=5b3ce3597851110001cf624855a46bb6be524c2e961b453d21f8ad6e&start=${source.longitude},${source.latitude}&end=${target.longitude},${target.latitude}'),
        headers: {'Accept': '*/*', 'Accept-Language': 'en-US'});
    // print(res.body);
    if (res.statusCode == 200) {
      var data = jsonDecode(res.body);
      List<dynamic> coordinates =
      data['features'][0]['geometry']['coordinates'];
      var lst = <Coordinate>[];
      coordinates.forEach((element) {
        lst.add(Coordinate.fromArray((element as List<dynamic>)));
      });
      print(lst.length);
      return lst;
    } else {
      throw Exception('');
    }
  }


  @override
  Future<bool> postNewObjectReport({params, images, token}) async {
    var url = Configs.hostName  + Configs.apiURI['add-object-report'].toString();

//    var response = await HTTPController.PostByJsonBearer(url, params, token);
    var response = await HTTPController.PostByJsonBearerAndImage(url, params, token, images);
//    var response = DataSample.tasks;

    print(response.toString());
    if ( response != null ){
      var _status = response["success"] as bool;
      if (_status == true) {
//      print(response['data']);

        //success
        if (response["message"].toString().contains("success"))
          return true;

        return false;
      } else {
        return false;
        throw Exception('--not getChecklistData');
      }
    }else{
      return false;
    }
  }


  @override
  Future<bool> postRatingLocation({params, token}) async {
    var url = Configs.hostName  + Configs.apiURI['add-rating-location'].toString();

    var response = await HTTPController.PostByJsonBearer(url, params, token);

    print(response.toString());
    if ( response != null ){
      var _status = response["success"] as bool;
      if (_status == true) {
//      print(response['data']);

        //success
        if (response["message"].toString().contains("success"))
          return true;

        return false;
      } else {
        return false;
        throw Exception('--not getChecklistData');
      }
    }else{
      return false;
    }
  }

  @override
  Future<Uint8List> getDetectModelFile({modelFileName, token}) async {
    try {
      var url = Configs.hostName + "/model/" + modelFileName;
      var response = HTTPController.GetAndReturnBuffer(url);
      return response;
    } catch (e) {
      throw e;
    }
  }

  @override
  Future<List<LocationInfo>> getNearbyLocation({lat, lng}) async {
    try {
      var url = "https://places.googleapis.com/v1/places:searchNearby";
      var body = {
        "maxResultCount": 10,
        "includedTypes": NEARBY_PLACES_INCLUDED_TYPE,
        "locationRestriction": {
          "circle": {
            "center": {
              "latitude": lat,
              "longitude": lng
            },
            "radius": 50000.0
          }
        }
      };
      var headers = {
        "Content-Type": "application/json",
        "X-Goog-Api-Key": GOOGLE_MAP_API_KEY,
        "X-Goog-FieldMask": "places.id,places.primaryType,places.displayName,places.location,places.photos,places.rating"
      };
      var response = await HTTPController.PostWithBody(url, body, headers);
      var locations = <LocationInfo>[];
      if (response != null && response["places"] != null) {
        (response["places"] as List<dynamic>).forEach((e) => {
          locations.add(LocationInfo.fromPlacesJson(e))
        });
      }
      return locations;
    } catch (e) {
      throw e;
    }
  }

}
