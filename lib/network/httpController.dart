import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';
//response
import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'dart:convert'as convert;
import 'dart:io';
import 'package:http/http.dart' as http;

class HTTPController {

  //GETT
  static Future<dynamic> Ping(String url) async {
    try {
      Dio dio = new Dio();
      (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
          (HttpClient client) {
        client.badCertificateCallback =
            (X509Certificate cert, String host, int port) => true;
        return client;
      };

      dio.options.connectTimeout = 5000; //5s
      // print("ping start $url");
      Response response = await dio.get(url);
      return response.statusCode;
    } catch (e) {
      print("ping error: $e");
      return 404;
    }
  }

  //GETT
  static Future<dynamic> Get(String url) async {
    try {
      Dio dio = new Dio();
      (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
          (HttpClient client) {
        client.badCertificateCallback =
            (X509Certificate cert, String host, int port) => true;
        return client;
      };

      Response response = await dio.get(url);

      return response.data;
    } catch (e) {
      print(e);
    }
  }

  //GETT
  static Future<dynamic> GetAuthorization(String url, String token) async {
    try {
      Dio dio = new Dio();
      (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
          (HttpClient client) {
        client.badCertificateCallback =
            (X509Certificate cert, String host, int port) => true;
        return client;
      };

      dio.options.headers["access_token"] = token;
      Response response = await dio.get(url);
      return response.data;
    } catch (e) {
      print(e);
    }
  }

  //GET
  static Future<dynamic> GetByJsonBearer(String url, token) async {
    try {
      final response = await http
          .get(Uri.parse(url),
          headers: {
            HttpHeaders.contentTypeHeader: "application/json",
            HttpHeaders.authorizationHeader: "Bearer $token"
          },
         );

      return json.decode(response.body);
    } catch (e) {
      print(e);
      if (e.toString().contains("unauthorized")){
        // NotificationLocal.instance.dialogExpiredSession();
      }
    }
  }

  //POST
  static Future<dynamic> Post(String url) async {
    try {
      Dio dio = new Dio();
      (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
          (HttpClient client) {
        client.badCertificateCallback =
            (X509Certificate cert, String host, int port) => true;
        return client;
      };

      Response response = await dio.post(url);
      return response.data;

    } catch (e) {
      print(e);
    }
  }

  //POST

  static Future<dynamic> PostByJson(String url, Map jsonMap) async {
    try {
      Dio dio = new Dio();
      (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
          (HttpClient client) {
        client.badCertificateCallback =
            (X509Certificate cert, String host, int port) => true;
        return client;
      };

      Response response = await dio.post(url, data: jsonMap);
      return response.data;
    } catch (e) {
      print(e);
    }
  }

  //POST
   static Future<dynamic> PostByJsonBearer(String url, Map jsonMap, token) async {
     try {
       final response = await http
           .post(Uri.parse(url),
           headers: {
             HttpHeaders.contentTypeHeader: "application/json",
             HttpHeaders.authorizationHeader: "Bearer $token"
           },
           body: json.encode(jsonMap));

       return json.decode(response.body);
     } catch (e) {
       print(e);
     }
   }

  //POST

  static Future<dynamic> PostAuthorization(String url, Map jsonMap, String token) async {
    try {
      Dio dio = new Dio();
      (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
          (HttpClient client) {
        client.badCertificateCallback =
            (X509Certificate cert, String host, int port) => true;
        return client;
      };

      dio.options.headers["access_token"] = token;
      Response response = await dio.post(url, data: jsonMap);
      return response.data;
    } catch (e) {
      print(e);
    }
  }


  static Future<dynamic> PostByJsonBearerAndImage(String url, Map jsonMap, String token, files) async {
    try{
      var dio = Dio();
      dio.options.headers['Accept'] = 'application/json';
      if (token == '') return false;
      dio.options.headers['Authorization'] = "Bearer $token";
      final _data = FormData.fromMap({
        'data': jsonMap
      });

      for (var file in files) {
        _data.files.add(
          MapEntry(
              'files[]',
              await MultipartFile.fromFileSync(file.path,
                  filename: file.path.split(Platform.pathSeparator).last)),
        );
      }

//      _data.files.add(MapEntry(
//          'file',
//          MultipartFile.fromFileSync(file.path,
//              filename: file.path.split(Platform.pathSeparator).last)));

      final response = await dio.post(
        url,
        data: _data,
      );

      // print("jịi");
      return response.data;
    } catch (e) {
      print(e);
    }
  }

  //DELETE
  static Future<dynamic> DeleteAuthorization(String url, String token) async {
    try {
      Dio dio = new Dio();
      print(url);
      print(token);
//      dio.options.headers["Bearer_token"] = token;
      Response response = await dio.delete(url,
          options: Options(headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer $token",
          }));
      // print(response);
      return response.data;
    } catch (e) {
      print(e);
    }
  }

  static Future<Uint8List> GetAndReturnBuffer(String url) async {
    try {
      final response = await http.get(Uri.parse(url));
      return response.bodyBytes;
    } catch (e) {
      throw e;
    }
  }

  static Future<dynamic> PostWithBody(String url, Map body, Map<String, dynamic> headers) async {
    try {
      Dio dio = new Dio();
      final response = await dio.post(url, data: body, options: Options(headers: headers));
      return response.data;
    } catch (e) {
      throw e;
    }
  }
}